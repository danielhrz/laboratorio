@props(['label'])

<label {{ $attributes->merge(['class' => 'block mb-1 text-xs sm:text-sm font-medium text-slate-600']) }}>
    {{ $label ?? $slot }}
</label>
<div wire:ignore>
    <select class="form-control" data-pharaonic="select2" data-component-id="{{ $this->id }}" wire:model="dx1" id="choices-multiple-remove-button" multiple>
        @foreach ($diagnosticos as $diagnostico)
        <option value="{{$diagnostico}}" {{ old('dx1') == $diagnostico ? 'selected':''}}>{{ $diagnostico }}</option>
        @endforeach
    </select>
</div>
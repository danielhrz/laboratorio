@props(['options' => []])

@php
$options = array_merge([
'enableTime'=> true,
'noCalendar'=> true,
'dateFormat'=> 'H:i:ss',
'time_24hr'=> true
], $options);
@endphp

<div wire:ignore>
    <input x-data="{ init() { flatpickr(this.$refs.input, {{json_encode((object)$options)}}); } }" 
    x-ref="input" type="text" {{ $attributes->merge(['class' => "text-uppercase border bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2"]) }} />
</div>
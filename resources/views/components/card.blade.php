<div {{ $attributes->merge(['class' => 'p-3 md:p-5 lg:p-6 bg-white rounded-lg border border-gray-200 shadow-md my-2 md:my-4 lg:my-6']) }}>

{{ $slot }}

</div>
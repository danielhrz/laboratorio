@extends('layouts.panel')
@section('content')

<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <h5 class="text-start mb-2 text-2xl font-semibold text-slate-700">Estadísticas</h5>

    <div class="container-chart-rejected-reasons relative p-1 w-full lg:w-3/4 m-auto mt-8 bg-white">
        <div class="flex justify-end items-center mb-4">
            <button data-chart="rejected-reasons" class="download text-cyan-700 hover:text-white border border-cyan-700 hover:bg-cyan-800 focus:ring-2 focus:outline-none focus:ring-cyan-300 font-medium rounded text-sm px-2 py-1.5 mr-2 text-center">
                <i class="fa-regular fa-image"></i>
            </button>
            <label class="font-normal text-slate-700 text-right w-10">Año:</label>
            <div class="ml-2 w-32">
                <select id="year-rejected-reasons" class="border {{ $errors->has('shift') ? 'shadow shadow-red-500/50 border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }}  bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                    @for ($i = 2022; $i<= date('Y'); $i++ ) <option value="{{$i}}" @if($i==date('Y')) selected @endif>{{ $i }}</option>
                        @endfor
                </select>
            </div>
        </div>
        <canvas id="container-chart-rejected-reasons" class="max-h-128 bg-white"></canvas>

        <div class="container-loader-rejected-reasons absolute w-full h-full top-0 flex justify-center items-center flex-col hidden">
            <div class="spinner-chart"></div>
            <p class="text-sm text-red-900">Cargando información...</p>
        </div>

    </div>

</div>
<input type="hidden" id="url" value="{{ url('/') }}">

<div id="tooltipDownload" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
    Descargar imagen
    <div class="tooltip-arrow" data-popper-arrow></div>
</div>


@endsection
<table>
    <tbody>
        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td></td><!-- B -->
            <td></td><!-- C -->
            <td></td><!-- D -->
            <td></td><!-- E -->
            <td></td><!-- F -->
            <td></td><!-- G -->
            <td></td><!-- H -->
            <td></td><!-- I -->
            <td></td><!-- J -->
            <td></td><!-- K -->
            <td colspan="3">SECRETARÍA DE SALUD DE LA CIUDAD DE MÉXICO SERVICIOS DE SALUD PÚBLICA DE LA CIUDAD DE MÉXICO DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA</td><!-- D -->
        </tr>

        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td colspan="3">Año exportado: {{ $year }}</td><!-- B -->
        </tr>
        <tr>
            <td></td><!-- A -->
            <td colspan="3">Fecha de exportación: {{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, HH:mm') }}</td>
        </tr>
        <tr>
            <td></td><!-- A -->
            <td colspan="3">Muestras Finalizadas</td>
        </tr>
        <tr></tr>

        <tr>
            <td></td>
            @foreach ($labels as $month)
            <td>{{$month}}</td>
            @endforeach
        </tr>

        @foreach ($diagnosticsCount as $diagnostic)
        <tr>
            @foreach ($diagnostic as $data)
            <td>{{ $data }}</td>
            @endforeach
        </tr>
        @endforeach
        <tr>
            <td>Total</td>
        </tr>
    </tbody>
</table>
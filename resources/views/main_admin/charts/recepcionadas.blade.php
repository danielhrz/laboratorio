@extends('layouts.panel')
@section('content')

<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <h5 class="text-start mb-2 text-2xl font-semibold text-slate-700">Estadísticas</h5>

    <div class="container-chart-samples-received-day relative p-1 w-full lg:w-3/4 m-auto mt-8 bg-white">
        <div class="flex justify-end items-center mb-4">
            <button data-chart="samples-received-day" class="download text-cyan-700 hover:text-white border border-cyan-700 hover:bg-cyan-800 focus:ring-2 focus:outline-none focus:ring-cyan-300 font-medium rounded text-sm px-2 py-1.5 mr-2 text-center">
                <i class="fa-regular fa-image"></i>
            </button>
            <label class="font-normal text-slate-700 text-right w-10">Fecha:</label>
            <div class="ml-2 w-32">
                <div class="relative z-0 w-full ml-2 group">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                        <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path>
                        </svg>
                    </div>
                    <input id="date-samples-received-day" readonly autocomplete="off" type="text" class="datepicker border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 pl-10" placeholder="Día...">
                </div>
            </div>
            <button id="search-samples-received-day" class="text-cyan-700 hover:text-white border border-cyan-700 hover:bg-cyan-800 focus:ring-2 focus:outline-none focus:ring-cyan-300 font-medium rounded text-sm px-2 py-1.5 ml-4 text-center">
                Buscar
            </button>
        </div>
        <canvas id="container-chart-samples-received-day" class="max-h-128 bg-white"></canvas>

        <div class="container-loader-samples-received-day absolute w-full h-full top-0 flex justify-center items-center flex-col hidden">
            <div class="spinner-chart"></div>
            <p class="text-sm text-red-900">Cargando información...</p>
        </div>

    </div>
    
    <livewire:main.table-jurisdiction-controller />

</div>
<input type="hidden" id="url" value="{{ url('/') }}">

<div id="tooltipDownload" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
    Descargar imagen
    <div class="tooltip-arrow" data-popper-arrow></div>
</div>


@endsection
@if(Auth::user()->id_role != 11)
<div class="flex items-center justify-center">
    <a href="{{ route('destinatarios.edit', $id ) }}" type=" button" wire:click="delete" class="button btn-option-edit text-blue-700 border border-blue-700 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded text-xs md:text-sm p-1.5 md:p-2.5 text-center flex items-center justify-center">
        <i class="fa-solid fa-pen-to-square"></i>
    </a>

    <button type="button" data-action="{{ route('destinatarios.destroy', $id ) }}" class="button btn-option-delete text-red-700 border border-red-700 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded text-xs md:text-sm p-1.5 md:p-2.5 text-center flex items-center justify-center">
        <i class="fa-solid fa-trash-can"></i>
    </button>
</div>
@endif
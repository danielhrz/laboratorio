@extends('layouts.panel')
@section('content')
<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <h5 class="mb-8 text-2xl font-medium text-slate-600 flex items-center justify-center">
        Agregar Destinatario
    </h5>
    <form action="{{ url('destinatarios') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="grid xl:grid-cols-2 xl:gap-x-6 xl:gap-y-0.5">
            <div class="mb-6 group">
                <label for="encargado" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Nombre del destinatario:</label>
                <input name="encargado" type="text" class="text-uppercase border {{ $errors->has('encargado') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" onkeyup="this.value=this.value.toUpperCase();" value="{{ old('encargado') }}" autofocus id="encargado" placeholder="Ingresar nombre completo del destinatario...">
            </div>

            <div class="mb-6 group">
                <label for="position" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Puesto del destinatario:</label>
                <input name="position" type="text" class="text-uppercase border {{ $errors->has('position') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" onkeyup="this.value=this.value.toUpperCase();" value="{{ old('position') }}" autofocus id="position" placeholder="Ingresar el puesto del destinatario...">
            </div>

            <div class="mb-6 group">
                <label for="jurisdiccion" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Jurisdicción:</label>
                <input name="jurisdiccion" type="text" class="text-uppercase border {{ $errors->has('jurisdiccion') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" onkeyup="this.value=this.value.toUpperCase();" value="{{ old('jurisdiccion') }}" autofocus id="jurisdiccion" placeholder="Ingresar la jurisdicción del destinatario...">
            </div>

            <div class="mb-6 group">
                <label for="address" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Dirección:</label>
                <input name="address" type="text" class="text-uppercase border {{ $errors->has('address') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" value="{{ old('address') }}" autofocus id="address" placeholder="Ingresar la dirección...">
            </div>
        </div>

        <div class="flex justify-center items-center">
            <button type="submit" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2">Registrar</button>
            <button type="button" class="button-after text-red-700 hover:text-red-50 border border-red-700 hover:bg-red-800 focus:ring-2 hover:shadow-lg hover:shadow-red-500/50 focus:outline-none focus:ring-red-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2">Regresar</button>
        </div>
    </form>
</div>
@endsection
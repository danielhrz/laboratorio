<table>
    <tbody>
        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td></td><!-- B -->
            <td></td><!-- C -->
            <td colspan="4">LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CIUDAD DE MÉXICO</td><!-- D -->
        </tr>
        <tr>
            <td></td><!-- A -->
            <td></td><!-- B -->
            <td></td><!-- C -->
            <td colspan="4">FORMATO DE RELACIÓN DE MUESTRAS PARA PROCESAMIENTO LESP-VE-FO-016/005</td><!-- D -->
            <td>Vigente a partir de: 23/02/2023</td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td></td><!-- B -->
            <td></td><!-- C -->
            <td></td><!-- D -->
            <td>{{-- FOLIO: --}}</td><!-- E -->
            <td></td><!-- F -->
            <td></td><!-- G -->
        </tr>
        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td>DIAGNÓSTICO:</td><!-- B -->
            <td>{{ $diagnostico }}</td><!-- C -->
            <td></td><!-- D -->
            <td>FECHA DE RECEPCIÓN:</td><!-- E -->
            <td>{{ \Carbon\Carbon::parse( $reception )->format('d/m/Y') }}</td><!-- F -->
            <td></td><!-- G -->
        </tr>
        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td>REMU</td><!-- B -->
            <td>DVE</td><!-- C -->
            <td></td><!-- D -->
            <td>FECHA DE ENTREGA:</td><!-- E -->
            <td>{{ \Carbon\Carbon::now()->format('d/m/Y') }}</td><!-- F -->
            <td></td><!-- G -->
        </tr>
        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Folio Lesp</td>
            <td colspan="2">Nombre</td>
            <td># De Plataforma, # De Caso, #JS/UMS</td>
            <td>Jurisdicción/Unidad</td>
            <td>Tipo de Muestra</td>
            <td>Observaciones</td>
        </tr>
        @php
        $index = 1;
        @endphp
        @foreach ($documentos as $dato )
        <tr>
            <td>{{ $index }}</td>
            <td>{{ $dato->folio_lesp }}</td>
            <td colspan="2">{{ $dato->nombre_paciente }}</td>
            <td>{{ $dato->folio_sisver }}</td>
            <td>{{ $dato->hospital }}</td>
            <td>{{ $dato->tipo_muestra }}</td>
            <td>{{ $dato->observaciones }} </td>
        </tr>
        @php
        $index++;
        @endphp

        @endforeach

        <tr></tr>
        <tr></tr>
        <tr>
            <td colspan="2">Elaboró</td>
            <td colspan="2">Revisó</td>
            <td colspan="2">Hora</td>
            <td colspan="2">Recibió</td>
        </tr>
        <tr>
            <td colspan="2" rowspan="3"></td>
            <td colspan="2" rowspan="3"></td>
            <td colspan="2" rowspan="3"></td>
            <td colspan="2" rowspan="3"></td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td colspan="4">Iniciales y Firma del personal del DA</td>
            <td colspan="4">Iniciales y Firma o Rúbrica del personal de DVE</td>
        </tr>
    </tbody>
</table>
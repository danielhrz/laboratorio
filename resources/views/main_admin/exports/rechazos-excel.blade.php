<table>
    <tbody>
        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td></td><!-- B -->
            <td colspan="3">SECRETARÍA DE SALUD DE LA CIUDAD DE MÉXICO SERVICIOS DE SALUD PÚBLICA DE LA CIUDAD DE MÉXICO DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA</td><!-- D -->
        </tr>

        <tr>
            <td></td><!-- A -->
            <td colspan="7">LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CIUDAD DE MÉXICO FORMATO PARA RECHAZO DE MUESTRAS</td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td colspan="3">DESTINATARIO:</td><!-- A -->
            <td colspan="6">{{ $destinatario->encargado }}</td><!-- B -->
            <td>Fecha:</td><!-- J -->
            <td colspan="2">{{ \Carbon\Carbon::now()->format('d/m/Y') }}</td><!-- K -->
        </tr>
        <tr>
            <td colspan="3">JURISDICCIÓN SANITARIA:</td><!-- A -->
            <td colspan="6">{{ $destinatario->jurisdiccion }}</td><!-- B -->
            <td>Hora:</td><!-- J -->
            <td colspan="2">{{ \Carbon\Carbon::now()->isoFormat('h:mm A') }}</td><!-- K -->
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="3">{{ $oficio }}</td>
        </tr>
        <tr>
            <td></td><!-- A -->
            <td colspan="10">COMUNICO A USTED LOS MOTIVOS DE RECHAZO DE LA(S) MUESTRA(S) ENVIADA(S) AL LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CDMX PARA SU PROCESAMIENTO.</td>
            <td></td><!-- L -->
        </tr>

        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Folio lesp</td>
            <td colspan="3">Paciente</td>
            <td colspan="2">Tipo de Muestra</td>
            <td colspan="2">Diagnóstico Solicitado</td>
            <td colspan="3">Motivo de Rechazo</td>
        </tr>
        @php
        $index = 1;
        @endphp
        @foreach ($documentos as $dato )
        <tr>
            <td>{{ $index }}</td>
            <td>{{ $dato->folio_lesp }}</td>
            <td colspan="3">{{ $dato->nombre_paciente }}</td>
            <td colspan="2">{{ $dato->tipo_muestra }}</td>
            <td colspan="2">
                @if ($dato->dx1 != 'N/A')
                {{ $dato->dx1 }}
                @endif
                @if ($dato->dx2 != 'N/A')
                {{ ','. $dato->dx2 }}
                @endif
                @if ($dato->dx3 != 'N/A')
                {{ ','. $dato->dx3 }}
                @endif
                @if ($dato->dx4 != 'N/A')
                {{ ','. $dato->dx4}}
                @endif
                @if ($dato->dx5 != 'N/A')
                {{ ','. $dato->dx5 }}
                @endif
            </td>
            <td colspan="3">{{ $dato->rechazos }}</td>

        </tr>
        @php
        $index++;
        @endphp

        @endforeach
        <tr></tr>
        <tr>
            <td colspan="3">Observaciones/ Especificaciones:</td>
            <td colspan="9">{{ $observaciones }}</td>
        </tr>

        <tr>
            <td colspan="12">{{ $responsable->nombre }}</td>
        </tr>
        <tr>
            <td colspan="12">Nombre y firma del responsable de la actividad</td>
        </tr>

        <tr></tr>
        <tr>
            <td></td>
            <td colspan="4">LESP-VE-FO-014/004</td>
            <td></td>
            <td colspan="4">Vigente a partir de: 04/10/2022</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="10">Semisótano del Hospital General Ajusco Medio "Dra. Obdulia Rodríguez Rodríguez". Encinos 41, Miguel Hidalgo 4ta Secc, Tlalpan, 14250 Ciudad de México, CDMX</td>
        </tr>
    </tbody>
</table>
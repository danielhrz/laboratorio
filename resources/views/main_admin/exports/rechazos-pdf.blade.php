<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>RECHAZOS</title>
    <style>
        .tarjeta-interior {
            float: left;
            margin-bottom: 1em;
            padding: 0.5em;
            box-sizing: border-box;
        }

        .tarjeta-interior p {
            margin: unset;
            padding: unset;
        }

        .container-table th {
            border: solid 1px #E4E4E4;
            font-weight: bold;
        }

        .container-table td,
        .container-table th {
            border: solid 1px #E4E4E4;
            text-align: center;
            padding: 3px 8px;
            font-size: 8pt;
        }

        @page {
            header: page-header;
            footer: page-footer;
        }
    </style>
</head>

<body>
    <div class="container-header">
        <div class="tarjeta-interior" style="width: 220px;">
            <img src="{{asset('panel/img/logo2.png')}}" style="width: 200px;height: auto;">
        </div>
        <div class="tarjeta-interior tarjeta-center" style="margin-left: 2.5em; width: 70px;">
            <img src="{{asset('panel/img/logo-laboratorio.png')}}" style="width: auto;height: 50px;">
        </div>
        <div class="tarjeta-interior" style="margin-left: 2.5em; width: 290px;">
            <p style="text-align:right; font-size: 8pt;">SECRETARÍA DE SALUD DE LA CIUDAD DE MÉXICO</p>
            <p style="text-align:right; font-size: 8pt;">SERVICIOS DE SALUD PÚBLICA DE LA CIUDAD DE MÉXICO</p>
            <p style="text-align:right; font-size: 8pt;">DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA</p>
        </div>
        <div class="tarjeta-interior tarjeta-center" style="margin-bottom: 0.1em; width: 460px;">
            <p style="font-size: 9pt; text-align: center; font-weight: bold;">LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CIUDAD DE MÉXICO FORMATO PARA RECHAZO DE MUESTRAS</p>
        </div>
        <div class="tarjeta-interior tarjeta-center" style="margin-bottom: 0.1em; margin-left: 2em; width: 200px; padding: unset;">
            <img src="{{asset('panel/img/image012.png')}}" style="width: auto;height: 70px; width:160px;">
        </div>
    </div>

    <div class="section-header" style="box-sizing: border-box;">

        <table style="width: 100%;">
            <tr>
                <td style="width: 70%">
                    <table>
                        <tr>
                            <td>
                                <p style="font-weight: bold; font-size: 8pt;">DESTINATARIO:</p>
                            </td>
                            <td>
                                <p style="font-size: 8pt; text-align:center;">{{ $destinatario->encargado }}</p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 30%">
                    <table>
                        <tr>
                            <td style="width: 50%">
                                <p style="font-weight: bold; font-size: 8pt; ">Fecha:</p>
                            </td>
                            <td style="width: 50%">
                                <p style="font-size: 8pt; text-align:center;">{{ \Carbon\Carbon::now()->format('d/m/Y') }}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 70%">
                    <table>
                        <tr>
                            <td>
                                <p style=" font-weight: bold; font-size: 8pt;">JURISDICCIÓN SANITARIA:</p>
                            </td>
                            <td>
                                <p style="font-size: 8pt; text-align:center;">{{ $destinatario->jurisdiccion }}</p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 30%">
                    <table>
                        <tr>
                            <td style="width: 50%">
                                <p style="font-weight: bold; font-size: 8pt;">Hora:</p>
                            </td>
                            <td style="width: 50%">
                                <p style="font-size: 8pt; text-align:center;">{{ \Carbon\Carbon::now()->isoFormat('h:mm A') }}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 70%"></td>
                <td style="width: 30%">
                    <table>
                        <tr>
                            <td style="width: 50%"></td>
                            <td style="width: 50%" colspan="2">
                                <p style="font-weight: bold; font-size: 8pt; text-align:center;">{{ $oficio }}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt; text-align:center; padding: 10px" colspan="2">
                    COMUNICO A USTED LOS MOTIVOS DE RECHAZO DE LA(S) MUESTRA(S) ENVIADA(S) AL LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CDMX PARA SU PROCESAMIENTO.
                </td>
            </tr>
        </table>

    </div>

    <div class="container-table" style="width: 100%;">
        <table style="width: 100%; border-collapse: collapse">
            <thead>
                <tr>
                    <th style="width:30px">No.</th>
                    <th style="width: 50px;">Folio lesp</th>
                    <th style="width:130px">Paciente</th>
                    <th style="width:130px">Tipo de Muestra</th>
                    <th style="width:130px">Diagnóstico Solicitado</th>
                    <th style="width:130px">Motivo de Rechazo</th>
                </tr>
            </thead>
            <tbody>


                @if( $documentos->count() > 0 )
                @php
                $index = 1;
                @endphp

                @foreach ($documentos as $dato)
                <tr>
                    <td style="width:30px">{{ $index }}</td>
                    <td style="width: 50px;">{{ $dato->folio_lesp }}</td>
                    <td style="width:130px">{{ $dato->nombre_paciente }}</td>
                    <td style="width:130px">{{ $dato->tipo_muestra }}</td>
                    <td style="width:130px">
                        @if ($dato->dx1 != 'N/A')
                        {{ $dato->dx1 }}
                        @endif
                        @if ($dato->dx2 != 'N/A')
                        {{ ','. $dato->dx2 }}
                        @endif
                        @if ($dato->dx3 != 'N/A')
                        {{ ','. $dato->dx3 }}
                        @endif
                        @if ($dato->dx4 != 'N/A')
                        {{ ','. $dato->dx4}}
                        @endif
                        @if ($dato->dx5 != 'N/A')
                        {{ ','. $dato->dx5 }}
                        @endif
                    </td>
                    <td style="width:130px">{{ $dato->rechazos }}</td>

                </tr>

                @php
                $index++;
                @endphp

                @endforeach

                @else
                <tr>
                    <td colspan="6" style="padding: 10px;font-size: 8pt; text-align:center;">No se encontraron muestras rechazadas para el oficio de entrada {{ $oficio }}</td>
                </tr>
                @endif


            </tbody>
        </table>
    </div>

    <br>
    <div class="footer" style="width: 100%">
        <table style="width: 100%;  border-collapse: collapse;">
            <tr>
                <td style="padding: 5px 10px;border: solid 1px #000;width:20%;height: 90px; font-size: 8pt; ">
                    <p>Observaciones/ Especificaciones:</p>
                </td>
                <td style="padding: 5px 10px;border: solid 1px #000; font-size: 8pt; padding: 10px; text-align:center;">{{ $observaciones }}</td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 5px 10px;border: solid 1px #000; font-size: 8pt; text-align:center; height: 60px; border-bottom: none; vertical-align: bottom;">
                    <u>{{ $responsable->nombre }}</u>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 30px;padding: 5px 10px;border: solid 1px #000; font-size: 8pt; text-align:center; font-weight: bold; border-top: none;">Nombre y firma del responsable de la actividad</td>
            </tr>
        </table>

    </div>

    <htmlpagefooter name="page-footer">
        <table width="100%" style="font-size: 8pt;">
            <tr>
                <td width="33%" style="text-align: center;">LESP-VE-FO-014/004</td>
                <td width="33%" style="text-align: center;">Vigente a partir de: 04/10/2022</td>
                <td width="33%" style="text-align: Center;">Página {PAGENO} de {nbpg}</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center; padding: 10px 50px;" colspan="8">Semisótano del Hospital General Ajusco Medio "Dra. Obdulia Rodríguez Rodríguez". Encinos 41, Miguel Hidalgo 4ta Secc, Tlalpan, 14250 Ciudad de México, CDMX</td>
            </tr>
        </table>
    </htmlpagefooter>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>LESP-DA-FO-005/004</title>
    <style>
        .tarjeta-interior {
            float: left;
            margin-bottom: 0.1em;
            padding: 0.5em;
            box-sizing: border-box;
        }

        .tarjeta-interior p {
            margin: unset;
            padding: unset;
        }

        .container-table th {
            border: solid 1px #000;
            font-weight: bold;
        }

        .container-table td,
        .container-table th {
            border: solid 1px #000;
            text-align: center;
            padding: 3px 8px;
            font-size: 8pt;
        }

        @page {
            header: page-header;
            footer: page-footer;
        }
    </style>
</head>

<body>


    @foreach ($documentos as $sample)
    <div class="container-table" style="page-break-inside: avoid; display:block; margin: auto; width: 100%;">
        <table style="width: 90%; border-collapse: collapse; display:block; margin: auto;">
            <thead>
                <tr>
                    <th style="">Folio LESP</th>
                    <th style="">Tipo de Muestra</th>
                    <th style="">Identificación</th>
                    <th style="">Jurisdicción</th>
                    <th style="">Folio</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $sample->folio_lesp }}</td>
                    <td>{{ $sample->tipo_muestra }}</td>
                    <td>{{ $sample->nombre_paciente }}</td>
                    <td>{{ $sample->hospital }}</td>
                    <td>{{ $sample->folio_sisver }}</td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: justify;">Observaciones: {{$sample->observaciones}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    @endforeach

    <htmlpageheader name="page-header">
        <br>
        <div class="container-header">
            <div class="tarjeta-interior" style="width: 220px;">
                <img src="{{asset('panel/img/logo2.png')}}" style="width: 200px;height: auto;">
            </div>
            <div class="tarjeta-interior tarjeta-center" style="margin-left: 2.5em; width: 70px;">
            </div>
            <div class="tarjeta-interior" style="margin-left: 2.5em; width: 290px;">
                <p style="text-align:right; font-size: 8pt;">SECRETARÍA DE SALUD DE LA CIUDAD DE MÉXICO</p>
                <p style="text-align:right; font-size: 8pt;">SERVICIOS DE SALUD PÚBLICA DE LA CIUDAD DE MÉXICO</p>
                <p style="text-align:right; font-size: 8pt;">DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA</p>
                <p style="text-align:right; font-size: 8pt;">LABORATORIO DE VIGILANCIA EPIDEMIOLOGICA</p>
            </div>

            <div class="tarjeta-interior tarjeta-center" style="margin-bottom: 0.1em; margin-left: 35em; width: 200px; padding: unset;">
                <img src="{{asset('panel/img/image012.png')}}" style="width: auto;height: 60px;">
            </div>

            <div class="" style="padding: 0.5em; box-sizing: border-box;">
                <p style="text-align:right; font-size: 10pt; margin: unset; padding: unset; margin-top: 0.6em;">Ciudad de México a {{ \Carbon\Carbon::parse($date_send)->isoFormat('D') }} de {{ \Carbon\Carbon::parse($date_send)->isoFormat('MMMM') }} de {{ \Carbon\Carbon::parse($date_send)->isoFormat('Y') }}</p>
                <p style="text-align:right; font-size: 10pt; margin: unset; padding: unset; margin-top: 0.6em;">OFICIO: {{ $oficio_doc }}</p>
                <p style="text-align:right; font-size: 10pt; margin: unset; padding: unset; margin-top: 0.6em;">ASUNTO: Envío de muestra para diagnóstico de {{ Str::title($virus) }}</p>
            </div>

            <div class="tarjeta-interior" style="margin-left: 0.1em; width: 210px;">
                <p style="text-align:left; font-size: 9pt; margin-top: 0.4em;"><b>{{ config("analysis.$virus.mandated_origin") }}</b></p>
                <p style="text-align:left; font-size: 9pt; margin-top: 0.4em;">{{ config("analysis.$virus.position_origin") }}</p>
                <p style="text-align:left; font-size: 9pt; margin-top: 0.4em;">Presente</p>
            </div>

            <div class="tarjeta-interior" style="padding: 0.5em; box-sizing: border-box; margin-top: 2em; width: 360px; margin-left: 7em;">
                <p style="text-align:right; font-size: 9pt; margin: unset; padding: unset; margin-top: 0.4em;">{{ config("analysis.$virus.mandated_received") }}</p>
                <p style="text-align:right; font-size: 9pt; margin: unset; padding: unset; margin-top: 0.4em;">{{ config("analysis.$virus.position_received") }}</p>
            </div>
        </div>

        <div class="section-text-header" style="box-sizing: border-box; padding-left: 0em 0.1em; display:block;">
            @if ($virus == "leishmaniasis")
            <p style="text-align:justify; font-size: 10pt;">Por medio del presente, me permito referirle las siguientes muestras para el diagnóstico de Leishmania.</p>
            @else
            <p style="text-align:justify; font-size: 10pt;">Por medio del presente, se solicita su amable intervención para el procesamiento de las muestras para el diagnóstico de {{$virus}} que a continuación se enlistan:</p>
            @endif
        </div>
    </htmlpageheader>

    <htmlpagefooter name="page-footer">
        <div>
            <p style="text-align:justify; font-size: 10pt;">Sin otro particular, aprovecho la ocasión para enviarle un cordial saludo.</p>
            <br>
            <p style="text-align:justify; font-size: 9pt;"><b>Atentamente</b></p>
            <p style="margin-top:10em; text-align:justify; font-size: 9pt;"><b>LIC. MARIANA ANGÉLICA MARTÍNEZ ARIAS</b></p>
            <p style="text-align:justify; font-size: 9pt;"><b>COORDINADORA DE ENLACE DEL DEPARTAMENTO</b></p>
            <p style="text-align:justify; font-size: 9pt;"><b>ADMINISTRATIVO DEL LVE</b></p>
            <p style="text-align:justify; font-size: 8pt;">c.e.p. ARCHIVO </p>
            <div class="" style="width: 50%; float:left; text-align:left;">
                <p style="font-size: 7pt;">{{ Auth::user()->initials }}</p>
            </div>
            <div class="" style="width: 50%; float:left; text-align:right;">
                <p style="font-size: 7pt;">Hora de salida al InDRE: ___________</p>
            </div>


        </div>
        <br>
        <table width="100%" style="font-size: 8pt;">
            <tr>
                <td colspan="3" style="text-align: right;">OFICIO: {{ $office_enter }} </td>
            </tr>
            <tr>
                <td width="33%" style="text-align: center;"></td>
                <td width="33%" style="text-align: center;"></td>
                <td width="33%" style="text-align: right; font-size: 9pt;">Página {PAGENO} de {nbpg}</td>
            </tr>
            <tr>
                <td width="33%" style="text-align: justify; font-size: 7pt;">Calle Encinos 41 Colonia Miguel Hidalgo 4ta Secc. Alcaldía Tlalpan, C.P. 14250, Ciudad de Mexico T. 5550381700 ext. 6875-6876-6877-6878-6879</td>
                <td width="33%" style="text-align: Center;">LESP-DA-FO-005/004 Vigente a partir de: 20/01/2023</td>
                <td width="33%" style="text-align: right; font-size: 9pt;">CIUDAD INNOVADORA Y DE DERECHOS</td>
            </tr>
        </table>
    </htmlpagefooter>
</body>

</html>
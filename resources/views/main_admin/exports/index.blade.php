@extends('layouts.panel')
@section('content')

<x-card>
    <h5 class="text-sm lg:text-lg font-semibold text-slate-600 mb-2 md:mb-4">{{ __('Descarga de reportes') }}</h5>
    <div class="card-body">
        <div class="container">
            <livewire:exports-data>
        </div>
    </div>
</x-card>

@if(Auth::user()->id_role == 1 or Auth::user()->id_role == 2 or Auth::user()->id_role == 3 or Auth::user()->id == 6 or Auth::user()->id_role == 7)
<x-card>
    <h5 class="text-sm lg:text-lg font-semibold text-slate-600 mb-2 md:mb-4">{{ __('Muestras rechazadas') }}</h5>

    <livewire:rechazados>

</x-card>

<x-card>
    <h5 class="text-sm lg:text-lg font-semibold text-slate-600 mb-2 md:mb-4">{{ __('LESP-DA-FO-005/004') }}</h5>

    @livewire('reports.format-five-four')
</x-card>
@endif

<x-card>
    <h5 class="text-sm lg:text-lg font-semibold text-slate-600 mb-2 md:mb-4">{{ __('Muestras rechazadas y finalizadas por Jurisdicción') }}</h5>

    @livewire('reports.rejected-and-finalized')
</x-card>

@endsection
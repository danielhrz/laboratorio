<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>FO 016</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        .tarjeta-interior {
            float: left;
            margin-bottom: 1em;
            padding: 0.5em;
            box-sizing: border-box;
        }

        .section-header table {
            font-size: 9pt;
            font-weight: bold;
        }

        .footer table {
            font-size: 9pt;
        }

        .footer table td {
            padding: 5px 10px;
            text-align: center;
            border: solid 2px #000;
        }

        .container-table td,
        .container-table th {
            border: solid 1px #E4E4E4;
            text-align: center;
            padding: 3px 8px;
            font-size: 8pt;
        }

        @page {
            header: page-header;
            footer: page-footer;
        }
    </style>
</head>

<body>
    <div class="container-header">
        <div class="tarjeta-interior" style="width: 200px;">
            <img src="{{asset('panel/img/logo.png')}}" style="width: 200px;height: auto; position:absolute; top:5%;">
        </div>
        <div class="tarjeta-interior tarjeta-center" style="margin-left: 2em; width: 50px;">
            <img src="{{asset('panel/img/logo-laboratorio.png')}}" style="width: auto;height: 50px; position:absolute; left:28%; top: 5%;">
        </div>
        <div class="tarjeta-interior" style="margin-left: 2em; width: 300px; margin-bottom: 0px; padding: 0px 0.5em;">
            <p style="font-size: 9pt; text-align: center;">LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CIUDAD DE MÉXICO</p>
            <p style="font-size: 9pt; text-align: center;">FORMATO DE RELACIÓN DE MUESTRAS PARA PROCESAMIENTO LESP-VE-FO-016/005</p>
        </div>
        <div class="tarjeta-interior tarjeta-center" style="width: 100%; padding: unset; margin-bottom: 0px;">
            <p style="font-size: 9pt; text-align: right; margin: 0px; padding: 0px;">Vigente a partir de: 23/02/2023</p>
        </div>
    </div>

    <div class="section-header" style="box-sizing: border-box;">
        <table>
            <tr>
                <td style="width: 50%;">
                    <table>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td style="width: 50%;">
                    <table>
                        <tr>
                            <td>{{-- FOLIO: --}}</td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td style="width:50%;">
                    <table>
                        <tr>
                            <td>DIAGÓSTICO:</td>
                            <td style="font-weight: 100; text-align: center;">{{ $diagnostico }}</td>
                        </tr>
                    </table>
                </td>
                <td style="width:50%;">
                    <table>
                        <tr>
                            <td style="width:40%;">FECHA DE RECEPCIÓN:</td>
                            <td style="width: 60%;font-weight: 100; text-align: center;">{{ \Carbon\Carbon::parse( $reception )->format('d/m/Y') }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td style="width:50%;">
                    <table>
                        <tr>
                            <td>
                                <p>REMU
                            </td>
                            <td></td>
                            <td style="width: 30px; border: solid 2px #000;"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>DVE</td>
                            <td></td>
                            <td style="width: 30px; border: solid 2px #000;"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td style="width:50%;">
                    <table>
                        <tr>
                            <td style="width:40%;">FECHA DE ENTREGA:</td>
                            <td style="width: 60%;font-weight: 100; text-align: center;">{{ \Carbon\Carbon::now()->format('d/m/Y') }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </div><br>

    <div class="container-table">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Folio Lesp</th>
                    <th>Nombre</th>
                    <th># De Plataforma, # De Caso, #JS/UMS</th>
                    <th>Jurisdicción/Unidad</th>
                    <th>Tipo de muestra</th>
                    <th>Observaciones</th>
                </tr>
            </thead>
            <tbody>


                @if( $documentos->count() > 0 )
                @php
                $index = 1;
                @endphp

                @foreach ($documentos as $dato)
                <tr>
                    <td>{{ $index }}</td>
                    <td>{{ $dato->folio_lesp }}</td>
                    <td>{{ $dato->nombre_paciente }}</td>
                    <td>{{ $dato->folio_sisver }}</td>
                    <td>{{ $dato->hospital }}</td>
                    <td>{{ $dato->tipo_muestra }}</td>
                    <td>{{ $dato->observaciones }} </td>
                </tr>

                @php
                $index++;
                @endphp

                @endforeach

                @else
                <tr>
                    <td colspan="7" style="padding: 10px;font-size: 9pt; text-align:center;">No se encontró información disponible.</td>
                </tr>
                @endif


            </tbody>
        </table>
    </div><br>

    <div class="footer">
        <table>
            <tr>
                <td style="font-weight: bold;">Elaboró</td>
                <td style="font-weight: bold;">Revisó</td>
                <td style="font-weight: bold;">Hora</td>
                <td style="font-weight: bold;">Recibió</td>
            </tr>
            <tr>
                <td class="font-weight-bold" style="min-height: 30px; height: 50px;"></td>
                <td class="font-weight-bold" style="min-height: 30px; height: 50px;"></td>
                <td class="font-weight-bold" style="min-height: 30px; height: 50px;"></td>
                <td class="font-weight-bold" style="min-height: 30px; height: 50px;"></td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: bold; width: 50%;">Iniciales y Firma del personal del DA</td>
                <td colspan="2" style="font-weight: bold; width: 50%;">Iniciales y Firma o Rúbrica del personal de DVE</td>
            </tr>
        </table>
    </div>

    <htmlpagefooter name="page-footer">
        <table width="100%" style="font-size: 8pt;">
            <tr>
                <td width="33%" style="text-align: center;"></td>
                <td width="33%" style="text-align: center;"></td>
                <td width="33%" style="text-align: right;">Página {PAGENO} de {nbpg}</td>
            </tr>
        </table>
        <br>
    </htmlpagefooter>

</body>

</html>
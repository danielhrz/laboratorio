<table>
    <tbody>
        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td colspan="3">SECRETARÍA DE SALUD DE LA CIUDAD DE MÉXICO SERVICIOS DE SALUD PÚBLICA DE LA CIUDAD DE MÉXICO DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA</td><!-- D -->
        </tr>

        <!-- <tr>
            <td></td>
            <td colspan="7">LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CIUDAD DE MÉXICO FORMATO PARA RECHAZO DE MUESTRAS</td>
        </tr> -->

        <tr></tr>
        <tr>
            <td></td><!-- A -->
            <td>Desde: {{ \Carbon\Carbon::parse($date_start_samples)->format('d/m/Y') }}</td><!-- B -->
            <td colspan="2">Al: {{ \Carbon\Carbon::parse($date_end_samples)->format('d/m/Y') }}</td>
        </tr>

        <tr></tr>
        <tr>
            <td>No.</td>
            <td>Hospital</td>
            <td>Finalizadas</td>
            <td>Rechazadas</td>
        </tr>
        @php
        $index = 1;
        @endphp
        @foreach ($samples as $jurisdiction )
        <tr>
            <td>{{ $index }}</td>
            <td>{{ $jurisdiction['name'] }}</td>
            <td>{{ $jurisdiction['finalized'] }}</td>
            <td>{{ $jurisdiction['rejected'] }}</td>
        </tr>
        @php
        $index++;
        @endphp

        @endforeach
        <tr>
            <td>{{$index}}</td>
            <td>Total</td>
            <td></td>
            <td></td>
        </tr>
        <tr></tr>
    </tbody>
</table>
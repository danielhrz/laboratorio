<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Muestra rechazada</title>
</head>

<body>
    <div style="display: flex; justify-content: center; align-items: center;">
        <img style="width: 240px;" src="{{ $message->embed(asset('panel/img/logo.png')) }}" alt="">
        <img style="width: auto; height: 55px;" src="{{ $message->embed(asset('panel/img/logo-laboratorio.png')) }}" alt="">
        <p style="margin-left: 20px;">LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CIUDAD DE MÉXICO</p>
    </div>

    <br>
    <hr>
    <footer>
        <p>Buen día,</p>
        <p>Por medio del presente se envía el formato de rechazo de muestras que fueron enviadas el día de hoy {{ \Carbon\Carbon::now()->isoFormat('DD/MM/YYYY') }} al Laboratorio de Vigilancia Epidemiológica de la CDMX.</p>
        <p>No omito comentarle que el formato original será entregado al enlace en las instalaciones del mismo.</p>
        <br>
        <p>Saludos cordiales.</p>
        <p>--</p>
        <p>Favor de dar acuse de recibido</p>
        <br>
        <p>Laboratorio de Vigilancia Epidemiológica de la CDMX</p>
        <p>Calle Encinos 41, Colonia Miguel Hidalgo 4ta Secc. Alcaldía Tlalpan, C.P. 14250, Ciudad de México</p>
        <p>Tel: 55-50-38-17-00 Ext: 6879</p>
    </footer>
</body>

</html>
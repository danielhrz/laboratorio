<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
    <meta name="author" content="Jorge Daniel Hernández Sánchez">
    <title>{{ config('app.name') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{ asset('panel/img/favicon.png') }}" type="image/x-icon">
    @livewireStyles
</head>

<body class="flex bg-gray-100 justify-center h-screen items-center">

    <div class="w-80 lg:w-1/4 max-w-md bg-white rounded-lg border-t-4 border-t-green-700 shadow-xl">
        <div class="mt-5 mb-2 flex justify-center items-center flex-wrap px-5">
            <img class="w-11/12 h-auto" src="{{ asset('panel/img/logo.png') }}" alt="" /><br>
            <span class="mt-6 px-2 py-0.5 ml-3 text-xs sm:text-sm md:text-base font-medium text-white bg-green-800 rounded-md">{{ config('app.name') }}</span>
        </div>

        <form action="{{ route('login') }}" class="p-5" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="mb-6">
                <label for="username" class="block mb-2 text-sm font-medium text-slate-500">Nombre de usuario</label>
                <input type="text" name="username" id="username" class="border {{ $errors->has('username') ? 'shadow shadow-red-500/50 border-red-600 text-red-900 placeholder-red-700 focus:ring-red-600 focus:border-red-600' : 'border-gray-200 focus:ring-green-600 focus:border-green-700' }}  text-gray-900 text-sm rounded block w-full px-2.5 py-1.5" placeholder="Ingresa tu nombre de usuario" value="{{ old('username') }}">
                @if ($errors->has('username'))
                <p class="mt-2 text-xs text-red-700"><span class="font-medium">{{ $errors->first('username') }}</p>
                @endif
            </div>
            <div class="mb-6">
                <label for="password" class="block mb-2 text-sm font-medium text-slate-500">Contraseña</label>
                <input type="password" name="password" id="password" class="border {{ $errors->has('username') ? 'shadow shadow-red-500/50 border-red-600 text-red-900 placeholder-red-700 focus:ring-red-600 focus:border-red-600' : 'border-gray-200 focus:ring-green-600 focus:border-green-700' }}  text-gray-900 text-sm rounded block w-full px-2.5 py-1.5" placeholder="Ingresa tu contraseña">
                @if ($errors->has('password'))
                <p class="mt-2 text-xs text-red-700"><span class="font-medium">{{ $errors->first('password') }}</p>
                @endif
            </div>

            <button type="submit" class="block m-auto text-white bg-green-800 hover:bg-green-700 focus:ring-4 focus:outline-none focus:ring-2 focus:ring-green-500 font-medium rounded text-sm w-full sm:w-auto px-5 py-1.5 text-center">Accesar</button>
            <br>

            <br>
            <p class="text-center text-xs font-normal text-slate-500">Dudas y aclaraciones ext. 5428</p>
        </form>
    </div>
    @livewireScripts
</body>

</html>
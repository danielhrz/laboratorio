<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>LESP-DA-FO-005/008</title>

    @include('pdfs.format-da-fo-five.style')
</head>

<body>


    <div class="container-table" style="page-break-inside: avoid; display:block; margin: auto; width: 100%;">
        <table style="width: 80%; border-collapse: collapse; display:block; margin: auto;">
            <thead>
                <tr>
                    <th class="text-sm" style="width:30px">Fecha de Recepción</th>
                    <th class="text-sm">Tipo de Muestra</th>
                    <th class="text-sm">Folio LVE</th>
                    <th class="text-sm">Folio SINAVE</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($results as $result)
                <tr>
                    <td class="bold text-sm">{{ \Carbon\Carbon::parse( $result->fecha_recepcion)->isoFormat('DD/MM/YYYY') }}</td>
                    <td class="bold text-sm">{{ $result->tipo_muestra }}</td>
                    <td class="bold text-sm">{{ $result->folio_lesp }}</td>
                    <td class="bold text-sm">{{ $result->folio_sisver }}</td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @include('pdfs.format-da-fo-five.header')

    @include('pdfs.format-da-fo-five.footer')


</body>

</html>
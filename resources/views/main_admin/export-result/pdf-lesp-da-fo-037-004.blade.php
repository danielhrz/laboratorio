<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>LESP-DA-FO-037/008</title>

    @include('pdfs.format-ve-fo-thirty-seven.style')
</head>

<body>


    @foreach ($results as $result)
    <div class="container-table" style="page-break-inside: avoid; display:block; margin: auto; padding: 0em 0.7em;">
        <table style="width: 100%; border-collapse: collapse">
            <thead>
                <tr>
                    <th style="width: 30%; font-size: 7pt; border-bottom: solid 2px #000;">Nombre del paciente</th>
                    <th style="width: 17%; font-size: 7pt; border-bottom: solid 2px #000;">Folio LVE</th>
                    <th style="width: 18%; font-size: 7pt; border-bottom: solid 2px #000;">Folio SINAVE</th>
                    <th style="width: 20%; font-size: 7pt; border-bottom: solid 2px #000;">Tipo de muestra</th>
                    <th style="width: 15%; font-size: 7pt; border-bottom: solid 2px #000;">Fecha de toma</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="left">{{ $result['nombre_paciente'] }}</td>
                    <td>{{ $result['folio_lesp'] }}</td>
                    <td>{{ $result['folio_sisver'] }}</td>
                    <td>{{ $result['tipo_muestra'] }}</td>
                    <td align="right">{{ \Carbon\Carbon::parse( $result['fecha_toma_muestra'])->isoFormat('DD/MM/YYYY') }}</td>
                </tr>
                <tr>
                    <td align="left" style="font-weight: bold;">Resultado(s):</td>
                    @if(stripos($result['dx1'], 'TOS FERINA') !== false )
                    <td align="center" style="font-weight: bold;"></td>
                    @else
                    <td align="center" style="font-weight: bold;">Valor de referencia:</td>
                    @endif
                    <td align="left" style="font-weight: bold;">Técnica utilizada:</td>
                    <td align="left" colspan="2">{{ $result['diagnostics'][0]['technique_name'] }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: solid 1px #000; padding: 6px 0px;">
                        @if (stripos($result['dx1'], 'VIRUS RESPIRATORIOS') !== false )
                        <table style="width: 100%; border-collapse: collapse;">
                            @foreach ( $result['diagnostics'] as $diagnosis )
                            <tr>
                                <td style="width: 60%; padding: 0px;">
                                    <table style="width: 100%;">
                                        <tr>
                                            @if ($print != 2)
                                            @if ( $diagnosis['analysis'] == 'INFLUENZA')
                                            <td align="left" style="width: 50%; padding: 0px; font-weight: bold; font-size: 7pt;">{{ $diagnosis['analysis'] }}</td>
                                            <td style="text-align: center; padding: 0px; font-size: 7pt;">{{ ($result['status_result'] == 'FINALIZADO') ? $diagnosis['result'] : $result['status_result'] }}</td>
                                            @else
                                            <td align="left" style="width: 50%; padding: 0px; font-weight: bold; font-size: 7pt;">{{ $diagnosis['analysis'] }}</td>
                                            <td style="text-align: center; padding: 0px; font-size: 7pt;">{{ $diagnosis['result'] }}</td>
                                            @endif
                                            @else
                                            @if ( $diagnosis['analysis'] == 'INFLUENZA')
                                            <td align="left" style="width: 50%; padding: 0px; font-weight: bold; font-size: 7pt;">{{ $diagnosis['analysis'] }}</td>
                                            <td style="text-align: center; padding: 0px; font-size: 7pt;">{{ $diagnosis['result'] }}</td>
                                            @endif
                                            @endif

                                        </tr>
                                    </table>
                                </td>
                                @if ($print != 2)
                                <td style="width: 40%;padding: 0px; font-size: 7pt;">{!! $diagnosis['reference_value'] !!}</td>
                                @elseif($print == 2 && $diagnosis['analysis'] == 'INFLUENZA')
                                <td style="width: 40%;padding: 0px; font-size: 7pt;">{!! $diagnosis['reference_value'] !!}</td>
                                @endif
                            </tr>
                            @endforeach
                        </table>
                        @elseif(stripos($result['dx1'], 'TOS FERINA') !== false )
                        <table style="border-collapse: collapse;">
                            <tr>
                                <td align="center" style="border: solid 1px #000;" colspan="5">Gen detectado</td>
                                <td align="center" style="border: solid 1px #000;" rowspan="2">Resultado</td>
                            </tr>
                            <tr>
                                <td align="center" style="border: solid 1px #000;">Is481</td>
                                <td align="center" style="border: solid 1px #000;">pIS1001</td>
                                <td align="center" style="border: solid 1px #000;">hIS1001</td>
                                <td align="center" style="border: solid 1px #000;">pxtS1</td>
                                <td align="center" style="border: solid 1px #000;">RP</td>
                            </tr>
                            <tr>
                                @foreach ( $result['diagnostics'] as $diagnosis )
                                @php
                                $tempResult = explode('|', $diagnosis['result'] )
                                @endphp
                                <td align="center" style="border: solid 1px #000;">{{ ($tempResult[0] == 'NEGATIVO') ? '-' : '+' }}</td>
                                <td align="center" style="border: solid 1px #000;">{{ ($tempResult[1] == 'NEGATIVO') ? '-' : '+' }}</td>
                                <td align="center" style="border: solid 1px #000;">{{ ($tempResult[2] == 'NEGATIVO') ? '-' : '+' }}</td>
                                <td align="center" style="border: solid 1px #000;">{{ ($tempResult[3] == 'NEGATIVO') ? '-' : '+' }}</td>
                                <td align="center" style="border: solid 1px #000;">{{ ($tempResult[4] == 'NEGATIVO') ? '-' : '+' }}</td>
                                <td align="center" style="border: solid 1px #000; font-size: 7pt;"><b>{{ $tempResult[5] }}</b></td>
                                @endforeach
                            </tr>
                        </table>
                        @elseif(stripos($result['dx1'], 'ROTAVIRUS') !== false)
                        <table style="width: 100%; border-collapse: collapse;">
                            <tr>
                                <td style="width:50%;">
                                    @foreach ( $result['diagnostics'] as $diagnosis )
                                    {{ $diagnosis['result'] }}
                                    @endforeach
                                </td>
                                <td style="width:50%;">
                                    <table style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td align="center" style="border: solid 1px #000;" colspan="2">Valor de corte</td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="border: solid 1px #000;">NEGATIVO</td>
                                            <td align="center" style="border: solid 1px #000;"> =< 0.150 UA</td>
                                        </tr>

                                        <tr>
                                            <td align="center" style="border: solid 1px #000;">POSITIVO</td>
                                            <td align="center" style="border: solid 1px #000;"> > 0.150 UA</td>
                                        </tr>
                                    </table>
                                </td>

                            </tr>
                        </table>
                        @else
                        <table style="width: 100%;">
                            @if (stripos($result['dx1'], 'ARBOVIRUS') !== false )
                            @foreach ( $result['diagnostics'] as $diagnosis )
                            @php
                            $string = explode(':', $diagnosis['reference_value'] );
                            @endphp
                            <tr>
                                <td style="width: 60%; padding: 0px;">{{ $string[0].': '.$diagnosis['result'] }}</td>
                                <td style="width: 40%;padding: 0px; font-size: 7pt;">{!! $diagnosis['reference_value'] !!}</td>
                            </tr>
                            @endforeach
                            @else
                            @foreach ( $result['diagnostics'] as $diagnosis )
                            <tr>
                                <td style="width: 60%; padding: 0px;">{{ $diagnosis['result'] }}</td>
                                <td style="width: 40%;padding: 0px; font-size: 7pt;">{!! $diagnosis['reference_value'] !!}</td>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                        @endif
                    </td>
                    @if(stripos($result['dx1'], 'TOS FERINA') !== false )
                    <td colspan="3" style="border-bottom: solid 1px #000;">
                        @foreach ( $result['diagnostics'] as $diagnosis )
                        <table style="width: 100%;">
                            @foreach ( $result['diagnostics'] as $diagnosis )
                            <tr>
                                <td align="left" style="padding: 3px 0px;"><b>Valor de referencia:</b> {!! $diagnosis['reference_value'] !!}</td>
                            </tr>
                            <tr>
                                <td align="left" style="padding: 3px 0px;font-size: 7pt;"><b>Interpretación:</b> {{ $result['interpretation_result'] }}</td>
                            </tr>
                            <tr>
                                <td align="left" style="padding: 3px 0px;font-size: 7pt;"><b>Comentarios adicionales: </b> {{ $result['result_comments'] }}</td>
                            </tr>
                            @endforeach
                        </table>
                        @endforeach
                    </td>
                    @else
                    <td colspan="3" align="left" style="border-bottom: solid 1px #000; font-size: 7pt; ">
                        <table>
                            <tr>
                                <td colspan="3" align="left" style="font-size: 7pt;"><b>Comentarios adicionales: </b> {{ $result['result_comments'] }}</td>
                            </tr>
                            <tr>
                                <td colspan="3" align="left" style="font-size: 7pt;"><b>Interpretación: </b> {{ $result['interpretation_result'] }}</td>
                            </tr>
                        </table>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td colspan="5" style="border-bottom: solid 1px #000; text-align:center;">
                        <span>Fin de la información</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div><br>
    @endforeach


    @include('pdfs.format-ve-fo-thirty-seven.header')
    @include('pdfs.format-ve-fo-thirty-seven.footer')
</body>

</html>
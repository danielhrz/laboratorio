@extends('layouts.panel')
@section('content')
<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <div class="page-header row no-gutters py-4">
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-10 col-sm-8">
            @include('main_admin.flash-message')
        </div>
        <div class="card w-100">
            <div class="card-header border-bottom">
                <h6 class="m-0 text-center" style="font-size: 22px;">{{ __('Formato LESP-DA-FO-005/008') }}</h6>
            </div>
            <br>
            <div class="card-body">
                <div class="container">
                    <livewire:results.format-five>
                </div>
            </div>
            <div class="card-header border-bottom">
                <h6 class="m-0 text-center" style="font-size: 22px;">{{ __('Formato LESP-DA-FO-037/008') }}</h6>
            </div>
            <br>
            <div class="card-body">
                <div class="container">
                    <livewire:results.format-thirty-seven>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
@endsection
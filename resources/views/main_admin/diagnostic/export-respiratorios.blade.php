<table>
    <tr>
        <td></td>
        <td colspan="4">Resultados de virus respiratorios de {{$start->isoFormat('D MMM YYYY')}} a {{$end->isoFormat('D MMM YYYY')}}</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">Fecha de generación:</td>
        <td colspan="2">{{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
    </tr>

    <tr></tr>
    <tr></tr>

    <tr>
        <th rowspan="2" scope="col" class="border px-6 py-1.5">
            Fecha
        </th>
        <th rowspan="2" scope="col" class="border px-6 py-1.5">
            Recepcionadas
        </th>
        <th rowspan="2" scope="col" class="border px-6 py-1.5">
            Finalizadas
        </th>
        @foreach ($juris as $th)
        @if ($th != 'N/A')
        <th scope="col" colspan="4" class="border px-6 py-1.5">
            {{ $th }}
        </th>
        @endif
        @endforeach
    </tr>
    <tr>
        @foreach ($juris as $th)
        @if ($th != 'N/A')
        <th scope="col" class="border px-6 py-1.5">
            SARS
        </th>
        <th scope="col" class="border px-6 py-1.5">
            INFLUENZA
        </th>
        <th scope="col" class="border px-6 py-1.5">
            VSR
        </th>
        <th scope="col" class="border px-6 py-1.5">
            NEGATIVAS
        </th>
        @endif
        @endforeach
    </tr>

    @forelse ($information as $info)

    <tr>
        @foreach ($info as $row)
        <td>{{$row}}</td>
        @endforeach
    </tr>

    @empty
    <tr></tr>
    @endforelse
</table>
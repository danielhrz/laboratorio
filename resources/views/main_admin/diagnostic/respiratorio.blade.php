@extends('layouts.panel')
@section('content')
<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <div class="page-header row no-gutters py-4">
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-10 col-sm-8">
            @include('main_admin.flash-message')
        </div>
        <div class="card w-100">
            <div class="card-header border-bottom">
                <h6 class="m-0 text-center" style="font-size: 22px;">{{ __('Virus respiratorios') }}</h6>
            </div>

            <div class="container-chart-respiratorios relative p-1 w-full lg:w-3/4 m-auto mt-8 bg-white">
                <div class="flex justify-end items-center mb-4">
                    <button data-chart="samples-received-day" class="download text-cyan-700 hover:text-white border border-cyan-700 hover:bg-cyan-800 focus:ring-2 focus:outline-none focus:ring-cyan-300 font-medium rounded text-sm px-2 py-1.5 mr-2 text-center">
                        <i class="fa-regular fa-image"></i>
                    </button>
                    <label class="font-normal text-slate-700 text-right w-10 mr-3">Fecha:</label>
                    <div id="dateRangePickerId" date-rangepicker class="flex items-center">
                        <div class="relative">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path>
                                </svg>
                            </div>
                            <input id="start" name="start" type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded focus:ring-green-500 focus:border-green-500 block w-full pl-10 p-2.5" placeholder="Inicio...">
                        </div>
                        <span class="mx-4 text-gray-500">a</span>
                        <div class="relative">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path>
                                </svg>
                            </div>
                            <input id="end" name="end" type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded focus:ring-green-500 focus:border-green-500 block w-full pl-10 p-2.5" placeholder="Hasta...">
                        </div>
                    </div>
                    <button id="search-samples" class="text-cyan-700 hover:text-white border border-cyan-700 hover:bg-cyan-800 focus:ring-2 focus:outline-none focus:ring-cyan-300 font-medium rounded text-sm px-2 py-1.5 ml-4 text-center">
                        Buscar
                    </button>
                </div>
                <canvas id="container-chart-respiratorios" class="max-h-128 bg-white"></canvas>

                <div class="container-loader-samples-received-day absolute w-full h-full top-0 flex justify-center items-center flex-col hidden">
                    <div class="spinner-chart"></div>
                    <p class="text-sm text-red-900">Cargando información...</p>
                </div>

            </div>

        </div>
    </div>

    <livewire:diagnostics.respiratorios />
</div>

<input type="hidden" id="url" value="{{ url('/') }}">

<div id="tooltipDownload" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
    Descargar imagen
    <div class="tooltip-arrow" data-popper-arrow></div>
</div>
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
<script>
    var targetChartRespiratorios;
    //Se crea una variable global para el archivo
    var chartRespiratorios;

    const plugin = {
        id: 'custom_canvas_background_color',
        beforeDraw: (chart) => {
            const {
                ctx
            } = chart;
            ctx.save();
            ctx.globalCompositeOperation = 'destination-over';
            ctx.fillStyle = '#FFFFFF';
            ctx.fillRect(0, 0, chart.width, chart.height);
            ctx.restore();
        }
    };

    $(document).ready(function() {

        var targetDownload = document.getElementById('tooltipDownload');
        var buttonsDownload = document.getElementsByClassName('download');
        Array.from(buttonsDownload).forEach(function(element) {
            let tooltip = new Tooltip(targetDownload, element);
        }, false);


        $('.download').click(function() {
            var chart = $(this).data('chart');
            var canvas, title;
            
            canvas = document.getElementById('container-chart-respiratorios');
            title = 'resultados de virus respiratorios.jpg';

            var a = document.createElement('a');
            a.href = canvas.toDataURL("image/jpeg");
            a.download = title;
            // Trigger the download
            a.click();
        });

        const dateRangePickerEl = document.getElementById('dateRangePickerId');

        Datepicker.locales.es = {
            days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
            daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            monthsTitle: "Meses",
            clear: "Borrar",
            weekStart: 1,
            format: "dd/mm/yyyy"
        };
        var calendar = new DateRangePicker(dateRangePickerEl, {
            language: 'es',
            autohide: true,
            clearBtn: false,
        });

        targetChartRespiratorios = document.getElementById('container-chart-respiratorios').getContext('2d');
        sampleReceive(0);

        $("#search-samples").click(function() {
            sampleReceive(1);
        });

        Livewire.on('TableReceivedDayCheck', () => {
            tableDay = true;
            Livewire.emit('TableReceivedDay');
        });

        Livewire.on('TableReceivedDay', () => {
            if (chartDay == true && tableDay == true) {
                $('.container-loader-samples-received-day').addClass('hidden');
            }
        });
    });

    function sampleReceive(status) {
        if (status == 1) {
            chartRespiratorios.destroy();
        }

        var start = $("#start").val();
        if (start != '') {
            start = start.split('/');
            start = start[2] + '-' + start[1] + '-' + start[0];
        } else {
            start = formatDate(new Date());
        }

        var end = $("#end").val();
        if (end != '') {
            end = end.split('/');
            end = end[2] + '-' + end[1] + '-' + end[0];
        } else {
            end = formatDate(new Date());
        }

        Livewire.emit('tableSamples', start, end);

        $.ajax({
            url: $('#url').val() + '/api/charts/respiratorios/' + start + '/' + end,
            method: 'GET',
            //async: false,
            beforeSend: function() {
                $('.container-loader-samples-received-day').removeClass('hidden');
                chartDay = false;
                tableDay = false;
            },
            success: function(response) {
                console.log(response);
                let title = 'Virus respiratorios de ' + response.start + ' al ' + response.end;
                chartRespiratorios = new Chart(targetChartRespiratorios, {
                    type: 'bar',
                    data: {
                        labels: response.labels,
                        datasets: [{
                            //label: labels,
                            data: response.datos,
                            backgroundColor: response.background,
                            borderColor: response.border,
                            borderWidth: 2,
                            font: function(context) {
                                var width = context.chart.width;
                                var size = Math.round(width / 32);

                                return {
                                    weight: 'bold',
                                    size: size
                                };
                            }
                        }]
                    },
                    plugins: [ChartDataLabels, plugin],
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true,
                                display: true,
                                stacked: true,
                                ticks: {
                                    font: function(context) {
                                        var width = context.chart.height;
                                        var size = Math.round(width / 50);

                                        return {
                                            //weight: 'bold',
                                            style: 'normal',
                                            size: size
                                        };
                                    },
                                },
                                title: {
                                    display: false,
                                    text: 'Número de muestras',
                                    color: '#212F3D',
                                    font: function(context) {
                                        var width = context.chart.height;
                                        var size = Math.round(width / 36);

                                        return {
                                            //weight: 'bold',
                                            style: 'normal',
                                            size: size
                                        };
                                    },
                                    padding: {
                                        top: 30,
                                        left: 0,
                                        right: 0,
                                        bottom: 0
                                    }
                                }
                            },
                            x: {
                                grid: {
                                    display: false,
                                },
                                beginAtZero: true,
                                fontSize: 8,
                                autoSkip: false,
                                maxRotation: 120,
                                minRotation: 90,
                                ticks: {
                                    font: function(context) {
                                        var width = context.chart.height;
                                        var size = Math.round(width / 50);

                                        return {
                                            //weight: 'bold',
                                            style: 'normal',
                                            size: size
                                        };
                                    },
                                }
                            }
                        },
                        plugins: {
                            datalabels: {
                                align: 'end',
                                anchor: 'end',
                                color: '#212F3D',
                                font: function(context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                formatter: Math.round
                            },
                            legend: false,
                            title: {
                                display: true,
                                text: title,
                                font: function(context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 34);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            }
                        }
                    }
                });
                chartDay = true;
                Livewire.emit('TableReceivedDay');
            }
        });
    }

    function formatDate(date) {
        return [
            date.getFullYear(),
            padTo2Digits(date.getMonth() + 1),
            padTo2Digits(date.getDate()),
        ].join('-');
    }

    function padTo2Digits(num) {
        return num.toString().padStart(2, '0');
    }
</script>
@endpush
<br>
@endsection
@extends('layouts.panel')
@section('content')
<div class="flex justify-start items-center text-xs md:text-sm mb-4">
    <a href="{{ url('main_admin') }}" class="mx-1 font-semibold text-slate-700"><i class="fa-solid fa-house"></i></a>
    <a href="{{ url('main_admin') }}" class="mx-1 font-semibold text-slate-700">Inicio</a>

    <span class="mx-1 font-semibold text-slate-700"><i class="fa-solid fa-chevron-right"></i></span>
    <span class="mx-1 font-semibold text-blue-700">Paludismo</span>
</div>

<div>
    @include('main_admin.flash-message')
</div>

<input type="hidden" id="rutaFullResults" value="{{ url('/') }}">

@livewire('paludismo.index-component')
@endsection
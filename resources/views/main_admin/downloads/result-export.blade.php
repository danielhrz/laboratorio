<table>

    <tr>
        <td></td>
        <td colspan="4">{{ $title }}</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">Fecha de exportación:</td>
        <td colspan="2">{{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
    </tr>

    <tr></tr>
    <tr>
        <td>indice</td>
        <td>año</td>
        <td>Folio LESP</td>
        <td>dx1</td>
        <td>dx2</td>
        <td>dx3</td>
        <td>dx4</td>
        <td>dx5</td>
        <td>Oficio de Entrada</td>
        <td>Nombre del Paciente</td>
        <td>Tipo de Muestra</td>
        <td>Fecha de Toma de Muestra</td>
        <td>Folio SINAVE</td>
        <td>Estatus REMU</td>
        <td>Estatus Resultado</td>
        <td># CRE/FORMATO/BITACORA</td>
        <td>Comentarios Adicionales</td>
        <td>Interpretación del Resultado</td>
        <td>Oficio de salida</td>
        <td>Fecha de entrega al AEER</td>
        <td>Diagnostico solicitado</td>
        <td>Kit empleado</td>
        <td>Valor de referencia</td>
        <td>Técnica empleada</td>
        <td>Analisis</td>
        <td>Resultado</td>
        <td>Nombre del Usuario que registro</td>
        <td>Fecha</td>
    </tr>
    <tbody>
        @php
        $index = 1;
        @endphp

        @foreach ($samples as $sample )
        <tr>
            <td>{{$index}}</td>
            <td>{{$sample->anio}}</td>
            <td>{{$sample->folio_lesp}}</td>
            <td>{{$sample->dx1}}</td>
            <td>{{$sample->dx2}}</td>
            <td>{{$sample->dx3}}</td>
            <td>{{$sample->dx4}}</td>
            <td>{{$sample->dx5}}</td>
            <td>{{$sample->oficio_entrada}}</td>
            <td>{{$sample->nombre_paciente}}</td>
            <td>{{$sample->tipo_muestra}}</td>
            <td>{{$sample->fecha_toma_muestra}}</td>
            <td>{{$sample->folio_sisver}}</td>
            <td>{{$sample->status}}</td>
            <td>{{$sample->status_result}}</td>
            <td>{{$sample->cre}}</td>
            <td>{{$sample->result_comments}}</td>
            <td>{{$sample->interpretation_result}}</td>
            <td>{{$sample->if_exit}}</td>
            <td>{{$sample->date_delivery}}</td>
            <td>{{$sample->diagnosis_name}}</td>
            <td>{{$sample->kit_name}}</td>
            <td>{{$sample->reference_value}}</td>
            <td>{{$sample->technique_name}}</td>
            <td>{{$sample->analysis}}</td>
            <td>{{$sample->result}}</td>
            <td>{{$sample->name.' '.$sample->paternal_surname.' '.$sample->maternal_surname}}</td>
            <td>{{$sample->fecha_recepcion}}</td>
        </tr>

        @php
        $index++;
        @endphp

        @endforeach
    </tbody>
</table>
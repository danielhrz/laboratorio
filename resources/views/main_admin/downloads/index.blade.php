@extends('layouts.panel')
@section('content')
<div id="container-component" class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 flex justify-center items-center flex-col">
    <div class="row">
        
        @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 3 or Auth::user()->id_role == 6 or Auth::user()->id == 18 or Auth::user()->id_role == 11)
            <fieldset class="border mt-3 mb-6 p-2 w-full lg:w-11/12 xl:w-3/4 2xl:max-w-4xl">
                <legend class="px-2 mb-2 text-sm xl:text-lg font-semibold tracking-tight text-green-800 dark:text-white">Reporte general de muestras</legend>
                <livewire:downloads.samples-downloads />
            </fieldset>
        @endif

        @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 5 or Auth::user()->id_role == 6 or Auth::user()->id == 18 or Auth::user()->id_role == 11)
        <fieldset class="border mt-3 mb-6 p-2 w-full lg:w-11/12 xl:w-3/4 2xl:max-w-4xl">
            <legend class="px-2 mb-2 text-sm xl:text-lg font-semibold tracking-tight text-green-800 dark:text-white">Reporte general de resultados</legend>
            <livewire:downloads.results-downloads />
        </fieldset>
        @endif
    </div>
</div>
<br>
<br>
@endsection
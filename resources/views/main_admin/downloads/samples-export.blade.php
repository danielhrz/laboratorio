<table>

    <tr>
        <td></td>
        <td colspan="4">{{ $title }}</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">Fecha de exportación:</td>
        <td colspan="2">{{ \Carbon\Carbon::now()->isoFormat('D MMM YYYY, h:mm A') }}</td>
    </tr>

    <tr></tr>
    <tr>
        <td>indice</td>
        <td>año</td>
        <td>Folio LESP</td>
        <td>dx1</td>
        <td>dx2</td>
        <td>dx3</td>
        <td>dx4</td>
        <td>dx5</td>
        <td>Oficio de Entrada</td>
        <td>Fecha de Recepción</td>
        <td>Jurisdicción/Hospital</td>
        <td>Nombre del Paciente</td>
        <td>Tipo de Muestra</td>
        <td>Fecha de Toma de Muestra</td>
        <td>Folio SISVER</td>
        <td>Persona que Recibe</td>
        <td>Estatus</td>
        <td>Rechazos</td>
        <td>Observaciones</td>
        <td>Aclaraciones Remu</td>
        <td>Hora de recepción</td>
        <td>Usuario que Registro</td>
        <td>Fecha</td>
    </tr>
    <tbody>
        @php
        $index = 1;
        @endphp

        @foreach ($samples as $sample )
        <tr>
            <td>{{$index}}</td>
            <td>{{$sample->anio}}</td>
            <td>{{$sample->folio_lesp}}</td>
            <td>{{$sample->dx1}}</td>
            <td>{{$sample->dx2}}</td>
            <td>{{$sample->dx3}}</td>
            <td>{{$sample->dx4}}</td>
            <td>{{$sample->dx5}}</td>
            <td>{{$sample->oficio_entrada}}</td>
            <td>{{$sample->fecha_recepcion}}</td>
            <td>{{$sample->hospital}}</td>
            <td>{{$sample->nombre_paciente}}</td>
            <td>{{$sample->tipo_muestra}}</td>
            <td>{{$sample->fecha_toma_muestra}}</td>
            <td>{{$sample->folio_sisver}}</td>
            <td>{{$sample->persona_recibe}}</td>
            <td>{{$sample->status}}</td>
            <td>{{$sample->rechazos}}</td>
            <td>{{$sample->observaciones}}</td>
            <td>{{$sample->aclaraciones_remu}}</td>
            <td>{{$sample->hora_recepcion}}</td>
            <td>{{$sample->users->full_name}}</td>
            <td>{{$sample->fecha_recepcion}}</td>
        </tr>

        @php
        $index++;
        @endphp

        @endforeach
    </tbody>
</table>
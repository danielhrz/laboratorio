<div class="flex items-center justify-center">
    <a href="{{ route('usuarios.edit', $id ) }}" type=" button" class="button tooltipButtonEdit text-blue-700 border border-blue-700 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded-full text-xs md:text-sm p-1.5 md:p-2.5 text-center flex items-center justify-center">
        <i class="fa-solid fa-pen-to-square"></i>
    </a>

    <button type="submit" data-action="{{ route('usuarios.destroy', $id ) }}" class="button btn-option-delete tooltipButtonDelete text-red-700 border border-red-700 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded-full text-xs md:text-sm p-1.5 md:p-2.5 text-center flex items-center justify-center">
        <i class="fa-solid fa-trash-can"></i>
    </button>
</div>
@extends('layouts.panel')
@section('content')
@include('main_admin.flash-message')

<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <h5 class="text-center mb-2 text-2xl font-semibold text-slate-700 mb-4 flex items-center">
        <a href="{{ url('usuarios/create') }}" id="button-create-user" data-tooltip-target="tooltip-default" type="button" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-2 py-1.5 text-center mr-2">
            <i class="fa-solid fa-user-plus"></i>
        </a>
        Lista de usuarios
    </h5>

    <div id="tooltip-default" role="tooltip" class="inline-block absolute invisible z-40 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700">
        Agregar usuario
        <div class="tooltip-arrow" data-popper-arrow></div>
    </div>

    <div class="overflow-x-auto relative {{-- mb-8 --}}">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <th scope="row" class="py-4 px-5 font-medium text-gray-900 whitespace-nowrap dark:text-white">#</th>
                    <th scope="col" class="py-3 px-6">Rol de Usuario</th>
                    <th scope="col" class="py-3 px-6">Nombre de Usario</th>
                    <th scope="col" class="py-3 px-6">Nombre</th>
                    <th scope="col" class="py-3 px-6">Jurisdicción</th>
                    <th scope="col" class="py-3 px-6">Ubicación</th>
                    <th scope="col" class="py-3 px-6">Teléfono / Ext</th>
                    <th scope="col" class="py-3 px-6">Correo Electrónico</th>
                    <th scope="col" class="py-3 px-6">Creado</th>
                    <th scope="col" class="py-3 px-6">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($usuarios->where('is_active', '=', '1') as $usuario)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <td scope="row" class="py-4 px-5 font-medium text-gray-900 whitespace-nowrap dark:text-white">{{ $usuario->id }}</td>
                        <td class="py-3 px-6">{{ $usuario->role->name }}</td>
                        <td class="py-3 px-6">{{ $usuario->username }}</td>
                        <td class="py-3 px-6">{{ $usuario->name }} {{ $usuario->paternal_surname }} {{ $usuario->maternal_surname }}</td>
                        <td class="py-3 px-6">{{ $usuario->juris->name }}</td>
                        <td class="py-3 px-6">{{ $usuario->areas->name }}</td>
                        <td class="py-3 px-6">{{ $usuario->phone }} / {{ $usuario->ext }}</td>
                        <td class="py-3 px-6">{{ $usuario->email }}</td>
                        <td class="py-3 px-6">{{ $usuario->created_at->diffForHumans() }}</td>
                        <td class="py-3 px-6">
                            <div class="flex items-center justify-center">
                                <a href="{{ url('/usuarios/'.$usuario->id.'/edit') }}" data-tooltip-target="tooltipUsersEdit-{{$usuario->id}}" data-tooltip-style="light" class="button btn-option-edit text-blue-700 border border-blue-700 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded-full text-xs md:text-sm p-1.5 md:p-2.5 text-center flex items-center justify-center">
                                    <i class="fa-solid fa-pen-to-square"></i>
                                </a>
                                <div id="tooltipUsersEdit-{{$usuario->id}}" role="tooltip" class="inline-block absolute invisible z-40 py-2 px-3 text-sm font-medium border border-blue-300 text-slate-700 bg-gray-50 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip">
                                    Editar Usuario
                                    <div class="tooltip-arrow" data-popper-arrow></div>
                                </div>
                                <div class="tooltip-arrow" data-popper-arrow></div>
                                <form method="POST" action="{{ url('/usuarios/'.$usuario->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" onclick="return confirm('¿Deseas eliminar este usuario?')" data-tooltip-target="tooltipUsersDelete-{{$usuario->id}}" data-tooltip-style="light" class="button btn-option-delete text-red-700 border border-red-700 hover:bg-red-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-red-300 font-medium rounded-full text-xs md:text-sm p-1.5 md:p-2 text-center flex items-center justify-center" title="Eliminar Usuario">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                    <div id="tooltipUsersDelete-{{$usuario->id}}" role="tooltip" class="inline-block absolute invisible z-40 py-2 px-3 text-sm font-medium border border-red-300 text-slate-700 bg-gray-50 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip">
                                        Eliminar Usuario
                                        <div class="tooltip-arrow" data-popper-arrow></div>
                                    </div>
                                    <div class="tooltip-arrow" data-popper-arrow></div>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div>
    {{ $usuarios->links('pagination::tailwind') }}
</div>

@endsection
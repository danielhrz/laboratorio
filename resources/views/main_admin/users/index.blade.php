@extends('layouts.panel')

@section('content')

@include('main_admin.flash-message')

<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <h5 class="text-center mb-2 text-2xl font-semibold text-slate-700 mb-4 flex items-center">
        <a href="{{ url('usuarios/create') }}" id="button-create-user" data-tooltip-target="tooltip-default" type="button" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-2 py-1.5 text-center mr-2">
            <i class="fa-solid fa-user-plus"></i>
        </a>
        Lista de usuarios
    </h5>
    <div id="tooltip-default" role="tooltip" class="inline-block absolute invisible z-40 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700">
        Agregar usuario
        <div class="tooltip-arrow" data-popper-arrow></div>
    </div>
    <br>

    <div class="container-table pb-3 px-3">
        <table id="users" class="w-full">
            <thead>
                <tr>
                    <th>Indice</th>
                    <th>Role</th>
                    <th>Usuario</th>
                    <th>Nombre</th>
                    <th>Jurisdicción</th>
                    <th>Ubicación</th>
                    <th>Teléfono</th>
                    <th>Ext</th>
                    <th>Correo Electrónico</th>
                    <th>Creado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>

    <!--    Creación de tooltip de editar    -->
    <div id="tooltipEdit" role="tooltip" class="inline-block absolute invisible z-40 py-2 px-3 text-sm font-medium border border-blue-300 text-slate-700 bg-gray-50 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip">
        Editar Registro
        <div class="tooltip-arrow" data-popper-arrow></div>
    </div>

    <!--    Creación de tooltip de borrar    -->
    <div id="tooltipDelete" role="tooltip" class="inline-block absolute invisible z-40 py-2 px-3 text-sm font-medium border border-red-300 text-slate-700 bg-gray-50 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip">
        Eliminar Registro
        <div class="tooltip-arrow" data-popper-arrow></div>
    </div>
</div>
<br>
<br>
<input type="hidden" id="rutaUsers" value="{{ route('dataTableUsers') }}">
@endsection
@extends('layouts.panel')
@section('content')
    <div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
        <h5 class="mb-8 text-2xl font-medium text-slate-600 flex items-center justify-center">
            Agregar Usuario
        </h5>
        <form action="{{ url('usuarios') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class=" grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <label for="id_role" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Rol</label>
                    <select class="border {{ $errors->has('id_role') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }}  bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="id_role" value="{{ old('id_role') }}" id="id_role">
                        <option selected disabled>Selecciona Rol</option>
                        @foreach ($roles as $role)
                        <option value="{{$role->id}}" {{(old('id_role')==$role->id)? 'selected':''}}>{{ $role->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-6 group">
                    <label for="username" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Nombre de Usuario</label>
                    <input name="username" type="text" class="text-uppercase border {{ $errors->has('username') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="username" value="{{ old('username') }}" autofocus id="username" placeholder="Ingresar Nombre de Usuario" >
                </div>
            </div>
            <div class=" grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <label for="name" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Nombre</label>
                    <input name="name" type="text" class="text-uppercase border {{ $errors->has('name') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="name" value="{{ old('name') }}" autofocus id="name" placeholder="Ingresar Nombre" onkeyup="this.value=this.value.toUpperCase();">
                </div>
                <div class="mb-6 group">
                    <label for="paternal_surname" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Apellido Paterno</label>
                    <input name="paternal_surname" type="text" class="text-uppercase border {{ $errors->has('paternal_surname') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="paternal_surname" value="{{ old('paternal_surname') }}" autofocus id="paternal_surname" placeholder="Ingresar Apellido Paterno" onkeyup="this.value=this.value.toUpperCase();">
                </div>
            </div>
            <div class=" grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <label for="maternal_surname" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Apellido Materno</label>
                    <input name="maternal_surname" type="text" class="text-uppercase border {{ $errors->has('maternal_surname') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="maternal_surname" value="{{ old('maternal_surname') }}" autofocus id="maternal_surname" placeholder="Ingresar Apellido Materno" onkeyup="this.value=this.value.toUpperCase();">
                </div>
                <div class="mb-6 group">
                    <label for="id_juris" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Jurisdicción</label>
                    <select class="border {{ $errors->has('id_juris') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }}  bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="id_juris" value="{{ old('id_juris') }}" id="id_juris">
                        <option selected disabled>Selecciona la Jurisdicción</option>
                        @foreach ($juris as $juri)
                        <option value="{{$juri->id}}" {{(old('id_juris')==$juri->id)? 'selected':''}}> {{ $juri->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class=" grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <label for="id_areas" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Ubicación</label>
                    <select class="border {{ $errors->has('id_areas') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }}  bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="id_areas" value="{{ old('id_areas') }}"  id="id_areas">
                        <option selected disabled>Selecciona Ubicación</option>
                        @foreach ($areas as $area)
                        <option value="{{$area->id}}" {{(old('id_areas')==$area->id)? 'selected':''}}> 
                            {{ $area->name}} 
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-6 group">
                    <label for="email" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Correo Electrónico</label>
                    <input name="email" type="email" class="text-uppercase border {{ $errors->has('email') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="email" value="{{ old('email') }}" autofocus id="email" placeholder="Ingresa Correo Electrónico">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class=" grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <label for="password" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Contraseña</label>
                    <input name="password" type="password"  class="text-uppercase border {{ $errors->has('password') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="password" id="password" placeholder="Ingresa Contraseña">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="mb-6 group">
                    <label for="password_confirmation" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Confirmar Contraseña</label>
                    <input name="password_confirmation" type="password" class="text-uppercase border {{ $errors->has('password_confirmation') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="password_confirmation" id="password_confirmation" placeholder="Confirmar Contraseña">
                </div>
            </div>
            <div class=" grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <label for="phone" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Teléfono</label>
                    <input type="text" class="text-uppercase border {{ $errors->has('phone') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="phone" value="{{ old('phone') }}" id="phone" placeholder="Ingresar Teléfono"> 
                </div>
                <div class="mb-6 group">
                    <label for="ext" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Extensión</label>
                    <input type="text" class="text-uppercase border {{ $errors->has('phone') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded  block w-full p-2" name="ext" value="{{ old('ext') }}" id="ext" placeholder="Ingresar Extensión"> 
                </div>
            </div>
            <button type="submit" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2">Registrar</button>
            <button type="button" class="button-after text-red-700 hover:text-red-50 border border-red-700 hover:bg-red-800 focus:ring-2 hover:shadow-lg hover:shadow-red-500/50 focus:outline-none focus:ring-red-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2">Regresar</button>
        </form>
    </div>
@endsection
<div class="form-check">
    @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 3 or Auth::user()->id_role == 6 or Auth::user()->id_role == 7)
        <input class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 focus:ring-2 change-status-massive" name="status-change[]" type="checkbox" value="{{ $id }}">
    @endif
</div>
<div class="flex items-center justify-center">
    @if( $operational_status != 'CONCLUIDO' || Auth::user()->id_role == 1 || Auth::user()->id_role == 6 )

    @if( Auth::user()->id_role == 1 or Auth::user()->id_role == 3 or Auth::user()->id_role == 6 or Auth::user()->id_role == 7 )
    <button data-muestra="{{ $id }}" class="show-modal-edit tooltipButtonEdit text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-md text-xs md:text-sm p-2 md:p-2.5 text-center flex items-center justify-center mr-2">
        <i class="fas fa-edit"></i></a>
    </button>
    @endif
    @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 6)
    <button type='submit' class="btn-option-delete tooltipButtonDelete text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-md text-xs md:text-sm p-2 md:p-2.5 text-center flex items-center justify-center mr-2" data-datos="{{ route('datos.destroy', $id) }}" data-toggle="tooltip" data-placement="bottom" title="Eliminar Datos">
        <i class="fas fa-trash-alt"></i>
    </button>
    @endif

    @endif
</div>
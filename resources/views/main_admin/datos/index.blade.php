@extends('layouts.panel')
@section('content')
@include('main_admin.flash-message')
    <div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">

        <h5 class="text-center mb-2 text-2xl font-semibold text-slate-700 mb-4 flex items-center">
            @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 2 or Auth::user()->id_role == 3 or Auth::user()->id_role == 6 or Auth::user()->id_role == 7)
            <button type="button" data-tooltip-target="tooltip-Registration" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-2 py-1.5 text-center mr-2" id="showModal">
                <i class="fas fa-folder-plus"></i>
            </button>
            <button type="button" data-tooltip-target="tooltip-import" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-2 py-1.5 text-center mr-2" data-modal-toggle="defaultModal">
                <i class="fas fa-file-import"></i>
            </button>
            @endif
            Lista de Datos Plataforma
        </h5>

        <div id="tooltip-Registration" role="tooltip" class="inline-block absolute invisible z-40 px-2 py-1.5 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700">
            Registro Manual de Datos Plataforma
            <div class="tooltip-arrow" data-popper-arrow></div>
        </div>
        <div id="tooltip-import" role="tooltip" class="inline-block absolute invisible z-40 px-2 py-1.5 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700">
            Importar Archivo
            <div class="tooltip-arrow" data-popper-arrow></div>
        </div>
        <div class="card-header border-bottom"></div>
        <br>
        <div>
            <button id="dropdownRightStartButton" data-dropdown-toggle="dropdownRightStart" data-dropdown-placement="right-start" class="mb-3 md:mb-0 text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-3 py-1.5 text-center inline-flex items-center dark:border-green-500 dark:text-green-500 dark:hover:text-white dark:hover:bg-green-600 dark:focus:ring-green-800" type="button">
                Mostrar / Ocultar columnas <svg class="ml-2 w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
            </button>
            <div id="dropdownRightStart" class="hidden z-10 w-44 bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700">
                <ul id="colums-dataTables" class="py-1 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdownRightStartButton">
                    <li>
                        <a class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white" data-column="5" href="#">Dx2</a>
                    </li>
                    <li>
                        <a class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white" data-column="6" href="#">Dx3</a>
                    </li>
                    <li>
                        <a class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white" data-column="7" href="#">Dx4</a>
                    </li>
                    <li>
                        <a class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white" data-column="8" href="#">Dx5</a>
                    </li>
                    <li>
                        <a class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white" data-column="23" href="#">Usuario capturista</a>
                    </li>
                </ul>
            </div>
        </div>
        <br>
        <div>
            @foreach ($datos as $dato)
            Registros Pendientes: <span class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">{{ $dato->total }}</span>
            @endforeach
            &nbsp; &nbsp;
            @foreach ($estatus as $estatu)
            Registros Finalizados: <span class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">{{ $estatu->total }}</span>
            @endforeach
            &nbsp; &nbsp;
            @foreach ($cancelados as $cancelado)
            Registros Cancelados: <span class="bg-red-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">{{ $cancelado->total }}</span>
            @endforeach
            &nbsp; &nbsp;
            @foreach ($rechazos as $rechazo)
            Registros Rechazos: <span class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">{{ $rechazo->total }}</span>
            @endforeach
        </div>
        <br>
        <div id="container-buttons-status" style="display: none;" data-folios="">
            Cambio de estatus:

            <button class="btn-action-status text-yellow-300 hover:text-white border border-yellow-300 hover:bg-yellow-300 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2 dark:border-yellow-300 dark:text-yellow-300 dark:hover:text-white dark:hover:bg-yellow-400 dark:focus:ring-yellow-900" data-toggle="tooltip" data-status="PENDIENTE" title="Estado Pendiente">
                <i class="fas fa-exclamation-triangle"></i>
            </button>

            <button class="btn-action-status text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2 dark:border-gray-600 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-800" data-toggle="tooltip" data-status="RECHAZO" title="Estado Rechazo">
                <i class="fas fa-undo"></i>
            </button>

            <button class="btn-action-status text-red-700 hover:text-white border border-red-700 hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2 dark:border-red-500 dark:text-red-500 dark:hover:text-white dark:hover:bg-red-600 dark:focus:ring-red-900" data-toggle="tooltip" data-status="CANCELADO" title="Estado Cancelado">
                <i class="fas fa-ban"></i>
            </button>

            <button class="btn-action-status text-blue-400 hover:text-white border border-blue-400 hover:bg-blue-400 focus:ring-4 focus:outline-none focus:ring-blue-400 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2 dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800" data-toggle="tooltip" data-status="FINALIZADO" title="Estado Finalizado">
                <i class="fas fa-check"></i>
            </button>
            <input type="hidden" id="ruta" value="{{ url('/') }}">
        </div>
        <br>
        <div class="grid md:grid-cols-4 md:gap-6">
            <div class="flex items-center group">
                <label for="start_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Desde:</label>
                <input type="date" class="ml-4 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-green-500 dark:focus:border-green-500 dx1" id="start_date" name="start_date" value="{{ old('start_date')  }}" required>
            </div>
            <div class="flex items-center group">
                <label for="end_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Hasta:</label>
                <input type="date" class="ml-4 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-green-500 dark:focus:border-green-500" id="end_date" name="end_date" value="{{ old('end_date') }}" required>
            </div>
            <div class="relative z-0 w-full group">
                <div class="flex items-center md:gap-6">
                    <label for="end_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Diagnostico:</label>
                    <select id="dx1" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-green-500 dark:focus:border-green-500 dx1" name="dx1">
                        <option value="dx1">Selecciona el Diagnostico</option>
                        @foreach ($diagnosticos as $diagnostico)
                        <option value="{{$diagnostico}}" {{ old('dx1') == $diagnostico ? 'selected':''}}>{{ $diagnostico }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div>
                <button id="filtrar" class="text-red-700 hover:text-white border border-red-700 hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-4 py-2 text-center mr-2 mb-2 dark:border-green-500 dark:text-green-500 dark:hover:text-white dark:hover:bg-green-600 dark:focus:ring-green-800">Filtrar</i></button>
            </div>
            <input type="hidden" id="tipoFolio" value="{{ $tipo }}">
        </div>
        <br><br>
        <div class="container-table overflow-x-auto relative p-1">
            <table class="w-full" id="federales">
                <thead class="text-xs lg:text-sm text-slate-500 bg-gray-50 font-semibold">
                    <tr>
                        <th></th>
                        <th class="py-2 px-2">Indice</th>
                        <th class="py-2 px-2">Año</th>
                        <th class="py-2 px-2">Folio LESP</th>
                        <th class="py-2 px-2">Dx1</th>
                        <th class="py-2 px-2">Dx2</th>
                        <th class="py-2 px-2">Dx3</th>
                        <th class="py-2 px-2">Dx4</th>
                        <th class="py-2 px-2">Dx5</th>
                        <th class="py-2 px-2">Oficio Entrada</th>
                        <th class="py-2 px-2">Fecha de Recepción</th>
                        <th class="py-2 px-2">Hora de Recepción</th>
                        <th class="py-2 px-2">Jurisdicción/Hospital</th>
                        <th class="py-2 px-2">Nombre del Paciente</th>
                        <th class="py-2 px-2">Tipo de Muestra</th>
                        <th class="py-2 px-2">Fecha de Toma de Muestra</th>
                        <th class="py-2 px-2">Folio Plataforma</th>
                        <th class="py-2 px-2">Personal que Registra la Muestra</th>
                        <th class="py-2 px-2">Estado</th>
                        <th class="py-2 px-2">Muestras Rechazadas</th>
                        <th class="py-2 px-2">Observaciones</th>
                        <th class="py-2 px-2">Aclaraciones REMU</th>
                        <th class="py-2 px-2">Fecha de Importación</th>
                        <th class="py-2 px-2">Usuario Capturista</th>
                        <th class="py-2 px-2 column-two-options">Acciones</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="2" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="3" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="4" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="5" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="6" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="7" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="8" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="9" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="10" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="11" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="12" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="13" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="14" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="15" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="16" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="17" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="18" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="19" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="20" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="21" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="22" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="23" />
                        </th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>

        <div id="tooltipEdit" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
            Editar Registro
            <div class="tooltip-arrow" data-popper-arrow></div>
        </div>
        <div id="tooltipDelete" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
            Eliminar Registro
            <div class="tooltip-arrow" data-popper-arrow></div>
        </div>

        <!-- Modal -->
        <div id="defaultModal" tabindex="-1" data-modal-placement="top-center" aria-hidden="true" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
            <div class="relative p-4 w-full max-w-2xl h-full md:h-auto">
                <!-- Modal content -->
                <div class="relative bg-white rounded-md shadow dark:bg-gray-700">
                    <!-- Modal header -->
                    <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                        <h3 class="text-xl font-semibold text-gray-900 dark:text-white" id="lableImportacion">
                            Importar Archivo
                        </h3>
                        <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="defaultModal">
                            <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            <span class="sr-only">Close modal</span>
                        </button>
                    </div>
                    <!-- Modal body -->
                    <livewire:importacion />
                </div>
            </div>
        </div>

        <div id="datos" tabindex="-1" data-modal-placement="top-center" aria-hidden="true" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
            <div class="relative p-4 w-full max-w-2xl h-full md:h-auto">
                <!-- Modal content -->
                <div class="relative bg-white rounded-md shadow dark:bg-gray-700">
                    <!-- Modal header -->
                    <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                        <h3 class="text-xl font-semibold text-gray-900 dark:text-white" id="labelAction">
                            
                        </h3>
                        <button id="closeModalDatos" type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                            <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            <span class="sr-only">Close modal</span>
                        </button>
                    </div>
                    <!-- Modal body -->
                    <livewire:federal-data />
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <input type="hidden" id="rutaDatos" value="{{ route('dataTableDatos') }}">
@endsection
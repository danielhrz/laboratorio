@extends('layouts.panel')
@section('content')
@include('main_admin.flash-message')
<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <h5 class="text-center mb-2 text-2xl font-semibold text-slate-700 mb-4 flex items-center">
        Lista de Resultados
    </h5>
    <br>
    <div class="container-table  overflow-x-auto relative p-1">
        <table class="w-full" id="queryResults">
            <thead class="text-xs lg:text-sm text-slate-500 bg-gray-50 font-semibold">
                <tr>
                    <th class="py-2 px-2">Indice</th>
                    <th class="py-2 px-2">Nombre del Paciente</th>
                    <th class="py-2 px-2">Folio LVE</th>
                    <th class="py-2 px-2">Folio SINAVE</th>
                    <th class="py-2 px-2">Oficio de Entrada</th>
                    <th class="py-2 px-2">Estado operacional</th>
                    <th class="py-2 px-2 column-two-options">Acciones</th>
                </tr>
                <tr>
                    <th></th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="1" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="2" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="3" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="4" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="5" />
                    </th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<br>
<br>
<input type="hidden" id="rutaQueryResults" value="{{ route('dataTableQuery') }}">
<input type="hidden" id="url" value="{{ url('/') }}">

<div id="tooltipShow" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
    Ver resultado
    <div class="tooltip-arrow" data-popper-arrow></div>
</div>

<div id="viewModalResult" tabindex="-1" data-modal-placement="top-center" aria-hidden="true" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
    <div class="relative p-4 w-full max-w-2xl h-full md:h-auto">
        <!-- Modal content -->
        <div class="relative bg-white rounded-md shadow dark:bg-gray-700">
            <!-- Modal header -->
            <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                <h3 class="text-xl font-semibold text-gray-900 dark:text-white" id="labelAction">
                    Resultado
                </h3>
                <button data-modal="view" type="button" class="close-modal text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="p-6 space-y-6">
                <section>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Oficio Entrada:</label>
                        <input type="text" id="view-oficio-entrada" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Nombre del Paciente:</label>
                        <input type="text" id="view_nombre_paciente" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Folio LVE:</label>
                        <input type="text" id="view_folio_lesp" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Folio SINAVE:</label>
                        <input type="text" id="view_folio_sisver" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Tipo de Muestra:</label>
                        <input type="text" id="view_tipo_muestra" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Fecha de Toma de Muestra:</label>
                        <input type="text" id="view_fecha_toma_muestra" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Jurisdicción/Hospital:</label>
                        <input type="text" id="view_hospital" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Estatus:</label>
                        <p class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" id="view_status_result"></p>
                    </div>
                    <hr>
                    <div id="body-view-result">
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!--  Estatus Concluido -->
<div data-popover id="tooltipCompleted" role="tooltip" class="absolute z-10 invisible inline-block w-64 text-sm font-light text-gray-500 transition-opacity duration-300 bg-white border border-gray-200 rounded-lg shadow-sm opacity-0 dark:text-gray-400 dark:border-gray-600 dark:bg-gray-800">
    <div class="px-3 py-2">
        <p>El análisis de la muestra está completo y usted puede imprimir el resultado para su difusión al paciente</p>
    </div>
    <div data-popper-arrow></div>
</div>
<!--  Estatus influenza -->
<div data-popover id="tooltipInfluenza" role="tooltip" class="absolute z-10 invisible inline-block w-64 text-sm font-light text-gray-500 transition-opacity duration-300 bg-white border border-gray-200 rounded-lg shadow-sm opacity-0 dark:text-gray-400 dark:border-gray-600 dark:bg-gray-800">
    <div class="px-3 py-2">
        <p>El análisis de la muestra no está completo, la muestra es sospechosa a influenza por lo que el laboratorio realizará la confirmación a este virus. El resultado aún no es concluyente</p>
    </div>
    <div data-popper-arrow></div>
</div>
<!--  Estatus reproceso -->
<div data-popover id="tooltipReturn" role="tooltip" class="absolute z-10 invisible inline-block w-64 text-sm font-light text-gray-500 transition-opacity duration-300 bg-white border border-gray-200 rounded-lg shadow-sm opacity-0 dark:text-gray-400 dark:border-gray-600 dark:bg-gray-800">
    <div class="px-3 py-2">
        <p>La muestra no obtuvo un resultado concluyente, el laboratorio se encargará de reprocesarla para descartar una muestra No Adecuada</p>
    </div>
    <div data-popper-arrow></div>
</div>
@endsection
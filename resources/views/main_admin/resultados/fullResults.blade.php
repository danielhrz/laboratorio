@extends('layouts.panel')
@section('content')
@include('main_admin.flash-message')
    <div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
        <h5 class="text-center mb-2 text-2xl font-semibold text-slate-700 mb-4 flex items-center">
            Lista de Resultados
        </h5>
        <br>
        <div class="container-table overflow-x-auto relative p-1">
            <table class="w-full" id="fullResults">
                <thead class="text-xs lg:text-sm text-slate-500 bg-gray-50 font-semibold">
                    <tr>
                        <th class="py-2 px-2">Indice</th>
                        <th class="py-2 px-2">Oficio Entrada</th>
                        <th class="py-2 px-2">Nombre del Paciente</th>
                        <th class="py-2 px-2">Folio LVE</th>
                        <th class="py-2 px-2">Folio SINAVE</th>
                        <th class="py-2 px-2">Tipo de Muestra</th>
                        <th class="py-2 px-2">Fecha de Toma de Muestra</th>
                        <th class="py-2 px-2">Dx1</th>
                        <th class="py-2 px-2">Dx2</th>
                        <th class="py-2 px-2">Dx3</th>
                        <th class="py-2 px-2">Dx4</th>
                        <th class="py-2 px-2">Dx5</th>
                        <th class="py-2 px-2">Kit</th>
                        <th class="py-2 px-2">Técnica Utilizada</th>
                        <th class="py-2 px-2">Valor de Referencia</th>
                        <th class="py-2 px-2">Resultado</th>
                        <th class="py-2 px-2">Iniciales</th>
                        <th class="py-2 px-2">#CRE</th>
                        <th class="py-2 px-2">Fecha de Entrega al AEER</th>
                        <th class="py-2 px-2">Estado de Remu</th>
                        <th class="py-2 px-2">Estado del Resultado</th>
                        <th class="py-2 px-2">#IF de Salida</th>
                        <th class="py-2 px-2">Comentarios Adicionales</th>
                        <th class="py-2 px-2">Interpretación del Resultado</th>
                        <th class="py-2 px-2">Fecha de Asiganación del Resultado</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="1" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="2" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="3" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="4" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="5" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="6" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="7" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="8" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="9" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="10" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="11" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="12" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="13" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="14" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="15" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="16" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="17" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="18" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="19" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="20" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="21" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="22" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="23" />
                        </th>
                        <th>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="24" />
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <br>
    <br>
    <input type="hidden" id="rutaFullResults" value="{{ route('dataTableFull') }}">
@endsection
@extends('layouts.panel')
@section('content')
@include('main_admin.flash-message')
<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">

    <h5 class="text-start mb-2 text-2xl font-semibold text-slate-700">
        Lista de Asignación de Resultados
    </h5>
    <br>
    <div>
        <button id="dropdownRightStartButton" data-dropdown-toggle="dropdownRightStart" data-dropdown-placement="right-start" class="mb-3 md:mb-0 text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-3 py-1.5 text-center inline-flex items-center dark:border-green-500 dark:text-green-500 dark:hover:text-white dark:hover:bg-green-600 dark:focus:ring-green-800" type="button">
            Mostrar / Ocultar columnas <svg class="ml-2 w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
        </button>
        <div id="dropdownRightStart" class="hidden z-10 w-44 bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700">
            <ul id="colums-dataTables-asignaciones" class="py-1 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdownRightStartButton">
                <li>
                    <a class="block py-2 px-4 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white" data-column="10" href="#">Aclaraciones REMU</a>
                </li>
            </ul>
        </div>
    </div>
    <br>
    <div id="container-buttons-status" style="display: none;" data-folios="">
        Cambio de estatus:
        <button class="btn-action-status text-yellow-300 hover:text-white border border-yellow-300 hover:bg-yellow-300 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2" data-toggle="tooltip" data-status="CORRECCIÓN" title="Resultado en corrección">
            <i class="fas fa-exclamation-triangle"></i>
        </button>
        <button class="btn-action-status text-fuchsia-600 hover:text-white border border-fuchsia-600 hover:bg-fuchsia-600 focus:ring-4 focus:outline-none focus:ring-fuchsia-600 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2" data-toggle="tooltip" data-status="PENDIENTE A CONFIRMACIÓN DE INFLUENZA" title="PENDIENTE A CONFIRMACIÓN DE INFLUENZA">
            <i class="fa-solid fa-hourglass-end"></i>
        </button>

        <button class="btn-action-status text-blue-700 hover:text-white border border-blue-700 hover:bg-blue-700 focus:ring-4 focus:outline-none focus:ring-blue-700 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2" data-toggle="tooltip" data-status="REPROCESO" title="REPROCESO">
            <i class="fa-solid fa-arrows-rotate"></i>
        </button>

        <button class="btn-action-status text-green-600 hover:text-white border border-green-600 hover:bg-green-600 focus:ring-4 focus:outline-none focus:ring-green-600 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2" data-toggle="tooltip" data-status="CONCLUIDO" title="Estado Finalizado">
            <i class="fas fa-check"></i>
        </button>

        <button class="btn-action-status btn-difficulties text-red-600 hover:text-white border border-red-600 hover:bg-red-600 focus:ring-4 focus:outline-none focus:ring-red-600 font-medium rounded-lg text-sm px-3 py-1.5 text-center mr-2 mb-2" data-toggle="tooltip" data-status="MUESTRA RETRASADA POR DIFICULTADES TÉCNICAS" title="MUESTRA RETRASADA POR DIFICULTADES TÉCNICAS">
            <i class="fa-solid fa-wrench"></i>
        </button>
    </div>
    <br>
    <input type="hidden" id="role-user" value="{{ Auth::user()->id_role }}">
    <div class="container-table overflow-x-auto relative p-1">
        <table class="w-full" id="asignaciones">
            <thead class="text-xs lg:text-sm text-slate-500 bg-gray-50 font-semibold">
                <tr>
                    @if (Auth::user()->id_role == 1 or Auth::user()->id_role == 6 or Auth::user()->id_role == 9)
                    <th class="py-2 px-2"></th>
                    @endif
                    <th class="py-2 px-2">#</th>
                    <th class="py-2 px-2">Oficio Entrada</th>
                    <th class="py-2 px-2">Nombre del Paciente</th>
                    <th class="py-2 px-2">Folio LVE</th>
                    <th class="py-2 px-2">Folio SINAVE</th>
                    <th class="py-2 px-2">Tipo de Muestra</th>
                    <th class="py-2 px-2">Fecha de Toma de Muestra</th>
                    <th class="py-2 px-2">Dx1</th>
                    <th class="py-2 px-2">Observaciones</th>
                    <th class="py-2 px-2">Aclaraciones REMU</th>
                    <th class="py-2 px-2">Fecha de Recepción REMU</th>
                    <th class="py-2 px-2">Hora de Recepción REMU</th>
                    <th class="py-2 px-2">Identificador de resultado</th>
                    <th class="py-2 px-2">Estado REMU</th>
                    <th class="py-2 px-2">Estado del resultado</th>
                    <th class="py-2 px-2">Estado operacional</th>
                    <th class="py-2 px-2 column-two-options">Acciones</th>
                </tr>
                <tr>
                    @if (Auth::user()->id_role == 1 or Auth::user()->id_role == 6 or Auth::user()->id_role == 9 )
                    <th>
                        <div class="form-check">
                            <input id="check-all" class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 focus:ring-2" type="checkbox">
                        </div>
                    </th>
                    @endif
                    <th></th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="2" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="3" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="4" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="5" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="6" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="7" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="8" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="9" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="10" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="11" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="12" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="13" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="14" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="15" />
                    </th>
                    <th>
                        <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded focus:ring-green-700 focus:border-green-700 block w-full p-2 filter-input" placeholder="Filtrar..." data-column="16" />
                    </th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<br>
<br>
<input type="hidden" id="rutaAsignaciones" value="{{ route('dataTableAsignaciones') }}">
<input type="hidden" id="url" value="{{ url('/') }}">

<div id="modal-result" tabindex="-1" data-modal-placement="top-center" aria-hidden="true" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
    <div class="relative p-4 w-full max-w-2xl h-full md:h-auto">
        <!-- Modal content -->
        <div class="relative bg-white rounded-md shadow dark:bg-gray-700">
            <!-- Modal header -->
            <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                <h3 class="text-xl font-semibold text-gray-900 dark:text-white" id="">
                    Registro de Resultado
                </h3>
                <button type="button" data-modal="create" class="close-modal text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="p-6 bg-white border border-gray-200">
                <div>
                    @include('main_admin.flash-message')
                </div>
                <form id="form-result" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="dato_federal_id" id="dato_federal_id">
                    <div class="grid gap-6 md:grid-cols-2">
                        <div class="mb-1 group">
                            <label for="oficio_entrada" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Oficio de Entrada</label>
                            <input name="oficio_entrada" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="oficio_entrada">
                        </div>
                        <div class="mb-1 group">
                            <label for="nombre_paciente" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Nombre del Paciente</label>
                            <input name="nombre_paciente" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="nombre_paciente">
                        </div>
                        <div class="mb-1 group">
                            <label for="folio_lesp" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Folio LVE</label>
                            <input name="folio_lesp" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="folio_lesp">
                        </div>
                        <div class="mb-1 group">
                            <label for="folio_sisver" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Folio SINAVE</label>
                            <input name="folio_sisver" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="folio_sisver">
                        </div>
                        <div class="mb-1 group">
                            <label for="tipo_muestra" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Tipo de Muestra</label>
                            <input name="tipo_muestra" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="tipo_muestra">
                        </div>
                        <div class="mb-1 group">
                            <label for="fecha_toma_muestra" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Fecha de Toma de Muestra</label>
                            <input name="fecha_toma_muestra" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="fecha_toma_muestra">
                        </div>
                    </div>
                    <div class="my-3" id="body-result-select">
                        <!-- Código para selección de resultado -->
                    </div>
                    <hr class="my-2">
                    <div class="grid gap-6 md:grid-cols-2">

                        <div class="mb-1 group">
                            <label for="cre" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600"># CRE/FORMATO/BITACORA</label>
                            <input name="cre" type="text" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="cre" placeholder="Ingresar CRE">
                            <di id="error_cre" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="date_delivery" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Fecha de entrega al AEER</label>
                            <input name="date_delivery" type="date" id="date_delivery" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                            <di id="error_date_delivery" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="result_identifier" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Identificador de resultado</label>
                            <div class="col-12 col-md-6">
                                <select id="result_identifier" class="select-status text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="result_identifier">
                                    <option value="SIN DEFINIR">SIN DEFINIR</option>
                                    <option value="POSITIVO">POSITIVO</option>
                                    <option value="NEGATIVO">NEGATIVO</option>
                                    <option value="SEGUIMIENTO">SEGUIMIENTO</option>
                                    <option value="INDETERMINADO">INDETERMINADO</option>
                                </select>
                            </div>
                            <di id="error_result_identifier" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="status_result" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Estatus</label>
                            <div class="col-12 col-md-6">
                                <select id="status_result" class="select-status text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="status_result">
                                    <option value="">Elija el estatus del resultado</option>
                                    <option value="FINALIZADO">FINALIZADO</option>
                                    <option value="FINALIZADO/ V. EPIDEMIOLOGICO">FINALIZADO/ V. EPIDEMIOLOGICO</option>
                                    <option value="PENDIENTE">PENDIENTE</option>
                                    <option value="REPROCESO">REPROCESO</option>
                                </select>
                            </div>
                            <di id="error_status_result" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="if_exit" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Oficio de salida</label>
                            <input name="if_exit" type="text" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="if_exit" placeholder="Ingresar el oficio de salida">
                            <di id="error_if_exit" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="coments" class="block mb-3 text-xs sm:text-sm font-medium text-slate-600">Comentarios Adicionales</label>
                            <textarea rows="4" name="coments" id="coments" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 block p-2.5 w-full text-xs sm:text-sm font-medium text-slate-600 rounded-md" placeholder="Ingresar comentarios adicionales"></textarea>
                            <di id="error_coments" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <button type="button" id="btn_search_interpretation_create" class="mb-2 text-blue-700 hover:text-white border border-blue-700 hover:bg-blue-800 focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded text-xs sm:text-sm font-medium px-2.5 py-0.5 text-center inline-flex items-center w-full">
                                <i class="mr-2 fa-solid fa-magnifying-glass"></i>
                                <p>Interpretación del Resultado</p>
                            </button>
                            <textarea rows="4" name="interpretation_result" id="interpretation_result" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 block p-2.5 w-full text-xs sm:text-sm font-medium text-slate-600 rounded-md" placeholder="Ingresar interpretación del resultado"></textarea>
                            <di id="error_interpretation_result" class="hidden"></di>
                        </div>
                        @if ( Auth::user()->id_role == 1 or Auth::user()->id_role == 5 or Auth::user()->id_role == 6)
                        <div class="mb-1 group">
                            <label for="observations" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Observaciones</label>
                            <textarea name="observations" id="observations" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 block p-2.5 w-full text-xs sm:text-sm font-medium text-slate-600 rounded-md" placeholder="Ingresar interpretación del resultado"></textarea>
                            <di id="error_observations" class="hidden"></di>
                        </div>
                        @endif

                    </div>
                    <div class="flex items-center justify-center p-3">
                        <button type="submit" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center">Aceptar</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Modal para editar el resultado -->
<div id="update-modal-result" tabindex="-1" data-modal-placement="top-center" aria-hidden="true" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
    <div class="relative p-4 w-full max-w-2xl h-full md:h-auto">
        <!-- Modal content -->
        <div class="relative bg-white rounded-md shadow dark:bg-gray-700">
            <!-- Modal header -->
            <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                <h3 class="text-xl font-semibold text-gray-900 dark:text-white" id="">
                    Edición de resultado
                </h3>
                <button type="button" data-modal="update" class="close-modal text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="p-6 bg-white border border-gray-200">
                <form id="update-form-result" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="dato_federal_id" id="update_dato_federal_id">
                    <input type="hidden" name="result_id" id="update_result_id">
                    <div class="grid gap-6 md:grid-cols-2">
                        <div class="mb-1 group">
                            <label for="update_oficio_entrada" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Oficio de Entrada</label>
                            <input name="oficio_entrada" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_oficio_entrada">
                        </div>
                        <div class="mb-1 group">
                            <label for="update_nombre_paciente" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Nombre del Paciente</label>
                            <input name="nombre_paciente" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_nombre_paciente">
                        </div>
                        <div class="mb-1 group">
                            <label for="update_folio_lesp" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Folio LVE</label>
                            <input name="folio_lesp" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_folio_lesp">
                        </div>
                        <div class="mb-1 group">
                            <label for="update_folio_sisver" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Folio SINAVE</label>
                            <input name="folio_sisver" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_folio_sisver">
                        </div>
                        <div class="mb-1 group">
                            <label for="update_tipo_muestra" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Tipo de Muestra</label>
                            <input name="tipo_muestra" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_tipo_muestra">
                        </div>
                        <div class="mb-1 group">
                            <label for="update_fecha_toma_muestra" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Fecha de Toma de Muestra</label>
                            <input name="fecha_toma_muestra" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_fecha_toma_muestra">
                        </div>
                    </div>
                    <div class="my-3" id="body-update-result-select">
                        <!-- Código para selección de resultado -->
                    </div>
                    <hr class="my-2">
                    <div class="grid gap-6 md:grid-cols-2">

                        <div class="mb-1 group">
                            <label for="update_cre" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600"># CRE/FORMATO/BITACORA</label>
                            <input name="cre" type="text" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_cre" placeholder="Ingresar CRE">
                            <di id="error_update_cre" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="update_date_delivery" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Fecha de entrega al AEER</label>
                            <input name="date_delivery" type="date" id="update_date_delivery" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                            <di id="error_update_date_delivery" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="update_result_identifier" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Identificador de resultado</label>
                            <div class="col-12 col-md-6">
                                <select id="update_result_identifier" class="select-status text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="result_identifier">
                                    <option value="SIN DEFINIR">SIN DEFINIR</option>
                                    <option value="POSITIVO">POSITIVO</option>
                                    <option value="NEGATIVO">NEGATIVO</option>
                                    <option value="SEGUIMIENTO">SEGUIMIENTO</option>
                                </select>
                            </div>
                            <di id="error_update_result_identifier" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="update_status_result" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Estatus</label>
                            <div class="col-12 col-md-6">
                                <select id="update_status_result" class="select-status text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="status_result">
                                    <option value="">Elija el estatus del resultado</option>
                                    <option value="FINALIZADO">FINALIZADO</option>
                                    <option value="FINALIZADO/ V. EPIDEMIOLOGICO">FINALIZADO/ V. EPIDEMIOLOGICO</option>
                                    <option value="PENDIENTE">PENDIENTE</option>
                                    <option value="REPROCESO">REPROCESO</option>
                                </select>
                            </div>
                            <di id="error_update_status_result" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="update_if_exit" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Oficio de salida</label>
                            <input name="if_exit" type="text" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_if_exit" placeholder="Ingresar el oficio de salida">
                            <di id="error_update_if_exit" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <label for="update_coments" class="block mb-3 text-xs sm:text-sm font-medium text-slate-600">Comentarios Adicionales</label>
                            <textarea rows="4" name="coments" id="update_coments" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 block p-2.5 w-full text-xs sm:text-sm font-medium text-slate-600 rounded-md" placeholder="Ingresar comentarios adicionales"></textarea>
                            <di id="error_update_coments" class="hidden"></di>
                        </div>
                        <div class="mb-1 group">
                            <button type="button" id="btn_search_interpretation_edit" class="mb-2 text-blue-700 hover:text-white border border-blue-700 hover:bg-blue-800 focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded text-xs sm:text-sm font-medium px-2.5 py-0.5 text-center inline-flex items-center w-full">
                                <i class="mr-2 fa-solid fa-magnifying-glass"></i>
                                <p>Interpretación del Resultado</p>
                            </button>
                            <textarea rows="4" name="interpretation_result" id="update_interpretation_result" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 block p-2.5 w-full text-xs sm:text-sm font-medium text-slate-600 rounded-md" placeholder="Ingresar interpretación del resultado"></textarea>
                            <di id="error_update_interpretation_result" class="hidden"></di>
                        </div>
                        @if ( Auth::user()->id_role == 1 or Auth::user()->id_role == 5 or Auth::user()->id_role == 6)
                        <div class="mb-1 group">
                            <label for="update_observations" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Observaciones</label>
                            <textarea name="observations" id="update_observations" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 block p-2.5 w-full text-xs sm:text-sm font-medium text-slate-600 rounded-md" placeholder="Ingresar interpretación del resultado"></textarea>
                            <di id="error_update_observations" class="hidden"></di>
                        </div>
                        @endif
                    </div>
                    <div class="flex items-center justify-center p-3">
                        <button type="submit" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center">Aceptar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="tooltipEdit" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
    Editar resultado
    <div class="tooltip-arrow" data-popper-arrow></div>
</div>

<div id="tooltipCreate" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
    Agregar un resultado
    <div class="tooltip-arrow" data-popper-arrow></div>
</div>

<div id="tooltipShow" role="tooltip" class="inline-block absolute invisible z-10 py-1.5 px-3 text-xs md:text-sm font-medium text-white bg-gray-900 rounded-md shadow-sm opacity-0 transition-opacity duration-300 tooltip">
    Ver resultado
    <div class="tooltip-arrow" data-popper-arrow></div>
</div>

<div id="viewModalResult" tabindex="-1" data-modal-placement="top-center" aria-hidden="true" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
    <div class="relative p-4 w-full max-w-2xl h-full md:h-auto">
        <!-- Modal content -->
        <div class="relative bg-white rounded-md shadow dark:bg-gray-700">
            <!-- Modal header -->
            <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                <h3 class="text-xl font-semibold text-gray-900 dark:text-white" id="labelAction">
                    Resultado
                </h3>
                <button data-modal="view" type="button" class="close-modal text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="p-6 space-y-6">
                <section>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Oficio Entrada:</label>
                        <input type="text" id="view-oficio-entrada" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Nombre del Paciente:</label>
                        <input type="text" id="view_nombre_paciente" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Folio LVE:</label>
                        <input type="text" id="view_folio_lesp" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Folio SINAVE:</label>
                        <input type="text" id="view_folio_sisver" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Tipo de Muestra:</label>
                        <input type="text" id="view_tipo_muestra" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Fecha de Toma de Muestra:</label>
                        <input type="text" id="view_fecha_toma_muestra" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Jurisdicción/Hospital:</label>
                        <input type="text" id="view_hospital" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <hr>
                    <div id="body-view-result">
                    </div>
                    <hr>
                    <fieldset class="border mt-4 mb-4">
                        <legend align="center" class="font-normal text-slate-700 px-2">Histórico de movimientos</legend>
                        <div id="view_history" class="max-h-36 overflow-y-auto"></div>
                    </fieldset>


                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Verificador de Resultado:</label>
                        <input type="text" id="view_verificador" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Fecha de estado operacional:</label>
                        <input type="text" id="view_date_complete_operational_status" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full"># CRE/Formato/Bitacora:</label>
                        <input type="text" id="view_cre" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Fecha de entrega al AEER:</label>
                        <input type="text" id="view_date_delivery" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Estatus:</label>
                        <p class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" id="view_status_result"></p>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full"># IF de Salida:</label>
                        <input type="text" id="view_if_exit" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Comentarios Adicionales:</label>
                        <textarea rows="1" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" id="view_coments" disabled></textarea>
                    </div>
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Interpretación del Resultado:</label>
                        <textarea rows="1" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" id="view_interpretation_result" disabled></textarea>
                    </div>
                    @if ( Auth::user()->id_role == 1 or Auth::user()->id_role == 5 or Auth::user()->id_role == 6)
                    <div class="flex justify-center items-center mb-4">
                        <label class="font-normal text-slate-700 text-right w-full">Observaciones:</label>
                        <textarea rows="1" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" id="view_observations" disabled></textarea>
                    </div>
                    @endif
                </section>
            </div>
        </div>
    </div>
</div>
@endsection
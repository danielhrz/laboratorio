@php
$diagnostic = \App\Diagnosis::where('full_name_analysis', $dx1)->first();
$action = 0;

if($diagnostic->time != 0 && ($status != "SIN RESULTADO" )) {
    $receptionTime = Carbon\Carbon::parse($fecha_recepcion . ' ' . $hora_recepcion);
    $currentTime = Carbon\Carbon::now();
    $time = $diagnostic->time / 3;
    $section = $currentTime->diffInHours($receptionTime);

    if (($operational_status != "CONCLUIDO" && $operational_status != "PENDIENTE A CONFIRMACIÓN DE INFLUENZA" ) && ( ($time * 2) < $section )) {
        $action = 5;
    }
}
@endphp

@if ( ( ($status_result !='SIN RESULTADO' && $operational_status !='CONCLUIDO' ) || $action == 5) && $status != 'RECHAZO') 
<div class="form-check">
    <input data-action="{{$action}}" data-status="{{$status}}" class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 focus:ring-2 change-status-massive" name="status-change[]" type="checkbox" value="{{ $id }}">
</div>

@endif
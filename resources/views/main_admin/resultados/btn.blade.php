<div class="flex items-center justify-center">
    @if ($status != 'RECHAZO')

    @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 5 or Auth::user()->id_role == 6 or Auth::user()->id_role == 7 or Auth::user()->id_role == 8 or Auth::user()->id_role == 9 or Auth::user()->id_role == 10)
    @if(Auth::user()->id_role != 9)
    @if ($status_result == 'SIN RESULTADO' && Request::is('dataTableAsignaciones'))
    <button data-muestra="{{ $id }}" class="show-result text-gray-700 hover:text-white border border-gray-700 hover:bg-gray-700 focus:ring-2 focus:outline-none focus:ring-gray-300 font-medium rounded-md text-sm p-2.5 text-center inline-flex mr-2 items-center">
        <i class="fa-regular fa-file"></i>
    </button>
    @else
    @if(!Request::is('dataTableQuery') )
    <!-- ( isset($operational_status) && $operational_status != 'CONCLUIDO') -->
    <button data-muestra="{{ $id }}" class="edit-result text-blue-700 hover:text-white border border-blue-700 hover:bg-blue-700 focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded-md text-sm p-2.5 text-center inline-flex mr-2 items-center">
        <i class="fa-solid fa-pen-to-square"></i>
    </button>
    @endif
    @endif
    @endif

    @if($status_result != 'SIN RESULTADO')
    <button data-muestra="{{ $id }}" class="view-result text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 focus:outline-none focus:ring-green-300 font-medium rounded-md text-xs md:text-sm p-1.5 md:p-2.5 text-center flex items-center justify-center">
        <i class="fa-sharp fa-solid fa-eye"></i>
    </button>
    @endif
    @endif

    @if( Request::is('dataTableQuery') && ( isset($operational_status) && $operational_status == 'CONCLUIDO') && $hospital != null)
    <a href="export/jurisdiction/{{$id}}" target="_blank" class="button btn-option-export text-fuchsia-700 border border-fuchsia-700 hover:bg-fuchsia-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-fuchsia-300 font-medium rounded-md text-xs md:text-sm p-1.5 md:p-2.5 text-center ml-2 flex items-center justify-center">
        <i class="fa-solid fa-print"></i>
    </a>
    @endif

    @endif


</div>
@if ($message = Session::get('success'))
<div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800" role="alert">
	<span class="font-medium">{{ $message }}</span>
</div>
@endif


@if ($message = Session::get('danger'))
<div id="alert-2" class="flex p-4 mb-4 bg-red-100 rounded-lg dark:bg-red-200" role="alert">
	<svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-red-700 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
		<path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
	</svg>
	<span class="sr-only">Info</span>
	<div class="ml-3 text-sm font-medium text-red-700 dark:text-red-800">
		<span class="font-medium">{{ $message }}</span>
	</div>
</div>
@endif


@if (session()->has('warning'))
<div id="alert-2" class="flex p-4 mb-4 bg-yellow-100 rounded-lg dark:bg-yellow-200" role="alert">
	<svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-yellow-700 dark:text-yellow-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
		<path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
	</svg>
	<span class="sr-only">Info</span>
	<div class="ml-3 text-sm font-medium text-yellow-700 dark:text-yellow-800">
		{{ session('warning') }}
	</div>
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('secondary'))
<div class="alert alert-secondary">
	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
	<strong>{{ $message }}</strong>
</div>
@endif

@if (Request::is('livewire/message/exports-data') && $errors->any() )
<div id="alert-2" class="flex p-4 mb-4 bg-red-100 rounded-lg dark:bg-red-200" role="alert">
	<svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-red-700 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
		<path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
	</svg>
	<span class="sr-only">Info</span>
	<div class="ml-3 text-sm font-medium text-red-700 dark:text-red-800">
		<span class="font-medium">Por favor rellene al menos un filtro y que sean valores validos.</span>
	</div>
</div>

@elseif (Request::is('livewire/message/rechazados') && $errors->any() )
<div id="alert-2" class="flex p-4 mb-4 bg-red-100 rounded-lg dark:bg-red-200" role="alert">
	<svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-red-700 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
		<path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
	</svg>
	<span class="sr-only">Info</span>
	<div class="ml-3 text-sm font-medium text-red-700 dark:text-red-800">
		<span class="font-medium">Por favor ingrese los datos marcados en rojo para generar el reporte.</span>
	</div>
</div>

@elseif ((Request::is('livewire/message/federal-data') || Request::is('livewire/message/results.format-five') || Request::is('livewire/message/results.format-thirty-seven') || Request::is('livewire/message/reports.format-five-four') || Request::is('livewire/message/reports.rejected-and-finalized')) && $errors->any() )
<div id="alert-2" class="flex p-4 mb-4 bg-red-100 rounded-lg dark:bg-red-200" role="alert">
	<svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-red-700 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
		<path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
	</svg>
	<span class="sr-only">Info</span>
	<div class="ml-3 text-sm font-medium text-red-700 dark:text-red-800">
		<span class="font-medium">Por favor ingrese los datos marcados en rojo.</span>
	</div>
</div>

@elseif (Request::is('livewire/message/importacion') && $errors->any() )
<div class="px-10 py-2 flex justify-center items-center">
	<div id="alert-2" class="flex p-4 mb-4 bg-red-100 rounded-lg dark:bg-red-200" role="alert">
		<svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-red-700 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
			<path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
		</svg>
		<span class="sr-only">Info</span>
		<div class="ml-3 text-sm font-medium text-red-700 dark:text-red-800">
			<span class="font-medium">Por favor, valida los siguientes errores:</span>
			<ul class="mt-1.5 ml-4 text-red-700 list-disc list-inside">
				@foreach ($errors->all() as $mensaje)
				<li>
					{{$mensaje}}
				</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>

@elseif ($errors->any())
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
	Por favor revise el archivo de importación para ver si hay errores.
</div>
@endif
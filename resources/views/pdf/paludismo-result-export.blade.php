<table>
    <tbody>
        <tr></tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="9">LABORATORIO DE VIGILANCIA EPIDEMIOLOGICA DE LA CIUDAD DE MÉXICO</td><!-- E -->
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="9">BASE DE DATOS "MUESTRAS INGRESADAS PARA DIAGNÓSTICO DE PALUDISMO" RTE-VE-034</td><!-- E -->
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="9">MUESTRAS PARA DIAGNÓSTICO</td><!-- E -->
        </tr>
        <tr></tr>
        <tr></tr>

        <tr>
            <td rowspan="3">NÚMERO </td>
            <td rowspan="3">FOLIO LESP</td>
            <td rowspan="3">FOLIO PLATAFORMA SINAVE</td>
            <td rowspan="3">TIPO DE DIAGNÓSTICO</td>
            <td rowspan="3">CLAVE LAMINILLA</td>
            <td colspan="2">RECEPCIÓN DE REMU</td>
            <td colspan="2">RECEPCIÓN EN EL ÁREA DE PALUDISMO</td>
            <td rowspan="3">TIPO DE MUESTRA</td>
            <td rowspan="3">SEMANA EPIDEM.</td>
            <td rowspan="3">NOMBRE DEL PACIENTE</td>
            <td colspan="2">TOMA DE MUESTRA</td>
            <td rowspan="3">JURISDICCIÓN</td>
            <td colspan="4">RESULTADO DE LA LECTURA</td>
            <td colspan="2">ENTREGA DE RESULTADOS AL AEER</td>
            <td colspan="2">REVISIÓN Y LIBERACIÓN DE RESULTADOS</td>
            <td rowspan="3">TIEMPO TOTAL DEL PROCESO</td>
            <td rowspan="3">CONTROL DE CALIDAD InDRE</td>
            <td rowspan="3">RESULTADO CONTROL DE CALIDAD InDRE</td>
            <td rowspan="3">BANCO DE MUESTRAS</td>
            <td rowspan="3">Pruebas de Diagnóstico Rápido (PDR)</td>
            <td rowspan="3">OBSERVACIONES</td>
        </tr>

        <tr>
            <td rowspan="2">HORA</td>
            <td rowspan="2">FECHA</td>

            <td rowspan="2">HORA</td>
            <td rowspan="2">FECHA</td>

            <td rowspan="2">HORA</td>
            <td rowspan="2">FECHA</td>

            <td rowspan="2">RESULTADO</td>
            <td rowspan="2">ESPECIE</td>
            <td colspan="2">DENSIDAD PARASITARIA</td>

            <td rowspan="2">HORA</td>
            <td rowspan="2">FECHA</td>
            <td rowspan="2">HORA</td>
            <td rowspan="2">FECHA</td>
        </tr>

        <tr>
            <td>EAS</td>
            <td>ESS</td>
        </tr>

        @foreach ($samples as $row)

        <tr>
            <td>{{($loop->index + 1)}}</td>
            <td>{{$row->folio_lesp}}</td>
            <td>{{$row->folio_sisver}}</td>
            <td>{{ $row->diagnostic_type }}</td>
            <td>{{ $row->key_lamella }}</td>
            <td>{{ \Carbon\Carbon::parse($row->hora_recepcion)->isoFormat('hh:mm A') }}</td>
            <td>{{ \Carbon\Carbon::parse($row->fecha_recepcion)->isoFormat('DD/MM/YYYY') }}</td>
            <td>{{ ($row->reception) ? \Carbon\Carbon::parse($row->reception)->isoFormat('hh:mm A') : '' }}</td>
            <td>{{ ($row->reception) ? \Carbon\Carbon::parse($row->reception)->isoFormat('DD/MM/YYYY') : '' }}</td>
            <td>{{ $row->tipo_muestra }}</td>
            <td>{{ $row->epidemiological_week }}</td>
            <td>{{ $row->nombre_paciente }}</td>
            <td>{{ ($row->fecha_toma_muestra) ? \Carbon\Carbon::parse($row->fecha_toma_muestra)->isoFormat('hh:mm A') : '' }}</td>
            <td>{{ ($row->sample_collection_time) ? \Carbon\Carbon::parse($row->sample_collection_time)->isoFormat('hh:mm A') : '' }}</td>
            <td>{{ $row->hospital }}</td>
            <td>{{ $row->reading_result }}</td>
            <td>{{ $row->species }}</td>
            <td>{{ $row->eas }}</td>
            <td>{{ $row->ess }}</td>
            <td>{{ ($row->date_delivery_results_aeer) ? \Carbon\Carbon::parse($row->date_delivery_results_aeer)->isoFormat('hh:mm A') : '' }}</td>
            <td>{{ ($row->date_delivery_results_aeer) ? \Carbon\Carbon::parse($row->date_delivery_results_aeer)->isoFormat('DD/MM/YYYY') : '' }}</td>
            <td>{{ ($row->date_complete_operational_status) ? \Carbon\Carbon::parse($row->date_complete_operational_status)->isoFormat('hh:mm A') : '' }}</td>
            <td>{{ ($row->date_complete_operational_status) ? \Carbon\Carbon::parse($row->date_complete_operational_status)->isoFormat('DD/MM/YYYY') : '' }}</td>
            <td>{{ $row->total_time }}</td>
            <td>{{ $row->quality_control }}</td>
            <td>{{ $row->quality_control_result }}</td>
            <td>{{ $row->sample_bank }}</td>
            <td>{{ $row->diagnostic_tests }}</td>
            <td>{{ $row->observations }}</td>

        </tr>

        @endforeach

    </tbody>
</table>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

<head>
    <meta charset="UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ (config('session.lifetime') * 60) + 3 }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Jorge Daniel Hernández Sánchez">
    <title>{{ config('app.name') }}</title>

    <link href="{{asset('panel/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{ asset('panel/img/favicon.png') }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('panel/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('panel/css/select2.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    @livewireStyles
</head>

<body class="overflow-x-hidden">
    @include('layouts.menu')

    <main class="wrapper-main-container min-h-screen bg-slate-100 relative">
        <div class="flex justify-between bg-white border-l border-b border-slate-200 h-16 sticky top-0 z-40">

            <section class="px-4 py-3 icon-menu">
                <a href="#" class="pr-4 action-menu text-base text-slate-500"><i class="fas fa-bars"></i></a>

                <a href="{{ url('main_admin') }}" class="hidden md:inline-flex justify-center items-center px-2 py-0.5 ml-3 text-xs sm:text-sm font-normal text-white bg-green-800 rounded-full">{{ config('app.name') }}</a>
                @if (isset($rutaSection) && isset($section))
                &nbsp;
                <a href="{{ url($rutaSection) }}" class="inline-flex justify-center items-center px-3 py-0.5 ml-3 text-xs sm:text-sm font-normal text-white bg-blue-600 rounded-full">{{ $section }}</a>
                @endif
                @if (isset($rutaSubSection) && isset($subSection))
                &nbsp;
                <a href="{{ url($rutaSubSection) }}" class="inline-flex justify-center items-center px-3 py-0.5 ml-3 text-xs sm:text-sm font-normal text-slate-600 bg-yellow-200 rounded-full">{{ $subSection }}</a>
                @endif
            </section>

            <section class="relative flex items-center order-last px-4 py-4  border-l border-slate-200">
                <button data-dropdown-toggle="dropdownNavbar" class="flex items-center w-full order-last space-x-4 hover:bg-transparent md:w-auto text-xs sm:text-sm md:text-base text-slate-600 space-x-4 min-w-2">
                    <img class="w-5 h-5 ring-1 md:w-7 md:h-7 p-1 md:right-2 rounded-full ring-gray-500" src="{{ asset('panel/img/profile.png') }}" alt="Bordered avatar">
                    <p class="text-xs sm:text-sm">{{ Auth::user()->name }}</p>
                    <i class="fa-solid fa-angle-down"></i>
                </button>
                <!-- Dropdown menu -->
                <div id="dropdownNavbar" class="z-10 hidden bg-white divide-y divide-gray-100 rounded shadow w-full">
                    <ul class="py-1" aria-labelledby="dropdownLargeButton">
                        <li>
                            <a class="block px-4 py-2 text-xs sm:text-sm hover:bg-gray-100 text-red-700" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i> {{ __('Cerrar Sesión') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </section>
        </div>

        <section class="main-container-content p-8">
            @yield('content')
        </section>
        <br>
        <br>
        <footer class="absolute bottom-0 w-full flex flex-row-reverse bg-white border-slate-200 px-10 py-4 border-l border-t border-slate-200">
            <span class="text-xs sm:text-sm md:text-base text-slate-500">Servicios de Salud Pública CDMX Copyright © {{ date('Y') }}</span>
        </footer>
    </main>

    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js" defer></script>

    <script src="{{ asset('js/sidebar.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('panel/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    @livewireScripts

    <script>
        $('#exampleModal').on('hide.bs.modal', function(event) {
            Livewire.emit('resetFiles')
        })
    </script>
    <script type="text/javascript">
        function validarExt() {
            var fileInput = document.getElementById('fileInput');
            var archivoRuta = fileInput.value;
            var extPermitidas = /(.csv)$/i;
            if (!extPermitidas.exec(archivoRuta)) {
                Livewire.emit('invalidFile');
                fileInput.value = '';
                return false;
            }
        }
    </script>
    <script>
        Livewire.on('refreshTable', postId => {
            t.ajax.reload(null, false);
        })
    </script>
    <script type="text/javascript">
        function limpiar() {
            document.getElementById('fileInput').value = '';
        }
    </script>
    <script>
        $(document).ready(function() {
            var modal;
            $("#showModal").click(function() {
                Livewire.emit('createModal');
                let targetEl = document.getElementById('datos');
                let options = {
                    placement: 'top-center',
                    backdropClasses: 'bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40',
                    onHide: () => {
                        // console.log('modal is hidden');
                    },
                    onShow: () => {
                        // console.log('modal is shown');
                    },
                    onToggle: () => {
                        // console.log('modal has been toggled');
                    }
                };
                modal = new Modal(targetEl, options);
                modal.show();
            })

            $('#closeModalDatos').click(function() {
                Livewire.emit('resetModals')
                modal.hide();
            })

            $(".container-table").on('click', '.show-modal-edit', function() {
                Livewire.emit('searchMuestra', $(this).data('muestra'));
                let targetEl = document.getElementById('datos');
                let options = {
                    placement: 'top-center',
                    backdropClasses: 'bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40',
                    onHide: () => {
                        // console.log('modal is hidden');
                    },
                    onShow: () => {
                        // console.log('modal is shown');
                    },
                    onToggle: () => {
                        // console.log('modal has been toggled');
                    }
                };
                modal = new Modal(targetEl, options);
                modal.show();
            })
        });
    </script>

    @if( Request::is('usuarios') )
    <script src="{{ asset('panel/js/jquery-users.js') }}"></script>
    @endif

    @if( Request::is('datos/all') || Request::is('datos/p') || Request::is('datos/cci') || Request::is('datos/lve') )
    <script src="{{ asset('panel/js/jquery-datos.js') }}"></script>
    @endif

    @if( Request::is('allocation') )
    <script src="{{ asset('panel/js/jquery-allocation.js') }}"></script>
    @endif

    @if( Request::is('log') )
    <script src="{{ asset('panel/js/jquery-log.js') }}"></script>
    @endif

    @if( Request::is('destinatarios') )
    <script src="{{ asset('panel/js/jquery-destinatarios.js') }}"></script>
    @endif

    @if( Request::is('full') )
    <script src="{{ asset('panel/js/jquery-full.js') }}"></script>
    @endif

    @if( Request::is('query') )
    <script src="{{ asset('panel/js/jquery-query.js') }}"></script>
    @endif

    @if ((Request::is('main_admin') || Request::is('grafica/ingresadas') || Request::is('grafica/rechazadas') || Request::is('grafica/motivos') || Request::is('grafica/jurisdicciones') || Request::is('grafica/hospitales') || Request::is('grafica/muestras') || Request::is('grafica/recepcionadas') ) && (Auth::user()->id_role != 8) )
    <script src="{{asset('panel/js/datepicker-full.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="{{ asset('panel/js/jquery-main.js') }}"></script>
    @endif

    @if( Request::is('descarga') )
    <script src="{{asset('panel/js/datepicker-full.js')}}"></script>
    <script type="module" src="{{ asset('panel/js/jquery-reports.js') }}"></script>
    @endif

    @if( Request::is('diagnostico/respiratorios') )
    <script src="{{asset('panel/js/datepicker-full.js')}}"></script>
    <!--  <script type="module" src="{{ asset('panel/js/jquery-reports.js') }}"></script> -->
    @endif

    @if( Request::is('paludismo') )
    <script src="{{ asset('js/jquery.ba-throttle-debounce.js') }}"></script>
    <link rel="stylesheet" href="{{asset('css/jquery.datetimepicker.min.css')}}">
    <script type="module" src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>
    <script type="module" src="{{ asset('js/jquery-time.js') }}"></script>
    @endif

    @stack('scripts')
</body>

</html>
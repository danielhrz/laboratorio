<aside class="container-sidebar w-0 h-0 sticky top-0 z-40">
    <nav class="sidebar-navigation h-screen overflow-y-auto bg-white shadow-md">
        <a href="#" class="flex items-center py-2 px-3 border-b border-slate-200 h-16">
            <img src="{{ asset('panel/img/logo.png') }}" class="w-full mr-3" alt="Flowbite Logo" />
        </a>
        <ul class="">
            <li class="border-b border-slate-200">
                <a href="{{ url('main_admin') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fas fa-home"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Inicio</span>
                </a>
            </li>
            @if(Auth::user()->id_role == 1)
            <li class="border-b border-slate-200">
                <a href="{{ url('usuarios') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fas fa-users"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Usuarios</span>
                </a>
            </li>
            @endif

            @if ( Auth::user()->id_role == 1 or Auth::user()->id_role == 3 or Auth::user()->id_role == 6 or Auth::user()->id_role == 7 or Auth::user()->id == 18 or Auth::user()->id_role == 9 or Auth::user()->id_role == 11)
            <li class="border-b border-slate-200 down-aside-menu cursor-pointer">
                <a class="transition ease-in-out duration-700 border-l-4 @if(Request::is('unidades') || Request::is('unidades/tipos')) border-green-800 bg-gray-100 @else border-white @endif flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-chart-pie"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Gráficas</span>
                    <i class="fa-solid fa-chevron-left transition ease-in-out duration-700"></i>
                </a>
                <ul class="hidden">
                    <li>
                        <a href="{{ url('grafica/ingresadas') }}" class="transition ease-in-out duration-700 @if(Request::is('grafica/ingresadas') ) text-blue-800 @else text-slate-600 @endif flex items-center pl-11 py-4 pr-5 text-xs font-normal hover:bg-gray-100 hover:text-green-600">
                            <i class="fa-solid fa-chart-column"></i>
                            <span class="flex-1 ml-3 whitespace-nowrap">Muestras ingresadas</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('grafica/rechazadas') }}" class="transition ease-in-out duration-700 @if(Request::is('grafica/rechazadas') ) text-blue-800 @else text-slate-600 @endif flex items-center pl-11 py-4 pr-5 text-xs font-normal hover:bg-gray-100 hover:text-green-600">
                            <i class="fa-solid fa-chart-column"></i>
                            <span class="flex-1 ml-3 whitespace-nowrap">Muestras rechazadas</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('grafica/motivos') }}" class="transition ease-in-out duration-700 @if(Request::is('grafica/motivos') ) text-blue-800 @else text-slate-600 @endif flex items-center pl-11 py-4 pr-5 text-xs font-normal hover:bg-gray-100 hover:text-green-600">
                            <i class="fa-solid fa-chart-column"></i>
                            <span class="flex-1 ml-3 whitespace-nowrap">Motivos de rechazo</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('grafica/jurisdicciones') }}" class="transition ease-in-out duration-700 @if(Request::is('grafica/jurisdicciones') ) text-blue-800 @else text-slate-600 @endif flex items-center pl-11 py-4 pr-5 text-xs font-normal hover:bg-gray-100 hover:text-green-600">
                            <i class="fa-solid fa-chart-column"></i>
                            <span class="flex-1 ml-3 whitespace-nowrap">Rechazos por jurisdicción</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('grafica/hospitales') }}" class="transition ease-in-out duration-700 @if(Request::is('grafica/hospitales') ) text-blue-800 @else text-slate-600 @endif flex items-center pl-11 py-4 pr-5 text-xs font-normal hover:bg-gray-100 hover:text-green-600">
                            <i class="fa-solid fa-chart-column"></i>
                            <span class="flex-1 ml-3 whitespace-nowrap">Rechazos por hospital</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('grafica/muestras') }}" class="transition ease-in-out duration-700 @if(Request::is('grafica/muestras') ) text-blue-800 @else text-slate-600 @endif flex items-center pl-11 py-4 pr-5 text-xs font-normal hover:bg-gray-100 hover:text-green-600">
                            <i class="fa-solid fa-chart-column"></i>
                            <span class="flex-1 ml-3 whitespace-nowrap">Rechazo por tipo de muestra</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('grafica/recepcionadas') }}" class="transition ease-in-out duration-700 @if(Request::is('grafica/recepcionadas') ) text-blue-800 @else text-slate-600 @endif flex items-center pl-11 py-4 pr-5 text-xs font-normal  hover:bg-gray-100 hover:text-green-600">
                            <i class="fa-solid fa-chart-column"></i>
                            <p class="ml-3">Muestras recepcionadas</p>
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            @if (Auth::user()->id_role == 1 or Auth::user()->id_role == 6 or Auth::user()->id_role == 9 or Auth::user()->id_role == 11)
            <li class="border-b border-slate-200 down-aside-menu cursor-pointer">
                <a class="transition ease-in-out duration-700 border-l-4 @if(Request::is('unidades') || Request::is('unidades/tipos')) border-green-800 bg-gray-100 @else border-white @endif flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-chart-simple"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Estadísticos resultados</span>
                    <i class="fa-solid fa-chevron-left transition ease-in-out duration-700"></i>
                </a>
                <ul class="hidden">
                    <li>
                        <a href="{{ url('diagnostico/respiratorios') }}" class="transition ease-in-out duration-700 @if(Request::is('grafica/ingresadas') ) text-blue-800 @else text-slate-600 @endif flex items-center pl-11 py-4 pr-5 text-xs font-normal hover:bg-gray-100 hover:text-green-600">
                            <i class="fa-solid fa-lungs-virus"></i>
                            <span class="flex-1 ml-3 whitespace-nowrap">Virus respiratorios</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 2 or Auth::user()->id_role == 3 or Auth::user()->id_role == 6 or Auth::user()->id_role == 7 or Auth::user()->id_role == 9 or Auth::user()->id == 8 or Auth::user()->id_role == 11)
            <li class="border-b border-slate-200">
                <a href="{{ url('datos/all') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-table-list"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap angle">Diagnósticos</span>
                </a>
            </li>
            <li class="border-b border-slate-200">
                <a href="{{ url('datos/p') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-table-list"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap angle">Paneles</span>
                </a>
            </li>
            <li class="border-b border-slate-200">
                <a href="{{ url('datos/cci') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-table-list"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap angle">Control de Calidad</span>
                </a>
            </li>
            <li class="border-b border-slate-200">
                <a href="{{ url('datos/lve') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-table-list"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Referenciadas</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->id_role != 8 /* && Auth::user()->id_role != 9 */ && Auth::user()->id_role != 10)
            <li class="border-b border-slate-200">
                <a href="{{ url('reportes') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fas fa-file-excel"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Reportes</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 3 or Auth::user()->id_role == 6 or (Auth::user()->id_role == 7 && Auth::user()->id != 17) or Auth::user()->id_role == 11)
            <li class="border-b border-slate-200">
                <a href="{{ url('log') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-timeline"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Historial de Modificaciones</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 5 or Auth::user()->id_role == 6 or Auth::user()->id_role == 7 or Auth::user()->id_role == 9 or Auth::user()->id_role == 11)
            <li class="border-b border-slate-200">
                <a href="{{ url('allocation') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa fa-clipboard" aria-hidden="true"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Asignación de Resultados</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->id_role == 1 || (auth()->user()->id == 40 || auth()->user()->id == 43 || auth()->user()->id == 46 || auth()->user()->id == 44 || auth()->user()->id == 18 || Auth::user()->id_role == 11) )
            <li class="border-b border-slate-200">
                <a href="{{ route('paludismo.index') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-table-cells-large"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Paludismo</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 5 or Auth::user()->id_role == 6 or Auth::user()->id_role == 7 or Auth::user()->id_role == 11)
            <li class="border-b border-slate-200">
                <a href="{{ url('results') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-download"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Reporte de Resultados</span>
                </a>
            </li>
            <li class="border-b border-slate-200">
                <a href="{{ url('destinatarios') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-arrows-turn-to-dots"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Destinatarios</span>
                </a>
            </li>
            <li class="border-b border-slate-200">
                <a href="{{ url('full') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-table-list"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Lista de Resultados</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->id_role == 1 or Auth::user()->id_role == 5 or Auth::user()->id_role == 6 or Auth::user()->id_role == 8 or Auth::user()->id_role == 10 or Auth::user()->id == 8 or Auth::user()->id == 16 or Auth::user()->id_role == 11)
            <li class="border-b border-slate-200">
                <a href="{{ url('query') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa fa-clipboard" aria-hidden="true"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Consulta de Resultados</span>
                </a>
            </li>
            @endif

            @if (Auth::user()->id_role == 1 or Auth::user()->id_role == 2 or Auth::user()->id_role == 3 or Auth::user()->id_role == 6 or Auth::user()->id_role == 5 or Auth::user()->id_role == 7 or Auth::user()->id == 18 or Auth::user()->id_role == 11)
            <li class="border-b border-slate-200">
                <a href="{{ url('descarga') }}" class="flex items-center py-4 px-5 text-xs font-normal text-slate-600 hover:border-l-4 hover:border-green-800 hover:bg-gray-100">
                    <i class="fa-solid fa-cloud-arrow-down"></i>
                    <span class="flex-1 ml-3 whitespace-nowrap">Reporte General</span>
                </a>
            </li>
            @endif
        </ul>
    </nav>
</aside>
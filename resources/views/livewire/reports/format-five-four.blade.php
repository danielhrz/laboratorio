<div>
    <form method="post" wire:submit.prevent="printDocument" class="mx-4 relative">
        <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="printDocument" wire:loading.class.remove="hidden">
            <p class="w-full text-center font-semibold text-slate-700 text-xs md:text-sm">Generando archivo...</p>
            <span class="loader-download"></span>
        </div>

        <div class="container mx-auto px-4">
            @include('main_admin.flash-message')
        </div>

        <div class="container mx-auto px-4">
            <div class="grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <x-label for="oficio_doc">Oficio del archivo:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="text" class="text-uppercase border {{ $errors->has('oficio_doc') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="oficio_doc" wire:model="oficio_doc" placeholder="VE /OSRef026/2023...">
                    </div>
                </div>

                <div class="mb-6 group">
                    <x-label for="date_send">Fecha de envió:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="date" class="text-uppercase border {{ $errors->has('date_send') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="date_send" wire:model="date_send">
                    </div>
                </div>
            </div>

            <div class="grid xl:grid-cols-1 xl:gap-6">
                <div class="mb-6 group">
                    <div class="form-group col-12 col-md-10">
                        <x-label for="folios_print">Folios LESP:</x-label>
                        <textarea class="text-uppercase border {{ $errors->has('folios_print') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" id="folios_print" rows="3" wire:model="folios_print" placeholder="Folios a imprimir, separados por comas."></textarea>
                    </div>
                </div>
            </div>

            <div class="flex justify-end">
                <button class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2" wire:click="printDocument"><i class="fa fa-download" aria-hidden="true"></i> PDF</button>
            </div>
        </div>

    </form>
</div>
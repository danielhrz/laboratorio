<div>
    <form method="post" wire:submit.prevent="submit" class="mx-4 relative">
        <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="submit" wire:loading.class.remove="hidden">
            <p class="w-full text-center font-semibold text-slate-700 text-xs md:text-sm">Generando archivo...</p>
            <span class="loader-download"></span>
        </div>

        <div class="container mx-auto px-4">
            @include('main_admin.flash-message')
        </div>


        <div class="container mx-auto px-4">
            <div class="grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <x-label for="date_start_samples">Fecha de Inicio:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="date" class="text-uppercase border {{ $errors->has('date_start_samples') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="date_start_samples" wire:model="date_start_samples">
                    </div>
                </div>

                <div class="mb-6 group">
                    <x-label for="date_end_samples">Fecha de Finalización:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="date" class="text-uppercase border {{ $errors->has('date_end_samples') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="date_end_samples" wire:model="date_end_samples">
                    </div>
                </div>
            </div>

            <div class="flex justify-end">
                <button class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2" wire:click="submit"><i class="fa fa-download" aria-hidden="true"></i> Excel</button>
            </div>
        </div>

    </form>
</div>
<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <div>
        @include('main_admin.flash-message')
    </div>
    <form wire:submit.prevent="submit" class="relative">
        {{ csrf_field() }}
        <div class="container-loader h-full w-full absolute items-center justify-center z-50 flex-wrap hidden" wire:loading.flex wire:target="submit" wire:loading.class.remove="hidden">
            <span class="loader-download"></span>
        </div>

        <div>
            <div class="grid gap-6 md:grid-cols-2">
                <div class="mb-6 group">
                    <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Año</label>
                    <input wire:model="anio" type="text" class="text-uppercase border {{ $errors->has('anio') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio" placeholder="Ingresar Año">
                    @if ($errors->has('anio'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('anio') }}</p>
                    @endif
                </div>
                <div class="mb-6 group">
                    <label for="folio_lesp" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Folio LESP</label>
                    <input wire:model="folio_lesp" type="text" class="text-uppercase border {{ $errors->has('folio_lesp') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" value="{{ old('folio_lesp') }}" id="folio_lesp" placeholder="Ingresar Folio LESP">
                    @if ($errors->has('folio_lesp'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('folio_lesp') }}</p>
                    @endif
                </div>
            </div>
            <div class="grid gap-6 md:grid-cols-2">
                <div class="mb-6 group">
                    <label for="oficio_entrada" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Oficio de Entrada</label>
                    <input wire:model="oficio_entrada" type="text" class="text-uppercase border {{ $errors->has('oficio_entrada') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" value="{{ old('oficio_entrada') }}" id="oficio_entrada" placeholder="Ingresar Ofiicio de Entrada">
                    @if ($errors->has('oficio_entrada'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('oficio_entrada') }}</p>
                    @endif
                </div>
                <div class="mb-6 group">
                    <label for="dx1" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Número de Diagnóstico</label>
                    <div class="col-md-12" wire:ignore>
                        <select class="form-control" id="choices-multiple-remove-button" placeholder="Selecciona maximo 5 diagnosticos" wire:model="dx1" multiple>
                            @foreach ($diagnosticos as $diagnostico)
                            <option value="{{$diagnostico}}" {{ old('dx1') == $diagnostico ? 'selected':''}}>{{ $diagnostico }}</option>
                            @endforeach
                        </select>
                    </div>
                    @if ($errors->has('dx1'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">Es requerido al menos un diagnóstico</p>
                    @endif
                </div>
            </div>
            <div class="grid gap-6 md:grid-cols-2">
                <div class="mb-6 group">
                    <label for="fecha_recepcion" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Fecha de Recepción</label>
                    <input wire:model="fecha_recepcion" type="date" id="fecha_recepcion" class="text-uppercase border {{ $errors->has('fecha_recepcion') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="fecha_recepcion" value="{{ old('fecha_recepcion')}}" autofocus>
                    @if ($errors->has('fecha_recepcion'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('fecha_recepcion') }}</p>
                    @endif
                </div>
                <div class="mb-6 group">
                    <label for="hora_recepcion" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Hora de Recepción</label>
                    <x-date id="flatpickr_operation_date" wire:model="hora_recepcion" />
                    @if ($errors->has('hora_recepcion'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">Ingresa la Hora de Recepción</p>
                    @endif
                </div>
            </div>
            <div class="grid gap-6 md:grid-cols-2">
                <div class="mb-6 group">
                    <label for="hospital" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Jurisdicción / Hospital</label>
                    <select class="text-uppercase border {{ $errors->has('hospital') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="hospital" value="{{ old('hospital') }}" id="hospital">
                        <option value="0">Selecciona la Jurisdicción/Hospital</option>
                        @foreach ($hospitales->sort() as $hospital)
                        <option value="{{$hospital}}" {{ old('hospital') == $hospital ? 'selected':''}}>{{ $hospital }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('hospital'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('hospital') }}</p>
                    @endif
                </div>
                <div class="mb-6 group">
                    <label for="nombre_paciente" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Nombre del Paciente</label>
                    <input wire:model="nombre_paciente" type="text" class="text-uppercase border {{ $errors->has('nombre_paciente') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" value="{{ old('nombre_paciente') }}" id="nombre_paciente" placeholder="Ingresar Nombre Completo del Paciente">
                    @if ($errors->has('nombre_paciente'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('nombre_paciente') }}</p>
                    @endif
                </div>
            </div>
            <div class="grid gap-6 md:grid-cols-2">
                <div class="mb-6 group">
                    <label for="tipo_muestra" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Tipo de Muestra</label>
                    <select class="text-uppercase border {{ $errors->has('tipo_muestra') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="tipo_muestra" value="{{ old('tipo_muestra') }}" id="tipo_muestra">
                        <option value="0">Selecciona el Tipo de Muestra</option>
                        @foreach ($muestras->sort() as $muestra)
                        <option value="{{$muestra}}" {{ old('tipo_muestra') == $muestra ? 'selected':''}}>{{ $muestra }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tipo_muestra'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('tipo_muestra') }}</p>
                    @endif
                </div>
                <div class="mb-6 group">
                    <label for="fecha_toma_muestra" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Fecha de Toma de Muestra</label>
                    <input wire:model="fecha_toma_muestra" type="date" id="fecha_toma_muestra" class="text-uppercase border {{ $errors->has('fecha_toma_muestra') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="fecha_toma_muestra" value="{{ old('fecha_toma_muestra')}}" autofocus>
                    @if ($errors->has('fecha_toma_muestra'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('fecha_toma_muestra') }}</p>
                    @endif
                </div>
            </div>
            <div class="grid gap-6 md:grid-cols-2">
                <div class="mb-6 group">
                    <label for="folio_sisver" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Folio Plataforma</label>
                    <input wire:model="folio_sisver" type="text" class="text-uppercase border {{ $errors->has('folio_sisver') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" value=" N/A {{ old('folio_sisver') }}" id="folio_sisver" placeholder="Ingresar Folio Plataforma">
                    @if ($errors->has('folio_sisver'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('folio_sisver') }}</p>
                    @endif
                </div>
                <div class="mb-6 group">
                    <label for="persona_recibe" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Personal que Registra la Muestra</label>
                    <select class="text-uppercase border {{ $errors->has('persona_recibe') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="persona_recibe" value="{{ old('persona_recibe') }}" id="persona_recibe">
                        <option value="0">Selecciona Personal</option>
                        @foreach ($personal->sort() as $persona)
                        <option value="{{ $persona }}" {{ old('persona_recibe') === $persona ? 'selected':''}}>{{ $persona }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('persona_recibe'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('persona_recibe') }}</p>
                    @endif
                </div>
            </div>
            <div class="grid gap-6 md:grid-cols-1">
                <div class="mb-6 group">
                    <label for="rechazos" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Motivo Rechazo</label>
                    <select class="text-uppercase border {{ $errors->has('persona_recibe') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="rechazos" value="{{ old('rechazos') }}" id="rechazos">
                        <option value="0">Selecciona Motivo Rechazo</option>
                        @foreach ($motivosRechazos->sort() as $motivoRechazo)
                        <option value="{{$motivoRechazo}}" {{ old('rechazos') == $motivoRechazo ? 'selected':''}}>{{ $motivoRechazo }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('rechazos'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('rechazos') }}</p>
                    @endif
                </div>
            </div>
            <div class="grid gap-6 md:grid-cols-2">
                <div class="mb-6 group">
                    <label for="observaciones" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Observaciones</label>
                    <textarea wire:model="observaciones" id="observaciones" class="text-uppercase border {{ $errors->has('observaciones') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" placeholder="Ingresar Observaciones">N/A {{ old('observaciones') }}</textarea>
                    @if ($errors->has('observaciones'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('observaciones') }}</p>
                    @endif
                </div>
                <div class="mb-6 group">
                    <label for="aclaraciones_remu" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Aclaraciones Internas de REMU</label>
                    <textarea wire:model="aclaraciones_remu" id="aclaraciones_remu" class="text-uppercase border {{ $errors->has('aclaraciones_remu') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" placeholder="Ingresar Aclaraciones Internas de REMU">N/A {{ old('aclaraciones_remu') }}</textarea>
                    @if ($errors->has('aclaraciones_remu'))
                    <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('aclaraciones_remu') }}</p>
                    @endif
                </div>
            </div>
            <div class="flex items-center justify-center p-6 space-x-2 rounded-b border-t border-gray-200">
                <button wire:loading.attr="disabled" wire:target="submit" type="submit" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center">Registrar</button>
            </div>
        </div>
    </form>
</div>
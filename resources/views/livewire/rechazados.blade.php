<div>
    <form method="post" wire:submit.prevent="submit" class="mx-4 relative">
        <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="submit" wire:loading.class.remove="hidden">
            <p class="w-full text-center font-semibold text-slate-700 text-xs md:text-sm">Generando archivo...</p>
            <span class="loader-download"></span>
        </div>

        <div class="container mx-auto px-4">
            @include('main_admin.flash-message')
        </div>

        <div class="container mx-auto px-4">
            <div class=" grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <x-label for="oficio">Oficio de entrada:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="text" class="text-uppercase border {{ $errors->has('oficio') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="oficio" wire:model="oficio" placeholder="SP0125/2022...">
                    </div>
                </div>
                <div class="mb-6 group">
                    <x-label for="end_date">Jurisdicción destinatario:</x-label>
                    <div class="col-12 col-md-6">
                        <select class="text-uppercase border {{ $errors->has('judi') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="judi" wire:change="searchEmail">
                            <option value="ninguna">Selecciona la jurisdicción</option>
                            @foreach ($destinatarios as $destinatario)
                            <option value="{{$destinatario->id}}" {{ old('judi') == $destinatario->id ? 'selected':''}}>{{ $destinatario->jurisdiccion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class=" grid xl:grid-cols-1 xl:gap-6">
                <div class="mb-6 group">
                    <x-label for="resp">Responsable:</x-label>
                    <div class="col-12 col-md-6">
                        <select class="text-uppercase border {{ $errors->has('resp') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="resp">
                            <option value="ninguna">Selecciona la persona responsable</option>
                            @foreach ($responsable as $resp )
                            <option value="{{$resp->id}}" {{ old('resp') == $resp->id ? 'selected':''}}>{{ $resp->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class=" grid xl:grid-cols-1 xl:gap-6">
                <div class="mb-6 group">
                    <x-label for="observaciones">Observaciones/ Especificaciones:</x-label>
                    <textarea class="text-uppercase border {{ $errors->has('observacion') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" id="observaciones" rows="3" wire:model="observacion" placeholder="Observaciones o especificaciones..."></textarea>
                </div>
            </div>

            <div class="mb-6 group">
                <x-label for="emails">Correos electrónicos a enviar el reporte (opcional):</x-label>
                <textarea class="text-uppercase border {{ $errors->has('emails') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" id="emails" rows="3" wire:model="emails" placeholder="Correos electrónicos separados por una coma..."></textarea>
            </div>


            <div class="flex justify-end">
                <button class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2" wire:click="assignMethod(1)"><i class="fa fa-download" aria-hidden="true"></i> Excel</button>
                <button class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2" wire:click="assignMethod(2)"><i class="fa fa-download" aria-hidden="true"></i> PDF</button>
            </div>
        </div>

    </form>
</div>
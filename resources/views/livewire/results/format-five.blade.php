<div>
    <form method="post" wire:submit.prevent="submit" class="mx-4 relative">
        <div class="container mx-auto px-4">
            @include('main_admin.flash-message')
        </div>

        <div class="container-loader h-full w-full absolute items-center justify-center flex-wrap hidden" wire:target="submit" wire:loading.class.remove="hidden" wire:loading.flex>
            <p class="w-100 text-center" style="color: #34495E; font-weight: bold;">Generando...</p>
            <span class="loader-download"></span>
        </div>

        <div class="container mx-auto px-4">
            <div class="grid xl:grid-cols-2 xl:gap-x-6 xl:gap-y-0.5">
                <div class="mb-6 group">
                    <label for="lesp" class="block mb-2 text-md font-medium text-gray-900 dark:text-gray-400">Folio LESP:</label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="text" class="text-uppercase border {{ $errors->has('lesp') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="lesp" wire:model="lesp" placeholder="Ejemplo: 000001-22">
                    </div>
                </div>

                <div class="mb-6 group">
                    <label for="asunto" class="block mb-2 text-md font-medium text-gray-900 dark:text-gray-400">Asunto del oficio:</label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="text" class="text-uppercase border {{ $errors->has('asunto') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="oficio" wire:model="asunto" placeholder="Ingrese el asunto del oficio">
                    </div>
                </div>

                <div class="mb-6 group">
                    <label for="oficio" class="block mb-2 text-md font-medium text-gray-900 dark:text-gray-400">Oficio de entrada:</label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="text" class="text-uppercase border {{ $errors->has('oficio') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="oficio" wire:model="oficio" placeholder="SP0125/2022...">
                    </div>
                </div>

                <div class="mb-6 group">
                    <label for="if_result" class="block mb-2 text-md font-medium text-gray-900 dark:text-gray-400">Oficio de salida:</label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="text" class="text-uppercase border {{ $errors->has('if_result') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="oficio" wire:model="if_result" placeholder="Ingrese el asunto del oficio">
                    </div>
                </div>
                <div class="mb-6 group">
                    <label for="destinatario" class="block mb-2 text-md font-medium text-gray-900 dark:text-gray-400">Destinatario:</label>
                    <div class="col-12 col-md-6">
                        <select class="text-uppercase border {{ $errors->has('destinatario') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="destinatario">
                            <option value="ninguna">Selecciona el destinatario</option>
                            @foreach ($destinatarios as $destina )
                            <option value="{{$destina->id}}">{{ $destina->encargado }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="flex justify-end">
                <!-- <button class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2" wire:click="assignMethod(1)"><i class="fa fa-download" aria-hidden="true"></i> Excel</button> -->
                <button class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2" wire:click="assignMethod(2)"><i class="fa fa-download" aria-hidden="true"></i> PDF</button>
            </div>
        </div>

    </form>
</div>
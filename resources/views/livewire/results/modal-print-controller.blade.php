<div>
    <div>
        <!-- Main modal -->
        <div id="modalSelectDownload" tabindex="-1" aria-hidden="true" class="{{ $hidden }}" {{ $dialog }}>
            <div class="relative px-4 w-full max-w-2xl h-full md:h-auto">
                <!--Modal content -->
                <div class="relative bg-white rounded-lg shadow">
                    <!-- Modal header -->
                    <div class="flex justify-between items-start p-4 rounded-t border-b">
                        <h3 class="text-xl font-normal text-slate-700">
                            Descargar
                        </h3>
                        <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center" wire:click="$emit('hidden')">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </div>
                    <!-- Modal body -->
                    <div class="px-6 pt-4 space-y-6">
                        <form wire:submit.prevent="submit" method="POST" class="relative">
                            <div class="container-loader h-full w-full absolute items-center justify-center flex-wrap hidden" wire:target="submit" wire:loading.class.remove="hidden" wire:loading.flex>
                                <p class="w-100 text-center" style="color: #34495E; font-weight: bold;">Generando...</p>
                                <span class="loader-download"></span>
                            </div>
                            <div class="container mx-auto px-4">
                                <div class="grid xl:grid-cols-2 xl:gap-x-6 xl:gap-y-0.5">

                                    <div class="mb-6 group">
                                        <label for="encargado" class="block mb-2 text-md font-medium text-gray-900 dark:text-gray-400">Encargado:</label>
                                        <div class="col-12 col-md-6">
                                            <select class="text-uppercase border {{ $errors->has('encargado') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="encargado">
                                                <option value="ninguna">Selecciona el encargado del laboratorio</option>
                                                @foreach ($managers as $manager )
                                                <option value="{{$manager->id}}">{{ $manager->manager }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class=" flex items-center justify-center px-6  mt-6 py-3 space-x-2 rounded-b border-t border-gray-200">
                                <button type="submit" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2"><i class="fa fa-download" aria-hidden="true"></i> PDF</button>
                                <button type="button" wire:click="$emit('hidden')" class="button-after text-red-700 hover:text-white border border-red-700 hover:bg-red-800 focus:ring-2 hover:shadow-lg hover:shadow-red-500/50 focus:outline-none focus:ring-red-300 font-medium rounded text-xs sm:text-sm px-7 py-1.5 text-center mr-2 mb-2">Cerrar</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
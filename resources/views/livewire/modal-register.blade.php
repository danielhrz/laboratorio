<div class="row">
    <div class="col">
        @include("livewire.$view")
    </div>
    <script>
        var select;
        document.addEventListener('livewire:load', function() {
            Livewire.on('selectCreate', id => {
                $("#labelAction").text('Registro Manual de Datos Plataforma');
                
                var fecha = new Date(); //Fecha actual
                var mes = fecha.getMonth()+1; //obteniendo mes
                var dia = fecha.getDate(); //obteniendo dia
                var ano = fecha.getFullYear(); //obteniendo año
                if(dia<10)
                    dia='0'+dia; //agrega cero si el menor de 10
                if(mes<10)
                    mes='0'+mes //agrega cero si el menor de 10
                document.getElementById('fecha_recepcion').value=ano+"-"+mes+"-"+dia;
                @this.set('fecha_recepcion', ano+"-"+mes+"-"+dia);

                createSelect2();
            });

            Livewire.on('selectUpdate', id => {
                $("#labelAction").text('Actualización de Datos Federales');
                select = $('#choices-multiple-remove-button').select2({
                    maximumSelectionLength: 5
                });
                $('#choices-multiple-remove-button').on('change', function() {
                    @this.set('dx1', $(this).val());
                });
                var vals = @this.dx1;
                select.val(vals).trigger("change");
            });

            Livewire.on('titleShow', id => {
                $("#labelAction").text('Detalle del Registro');

            });
        })


        function createSelect2() {
            $('#choices-multiple-remove-button').select2({
                maximumSelectionLength: 5
            });
            $('#choices-multiple-remove-button').on('change', function() {
                @this.set('dx1', $(this).val());
            });
        }
    </script>
</div>
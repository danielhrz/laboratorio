<div class="flex items-center justify-around">

    <div x-data="{ tooltip: 'Modificar datos' }">
        <button x-tooltip="tooltip" data-identifier="{{ $id }}" type=" button" class="button__edit text-blue-700 border border-blue-700 hover:bg-blue-700 hover:text-white focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded lg:rounded-md text-xs md:text-sm w-6.5 h-6.5 md:w-8 md:h-8 text-center flex items-center justify-center">
            <i class="fa-regular fa-pen-to-square"></i>
        </button>
    </div>


</div>
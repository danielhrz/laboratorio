<div class="bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700 relative">
    <div class="container-loader h-full w-full absolute items-center justify-center z-50 flex-wrap hidden" wire:loading.flex wire:target="export" wire:loading.class.remove="hidden">
        <span class="loader-download"></span>
    </div>
    <header class="flex items-center justify-between py-3 px-4 lg:py-4 lg:px-6 border-b border-slate-200">
        <h5 class="text-center text-sm lg:text-lg font-semibold text-slate-600">Muestras para diagnostico de Paludismo</h5>

        <div x-data="{ tooltip: 'Exportar información' }">
            <button x-tooltip="tooltip" wire:click="export" type="button" class="flex space-x-2 items-center bg-purple-800 hover:bg-purple-700 text-white focus:ring-2 focus:outline-none focus:ring-purple-300 font-semibold rounded text-xs lg:text-sm  px-1.5 py-1 lg:px-2 lg:py-1.5">
                <i class="fa-solid fa-cloud-arrow-down"></i>
                <span>Exportar</span>
            </button>
        </div>
    </header>

    <div class="container_table p-3 md:p-5 lg:p-6" wire:ignore>
        <div class="flex items-center space-x-2 mb-5" wire:ignore>
            <span class="text-slate-600 text-xs sm:text-sm">Periodo:</span>
            <input readonly type="text" id="start_date" data-emit='start_date' value="{{ \Carbon\Carbon::now()->isoFormat('Y-MM-DD') }}" autocomplete="off" class="w-28 border-gray-300 focus:ring-green-700 focus:border-green-700 border bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block px-1.5 py-1 lg:px-2 lg:py-1.5">
            <span class="text-slate-600 text-xs sm:text-sm"> al </span>
            <input readonly type="text" id="end_date" data-emit='end_date' value="{{ \Carbon\Carbon::now()->isoFormat('Y-MM-DD') }}" autocomplete="off" class="w-28 border-gray-300 focus:ring-green-700 focus:border-green-700 border bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block px-1.5 py-1 lg:px-2 lg:py-1.5">

            <div x-data="{ tooltip: 'Generar tabla' }">
                <button x-tooltip="tooltip" id="search_data" type="button" class="flex space-x-2 items-center bg-teal-800 hover:bg-teal-700 text-white focus:ring-2 focus:outline-none focus:ring-teal-300 font-semibold rounded text-xs lg:text-sm  px-1.5 py-1 lg:px-2 lg:py-1.5">
                    <i class="fa-solid fa-magnifying-glass"></i>
                    <span>Buscar</span>
                </button>
            </div>
        </div>

        <div class="p-4 text-xs md:text-sm text-yellow-700 bg-yellow-100 rounded-lg dark:bg-yellow-200 dark:text-yellow-800 mb-6" role="alert">
            <span class="font-medium">La información visible sera unicamente aquellas muestras que ya cuentan con un resultado asignado y el número máximo de muestras a exportar no debe superar las 5000 muestras.</span>
        </div>

        <table class="w-full" id="table_paludismo">
            <thead class="text-xs lg:text-sm text-slate-500 bg-gray-50 font-semibold">
                <tr>
                    <th class="py-2 px-2">#</th>
                    <th class="py-2 px-2">Folio LESP</th>
                    <th class="py-2 px-2">Nombre del Paciente</th>
                    <th class="py-2 px-2">Fecha de recepción</th>
                    <th class="py-2 px-2">Folio SINAVE</th>
                    <th class="py-2 px-2">Tipo de diagnostico</th>
                    <th class="py-2 px-2">Clave Laminilla</th>
                    <th class="py-2 px-2">Semana Epidem.</th>
                    <th class="py-2 px-2">Tiempo total</th>
                    <th class="py-2 px-2">Banco de muestras</th>
                    <th class="py-2 px-2">PDR</th>
                    <th class="py-2 px-2 column-two-options">Opciones</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- Main modal -->
    <div id="modal_information_extension" tabindex="-1" aria-hidden="true" class="{{ $hidden }}" {{ $dialog }}>
        <div class="relative px-4 w-full max-w-2xl h-full md:h-auto">
            <!--Modal content -->
            <div class="relative bg-white rounded-lg shadow">
                <!-- Modal header -->
                <div class="flex justify-between items-start p-4 rounded-t border-b">
                    <h3 class="text-xl font-normal text-slate-700">
                        Edición de datos adicionales
                    </h3>
                    <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center" wire:click="$emit('hidden')">
                        <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                        </svg>
                    </button>
                </div>
                <!-- Modal body -->
                <div class="px-4 pt-4">
                    <form wire:submit.prevent="submit" method="POST" class="relative">
                        <div class="container-loader h-full w-full absolute items-center justify-center flex-wrap hidden" wire:target="submit" wire:loading.class.remove="hidden" wire:loading.flex>
                            <p class="w-100 text-center" style="color: #34495E; font-weight: bold;">Guardando...</p>
                            <span class="loader-download"></span>
                        </div>
                        <div class="container mx-auto">
                            <div class="grid xl:grid-cols-2 xl:gap-x-6 xl:gap-y-1">
                                <div class="mb-1 group">
                                    <label for="diagnostic_type" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Tipo de diagnostico</label>
                                    <div class="col-12 col-md-6">
                                        <select autocomplete="off" id="diagnostic_type" class="select-status border {{ $errors->has('information.diagnostic_type') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="information.diagnostic_type">
                                            <option value="0">Seleccione una opción</option>
                                            @foreach( config('const.diagnostic_type') as $type)
                                            <option value="{{$type}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <di id="error_diagnostic_type" class="hidden"></di>
                                </div>

                                <div class="mb-1 group">
                                    <label for="key_lamella" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Clave Laminilla</label>
                                    <input autocomplete="off" wire:model="information.key_lamella" type="text" id="key_lamella" class="border {{ $errors->has('information.key_lamella') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                </div>

                                <div class="mb-1 group">
                                    <label for="reception" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Entrada al area de paludismo</label>
                                    <input autocomplete="off" data-emit='reception' wire:model="reception" id="reception" type="text" class="border {{ $errors->has('reception') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                </div>

                                <div class="mb-1 group">
                                    <label for="epidemiological_week" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Semana epidemiologica</label>
                                    <input autocomplete="off" wire:model="information.epidemiological_week" type="text" id="epidemiological_week" class="border {{ $errors->has('information.epidemiological_week') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                </div>

                                <div class="mb-1 group">
                                    <label for="sample_collection_time" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Hora de toma de muestra</label>
                                    <input autocomplete="off" data-emit='sample_collection_time' wire:model="sample_collection_time" type="text" id="sample_collection_time" class="border {{ $errors->has('sample_collection_time') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                </div>

                                <div class="mb-1 group">
                                    <label for="reading_result" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Resultado de la lectura</label>
                                    <div class="col-12 col-md-6">
                                        <select autocomplete="off" wire:model="information.reading_result" id="reading_result" class="select-status border {{ $errors->has('information.reading_result') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                            <option value="0">Seleccione una opción</option>
                                            @foreach( config('const.reading_result') as $type)
                                            <option value="{{$type}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <di id="error_reading_result" class="hidden"></di>
                                </div>

                                <div class="mb-1 group">
                                    <label for="species" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Especie</label>
                                    <div class="col-12 col-md-6">
                                        <select autocomplete="off" wire:model="information.species" id="species" class="select-status border {{ $errors->has('information.species') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                            <option value="0">Seleccione una opción</option>
                                            @foreach( config('const.species') as $type)
                                            <option value="{{$type}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <di id="error_species" class="hidden"></di>
                                </div>

                                <div class="mb-1 group">
                                    <label for="eas" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Densidad parasitaria EAS</label>
                                    <input autocomplete="off" wire:model="information.eas" type="text" id="eas" class="border {{ $errors->has('information.eas') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                </div>

                                <div class="mb-1 group">
                                    <label for="ess" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Densidad parasitaria ESS</label>
                                    <input autocomplete="off" wire:model="information.ess" type="text" id="ess" class="border {{ $errors->has('information.ess') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                </div>

                                <div class="mb-1 group">
                                    <label for="date_delivery_results_aeer" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Entrega de resultados al AEER</label>
                                    <input autocomplete="off" data-emit='date_delivery_results_aeer' wire:model="date_delivery_results_aeer" type="text" id="date_delivery_results_aeer" class="border {{ $errors->has('date_delivery_results_aeer') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                </div>

                                <div class="mb-1 group">
                                    <label for="total_time" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Tiempo total del proceso(hrs)</label>
                                    <input autocomplete="off" wire:model="information.total_time" type="number" step="0.01" id="total_time" class="border {{ $errors->has('information.total_time') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                </div>

                                <div class="mb-1 group">
                                    <label for="quality_control" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Control de calidad InDRE</label>
                                    <div class="col-12 col-md-6">
                                        <select autocomplete="off" wire:model="information.quality_control" id="quality_control" class="select-status border {{ $errors->has('information.quality_control') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                            <option value="0">Seleccione una opción</option>
                                            @foreach( config('const.quality_control') as $type)
                                            <option value="{{$type}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <di id="error_quality_control" class="hidden"></di>
                                </div>

                                <div class="mb-1 group">
                                    <label for="quality_control_result" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Resultado control de calidad InDRE</label>
                                    <div class="col-12 col-md-6">
                                        <select autocomplete="off" wire:model="information.quality_control_result" id="quality_control_result" class="select-status border {{ $errors->has('information.quality_control_result') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                            <option value="0">Seleccione una opción</option>
                                            @foreach( config('const.quality_control_result') as $type)
                                            <option value="{{$type}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <di id="error_quality_control_result" class="hidden"></di>
                                </div>

                                <div class="mb-1 group">
                                    <label for="sample_bank" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Banco de muertas</label>
                                    <div class="col-12 col-md-6">
                                        <select autocomplete="off" wire:model="information.sample_bank" id="sample_bank" class="select-status border {{ $errors->has('information.sample_bank') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                            <option value="0">Seleccione una opción</option>
                                            @foreach( config('const.sample_bank') as $type)
                                            <option value="{{$type}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <di id="error_sample_bank" class="hidden"></di>
                                </div>

                                <div class="mb-1 group">
                                    <label for="diagnostic_tests" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Pruebas de diagnostico rápido</label>
                                    <div class="col-12 col-md-6">
                                        <select autocomplete="off" wire:model="information.diagnostic_tests" id="diagnostic_tests" class="select-status border {{ $errors->has('information.diagnostic_tests') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                            <option value="0">Seleccione una opción</option>
                                            @foreach( config('const.diagnostic_tests') as $type)
                                            <option value="{{$type}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <di id="error_diagnostic_tests" class="hidden"></di>
                                </div>

                                <div class="mb-1 group col-span-2">
                                    <label for="observations" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Observaciones</label>
                                    <textarea autocomplete="off" wire:model="information.observations" id="observations" class="border {{ $errors->has('information.observations') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2">
                                    </textarea>
                                </div>
                            </div>


                        </div>

                        <div class=" flex items-center justify-center px-6  mt-6 py-3 space-x-2 rounded-b border-t border-gray-200">
                            <button type="submit" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2">Guardar</button>
                            <button type="button" wire:click="$emit('hidden')" class="text-red-700 hover:text-white border border-red-700 hover:bg-red-800 focus:ring-2 hover:shadow-lg hover:shadow-red-500/50 focus:outline-none focus:ring-red-300 font-medium rounded text-xs sm:text-sm px-7 py-1.5 text-center mr-2 mb-2">Cerrar</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function() {
            Livewire.on('start_date', date => {
                @this.start_date = date;
            });
            Livewire.on('end_date', date => {
                @this.end_date = date;
            });

            Livewire.on('reception', date => {
                @this.reception = date;
            });
            Livewire.on('sample_collection_time', date => {
                @this.sample_collection_time = date;
            });
            Livewire.on('date_delivery_results_aeer', date => {
                @this.date_delivery_results_aeer = date;
            });
        });
    </script>
</div>
<div class="p-6 bg-white border border-gray-200">
    <div>
        @include('main_admin.flash-message')
    </div>
    <form wire:submit.prevent="submit">
        {{ csrf_field() }}
        <div class="grid gap-6 md:grid-cols-2">
            <div class="mb-1 group">
                <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Oficio de Entrada</label>
                <input wire:model="oficio_entrada" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio">
            </div>
            <div class="mb-1 group">
                <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Nombre del Paciente</label>
                <input wire:model="nombre_paciente" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio" placeholder="Ingresar CRE" >
            </div>
            <div class="mb-1 group">
                <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Folio LVE</label>
                <input wire:model="folio_lesp" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio" placeholder="Ingresar CRE" >
            </div>
            <div class="mb-1 group">
                <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Folio SINAVE</label>
                <input wire:model="folio_sisver" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio" placeholder="Ingresar CRE" >
            </div>
            <div class="mb-1 group">
                <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Tipo de Muestra</label>
                <input wire:model="tipo_muestra" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700' bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio" placeholder="Ingresar CRE" >
            </div>
            <div class="mb-1 group">
                <label for="iniciales" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Iniciales</label>
                <input wire:model="iniciales" type="text" class="text-uppercase border {{ $errors->has('iniciales') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="iniciales" placeholder="Ingresar las iniciales">
                @if ($errors->has('iniciales'))
                <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('iniciales') }}</p>
                @endif
            </div>
        </div>
        @if (!is_null($diagnostics))
        @for ($i = 0; $i < $diagnostics->count(); $i++ )
        <hr class="my-2">
            <div>
                <div class="grid gap-6 md:grid-cols-2">
                    <div class="mb-1 group">
                        <label for="diagnosis" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Diagnostico:</label>
                        <div class="col-12 col-md-6">
                            <select class="text-uppercase border {{ $errors->has('diagnosis') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="diagnosis.{{ $diagnostics[$i]->id }}.id">
                                <option value="0">Selecciona el diagnostico</option>
                                <option value="{{ $diagnostics[$i]->id }}">{{ $diagnostics[$i]->analysis }}</option>
                            </select>
                        </div>
                        @if ($errors->has('diagnosis'))
                        <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('diagnosis') }}</p>
                        @endif
                    </div>
                    <div class="mb-1 group">
                        <label for="kit" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Kit:</label>
                        <div class="col-12 col-md-6">
                            <select class="text-uppercase border {{ $errors->has('kit') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="kit.{{ $diagnostics[$i]->id }}.id">
                                <option value="0">Selecciona el kit</option>
                                @if(isset($kit[$diagnostics[$i]->id]))
                                @for ($j = 0; $j < count($kit[$diagnostics[$i]->id]); $j++)
                                    <option value="{{ $kit[$diagnostics[$i]->id][$j]['kit_id'] }}">{{ $kit[$diagnostics[$i]->id][$j]['kit_name'] }}</option>
                                @endfor
                                @endif
                            </select>
                        </div>
                        @if ($errors->has('kit'))
                        <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('kit') }}</p>
                        @endif
                    </div>
                    <div class="mb-1 group">
                        <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Técnica Utilizada</label>
                        <input wire:model="anio" type="text" class="text-uppercase border {{ $errors->has('anio') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio" placeholder="Ingresar CRE" >
                        @if ($errors->has('anio'))
                        <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('anio') }}</p>
                            @endif
                    </div>
                    <div class="mb-1 group">
                        <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Valor de Referencia</label>
                        <input wire:model="anio" type="text" class="text-uppercase border {{ $errors->has('anio') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio" placeholder="Ingresar CRE" >
                        @if ($errors->has('anio'))
                        <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('anio') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        @endfor
        @endif
        <hr class="my-2">
        <div class="grid gap-6 md:grid-cols-2">
            <div class="mb-1 group">
                <label for="anio" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600"># CRE/FORMATO/BITACORA</label>
                <input wire:model="anio" type="text" class="text-uppercase border {{ $errors->has('anio') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="anio" placeholder="Ingresar CRE" >
                @if ($errors->has('anio'))
                <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('anio') }}</p>
                @endif
            </div>
            <div class="mb-1 group">
                <label for="observaciones" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Resultado</label>
                <textarea wire:model="observaciones" id="observaciones" class="text-uppercase border {{ $errors->has('observaciones') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" placeholder="Ingresar Observaciones" >N/A {{ old('observaciones') }}</textarea>
                @if ($errors->has('observaciones'))
                <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('observaciones') }}</p>
                @endif
            </div>
            <div class="mb-1 group">
                <label for="observaciones" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Comentarios Adicionales</label>
                <textarea wire:model="observaciones" id="observaciones" class="text-uppercase border {{ $errors->has('observaciones') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" placeholder="Ingresar Observaciones" >N/A {{ old('observaciones') }}</textarea>
                @if ($errors->has('observaciones'))
                <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('observaciones') }}</p>
                @endif
            </div>
            <div class="mb-1 group">
                <label for="observaciones" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Interpretación del Resultado</label>
                <textarea wire:model="observaciones" id="observaciones" class="text-uppercase border {{ $errors->has('observaciones') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" placeholder="Ingresar Observaciones" >N/A {{ old('observaciones') }}</textarea>
                @if ($errors->has('observaciones'))
                <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('observaciones') }}</p>
                @endif
            </div>
            <div class="mb-1 group">
                <label for="fecha_toma_muestra" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">FECHA DE ENTREGA DE RESULTADOS AL AEER</label>
                <input wire:model="fecha_toma_muestra" type="date" id="fecha_toma_muestra" class="text-uppercase border {{ $errors->has('fecha_toma_muestra') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="fecha_toma_muestra" value="{{ old('fecha_toma_muestra')}}" autofocus>
                @if ($errors->has('fecha_toma_muestra'))
                <p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">{{ $errors->first('fecha_toma_muestra') }}</p>
                @endif
            </div>
        </div>
        <div class="flex items-center justify-center p-6 space-x-2 rounded-b border-t border-gray-200">
            <button type="submit" class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center">Registrar</button>
        </div>
    </form>
</div>

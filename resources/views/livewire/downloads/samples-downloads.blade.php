<div>
    <form class="px-6 bg-white" wire:submit.prevent="submit" id="download-samples">

        <div class="relative">
            <div class="container-loader h-full w-full absolute flex items-center justify-center z-30 flex-wrap hidden" wire:target="submit" wire:loading.class.remove="hidden">
                <p class="w-full text-center text-slate-700 text-xs md:text-sm">Generando...</p>
                <span class="loader-download"></span>
            </div>

            <div class="grid xl:grid-cols-2 xl:gap-x-10 xl:gap-y-0.5">
                <div class="mb-6 group flex justify-center items-center">
                    <label class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Desde:</label>
                    <div class="relative z-0 w-full ml-2 group">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path>
                            </svg>
                        </div>
                        <input id="start-type-samples" autocomplete="off" type="text" wire:model="date_start" class="datepicker border {{ $errors->has('date_start') ? 'shadow shadow-red-500/50 border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }}  bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 pl-10" placeholder="Fecha de inicio">
                    </div>
                </div>
                <div class="mb-6 group flex justify-center items-center">
                    <label class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Hasta:</label>
                    <div class="relative z-0 w-full ml-2 group">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path>
                            </svg>
                        </div>
                        <input id="end-type-samples" autocomplete="off" type="text" wire:model="date_end" class="datepicker border {{ $errors->has('date_end') ? 'shadow shadow-red-500/50 border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }}  bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 pl-10" placeholder="Fecha de fin">
                    </div>
                </div>
            </div>
            <div class="flex justify-end items-start">
                <button type="submit" class="rounded-full text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:border-green-500 dark:text-green-500 dark:hover:text-white dark:hover:bg-green-600 dark:focus:ring-green-800">
                    <i class="fa-solid fa-cloud-arrow-down"></i> Descargar
                </button>
            </div>
        </div>
    </form>
    <script>
        document.addEventListener('livewire:load', function() {
            $("#start-type-samples").on('change', function() {
                let id = $(this).attr('id');
                @this.date_start = $(this).attr('value');
            });

            $("#end-type-samples").on('change', function() {
                let id = $(this).attr('id');
                @this.date_end = $(this).attr('value');
            });
        });
    </script>
</div>
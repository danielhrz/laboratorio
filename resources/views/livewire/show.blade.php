<div>
    @include('main_admin.flash-message')
</div>
<div class="flex justify-center p-6 space-y-6">
    <ul class="max-w-md divide-y divide-gray-200 dark:divide-gray-700">
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Año:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->anio}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Folio LESP:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->folio_lesp}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Diagnóstico:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    1: {{$datos->dx1}} <br>
                    2: {{$datos->dx2}} <br>
                    3: {{$datos->dx3}} <br>
                    4: {{$datos->dx4}} <br>
                    5: {{$datos->dx5}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Oficio Entrada:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->oficio_entrada}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Fecha de Recepción:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->fecha_recepcion}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Hora de Recepción:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->hora_recepcion}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Jurisdicción/Hospital:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->hospital}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Nombre del Paciente:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->nombre_paciente}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Tipo de Muestra:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->tipo_muestra}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Folio Plataforma:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->folio_sisver}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Personal que Registra la Muestra:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->persona_recibe}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Estado:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    @if ($datos->status == 'PENDIENTE')
                    <span data-value="PENDIENTE" class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900" >PENDIENTE</span >
                    @endif
                    @if ($datos->status == 'FINALIZADO')
                    <span data-value="FINALIZADO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800" >FINALIZADO</span >
                    @endif
                    @if ($datos->status == 'CANCELADO')
                    <span data-value="CANCELADO" class="bg-red-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900" >CANCELADO</span >
                    @endif
                    @if ($datos->status == 'RECHAZO')
                    <span data-value="RECHAZO" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300" >RECHAZO</span >
                    @endif
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Muestras Rechazadas:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->rechazos}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Observaciones:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->observaciones}}
                </div>
            </div>
        </li>
        <li class="pb-3 sm:pb-4">
            <div class="flex items-center space-x-4">
                <div class="flex-1 min-w-0">
                    <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                        Aclaraciones REMU:
                    </p>
                </div>
                <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {{$datos->aclaraciones_remu}}
                </div>
            </div>
        </li>
    </ul>
</div>
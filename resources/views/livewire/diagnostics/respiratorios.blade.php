<div class="relative" wire:loading.class="h-48">
    <br>
    <div class="absolute w-full h-full top-0 flex justify-center items-center flex-col hidden" wire:loading.class.remove="hidden">
        <div class="spinner-chart"></div>
        <p class="text-sm text-red-900">Cargando información...</p>
    </div>


    <h3 class="text-center text-slate-600 text-sm lg:text-base font-semibold">Desglose de muestras</h3>
    <div class="flex justify-end items-start">
        <button type="button" wire:click="export" class="rounded-full text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:border-green-500 dark:text-green-500 dark:hover:text-white dark:hover:bg-green-600 dark:focus:ring-green-800">
            <i class="fa-solid fa-cloud-arrow-down"></i> Descargar tabla
        </button>
    </div>


    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-8" wire:loading.class="hidden">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 text-center">
                <tr>
                    <th rowspan="2" scope="col" class="border px-6 py-1.5">
                        Fecha
                    </th>
                    <th rowspan="2" scope="col" class="border px-6 py-1.5">
                        Recepcionadas
                    </th>
                    <th rowspan="2" scope="col" class="border px-6 py-1.5">
                        Finalizadas
                    </th>
                    @foreach ($juris as $th)
                    @if ($th != 'N/A')
                    <th scope="col" colspan="4" class="border px-6 py-1.5">
                        {{ $th }}
                    </th>
                    @endif
                    @endforeach
                </tr>
                <tr>
                    @foreach ($juris as $th)
                    @if ($th != 'N/A')
                    <th scope="col" class="border px-6 py-1.5">
                        SARS
                    </th>
                    <th scope="col" class="border px-6 py-1.5">
                        INFLUENZA
                    </th>
                    <th scope="col" class="border px-6 py-1.5">
                        VSR
                    </th>
                    <th scope="col" class="border px-6 py-1.5">
                        NEGATIVAS
                    </th>
                    @endif
                    @endforeach
                </tr>
                
            </thead>
            <tbody>
                @php
                $index = 1;
                @endphp


                @forelse ($information as $info)

                @if ($index == 1)
                <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                    @foreach ($info as $row)
                    <td class="uppercase text-xs px-6 py-2 text-center">{{$row}}</td>
                    @endforeach
                </tr>
                @php
                $index = 2;
                @endphp
                @else
                <tr class="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                    @foreach ($info as $row)
                    <td class="uppercase text-xs px-6 py-2 text-center">{{$row}}</td>
                    @endforeach
                </tr>
                @php
                $index = 1;
                @endphp
                @endif

                @empty

                @endforelse


            </tbody>
        </table>
    </div>

</div>
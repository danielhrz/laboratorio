<div class="relative" wire:loading.class="h-48">
    <br>
    <div class="absolute w-full h-full top-0 flex justify-center items-center flex-col hidden" wire:loading.class.remove="hidden">
        <div class="spinner-chart"></div>
        <p class="text-sm text-red-900">Cargando información...</p>
    </div>
    <h3 class="text-center text-slate-600 text-sm lg:text-base font-semibold">Desglose de muestras recepcionadas por día</h3>

    @if (is_array($thead))

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-8" wire:loading.class="hidden">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 text-center">
                <tr>
                    @foreach ($thead as $th)
                    <th scope="col" class="px-6 py-3">
                        {{ $th }}
                    </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @php
                $index = 1;
                @endphp
                @foreach ($information as $row)

                @if ($index == 1)
                <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                    @foreach ($row as $td)
                    <td class="text-xs px-6 py-4 text-center">{{$td}}</td>
                    @endforeach
                </tr>
                @php
                $index = 2;
                @endphp
                @else
                <tr class="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                    @foreach ($row as $td)
                    <td class="text-xs px-6 py-4 text-center">{{$td}}</td>
                    @endforeach
                </tr>
                @php
                $index = 1;
                @endphp
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
    @endif

</div>
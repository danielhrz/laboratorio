<div>
    <div class="">
        @include('main_admin.flash-message')
    </div>
    <form class="p-4" wire:submit.prevent="submit" method="POST" enctype="multipart/form-data">
        @csrf
        <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" for="file">Selecciona archivo CSV</label>
        <input class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" aria-describedby="user_avatar_help" id="file" type="file" wire:model="file" wire:target="submit" wire:loading.class.remove="d-none"  onchange="return validarExt()">
        <br>
        <button type="submit" class="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-md text-sm px-3.5 py-1.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Importar</button>
        {{-- <input type ="button" class="btn btn-warning" onclick="limpiar()" value="Limpiar"/> --}}
    </form>
{{--     <div class="container-loader h-100 w-100 position-absolute align-items-center justify-content-center flex-wrap d-none" >
        <p class="w-100 text-center" style="color: #34495E; font-weight: bold;">Importando...</p>
        <span class="loader-download"></span>
    </div> --}}
</div>

<div>
    <form method="post" wire:submit.prevent="submit" class="mx-4 relative">

        <div class="container-loader h-full w-full absolute items-center justify-center z-20 flex-wrap hidden" wire:loading.flex wire:target="submit" wire:loading.class.remove="hidden">
            <p class="w-full text-center font-semibold text-slate-700 text-xs md:text-sm">Generando archivo...</p>
            <span class="loader-download"></span>
        </div>

        <div class="container mx-auto px-4">
            @include('main_admin.flash-message')
        </div>

        <div class="container mx-auto px-4">
            <div class=" grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <x-label for="dx1">Diagnóstico:</x-label>
                    <div class="col-12 col-md-6">
                        <select class="text-uppercase border {{ $errors->has('dx1') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" wire:model="dx1">
                            <option value="0">Selecciona el Diagnostico</option>
                            @foreach ($diagnostics as $diagnostico)
                            <option value="{{$diagnostico->diagnosis_name}}">{{ $diagnostico->diagnosis_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mb-6 group">
                    <x-label for="start-date">Fecha de recepción:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="date" class="text-uppercase border {{ $errors->has('reception') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="reception" wire:model="reception">
                    </div>
                </div>
            </div>
            <div class="grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <x-label for="start-date">Desde:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="date" class="text-uppercase border {{ $errors->has('startDate') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="start-date" wire:model="startDate">
                    </div>
                </div>
                <div class="mb-6 group">
                    <x-label for="end-date"> Hasta:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="date" class="text-uppercase border {{ $errors->has('endDate') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="end-date" wire:model="endDate">
                    </div>
                </div>
            </div>
            <div class="grid xl:grid-cols-2 xl:gap-6">
                <div class="mb-6 group">
                    <x-label for="folio-start">Folio desde:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="text" class="text-uppercase border {{ $errors->has('folioStart') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="folio-start" wire:model="folioStart" placeholder="Num. de folio...">
                    </div>
                </div>
                <div class="mb-6 group">
                    <x-label for="folio-end">Folio hasta:</x-label>
                    <div class="col-12 col-md-6 col-lg-5">
                        <input type="text" class="text-uppercase border {{ $errors->has('folioEnd') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="folio-end" wire:model="folioEnd" placeholder="Num. de folio...">
                    </div>
                </div>
            </div>
            <div class="grid xl:grid-cols-1 xl:gap-6">
                <div class="mb-6 group">
                    <div class="form-group col-12 col-md-10">
                        <x-label for="folio-end">Folios a omitir:</x-label><br>
                        <textarea class="text-uppercase border {{ $errors->has('except') ? 'border-red-600 focus:ring-red-700 focus:border-red-700' : 'border-gray-300 focus:ring-green-700 focus:border-green-700' }} block p-2.5 w-full text-sm text-gray-900 rounded-lg" id="exampleFormControlTextarea1" rows="3" wire:model="except" placeholder="Folios a omitir, separados por comas o en caso de ser un rango de folios separado por guion... Ejemplo: 234-500"></textarea>
                    </div>
                </div>
            </div>

            <div class="flex justify-end">
                <button class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2" wire:click="assignMethod(1)"><i class="fa fa-download" aria-hidden="true"></i> Excel</button>
                <button class="text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded text-xs sm:text-sm px-5 py-1.5 text-center mr-2 mb-2" wire:click="assignMethod(2)"><i class="fa fa-download" aria-hidden="true"></i> PDF</button>
            </div>
        </div>

    </form>
</div>
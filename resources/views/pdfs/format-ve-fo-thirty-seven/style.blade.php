<style>
    /*========== Inicio de estilos del membrete ==========*/
    .container_section {
        float: left;
        margin-bottom: 0.1em;
        padding: 0.5em;
        box-sizing: border-box;
        /*  border: solid 1px #000; */
    }

    .container_logo_institution {
        width: 220px;
    }

    .logo_institutional {
        width: 160px;
        height: auto;
    }

    .container_section.container_logo_lve {
        margin-left: 2.5em;
        padding-top: 1.5em;
        width: 70px;
    }

    .logo_lve {
        width: auto;
        height: 60px;
    }

    .container_section.legend {
        padding-top: 2em;
        margin-left: 2.5em;
        width: 290px;
    }

    .container_section.legend p {
        font-size: 7pt;
        text-align: left;
        margin: 0px;
        margin-top: 1px;
    }

    .container_section.legend .institution {
        font-size: 8pt;
        font-weight: bold;
    }

    .container_section.container_title {
        width: 460px;
    }

    .container_title p {
        font-size: 9pt;
        margin: 0px;
        text-align: center;
        font-weight: bold;
    }

    .letterhead_section_data {
        box-sizing: border-box;
        padding: 0em 0.7em;
        display: block;
    }

    .letterhead_section_data_table {
        width: 100%;
        border-collapse: collapse;
    }

    .letterhead_table_td_secondary,
    .letterhead_table_td_tittle {
        width: 50%;
        font-size: 8.5pt;
        font-weight: bold;
        padding: 2px 3px;
    }

    .border_top {
        border-top: solid 1px #000;
    }

    .border_left {
        border-left: solid 1px #000;
    }

    .border_right {
        border-right: solid 1px #000;
    }

    .border_bottom {
        border-bottom: solid 1px #000;
    }

    .letterhead_table_td_tittle {
        font-size: 8pt;
    }

    .letterhead_table_td_secondary {
        font-size: 8.5pt;
    }

    /*========== Fin de estilos del membrete ==========*/

    /*========== Inicio de estilos del membrete inferior ==========*/
    .bottom_rubric_section {
        width: 33%;
        float: left;
        text-align: left;
    }

    .text_bottom_rubric {
        font-size: 7pt;
        font-weight: bold;
    }

    .firm_img {
        width: auto;
        height: 70px;
        display: block;
        margin: auto;
    }

    .container_primary_firm {
        width: 100%;
        padding: 0em 0.7em;
        display: block;
        padding-left: 180px;
    }

    .container_table_td_firm {
        height: 90px;
        padding: 0px 10px;
        border: solid 1px #000;
        width: 350px;
    }

    .text_firm_container {
        vertical-align: top;
        border: solid 1px #000;
        font-size: 7pt;
        text-align: center;
        vertical-align: bottom;
    }

    .bottom_text_date {
        padding: 5px 10px;
        font-size: 7pt;
        vertical-align: bottom;
        font-size: 7pt;
        font-weight: bold;
    }

    .bottom_legend_text_container,
    .bottom_legend_img {
        float: left;
        padding: 0.5em;
        box-sizing: border-box;
    }

    .container_legend {
        width: 310px;
        margin: auto;
    }

    .container_footer {
        border-top: solid 4px #9d2148;
    }

    .bottom_legend_text {
        text-align: center;
        font-size: 7pt;
        font-weight: bold;
    }

    .bottom_text_address {
        width: 200px;
        text-align: start;
        font-size: 7pt;
        font-weight: bold;
        color: #55585a;
        float: left;
    }

    .bottom_text_legend {
        width: 210px;
        text-align: center;
        font-size: 7pt;
        font-weight: bold;
    }

    .container_footer_text {
        float: left;
        width: 410px;
    }

    .bottom_legend_img {
        float: right;
        width: 300px;
        padding: 0px;
        margin: 0px;
    }

    .img_footer {
        float: left;
        height: 75px;
        width: auto;
        padding: 0px;
        margin: 0px;
    }

    .container-table td,
    .container-table th {
        text-align: center;
        padding: 3px 8px;
        font-size: 7pt;
    }

    @page {
        header: page-header;
        footer: page-footer;
    }
</style>
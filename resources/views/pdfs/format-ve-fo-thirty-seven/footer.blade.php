<htmlpagefooter name="page-footer">

    <div class="bottom_rubric_container">
        <div class="bottom_rubric_section" style="text-align:left;">
            <p class="text_bottom_rubric">Generó: {{ isset($genero) ? $genero: Auth::user()->initials }}</p>
            @if (isset($rubrica) && $rubrica[0] != null)
            <img src='{{asset("img/rubric/$rubrica[0]")}}' style="width: 40px; height: auto;" />
            @endif
        </div>
        <div class="bottom_rubric_section" style="text-align:center;">&nbsp;
            @if(isset($rubrica) )
            <img src='{{asset("img/Sello-Laboratorio-Vigilancia-Epidemiologica.png")}}' style="width: 210px; height: auto;" />
            @endif
        </div>
        <div class="bottom_rubric_section" style="text-align:right;">
            <p class="text_bottom_rubric">Revisó: {{ $initials }}</p>
            @if (isset($rubrica) && $rubrica[1] != null)
            <img src='{{asset("img/rubric/$rubrica[1]")}}' style="width: 40px; height: auto;" />
            @endif
        </div>
    </div>


    <!-- Inicio de la sección referente a la firma -->
    <div class="container_primary_firm">
        <table class="letterhead_section_data_table">
            <tr>
                <td class="container_table_td_firm">
                    @if(isset($rubrica) )
                    <center>
                        @if ($manager->firm != null)
                        <img src='{{asset("img/firm/$manager->firm")}}' class="firm_img" />
                        @endif
                    </center>
                    @endif
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="text_firm_container">
                    @if( !is_null($manager) )
                    <b>{{ $manager->manager }}</b><br>
                    <b>Laboratorio de {{ Str::of($results[0]['diagnostics'][0]['diagnosis_name'])->title() }}</b><br>
                    <b>&nbsp;</b>
                    @endif
                </td>

                <td class="bottom_text_date">
                    {{-- Fecha de impresión: {{ \Carbon\Carbon::now()->isoFormat('DD/MM/YYYY') }} --}}
                </td>
            </tr>
        </table>

    </div>
    <!-- Fin de la sección referente a la firma -->
    <br>
    <!-- Inicio de la sección del membrete inferior -->
    <div class="bottom_legend_container">
        <div class="container_legend">
            <p class="bottom_legend_text">La interpretación del presente resultado queda a reserva del personal médico solicitante.</p>
            <p class="bottom_legend_text">“El presente documento está protegido por lo establecido en la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados (LGPDPPSO), cualquier uso mal intencionado será acreedor a las sanciones que de ella emanen”.</p>
            {{-- <p class="bottom_legend_text">Queda prohibida su reproducción total o parcial.</p> --}}

        </div>

        <br>

        <div class="container_footer">
            <div class="container_footer_text">
                <p class="bottom_text_address">
                    Calle Encinos 41, Colonia Miguel Hidalgo 4ta Secc.Alcaldía Tlalpan, <br>
                    C.P. 14250, Ciudad de México <br>
                    T. 5550381700 ext. 6875-6876-6877-6878-6879

                </p>

                <p class="bottom_text_legend">
                    Página {PAGENO} de {nbpg}<br><br><br>
                    LESP-VE-FO-037/008 <br>
                    Vigente a partir del 27/01/2025
                </p>
            </div>

            <div class="bottom_legend_img">
                <img src=" {{asset('img/mujeres.png')}}" class="logo_footer logo_institutional_mujeres" class="img_footer">

                <img src=" {{asset('img/tenochtitlan.png')}}" class="logo_footer logo_institutional_year" class="img_footer">
            </div>


        </div>
    </div>
    <!-- Fin de la sección del membrete inferior -->
</htmlpagefooter>
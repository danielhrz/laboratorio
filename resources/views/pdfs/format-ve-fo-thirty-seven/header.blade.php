<htmlpageheader name="page-header">
    <div class="letterhead_container">
        <div class="container_section container_logo_institution">
            <img src="{{asset('img/logo.jpeg')}}" class="logo_institutional">
        </div>

        <div class="container_section container_logo_lve">
            <img src="{{asset('panel/img/logo-laboratorio.png')}}" class="logo_lve">
        </div>

        <div class="container_section legend">
            <p class="institution">SECRETARÍA DE SALUD DE LA CIUDAD DE MÉXICO</p>
            <p>SERVICIOS DE SALUD PÚBLICA DE LA CIUDAD DE MÉXICO</p>
            <p>DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA</p>
        </div>

        <div class="container_section container_title">
            <p>LABORATORIO DE VIGILANCIA EPIDEMIOLÓGICA DE LA CIUDAD DE MÉXICO INFORME DE RESULTADOS</p>
        </div>

    </div>

    <div class="letterhead_section_data">
        <table class="letterhead_section_data_table">
            <tr>
                <td class="letterhead_table_td_tittle border_top border_left border_right">Destinatario:</td>
                <td class="border_top border_left border_right">
                    <table class="letterhead_section_data_table">
                        <tr>
                            <td class="letterhead_table_td_tittle">Unidad solicitante:</td>
                            @if (!isset($judi))
                            <td class="letterhead_table_td_tittle" align="right">{{ $oficio }}/{{ $exit }}</td>
                            @endif
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="letterhead_table_td_secondary border_left border_right border_bottom">
                    <p>{{ $receiver->encargado }}</p>
                    <p>{{ $receiver->position }}</p>
                </td>
                <td class="letterhead_table_td_secondary border_left border_right border_bottom">
                    <p>{{ $receiver->jurisdiccion }}</p>
                </td>
            </tr>
            <tr>
                <td class="letterhead_table_td_tittle  border_left border_right border_bottom">
                    Diagnóstico solicitado: {{ $results[0]['diagnostics'][0]['diagnosis_name'] }}
                </td>
                @if ($results[0]['diagnostics'][0]['diagnosis_name'] == 'VIRUS RESPIRATORIOS')
                @php
                $isPendiente = false;
                $emissionDate = '';
                foreach ($results as $result) {
                if($result['status_result'] == 'PENDIENTE' ) {
                $isPendiente = true;
                $emissionDate = \Carbon\Carbon::parse($result['date_created_result'])->isoFormat('DD/MM/YYYY');
                break;
                } else if(!is_null($result['date_complete_operational_status']) && $emissionDate == '' ) {
                $emissionDate = \Carbon\Carbon::parse($result['date_complete_operational_status'])->isoFormat('DD/MM/YYYY');
                }
                }
                @endphp

                <td class="letterhead_table_td_tittle border_left border_right border_bottom">
                    Fecha de emisión: {{ $emissionDate }}
                </td>
                @else
                <td class="letterhead_table_td_tittle border_left border_right border_bottom">
                    Fecha de emisión: {{ (is_null($results[0]['date_complete_operational_status'])) ? '' : \Carbon\Carbon::parse($results[0]['date_complete_operational_status'])->isoFormat('DD/MM/YYYY') }}
                </td>
                @endif
            </tr>
            <tr>
                <td colspan="2" style="height:2em;"></td>
            </tr>
            <tr>
                <td colspan="2" class="letterhead_table_td_tittle">Por este conducto se emite el resultado de la(s) muestra(s) recibida(s) el día {{ \Carbon\Carbon::parse($results[0]['fecha_recepcion'])->isoFormat('DD \d\e MMMM \d\e\l YYYY') }}, para su análisis en el LVE-CDMX proveniente del área/unidad a su digno cargo.</td>
            </tr>
        </table>
    </div>
</htmlpageheader>
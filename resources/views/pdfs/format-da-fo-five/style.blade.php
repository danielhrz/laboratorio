<style>
    .m-0 {
        margin: 0px;
    }

    .p-0 {
        padding: 0px;
    }

    .text-center {
        text-align: center;
    }

    .text-justify {
        text-align: justify;
    }

    .bold {
        font-weight: bold;
    }

    .text-sm {
        font-family: 'roboto';
        font-size: 8pt;
    }

    .text-base {
        font-family: 'roboto';
        font-size: 9pt;
    }

    .text-lg {
        font-family: 'roboto';
        font-size: 10pt;
    }

    /*========== Inicio de estilos del membrete ==========*/
    .container_section {
        float: left;
        margin-bottom: 0.1em;
        padding: 0.5em 0em;
        box-sizing: border-box;
    }

    .container_logo_institution {
        width: 220px;
    }

    .logo_institutional {
        width: 160px;
        height: auto;
    }

    .container_section.container_logo_lve {
        margin-left: 2.5em;
        padding-top: 1.5em;
        width: 70px;
    }

    .logo_lve {
        width: auto;
        height: 60px;
    }

    .container_section.legend {
        padding-top: 2em;
        margin-left: 2.5em;
        width: 340px;
    }

    .container_section_presente {
        margin-left: 0em;
        width: 290px;
    }

    .text_presente {
        text-align: left;
        font-family: 'roboto';
        font-size: 9pt;
        margin: 0px;
        margin-top: 3px;
    }

    .container_section_officio {
        margin-left: 20em;
        width: 400px;
        margin-top: -2em;
    }

    .text_officio {
        text-align: right;
        margin: 0px;
        margin-top: 3px;
        font-family: 'roboto';
    }

    .container_section.legend .institution {
        font-size: 8.5pt;
        font-weight: bold;
        font-family: 'roboto';
    }

    .container_section.container_title {
        width: 460px;
    }

    .container_title p {
        font-size: 9pt;
        font-family: 'roboto';
        margin: 0px;
        text-align: center;
        font-weight: bold;
    }

    .container-table th {
        border: solid 1px #000;
    }

    .container-table td,
    .container-table th {
        border: solid 1px #000;
        text-align: center;
        padding: 3px 8px;
    }

    /* ++++++++++++  Inicio de estilos del pie de pagina   ++++++++++++++ */

    .container_footer {
        border-top: solid 4px #9d2148;
    }

    .bottom_letterhead_section {
        float: left;
        margin-bottom: 0.1em;
        box-sizing: border-box;
        position: absolute;
        bottom: 3%;
    }

    .container_legend {
        width: 310px;
        margin: auto;
    }

    .bottom_legend_text {
        font-family: 'roboto';
        text-align: center;
        font-size: 7pt;
        font-weight: bold;
    }

    .bottom_text_address {
        width: 200px;
        text-align: start;
        font-size: 7pt;
        font-family: 'roboto';
        font-weight: bold;
        color: #55585a;
        float: left;
    }

    .bottom_text_legend {
        width: 210px;
        text-align: center;
        font-size: 7pt;
        font-family: 'roboto';
        font-weight: bold;
    }

    .container_footer_text {
        float: left;
        width: 410px;
    }

    .bottom_legend_img {
        float: right;
        width: 300px;
        padding: 0px;
        margin: 0px;
    }

    .img_footer {
        float: left;
        height: 75px;
        width: auto;
        padding: 0px;
        margin: 0px;
    }


    .bottom_letterhead_officio {
        width: 90%;
        font-weight: bold;
        font-family: 'roboto';
        font-size: 7pt;
        text-align: right;
    }

    @page {
        header: page-header;
        footer: page-footer;
    }
</style>
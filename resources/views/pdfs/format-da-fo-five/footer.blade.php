<htmlpagefooter name="page-footer">
    <div class="container_bottom">
        <p class="bold text-base">Sin otro particular, reciba un cordial saludo.</p>
        <br><br>
        <p class="text-center bold text-base">A T E N T A M E N T E</p>

        <br><br>

        <p class="text-center bold text-base">LIC. MARIANA ANGÉLICA MARTÍNEZ ARIAS</p>
        <p class="text-center bold text-base">COORDINADORA DE ENLACE ADMNISTRATIVA</p>

    </div>

    <br><br>

    <p class="bottom_letterhead_officio"> Oficio N VE/{{ $oficio }} </p>
    <br>

    <!-- Inicio de la sección del membrete inferior -->
    <div class="container_footer">
        <div class="container_footer_text">
            <p class="bottom_text_address">
                Calle Encinos 41, Colonia Miguel Hidalgo 4ta Secc.Alcaldía Tlalpan, <br>
                C.P. 14250, Ciudad de México <br>
                T. 5550381700 ext. 6875-6876-6877-6878-6879
            </p>

            <p class="bottom_text_legend">
                Página {PAGENO} de {nbpg}<br><br><br>
                LESP-DA-FO-005/008 <br>
                Vigente a partir de: 27/01/2025
            </p>
        </div>

        <div class="bottom_legend_img">
            <img src=" {{asset('img/mujeres.png')}}" class="logo_footer logo_institutional_mujeres" class="img_footer">

            <img src=" {{asset('img/tenochtitlan.png')}}" class="logo_footer logo_institutional_year" class="img_footer">
        </div>

    </div>
    <!-- Fin de la sección del membrete inferior -->
</htmlpagefooter>
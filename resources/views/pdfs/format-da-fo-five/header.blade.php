<htmlpageheader name="page-header">
    <br>
    <div class="letterhead_container">
        <div class="container_section container_logo_institution">
            <img src="{{asset('img/logo.jpeg')}}" class="logo_institutional">
        </div>

        <div class="container_section container_logo_lve">
        </div>

        <div class="container_section legend">
            <p class="bold text-lg m-0 p-0">SECRETARÍA DE SALUD DE LA CIUDAD DE MÉXICO</p>
            <p class="bold text-sm m-0 p-0">SERVICIOS DE SALUD PÚBLICA DE LA CIUDAD DE MÉXICO</p>
            <p class="bold text-sm m-0 p-0">DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA</p>
        </div>


        <div class="container_section container_section_officio">
            <p class="text_officio bold text-base">Ciudad de México a {{ \Carbon\Carbon::now()->isoFormat('D') }} de {{ \Carbon\Carbon::now()->isoFormat('MMMM') }} de {{ \Carbon\Carbon::now()->isoFormat('Y') }}</p>
            <p class="text_officio bold text-base">OFICIO: VE/{{ strtoupper($exit) }}</p>
            <p class="text_officio bold text-base">ASUNTO: {{ $affair }}</p>
        </div>

        <div class="container_section container_section_presente">
            <p class="text_presente bold text-justify">{{ $receiver->encargado }}</p>
            <p class="text_presente bold text-justify">{{ $receiver->position }}</p>
            <p class="text_presente bold text-justify">{{ $receiver->address }}</p>
            <p class="text_presente bold">P R E S E N T E</p>
        </div>

    </div>


    <div class="section-text-header" style="box-sizing: border-box; padding-left: 0em 0.1em; display:block;">
        <p class="bold text-base">Por medio del presente, se emite el resultado de la(s) muestra(s) enviada(s) el dia {{ \Carbon\Carbon::parse($results[0]->fecha_recepcion)->isoFormat('DD \d\e MMMM') }} del presente año para su análisis en el <b>LVE-CDMX</b> proveniente del área/unidad a su digno cargo.</p>
    </div>
</htmlpageheader>
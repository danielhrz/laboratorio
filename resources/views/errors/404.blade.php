@extends('layouts.panel')
@section('content')
<div class="p-6 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <h6 class="justify-center text-center mb-2 text-5xl font-semibold text-slate-700 mb-4 flex items-center"><i class="fa-regular fa-face-frown fa-5x"></i></h6>
    <h6 class="justify-center text-center mb-2 text-5xl font-semibold text-slate-700 mb-4 flex items-center">404</h6>
    <h6 class="justify-center text-center mb-2 text-2xl font-semibold text-slate-700 mb-4 flex items-center">No cuentas con los privilegios para acceder a esta ruta</h6>
    <h4 class="justify-center text-center mb-2 text-xl font-semibold text-slate-700 mb-4 flex items-center">Solicita los permisos con el Administrador</h4>
    <div class="flex items-center justify-center p-6 space-x-2">
    <a href="main_admin" class="justify-center inline-flex items-center py-2 px-3 text-sm font-medium text-center text-green-700 hover:text-white border border-green-700 hover:bg-green-800 focus:ring-2 hover:shadow-lg hover:shadow-green-500/50 focus:outline-none focus:ring-green-300 font-medium rounded">
        Regresar
        <svg aria-hidden="true" class="ml-2 -mr-1 w-4 h-4" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
    </a>
    </div>
</div>
@endsection
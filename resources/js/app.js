import 'flowbite';
import swal from 'sweetalert2';

$(document).ready(function () {
    $('.button-after').click(function () {
        window.history.back();
    });

    window.Swal = swal;
});
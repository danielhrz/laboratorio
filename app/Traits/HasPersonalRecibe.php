<?php

namespace App\Traits;


trait HasPersonalRecibe
{
    public static function personalRecibeOptions()
    {
        return collect([
            self::PERSONAL_UNO,
            self::PERSONAL_DOS,
            /*self::PERSONAL_TRES,*/
            self::PERSONAL_CUATRO,
            self::PERSONAL_CINCO, 
            /* self::PERSONAL_SEIS, */
            /* self::PERSONAL_SIETE, */
            /* self::PERSONAL_OCHO, */
            self::PERSONAL_NUEVE,
/*             self::PERSONAL_DIES, */
            /* self::PERSONAL_ONCE, */
            /* self::PERSONAL_DOCE, */
            self::PERSONAL_TRECE,
            self::PERSONAL_CATORCE,
        ]);
    }
}
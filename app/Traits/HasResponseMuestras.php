<?php

namespace App\Traits;


trait HasResponseMuestras
{
    public static function responseMuestrasOptions()
    {
        return collect([
            self::RESPONSE_MUESTRAS_SI,
            self::RESPONSE_MUESTRAS_NO,
        ]);
    }
}

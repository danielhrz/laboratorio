<?php

namespace App\Traits;
trait HasMotivosRechazos
{
    public static function motivosRechazosOptions()
    {
        return collect([
            self::R_1,
            self::R_2,
            self::R_3,
            self::R_4,
            self::R_5,
            self::R_6,
            self::R_7,
            self::R_8,
            self::R_9,
            self::R_10,
            self::R_11,
            self::R_12,
            self::R_13,
            self::R_14,
            self::R_15,
            self::R_16,
            self::R_17,
            self::R_18,
            self::R_19,
            self::R_20,
            self::R_21,
            self::R_22,
            self::R_23,
/*             self::R_24, */
            self::R_25,
            self::R_26,
            self::R_27,
            self::R_28,
            self::R_29,
            self::R_30,
            self::R_31,
            self::R_32,
            self::R_33,
            self::R_34,
            self::R_35,
            self::R_36,
            self::R_37,
            self::R_38,
            self::R_39,
            self::R_40,
            self::R_41,
            self::R_42,
            self::R_43,
            self::R_44,
        ]);
    }
}
<?php

namespace App\Traits;
trait HasDiagnosticos
{
    public static function diagnosticosOptions()
    {
        return collect([
            self::DX_1,
            self::DX_2,
            self::DX_3,
            self::DX_4,
            self::DX_5,
            self::DX_6,
            self::DX_7,
            self::DX_8,
            self::DX_9,
            self::DX_10,
            self::DX_11,
            self::DX_12,
            self::DX_13,
            self::DX_14,
            self::DX_15,
            self::DX_16,
            self::DX_17,
            self::DX_18,
            self::DX_19,
            self::DX_20,
            self::DX_21,
            self::DX_22,
            self::DX_23,
            self::DX_24,
            self::DX_25,
            self::DX_26,
        ]);
    }
}
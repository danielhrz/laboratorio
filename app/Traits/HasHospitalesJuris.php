<?php

namespace App\Traits;


trait HasHospitalesJuris
{
    public static function hospitalesJurisOptions()
    {
        return collect([
            self::HOSPITALES_1,
            self::HOSPITALES_2,
            self::HOSPITALES_3,
            self::HOSPITALES_4,
            self::HOSPITALES_5,
            self::HOSPITALES_6,
            self::HOSPITALES_7,
            self::HOSPITALES_8,
            self::HOSPITALES_9,
            self::HOSPITALES_10,
            self::HOSPITALES_11,
            self::HOSPITALES_12,
            self::HOSPITALES_13,
            self::HOSPITALES_14,
            self::HOSPITALES_15,
            self::HOSPITALES_16,
            self::HOSPITALES_17,
            self::HOSPITALES_18,
            self::HOSPITALES_19,
            self::HOSPITALES_20,
            self::HOSPITALES_21,
            self::HOSPITALES_22,
            self::HOSPITALES_23,
            self::HOSPITALES_24,
            self::HOSPITALES_25,
            self::HOSPITALES_26,
            self::HOSPITALES_27,
            self::HOSPITALES_28,
            self::HOSPITALES_29,
            self::HOSPITALES_30,
            self::HOSPITALES_31,
            self::HOSPITALES_32,
            self::HOSPITALES_33,
            self::HOSPITALES_34,
            self::HOSPITALES_35,
            self::HOSPITALES_36,
            self::HOSPITALES_37,
            self::HOSPITALES_38,
            self::HOSPITALES_39,
            self::HOSPITALES_40,
            self::HOSPITALES_41,
            self::HOSPITALES_42,
            self::HOSPITALES_43,
            self::HOSPITALES_44,
            self::HOSPITALES_45,
            self::HOSPITALES_46,
            self::HOSPITALES_47,
            self::HOSPITALES_48,
            self::HOSPITALES_49,
            self::HOSPITALES_50,
            self::HOSPITALES_51,
            self::HOSPITALES_52,
            self::HOSPITALES_53,
            self::HOSPITALES_54,
            self::HOSPITALES_55,
            self::HOSPITALES_56,
        ]);
    }
}
<?php

namespace App\Traits;


trait HasStatus
{
    public static function statusOptions()
    {
        return collect([
            self::STATUS_1,
            self::STATUS_2,
            self::STATUS_3,
            self::STATUS_4,
        ]);
    }
}
<?php

namespace App\Traits;


trait HasTipoMuestras
{
    public static function tipoMuestrasOptions()
    {
        return collect([
            self::MUESTRAS_EXUDADO_FARÍNGEO,
            self::MUESTRAS_EXUDADO_NASOFARINGEO,
            self::MUESTRAS_LAMINILLA,
            self::MUESTRAS_EXPECTORACIÓN,
            self::MUESTRAS_BIOPSIA,
            self::MUESTRAS_SANGRE_TOTAL,
            self::MUESTRAS_MATERIA_FECAL,
            self::MUESTRAS_LAMINILLA_SANGRE_TOTAL,
            self::MUESTRAS_LAVADO_BRONQUIAL,
            self::MUESTRAS_LAVADO_GASTRICO,
            self::MUESTRAS_ENCÉFALO_CANINO,
            self::MUESTRAS_ENCÉFALO_FELINO,
            self::MUESTRAS_SALIVA,
            self::MUESTRAS_LÍQUIDO_CEFALORRAQUÍDEO,
            self::MUESTRAS_LIQUÍDO_PLEURAL,
            self::MUESTRAS_OTROS_LÍQUIDOS,
            self::MUESTRAS_EXUDADO_FARINGEO_EXUDADO_NASOFARINGEO,
            self::MUESTRAS_ESPÉCIMEN_COMPLETO,
            self::MUESTRAS_NA,
            self::MUESTRAS_SD,
            self::MUESTRAS_RNA,
            self::MUESTRAS_CLINICA,
            self::MUESTRAS_SUERO,
            self::MUESTRAS_QUIROPTERO,
            self::MUESTRAS_CEPA,
            self::MUESTRAS_ORINA,
            self::MUESTRAS_LÍQUIDO_AMNIÓTICO,
            self::MUESTRAS_NECROPSIA,
            self::MUESTRAS_ADN,
            self::MUESTRAS_ENCÉFALO,
            self::MUESTRAS_EXTRACTO,
            self::MUESTRAS_FROTIS,
            self::MUESTRAS_EXTENDIDO_MEDULA_OSEA,
            self::MUESTRAS_IMPRONTA_PAPEL_WHATMAN,
        ]);
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rechazos extends Model
{
    protected $table = 'motivos_rechazos';

    public function DatosFederales()
    {
        return $this->belongsTo(DatosFederales::class);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class MessageReceived extends Mailable
{
    use Queueable, SerializesModels;
    public $file;
    public $user;

    public $subject = 'Reporte de muestras rechazadas';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $filename, $oficio )
    {
        $this->file = $filename;
        $this->user = Auth::user();
        $this->oficio = $oficio;
        $this->subject = $oficio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $path = storage_path('app/public/' . $this->file);
        return $this->view('main_admin.exports.message-received', ['oficio' => $this->oficio] )->attach( $path );
    }
}

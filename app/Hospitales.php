<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospitales extends Model
{
    protected $table = 'hospitales_jurisdicciones';

    public function RecepcionMuestras()
    {
        return $this->belongsTo(RecepcionMuestras::class);
    }

    public function federales()
    {
        return $this->belongsTo(DatosFederales::class);
    }
}

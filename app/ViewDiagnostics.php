<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewDiagnostics extends Model
{
    protected $table = 'view_diagnostics';
}

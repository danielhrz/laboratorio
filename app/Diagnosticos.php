<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnosticos extends Model
{
    protected $table = 'diagnosticos';

    public function DatosFederales()
    {
        return $this->belongsTo(DatosFederales::class);
    }
}

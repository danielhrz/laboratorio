<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\Exportable;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;


class ReportFederalData extends DefaultValueBinder implements WithStyles, FromView, WithDrawings, WithTitle, WithColumnWidths, WithCustomValueBinder
{
    use Exportable;

    public $numRows = 0;
    static $componentLivewire;


    public function __construct( $data, $diagnostico, $reception )
    {
        $this->documentos = $data;
        $this->diagnosis = $diagnostico;
        $this->reception = $reception;
    }

    public function view(): view
    {
        $this->numRows = $this->documentos->count();

        return view('main_admin.exports.datos', [
            'documentos' => $this->documentos,
            'diagnostico' => $this->diagnosis,
            'reception' => $this->reception
        ]);
    }

    

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('C2:F3')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ]);

        $sheet->getStyle('G3')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);

        $sheet->getStyle('B6:B10')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);
        $sheet->getStyle('C10')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        $sheet->getStyle('E6:E10')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ]
        ]);

        $sheet->getStyle('F8')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);

        $sheet->getStyle('F10')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);
        $sheet->getRowDimension('12')->setRowHeight(20);

        $sheet->getStyle('A12:H12')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
            ],
        ]);

        $temp = $this->numRows +  13;
        
        if($temp > 13 ) {
            for( $i = 13; $i < $temp; $i++) {
                $sheet->getStyle('A' . $i . ':H' . $i)->applyFromArray(['borders' => [
                        'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000'],
                    ],
                ],
            ]);
            }
        } else {
            $sheet->getStyle('A13:H13')->applyFromArray(['borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000'],
                    ],
                ],
            ]);
        }

        $temp = $temp + 2;
        $sheet->getRowDimension($temp)->setRowHeight(20);
        $sheet->getStyle('A' . $temp . ':H' . ($temp+4))->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ]
            ],
        ]);
        $sheet->getStyle('A' . $temp . ':H' . $temp)->applyFromArray([
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE,
                    'color' => ['argb' => '000'],
                ],
            ]
        ]);    
        $sheet->getStyle('A' . ($temp+1) . ':H' . ($temp+1))->applyFromArray([
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE,
                    'color' => ['argb' => '000'],
                ],
            ]
        ]);        
    }

    public function drawings()
    {
        $gdImage = @imagecreatetruecolor(30, 20) or die('Cannot Initialize new GD image stream');
        $textColor = imagecolorallocate($gdImage, 0, 0, 0);
        //Dibujamos el rectángulo
        imagefilledrectangle($gdImage, 2, 2, 27, 17, imagecolorallocate($gdImage, 255, 255, 255));
        imagestring($gdImage, 1, 10, 5,  ' ', $textColor);

        $drawing = new MemoryDrawing();
        $drawing->setName('Rectángulo');
        $drawing->setDescription('Rectángulo para la X');
        $drawing->setImageResource($gdImage);
        $drawing->setRenderingFunction( MemoryDrawing::RENDERING_JPEG);
        $drawing->setMimeType( MemoryDrawing::MIMETYPE_DEFAULT);
        $drawing->setCoordinates('B10');
        $drawing->setOffsetX(70);

        $drawing2 = new MemoryDrawing();
        $drawing2->setName('Rectángulo');
        $drawing2->setDescription('Rectángulo para la X');
        $drawing2->setImageResource($gdImage);
        $drawing2->setRenderingFunction(MemoryDrawing::RENDERING_JPEG);
        $drawing2->setMimeType(MemoryDrawing::MIMETYPE_DEFAULT);
        $drawing2->setCoordinates('C10');
        $drawing2->setOffsetX(70);

        $servicios = new Drawing();
        $servicios->setName('Logo de servicios de salud');
        $servicios->setDescription('This is my logo');
        $servicios->setPath(public_path('/panel/img/logo.png'));
        $servicios->setHeight(60);
        $servicios->setCoordinates('A1');

        $logo = new Drawing();
        $logo->setName('Logo de laboratorio');
        $logo->setDescription('This is my logo');
        $logo->setPath(public_path('/panel/img/logo-laboratorio.png'));
        $logo->setHeight(75);
        $logo->setCoordinates('C1');

        return [$drawing, $drawing2, $servicios, $logo];
    }

    public function title(): string
    {
        return 'FO-016-ORDINARIAS';
    }

    public function columnWidths(): array
    {
        return ['A' => 20, 'B' => 20, 'C' => 25, 'D' => 20, 'E' => 35, 'F' => 18, 'G' => 30, 'H' => 30, 'I' => 35, 'J' => 35, 'K' => 35, 'L' => 20, 'M' => 15, 'N' => 20, 'O' => 35];
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

}

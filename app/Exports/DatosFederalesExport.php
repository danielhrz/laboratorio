<?php

namespace App\Exports;

use App\DatosFederales;
use Maatwebsite\Excel\Concerns\FromCollection;

class DatosFederalesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DatosFederales::all()->where('is_active', 1);
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Illuminate\Contracts\View\View;


use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\Exportable;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class PaludismoResultExport extends DefaultValueBinder implements WithStyles, FromView, WithColumnWidths, WithTitle, WithCustomValueBinder, WithDrawings
{
    use Exportable;

    public $numRows = 0;
    private $title, $data;
    public function __construct($data)
    {
        //$this->title = $title;
        $this->data = $data;
    }

    /**
     * Función para cargar una vista html y generar el excel
     */
    public function view(): view
    {
        $this->numRows = $this->data->count();

        return view('pdf.paludismo-result-export', [
            'samples' => $this->data,
            //'title' => $this->title
        ]);
    }

    public function drawings()
    {
        
        $logo = new Drawing();
        $logo->setName('Logo de laboratorio');
        $logo->setDescription('This is my logo');
        $logo->setPath(public_path('/panel/img/logo-laboratorio.png'));
        $logo->setHeight(75);
        $logo->setCoordinates('D1');
        $logo->setOffsetX(50);
        $logo->setOffsetY(5);

        

        return [$logo];
    }

    /**
     * Función para generar los estilos que contendrá el archivo exportado
     */
    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:W1')->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 16
            ],
            'alignment' => [
                //'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ]
        ]);
        $sheet->getRowDimension('7')->setRowHeight(20);
        $sheet->getRowDimension('8')->setRowHeight(20);
        $sheet->getRowDimension('9')->setRowHeight(20);
        $sheet->getStyle('A7:AC9')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'F1C40F',
                ], 'endColor' => [
                    'argb' => 'F1C40F'
                ]
            ]
        ]);

        $sheet->getStyle('A' . (10) . ':AC' . ($this->numRows +  9))->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000'],
                ],
            ],
        ]);


        
    }

    /**
     * Función para modificar el ancho de las columnas del excel
     */
    public function columnWidths(): array
    {
        return [
            'A' => 10, 'B' => 20, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 30, 'K' => 25,
            'L' => 30, 'M' => 20, 'N' => 20, 'O' => 30, 'P' => 20, 'Q' => 20, 'R' => 20, 'S' => 20, 'T' => 30, 'U' => 20, 'V' => 20, 'W' => 20,
            'X' => 25, 'Y' => 25, 'Z' => 25, 'AA' => 25, 'AB' => 25, 'AC' => 40
        ];
    }

    /**
     * Función para asignar el titulo del documento
     */
    public function title(): string
    {
        return 'Paludismo';
    }

    /**
     * Función que fuerza a los campos enteros a guardarlos como string en el archivo
     */
    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}

<?php

namespace App\Exports\Charts;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class RejectedExport implements WithStyles, FromView, WithTitle, WithColumnWidths
{
    use Exportable;
    public $labels, $diagnosticsCount, $year;
    public $numRows = 0;

    public function __construct($labels, $diagnosticsCount, $year)
    {
        $this->labels = $labels;
        $this->diagnosticsCount = $diagnosticsCount;
        $this->year = $year;
        $this->numRows = $diagnosticsCount->count();
    }

    public function view(): view
    {
        return view('main_admin.charts.rejected-excel', [
            'labels' => $this->labels,
            'diagnosticsCount' => $this->diagnosticsCount,
            'year' => $this->year
        ]);
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('L2')->applyFromArray([
            'font' => [
                /* 'bold' => true, */
                'size' => 9,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);
        $sheet->getRowDimension('2')->setRowHeight(50);
        $sheet->getStyle('A8:N8')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'F1C40F',
                ], 'endColor' => [
                    'argb' => 'F1C40F'
                ]
            ]
        ]);

        $sheet->getRowDimension('8')->setRowHeight(24);
        $sheet->getStyle('B4:D6')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);


        $temp = $this->numRows +  9;
        for ($i = 9; $i <= $temp; $i++) {
            $sheet->getStyle('A' . $i . ':N' . $i)->applyFromArray([
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000'],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ],
                ],
            ]);
            $sheet->setCellValue('N' . $i, "=SUM(B$i:M$i)");
            $sheet->getCell('N' . $i)->getCalculatedValue();
        }
        //Realizamos las sumas de los meses
        $sheet->setCellValue('B' . $temp, "=SUM(B9:B" . ($temp - 1) . ")");
        $sheet->getCell('B' . $temp)->getCalculatedValue();
        $sheet->setCellValue('C' . $temp, "=SUM(C9:C" . ($temp - 1) . ")");
        $sheet->getCell('C' . $temp)->getCalculatedValue();
        $sheet->setCellValue('D' . $temp, "=SUM(D9:D" . ($temp - 1) . ")");
        $sheet->getCell('D' . $temp)->getCalculatedValue();
        $sheet->setCellValue('E' . $temp, "=SUM(E9:E" . ($temp - 1) . ")");
        $sheet->getCell('E' . $temp)->getCalculatedValue();
        $sheet->setCellValue('F' . $temp, "=SUM(F9:F" . ($temp - 1) . ")");
        $sheet->getCell('F' . $temp)->getCalculatedValue();
        $sheet->setCellValue('G' . $temp, "=SUM(G9:G" . ($temp - 1) . ")");
        $sheet->getCell('G' . $temp)->getCalculatedValue();
        $sheet->setCellValue('H' . $temp, "=SUM(H9:H" . ($temp - 1) . ")");
        $sheet->getCell('H' . $temp)->getCalculatedValue();
        $sheet->setCellValue('I' . $temp, "=SUM(I9:I" . ($temp - 1) . ")");
        $sheet->getCell('I' . $temp)->getCalculatedValue();
        $sheet->setCellValue('J' . $temp, "=SUM(J9:J" . ($temp - 1) . ")");
        $sheet->getCell('J' . $temp)->getCalculatedValue();
        $sheet->setCellValue('K' . $temp, "=SUM(K9:K" . ($temp - 1) . ")");
        $sheet->getCell('K' . $temp)->getCalculatedValue();
        $sheet->setCellValue('L' . $temp, "=SUM(L9:L" . ($temp - 1) . ")");
        $sheet->getCell('L' . $temp)->getCalculatedValue();
        $sheet->setCellValue('M' . $temp, "=SUM(M9:M" . ($temp - 1) . ")");
        $sheet->getCell('M' . $temp)->getCalculatedValue();
    }

    public function title(): string
    {
        return 'Rechazadas';
    }

    public function columnWidths(): array
    {
        return ['A' => 30, 'B' => 18, 'C' => 18, 'D' => 18, 'E' => 18, 'F' => 18, 'G' => 18, 'H' => 18, 'I' => 18, 'J' => 18, 'K' => 18, 'L' => 18, 'M' => 18];
    }
}

<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Style\Color;

class RejectedAndFinalizedExport implements WithStyles, FromView, WithTitle, WithColumnWidths
{
    use Exportable;

    public $numRows = 0;
    private $data, $date_start_samples, $date_end_samples;

    public function __construct($data, $date_start_samples, $date_end_samples)
    {
        $this->data = $data;
        $this->date_start_samples = $date_start_samples;
        $this->date_end_samples = $date_end_samples;
    }

    public function view(): view
    {
        $this->numRows = $this->data->count();

        return view('main_admin.exports.rejected-and-finalized', [
            'samples' => $this->data,
            'date_start_samples' => $this->date_start_samples,
            'date_end_samples' => $this->date_end_samples,
        ]);
    }

    /**
     * Función para generar los estilos que contendrá el archivo exportado
     */
    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('B2')->applyFromArray([
            'font' => [
                /* 'bold' => true, */
                'size' => 9,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);
        $sheet->getRowDimension('2')->setRowHeight(50);
        $sheet->getRowDimension('6')->setRowHeight(24);

        $sheet->getStyle('A6:D6')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'F1C40F',
                ], 'endColor' => [
                    'argb' => 'F1C40F'
                ]
            ]
        ]);
        $sheet->getStyle('B4:D4')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);


        $temp = $this->numRows +  7;
        for ($i = 7; $i <= $temp; $i++) {
            $sheet->getRowDimension($i)->setRowHeight(20);
            $sheet->getStyle('A' . $i . ':D' . $i)->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000'],
                    ],
                ],
            ]);
        }

        $sheet->setCellValue('C' . $temp, "=SUM(C7:C" . ($temp - 1) . ")");
        $sheet->getCell('C' . $temp)->getCalculatedValue();
        $sheet->setCellValue('D' . $temp, "=SUM(D7:D" . ($temp - 1) . ")");
        $sheet->getCell('D' . $temp)->getCalculatedValue();
    }

    /**
     * Función para modificar el ancho de las columnas del excel
     */
    public function columnWidths(): array
    {
        return [
            'A' => 10, 'B' => 48, 'C' => 16, 'D' => 16
        ];
    }

    /**
     * Función para asignar el titulo del documento
     */
    public function title(): string
    {
        return 'Muestras rechazadas y finalizadas';
    }
}

<?php

namespace App\Exports\Diagnostics;

use App\Models\ViewAppointments;
use Illuminate\Contracts\View\View;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;


use Maatwebsite\Excel\Concerns\WithTitle;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Style\Fill;

use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class Respiratorios extends DefaultValueBinder implements WithStyles, FromView, WithColumnWidths, WithTitle, WithCustomValueBinder
{
    public $numRows = 0;
    public $startDate, $endDate, $information, $jurisdictions;

    public function __construct($startDate, $endDate, $data, $juris)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->information = $data;
        $this->jurisdictions = $juris;
    }

    /**
     * Función para cargar una vista html y generar el excel
     */
    public function view(): view
    {

        return view('main_admin.diagnostic.export-respiratorios', [
            'information' => $this->information,
            'start' => $this->startDate,
            'end' => $this->endDate,
            'juris' => $this->jurisdictions
        ]);
    }

    /**
     * Función para generar los estilos que contendrá el archivo exportado
     */
    public function styles(Worksheet $sheet)
    {
        $col = ($this->jurisdictions->count() * 4) + 3;
        $sheet->getStyle('B2:D2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);


        //$sheet->getRowDimension('5')->setRowHeight(20);

        $sheet->getStyleByColumnAndRow(1, 5, $col, 6)->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'F1C40F',
                ], 'endColor' => [
                    'argb' => 'F1C40F'
                ]
            ]
        ]);

        $temp = $this->information->count() +  6;
        
        for ($i = 7; $i <= $temp; $i++) {
            $sheet->getStyleByColumnAndRow(1, 7, $col, $temp)->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000'],
                    ],
                ],
            ]);
        }
    }

    /**
     * Función para modificar el ancho de las columnas del excel
     */
    public function columnWidths(): array
    {
        return [
            'A' => 15, 'B' => 15, 'C' => 15, 'D' => 15, 'E' => 15, 'F' => 15, 'G' => 15, 'H' => 15, 'I' => 15, 'J' => 15, 'K' => 15, 'L' => 15, 'M' => 15, 'N' => 15, 'O' => 15,
            'P' => 15, 'Q' => 15, 'R' => 15, 'S' => 15, 'T' => 15, 'U' => 15, 'V' => 15, 'W' => 15, 'X' => 15, 'Y' => 15, 'Z' => 15,

            'AA' => 15, 'AB' => 15, 'AC' => 15, 'AD' => 15, 'AE' => 15, 'AF' => 15, 'AG' => 15, 'AH' => 15, 'AI' => 15, 'AJ' => 15, 'AK' => 15, 'AL' => 15, 'AM' => 15, 'AN' => 15, 'AO' => 15,
            'AP' => 15, 'AQ' => 15, 'AR' => 15, 'AS' => 15, 'AT' => 15, 'AU' => 15, 'AV' => 15, 'AW' => 15, 'AX' => 15, 'AY' => 15, 'AZ' => 15,

            'BA' => 15, 'BB' => 15, 'BC' => 15, 'BD' => 15, 'BE' => 15, 'BF' => 15, 'BG' => 15, 'BH' => 15, 'BI' => 15, 'BJ' => 15, 'BK' => 15, 'BL' => 15, 'BM' => 15, 'BN' => 15, 'BO' => 15,
            'BP' => 15, 'BQ' => 15, 'BR' => 15, 'BS' => 15, 'BT' => 15, 'BU' => 15, 'BV' => 15, 'BW' => 15, 'BX' => 15, 'BY' => 15, 'BZ' => 15,

            'CA' => 15, 'CB' => 15, 'CC' => 15, 'CD' => 15, 'CE' => 15, 'CF' => 15, 'CG' => 15, 'CH' => 15, 'CI' => 15, 'CJ' => 15, 'CK' => 15, 'CL' => 15, 'CM' => 15, 'CN' => 15, 'CO' => 15,
            'CP' => 15, 'CQ' => 15, 'CR' => 15, 'CS' => 15, 'CT' => 15, 'CU' => 15, 'CV' => 15, 'CW' => 15, 'CX' => 15, 'CY' => 15, 'CZ' => 15,

            'DA' => 15, 'DB' => 15, 'DC' => 15, 'DD' => 15, 'DE' => 15, 'DF' => 15, 'DG' => 15, 'DH' => 15, 'DI' => 15, 'DJ' => 15, 'DK' => 15, 'DL' => 15, 'DM' => 15, 'DN' => 15, 'DO' => 15,
            'DP' => 15, 'DQ' => 15, 'DR' => 15, 'DS' => 15, 'DT' => 15, 'DU' => 15, 'DV' => 15, 'DW' => 15, 'DX' => 15, 'DY' => 15, 'DZ' => 15,

            'DA' => 15, 'EB' => 15, 'EC' => 15, 'ED' => 15, 'EE' => 15, 'EF' => 15, 'EG' => 15, 'EH' => 15, 'EI' => 15, 'EJ' => 15, 'EK' => 15, 'EL' => 15, 'EM' => 15, 'EN' => 15, 'EO' => 15,
            'EP' => 15, 'EQ' => 15, 'ER' => 15, 'ES' => 15, 'ET' => 15, 'EU' => 15, 'EV' => 15, 'EW' => 15, 'EX' => 15, 'EY' => 15, 'EZ' => 15,
        ];
    }

    /**
     * Función para asignar el titulo del documento
     */
    public function title(): string
    {
        return 'Detalles de resultados';
    }

    /**
     * Función que fuerza a los campos enteros a guardarlos como string en el archivo
     */
    public function bindValue(Cell $cell, $value)
    {
        /* if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        } */

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}

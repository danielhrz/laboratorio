<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Destinatario;


class RechazadosExport implements WithStyles, FromView, WithDrawings, WithTitle, WithColumnWidths
{

    use Exportable;

    public $numRows = 0;
    static $componentLivewire;


    public function __construct( $datosFederales, $infoBasica, $oficio, $judi, $observacion, $resp )
    {
        $this->documentos = $datosFederales;
        $this->oficio = $oficio;
        $this->info = $infoBasica;
        $this->destinatario = $judi;
        $this->observaciones = $observacion;
        $this->responsable = $resp;
    }

    public function view(): view
    {
        $this->numRows = $this->documentos->count();
        return view('main_admin.exports.rechazos-excel', [
            'documentos' => $this->documentos,
            'oficio' => $this->oficio,
            'basica' => $this->info,
            'destinatario' => $this->destinatario,
            'observaciones' => $this->observaciones,
            'responsable' => $this->responsable
        ]);
    }

    

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A6:A8')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ]);
        $sheet->getStyle('I2')->applyFromArray([
            'font' => [
                /* 'bold' => true, */
                'size' => 9,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);
        $sheet->getStyle('B3')->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 12,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);

        $sheet->getStyle('K3')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);

        $sheet->getStyle('B9')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);

        $sheet->getStyle('J6:J7')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ]);
        $sheet->getStyle('K6:K7')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ]);

        $sheet->getStyle('J8')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ]);
        $sheet->getRowDimension('2')->setRowHeight(45);
        $sheet->getRowDimension('3')->setRowHeight(47);
        $sheet->getRowDimension('9')->setRowHeight(40);

        $sheet->getStyle('A11:L11')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
            ],
        ]);

        $temp = $this->numRows +  12;
        for( $i = 12; $i < $temp; $i++) {
            $sheet->getStyle('A' . $i . ':L' . $i)->applyFromArray(['borders' => [
                    'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000'],
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
            ],
        ]);
        }

        $sheet->getStyle('A'.$temp.':L'.$temp)->applyFromArray([
            'font' => [
                'bold' => true,
            ]
        ]);

        for($i = 1; $i <= 1; $i++) {
            $temp = $temp + 1;

            $sheet->getRowDimension($temp)->setRowHeight(60);

            $sheet->getStyle('A' . $temp)->applyFromArray([
                'font' => [
                    'bold' => true,
                ]
            ]);
            $sheet->getStyle('A'.$temp.':L'.$temp)->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                        'color' => ['argb' => '000'],
                    ],
                ],
            ]);
        }
        $sheet->getStyle('A' . ($temp + 2). ':L' . ($temp + 2))->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
            ],
        ]);

        $sheet->getStyle('A' . ($temp + 1) . ':L' . ($temp + 1))->applyFromArray([
            'font' => [
                'underline' => true
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM,
                'wrapText' => true,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],
                /*'vertical' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    'color' => ['argb' => '000'],
                ],*/
            ],
        ]);
        $sheet->getRowDimension(($temp + 1))->setRowHeight(60);

        $sheet->getStyle('A' . ($temp + 2))->applyFromArray([
            'font' => [
                'bold' => true,
            ]
        ]);

        $sheet->getStyle('A' . ($temp + 4) .':L' . ($temp + 5))->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);
        $sheet->getRowDimension($temp+5)->setRowHeight(40);
        
    }

    public function drawings()
    {
        $servicios = new Drawing();
        $servicios->setName('Logo de servicios de salud');
        $servicios->setDescription('This is my logo');
        $servicios->setPath(public_path('/panel/img/logo2.png'));
        $servicios->setHeight(60);
        $servicios->setOffsetY(10);
        $servicios->setCoordinates('A1');

        $logo = new Drawing();
        $logo->setName('Logo de laboratorio');
        $logo->setDescription('This is my logo');
        $logo->setPath(public_path('/panel/img/logo-laboratorio.png'));
        $logo->setHeight(75);
        $logo->setCoordinates('E1');
        $logo->setOffsetX(50);
        $logo->setOffsetY(5);

        $img = new Drawing();
        $img->setName('img de laboratorio');
        $img->setDescription('This is my img');
        $img->setPath(public_path('/panel/img/image012.png'));
        $img->setWidth(145);
        $img->setCoordinates('K3');
        $img->setOffsetY(1);

        return [$servicios, $logo, $img];
    }

    public function title(): string
    {
        return 'RECHAZOS';
    }

    public function columnWidths(): array
    {
        return ['A' => 5, 'B' => 11, 'C' => 11, 'D' => 11, 'E' => 11, 'F' => 11, 'G' => 11, 'H' => 11, 'I' => 11, 'J' => 11, 'K' => 11];
    }
}

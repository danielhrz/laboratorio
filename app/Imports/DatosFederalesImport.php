<?php

namespace App\Imports;

use App\DatosFederales;
use App\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;

class DatosFederalesImport implements ToModel, WithHeadingRow, WithValidation
{
    private $id;
    public function __construct($user) {
        $this->id = $user;
    }
    /* private $rechazos;
    public function __construct()
    {
        $this->rechazos = Rechazos::pluck('id', 'nombre');
    } */
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        if($row['fecha_recepcion'] != '' ){

            $input = explode('/', $row['fecha_recepcion']);

            $input = (count($input) == 3)? $input: explode('-', $row['fecha_recepcion']);

            $input = (count($input) == 3)? $input: explode('/', date('d/m/Y'));
            
            $date = $input[2].'-'.$input[1].'-'.$input[0];

            $input1 = explode('/', $row['fecha_toma_muestra']);

            $input1 = (count($input1) == 3)? $input1: explode('-', $row['fecha_toma_muestra']);

            $input1 = (count($input1) == 3)? $input1: explode('/', date('d/m/Y'));
            
            $date1 = $input1[2].'-'.$input1[1].'-'.$input1[0];

            return new DatosFederales([
                'anio' => $row['anio'],
                'folio_lesp' => $row['folio_lesp'],
                'dx1' => $row['dx1'],
                'dx2' => $row['dx2'],
                'dx3' => $row['dx3'],
                'dx4' => $row['dx4'],
                'dx5' => $row['dx5'],
                'oficio_entrada' => $row['oficio_entrada'],
                'fecha_recepcion' => $date,
                'hora_recepcion' => $row['hora_recepcion'],
                'hospital' => $row['hospital'],
                'nombre_paciente' => $row['nombre_paciente'],
                'tipo_muestra' => $row['tipo_muestra'],
                'fecha_toma_muestra' => $date1,
                'folio_sisver' => $row['folio_sisver'],
                'persona_recibe' => $row['persona_recibe'],
                'status' => $row['status'],
                'rechazos' => $row['rechazos'],
                'observaciones' => $row['observaciones'],
                'aclaraciones_remu' => $row['aclaraciones_remu'],
                'id_users' => $this->id,
            ]);
        }else {

        }

    }

    public function rules(): array
    {
        return [
                '*.anio' => 'numeric|required',
                '*.folio_lesp' => 'required|unique:datos_federales',
                '*.dx1' => 'required',
                '*.dx2' => 'required',
                '*.dx3' => 'required',
                '*.dx4' => 'required',
                '*.dx5' => 'required',
                '*.oficio_entrada' => 'required',
                '*.fecha_recepcion' => 'required',
                '*.hora_recepcion' => 'required',
                '*.hospital' => 'required',
                '*.nombre_paciente' => 'required',
                '*.tipo_muestra' => 'required',
                '*.fecha_toma_muestra' => 'required',
                '*.folio_sisver' => 'required',
                '*.persona_recibe' => 'required',
                '*.observaciones' => 'required',
                '*.aclaraciones_remu' => 'required',
                '*.rechazos' => 'required'
            ];
    }
}
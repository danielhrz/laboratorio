<?php

namespace App\Http\Livewire\Diagnostics;

use App\DatosFederales;
use App\Exports\Diagnostics\Respiratorios as DiagnosticsRespiratorios;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Carbon\CarbonPeriod;
use Maatwebsite\Excel\Facades\Excel;

class Respiratorios extends Component
{
    public $dates = [];
    public $information = array();
    public $juris = [];
    public $startDate, $endDate;
    protected $listeners = ['tableSamples'];

    public function render()
    {
        return view('livewire.diagnostics.respiratorios');
    }

    public function tableSamples($start, $end) {

        $indexJurisdiction = 0;

        $this->startDate = Carbon::createFromFormat('Y-m-d', $start);
        $this->endDate = Carbon::createFromFormat('Y-m-d', $end);

        $dateRange = CarbonPeriod::create($this->startDate, $this->endDate);
        $dates = $dateRange->toArray();

        $this->juris = collect();
        $start = $start;// . ' 00:00:00';
        $end = $end;// . ' 23:59:59';
        $this->information = collect();
        //\DB::enableQueryLog();
        $jurisdictions = DB::table('datos_federales')->select('hospital')->where(function($query){
            $query->where('dx1', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx1', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx1', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
            ->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
            ->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
            ->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
            ->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2');
        })->whereBetween('fecha_recepcion', [$start, $end])->distinct()->get();
        //dd(\DB::getQueryLog());
        foreach ($jurisdictions as $jurisdiction) {
            $this->juris->push($jurisdiction->hospital);
        }
        $this->juris->push('TOTAL/DIA');

        $indexDate = 0;
        $total = collect();

        $totalGlobalSars = 0;
        $totalGlobalInfluenza = 0;
        $totalGlobalVsr = 0;
        $totalGlobalNegativas = 0;
        foreach ($dates as $date) {
            $temp = collect();
            $temp->push($date->isoFormat('D MMM YYYY'));
            
            if( $jurisdictions->count() > 0 ) {

                $received =
                $samplesReceived = DatosFederales::where('is_active', 1)->where(function ($query) {
                    $query->where('dx1', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx1', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx1', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
                    ->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
                    ->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
                    ->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
                    ->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2');
                })->whereBetween('fecha_recepcion', [$date->startOfDay()->toDateTimeString(), $date->endOfDay()->toDateTimeString()])->count();

                //$numData->push($samplesReceived);
                //DB::enableQueryLog();//whereNot
                $finish = DB::table('view_full_results')->select('id')->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->whereBetween('fecha_recepcion', [$date->startOfDay()->toDateTimeString(), $date->endOfDay()->toDateTimeString()])->where('status_result', 'FINALIZADO')->groupBy('id')->get();
                //dd(DB::getQueryLog());

                $finish = $finish->count();
                //DB::table('view_full_results')->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->whereBetween('created_at_result', [$date->startOfDay()->toDateTimeString(), $date->endOfDay()->toDateTimeString()])->where('status_result', 'FINALIZADO')->count();

                $temp->push($received);
                $temp->push($finish);

                if($indexDate == 0) {
                    $total->push('Total', $received, $finish);
                } else {
                    $total[1] = $total[1] + $received;
                    $total[2] = $total[2] + $finish;
                }

                $globalSars = 0;
                $globalInfluenza = 0;
                $globalVsr = 0;
                $globalNegativas = 0;

                $indexJurisdiction = 3;
                $column = 1;
                foreach ($jurisdictions as $hospital) {
                    //Muestras SARS positivas
                    $sarsCov = DB::table('view_full_results')->where('hospital', $hospital->hospital)->whereBetween('fecha_recepcion', [$date->startOfDay()->toDateTimeString(), $date->endOfDay()->toDateTimeString()])->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->where('analysis', 'SARS-COV-2')->where('result', 'like', '%posi%')->count();
                    //Muestras INFLUENZA positivas
                    $influenza = DB::table('view_full_results')->where('hospital', $hospital->hospital)->whereBetween('fecha_recepcion', [$date->startOfDay()->toDateTimeString(), $date->endOfDay()->toDateTimeString()])->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->where('analysis', 'INFLUENZA')->where(function ($query) {
                        $query->where('result', 'like', '%posi%')
                            ->orWhere('result', 'like', '%INF%');
                    })->count();

                    //Muestras VSR positivas
                    $vsr = DB::table('view_full_results')->where('hospital', $hospital->hospital)->whereBetween('fecha_recepcion', [$date->startOfDay()->toDateTimeString(), $date->endOfDay()->toDateTimeString()])->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->where('analysis', 'VSR')->where('result', 'like', '%posi%')->count();

                    //Muestras negativas
                    $negativas = DB::table('view_full_results')->where('hospital', $hospital->hospital)->whereBetween('fecha_recepcion', [$date->startOfDay()->toDateTimeString(), $date->endOfDay()->toDateTimeString()])->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->where(function ($query) {
                        $query->where('result', 'not like', '%posi%')
                        ->where('result', 'not like', '%INF%');
                    })->count();

                    //Se agregan los números al arreglo individual
                    $temp->push($sarsCov, $influenza, $vsr, $negativas);

                    if($indexDate == 0 ) {
                        $total->push($sarsCov, $influenza, $vsr, $negativas);
                    } else {
                        $total[ $indexJurisdiction ] = $total[ $indexJurisdiction ] + $sarsCov;
                        $total[ $indexJurisdiction + 1 ] = $total[ $indexJurisdiction + 1 ] + $influenza;
                        $total[ $indexJurisdiction + 2 ] = $total[ $indexJurisdiction + 2 ] + $vsr;
                        $total[ $indexJurisdiction + 3 ] = $total[ $indexJurisdiction + 3 ] + $negativas;
                    }

                    //Suma de totales por fecha
                    $globalSars = $globalSars + $sarsCov;
                    $globalInfluenza = $globalInfluenza + $influenza;
                    $globalVsr = $globalVsr + $vsr;
                    $globalNegativas = $globalNegativas + $negativas;

                    
                    if( $jurisdictions->count() == $column ) {

                        $total[$indexJurisdiction + 4] = $globalSars;
                        $total[$indexJurisdiction + 5] = $globalInfluenza;
                        $total[$indexJurisdiction + 6] = $globalVsr;
                        $total[$indexJurisdiction + 7] = $globalNegativas;
                        
                    }

                    $indexJurisdiction = $indexJurisdiction + 4;
                    $column++;
                }

                $totalGlobalSars = $totalGlobalSars + $globalSars;
                $totalGlobalInfluenza = $totalGlobalInfluenza + $globalInfluenza;
                $totalGlobalVsr = $totalGlobalVsr + $globalVsr;
                $totalGlobalNegativas = $totalGlobalNegativas + $globalNegativas;

                $temp->push($globalSars, $globalInfluenza, $globalVsr, $globalNegativas);
            } else {
                $temp->push(0, 0);
            }
            $indexDate++;
            $this->information->push( $temp->toArray() );
        }
        $total[$indexJurisdiction] = $totalGlobalSars;
        $total[$indexJurisdiction + 1] = $totalGlobalInfluenza;
        $total[$indexJurisdiction + 2] = $totalGlobalVsr;
        $total[$indexJurisdiction + 3] = $totalGlobalNegativas;

        $this->information->push($total->toArray());
        $this->emit('TableReceivedDayCheck');
    }

    public function export() {
        $filename = 'Resultados_de_virus_respiratorios_' . date('dmYHis') . '.xlsx';
        return Excel::download(new DiagnosticsRespiratorios($this->startDate, $this->endDate, $this->information, $this->juris), $filename);
        
    }
}

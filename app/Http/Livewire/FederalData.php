<?php

namespace App\Http\Livewire;

use App\ChangesDatosFederales;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\DatosFederales;
use Illuminate\Validation\Rule;

class FederalData extends Component
{
    public $view = 'blank';

    public $nombre_paciente, $oficio_entrada, $fecha_toma_muestra, $folio_sisver, $folio_lesp, $fecha_recepcion,$hora_recepcion, $dx1, 
    $muestras, $id_users, $personal, $hospitales, $diagnosticos, $motivosRechazos, $attributes;

    public $anio;
    public $observaciones = "N/A";
    public $aclaraciones_remu = "N/A";
    public $rechazos = 0;
    public $tipo_muestra = 0;
    public $hospital = 0;
    public $persona_recibe = 0;

    public $options = [
        'minDate' => 'today',
        'maxDate' => "2022-05-31",
        'inline' => true,
        'monthSelectorType' => 'static',
        'dateFormat' => 'd-m-Y',
        'enableTime' => false,
        'altFormat' =>  'j F Y',
        'altInput' => true,
    ];
    protected $listeners = ['resetModals' => 'resetModal', 'searchMuestra' => 'searchMuestra', 'createModal' => 'modalCreate'];
    public $datos;

    public function submit(){
        $this->id_users = Auth::user()->id;
        $this->validate([
            'nombre_paciente' => ['required', 'string', 'max:250'],
            'tipo_muestra' => ['required', 'string', 'min:2', 'different:0'],
            'hospital' => ['required', 'string', 'min:2'],
            'persona_recibe' => ['required', 'string', 'min:2'],
            'oficio_entrada' => ['required', 'string'],
            'fecha_toma_muestra' => ['required', 'date'],
            'folio_sisver' => ['required', 'string', 'max:200'],
            'folio_lesp' => ['required', 'string', 'max:250', Rule::unique('datos_federales', 'folio_lesp')->where('is_active', 1)],
            'anio' => ['required'],
            'observaciones' => ['required'],
            'fecha_recepcion' => ['required', 'date'],
            'hora_recepcion' => ['required'],
            'aclaraciones_remu' => ['required', 'string'],
            'dx1' => ['required', 'array', 'min:1'],
            'rechazos' => ['required', 'string', 'min:2'],
            'id_users' => ['required'],
        ]);

        $atributos['nombre_paciente'] = $this->nombre_paciente;
        $atributos['tipo_muestra'] = $this->tipo_muestra;
        $atributos['hospital'] = $this->hospital;
        $atributos['persona_recibe'] = $this->persona_recibe;
        $atributos['oficio_entrada'] = $this->oficio_entrada;
        $atributos['fecha_toma_muestra'] = $this->fecha_toma_muestra;
        $atributos['folio_sisver'] = $this->folio_sisver;
        $atributos['folio_lesp'] = $this->folio_lesp;
        $atributos['anio'] = $this->anio;
        $atributos['observaciones'] = $this->observaciones;
        $atributos['fecha_recepcion'] = $this->fecha_recepcion;
        $atributos['hora_recepcion'] = $this->hora_recepcion;
        $atributos['aclaraciones_remu'] = $this->aclaraciones_remu;
        $atributos['rechazos'] = $this->rechazos;
        $atributos['id_users'] = $this->id_users;
        $atributos['status'] = 'PENDIENTE';

        for( $i = 0; $i < 5; $i++ ) {
            if( is_array ($this->dx1) && count( $this->dx1 ) > $i ) {
                $atributos['dx'.($i+1)] = $this->dx1[$i];
            } else {
                $atributos['dx'.($i+1)] = 'N/A';
            }
        }

        $this->datos = DatosFederales::create($atributos);
        session()->flash('success', 'Datos registrados con éxito!'); 
        $this->view = 'show';
        $this->emit('titleShow');
        $this->emit('refreshTable');
    }

    public function mount(){
        $this->anio = date('y');
    }

    public function resetModal(){
        $this->reset();
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function searchMuestra($value){
        $changes = ChangesDatosFederales::where('dato_federal_id', $value)->get();
        $this->datos = DatosFederales::findOrFail($value); 
        if( $changes->isEmpty() ) {
            $changes = $this->datos->toArray();
            $changes['user_id'] = $changes['id_users'];
            $changes['dato_federal_id'] = $value;
            ChangesDatosFederales::create($changes);
        }
        $this->nombre_paciente = $this->datos->nombre_paciente;
        $this->tipo_muestra = (!is_bool(DatosFederales::tipoMuestrasOptions()->search($this->datos->tipo_muestra)) ) ? $this->datos->tipo_muestra: '0';
        $this->hospital = (!is_bool(DatosFederales::hospitalesJurisOptions()->search($this->datos->hospital))  ) ? $this->datos->hospital: '0';
        $this->persona_recibe = (!is_bool(DatosFederales::personalRecibeOptions()->search($this->datos->persona_recibe)) ) ? $this->datos->persona_recibe: '0';
        $this->oficio_entrada = $this->datos->oficio_entrada;
        $this->fecha_toma_muestra = $this->datos->fecha_toma_muestra;
        $this->folio_sisver = $this->datos->folio_sisver;
        $this->folio_lesp = $this->datos->folio_lesp;
        $this->anio = $this->datos->anio;
        $this->observaciones = $this->datos->observaciones;
        $this->fecha_recepcion = $this->datos->fecha_recepcion;
        $this->hora_recepcion = $this->datos->hora_recepcion;
        $this->aclaraciones_remu = $this->datos->aclaraciones_remu;
        $this->rechazos = (!is_bool(DatosFederales::motivosRechazosOptions()->search($this->datos->rechazos)) ) ? $this->datos->rechazos: '0';
        $this->id_users = $this->datos->id_users;
        $this->dx1 = array();
        if($this->datos->dx1 !="N/A") {
            array_push($this->dx1, $this->datos->dx1);
        }
        if ($this->datos->dx2 != "N/A") {
            array_push($this->dx1, $this->datos->dx2);
        }
        if ($this->datos->dx3 != "N/A") {
            array_push($this->dx1, $this->datos->dx3);
        }
        if ($this->datos->dx4 != "N/A") {
            array_push($this->dx1, $this->datos->dx4);
        }
        if ($this->datos->dx5 != "N/A") {
            array_push($this->dx1, $this->datos->dx5);
        }
        $this->muestras = DatosFederales::tipoMuestrasOptions();
        $this->personal = DatosFederales::personalRecibeOptions();
        $this->hospitales = DatosFederales::hospitalesJurisOptions();
        $this->diagnosticos = DatosFederales::diagnosticosOptions();
        $this->motivosRechazos = DatosFederales::motivosRechazosOptions();
        $this->view = 'edit';
        $this->emit('selectUpdate');
    }

    public function render()
    {
        return view('livewire.modal-register');
    }

    public function modalCreate() {
/*         $this->datos = DatosFederales::findOrFail(95);
        $this->view = 'show'; */
        $this->muestras = DatosFederales::tipoMuestrasOptions();
        $this->personal = DatosFederales::personalRecibeOptions();
        $this->hospitales = DatosFederales::hospitalesJurisOptions();
        $this->diagnosticos = DatosFederales::diagnosticosOptions();
        $this->motivosRechazos = DatosFederales::motivosRechazosOptions();
        $this->view = 'federal-data';
        $this->emit('selectCreate');
    }

    public function update()
    {
        $this->validate([
            'nombre_paciente' => ['required', 'string', 'max:250'],
            'tipo_muestra' => ['required', 'string', 'min:2'],
            'hospital' => ['required', 'string', 'max:250', 'min:2'],
            'persona_recibe' => ['required', 'string', 'min:2'],
            'oficio_entrada' => ['required', 'string'],
            'fecha_toma_muestra' => ['required', 'date'],
            'folio_sisver' => ['required', 'string', 'max:200'],
            'folio_lesp' => ['required', 'string', 'max:250'],
            'anio' => ['required'],
            'observaciones' => ['required', 'string'],
            'fecha_recepcion' => ['required', 'date'],
            'hora_recepcion' => ['required'],
            'aclaraciones_remu' => ['required', 'string'],
            'dx1' => ['required', 'array', 'min:1'],
            'rechazos' => ['required', 'string', 'min:2'],
            'id_users' => ['required'],
        ]);

        $atributos['nombre_paciente'] = $this->nombre_paciente;
        $atributos['tipo_muestra'] = $this->tipo_muestra;
        $atributos['hospital'] = $this->hospital;
        $atributos['persona_recibe'] = $this->persona_recibe;
        $atributos['oficio_entrada'] = $this->oficio_entrada;
        $atributos['fecha_toma_muestra'] = $this->fecha_toma_muestra;
        $atributos['folio_sisver'] = $this->folio_sisver;
        $atributos['folio_lesp'] = $this->folio_lesp;
        $atributos['anio'] = $this->anio;
        $atributos['observaciones'] = $this->observaciones;
        $atributos['fecha_recepcion'] = $this->fecha_recepcion;
        $atributos['hora_recepcion'] = $this->hora_recepcion;
        $atributos['aclaraciones_remu'] = $this->aclaraciones_remu;
        $atributos['rechazos'] = $this->rechazos;
        $atributos['id_users'] = $this->id_users;

        for ($i = 0; $i < 5; $i++) {
            if (is_array($this->dx1) && count($this->dx1) > $i) {
                $atributos['dx' . ($i + 1)] = $this->dx1[$i];
            } else {
                $atributos['dx' . ($i + 1)] = 'N/A';
            }
        }
        
        DatosFederales::where('id', '=', $this->datos->id)->update($atributos);
        session()->flash('success', 'Datos actualizados con éxito!');
        $this->datos = DatosFederales::findOrFail($this->datos->id);

        if (true) {
            $changes = $this->datos->toArray();
            $changes['dato_federal_id'] = $this->datos->id;
            $changes['user_id'] = Auth::user()->id;
            ChangesDatosFederales::create($changes);
        }
        $this->view = 'show';
        $this->emit('titleShow');
        $this->emit('refreshTable');
    }

}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use PDF;
use App\DatosFederales;
use App\Diagnosis;

ini_set('memory_limit', '-1');
ini_set('max_execution_time', '600'); //300 seconds = 5 minutes
use App\Exports\ReportFederalData;
class ExportsData extends Component
{
    public $type = 0, $diagnosticos, $except;
    public $startDate, $endDate, $folioStart, $folioEnd, $dx1, $reception;
    //protected $listeners = ['assignMethod' => 'assignMethod'];

    public $diagnostics;

    public function render()
    {
        $this->diagnostics = Diagnosis::select('diagnosis_name')->orderBy('diagnosis_name', 'ASC')->distinct()->get();
        return view('livewire.exports-data');
    }

    public function submit() {
        if( is_null($this->startDate) && is_null($this->endDate) && is_null($this->folioStart) && is_null($this->folioEnd) ) {
            $this->validate();
        } else {
            $this->resetErrorBag();
            $this->resetValidation();
            if( (!is_null($this->startDate) && $this->startDate != '' ) || (!is_null($this->endDate) && $this->endDate != '' ) ) {
                $validatedData = $this->validate([
                    'startDate' => ['required', 'date'],
                    'endDate' => ['required', 'date'],
                ]);
            }
            $this->validateOnly('dx1');

            if (!is_null($this->folioStart)) {
                $this->validateOnly('folioStart');
            }

            if (!is_null($this->folioEnd)) {
                $this->validateOnly('folioEnd');
            }

            $diagnostics = Diagnosis::where('diagnosis_name',$this->dx1)->get();
            //\DB::enableQueryLog();
            $documentos = DatosFederales::TypeDiagnosis( $diagnostics )
                ->FolioStart($this->folioStart)
                ->FolioEnd($this->folioEnd)
                ->RangeDate($this->startDate, $this->endDate)
                ->Except( $this->except )
                ->selectRaw('*, CAST(folio_lesp AS CHAR ) AS folio')
                ->orderBy('folio', 'ASC')->get();
            

            //dd(\DB::getQueryLog()); // Show results of log
            
            if ($this->type == 1) {
                $filename = 'FO 016 ' . date('d-m-Y') . '.xlsx';
                return (new ReportFederalData($documentos, $this->dx1, $this->reception ))->download($filename, \Maatwebsite\Excel\Excel::XLSX);
            } else {
                
                //Creación del pdf
                if($documentos->count() > 3500 ) {
                    session()->flash('warning', 'No se puede generar el archivo PDF por que excede los 3500 registros.');
                } else if($documentos->count() == 0) {
                    session()->flash('warning', 'No se puede generar un reporte descargable debido a que no encontraron registros con los datos ingresados en el formulario.');
                } else {
                    /* Se realiza check de cuantos diagnósticos solicitados tiene la muestra */
                    $diagnostico = DatosFederales::select('dx1', 'dx2', 'dx3', 'dx4', 'dx5')->TypeDiagnosis($diagnostics)
                        ->FolioStart($this->folioStart)->FolioEnd($this->folioEnd)
                        ->RangeDate($this->startDate, $this->endDate)->Except($this->except)->distinct()->get();
                    if ($diagnostico->count() != 1) {
                        $diagnostico = explode('.', $diagnostico[0]->dx1);
                        $diagnostico = $diagnostico[0] . '.0';
                    } else {
                        if ($diagnostico[0]->dx2 != 'N/A' || $diagnostico[0]->dx3 != 'N/A' || $diagnostico[0]->dx4 != 'N/A' || $diagnostico[0]->dx5 != 'N/A') {
                            $diagnostico = explode('.', $diagnostico[0]->dx1);
                            $diagnostico = $diagnostico[0] . '.0';
                        } else {
                            $diagnostico = $diagnostico[0]->dx1;
                        }
                    }
                    
                    $filename = 'FO 016 ' . date('d-m-Y') . '.pdf';
                    $data['documentos'] = $documentos;
                    $data['diagnostico'] = $diagnostico;
                    $data['reception'] = $this->reception;
                    $pdf = PDF::loadView('main_admin.exports.pdf', $data);
                    $path = storage_path('app/public/' . $filename);


                    $pdf->save($path);
                    return response()->download($path, $filename)->deleteFileAfterSend(true);
                }
            }
        }
    }

    public function assignMethod( $type ) {
        $this->emit('startCreateFile');
        $this->type = $type;
    }

    protected $rules = [
            'dx1' => ['required'],
            'reception' => ['required', 'date'],
            'startDate' => ['required', 'date'],
            'endDate' => ['required', 'date'],
            'folioStart' => ['required', 'string', 'regex:/^[0-9A-Za-z]+\-[0-9]+$/'],
            'folioEnd' => ['required', 'string', 'regex:/^[0-9A-Za-z]+\-[0-9]+$/'],
    ];
}

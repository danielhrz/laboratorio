<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\DatosFederales;
use App\Destinatario;
use App\Email;
use App\Exports\RechazadosExport;
use App\Mail\MessageReceived;
use App\PersonalRechazos;

use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;

use PDF;

class Rechazados extends Component
{
    public $oficio;
    public $type = 0;
    public $judi;
    public $observacion;
    public $destinatarios;
    public $responsable;
    public $resp;
    public $emails;
    public function render()
    {
        $this->destinatarios = Destinatario::where('is_active', 1)->orderBy('jurisdiccion', 'ASC')->get();
        $this->responsable = PersonalRechazos::where('is_active', 1)->orderBy('nombre', 'ASC')->get();
        return view('livewire.rechazados');
    }

    protected $rules = [
        'oficio' => ['required', 'string'],
        'judi' => ['required', 'numeric', 'exists:destinatarios,id'],
        'resp' => ['required', 'numeric', 'exists:personal_rechazos,id'],
        'observacion' => ['required', 'string']
    ];

    public function assignMethod($type)
    {
        $this->emit('startCreateFile');
        $this->type = $type;
    }

    public function submit()
    {
        $filename = null;
        $error = false;
        $this->validate();
        $data = DatosFederales::where('oficio_entrada', $this->oficio)->first();
        if( is_null($data) ) {
            $error = true;
            session()->flash('danger', 'No se puede generar el archivo debido a que el oficio de entrada no existe.');
        } else {
            $documentos = DatosFederales::where('oficio_entrada', $this->oficio)->where('status', 'RECHAZO')->get();
            $destino = Destinatario::where('id', $this->judi )->first();
            $persona = PersonalRechazos::where('id', $this->resp)->first();

            
            if ($this->type == 1) {
                $filename = 'Rechazados ' . date('d-m-Y His') . '.xlsx';
                
                Excel::store(new RechazadosExport($documentos, $data, $this->oficio, $destino, $this->observacion, $persona), $filename, 'public');
            } else {
                if ($documentos->count() > 2000) {
                    $error = true;
                    session()->flash('warning', 'No se puede generar el archivo PDF por que excede los 2000 registros.');
                } else {
                    $filename = 'Rechazados ' . date('d-m-Y His') . '.pdf';
                    
                    $basica = $data;
                    $data = null;
                    $data['documentos'] = $documentos;
                    $data['basica'] = $basica;
                    $data['oficio'] = $this->oficio;
                    $data['destinatario'] = $destino;
                    $data['observaciones'] = $this->observacion;
                    $data['responsable'] = $persona;
                    $pdf = PDF::loadView('main_admin.exports.rechazos-pdf', $data);
                    $pdf->save(storage_path('app/public/' . $filename));
                }
            }

            if(!$error) {
                $path = storage_path('app/public/' . $filename);
                if( !is_null( $this->emails ) && $this->emails != '') {

                    $aEmails = Str::of( $this->emails )->explode(',');
                    foreach ($aEmails as $email) {
                        if($email != '' || $email != ' ' || $email != null) {
                            Mail::to( trim( $email ) )->cc('lve_cdmx@sersalud.cdmx.gob.mx')->send(new MessageReceived($filename, $this->oficio));
                        }
                    }
                }
                return response()->download($path, $filename)->deleteFileAfterSend(true);
            }
        }
    }

    public function searchEmail() {
        $emails = Email::where('is_active', 1)->where('id_juri', $this->judi)->get();
        if($emails->isNotEmpty() ) {
            $this->emails = '';
            foreach($emails as $email ) {
                $this->emails .= $email->email.',';
            }
            $this->emails = substr($this->emails, 0, -1);
        } else {
            $this->emails = null;
        }
    }
}

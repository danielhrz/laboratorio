<?php

namespace App\Http\Livewire\Reports;

use App\DatosFederales;
use App\ViewDiagnostics;
use Livewire\Component;
use Illuminate\Support\Str;
use PDF;

class FormatFiveFour extends Component
{
    public $oficio_doc, $date_send, $folios_print, $observations;

    protected $rules = [
        'oficio_doc' => ['required', 'string'],
        'date_send' => ['required', 'date'],
        'folios_print' => ['required', 'string'],
    ];

    protected $validationAttributes = [
        'oficio_doc' => 'oficio del documento',
        'date_send' => 'fecha de envió',
        'folios_print' => 'folios a imprimir',
    ];

    public function render()
    {
        return view('livewire.reports.format-five-four');
    }

    public function printDocument()
    {
        $this->validate();
        $folios = Str::of($this->folios_print)->explode(',');
        $aux = array();
        foreach($folios as $folio) {
            array_push($aux, Str::of($folio)->lower()->trim() );
        }
        $datosFederales = DatosFederales::where('is_active', 1)->whereIn('folio_lesp', $aux )->orderBy('folio_lesp', 'ASC')->get();
        
        $errors = true;
        $filename = 'Archivo LESP-DA-FO-005 004 ' . date('dmYHis') . '.pdf';

        if ($datosFederales->count() > 3500) {
            session()->flash('warning', 'No se puede generar el archivo PDF por que excede los 3500 registros.');
        } else if ($datosFederales->count() == 0) {
            session()->flash('warning', 'No se puede generar un reporte descargable debido a que no encontraron registros con los datos ingresados en el formulario.');
        } else {
            
            if( $this->isDiagnostic( $datosFederales[0], 'arbovirus' ) || $this->isDiagnostic($datosFederales[0], 'rabia')|| $this->isDiagnostic($datosFederales[0], 'leishmaniasis')) {
                $errors = false;
                if($this->isDiagnostic($datosFederales[0], 'arbovirus')) {
                    $virus = "arbovirus";
                }
                if ($this->isDiagnostic($datosFederales[0], 'rabia')) {
                    $virus = "rabia";
                }

                if ($this->isDiagnostic($datosFederales[0], 'leishmaniasis')) {
                    $virus = "leishmaniasis";
                }

                $data = null;
                $data['oficio_doc'] = $this->oficio_doc;
                $data['date_send'] = $this->date_send;
                $data['documentos'] = $datosFederales;
                $data['office_enter'] = null;
                $folios = DatosFederales::select('oficio_entrada')->where('is_active', 1)->whereIn('folio_lesp', $aux )->distinct()->get();
                foreach( $folios as $folio) {
                    $data['office_enter'] .= "$folio->oficio_entrada, ";
                }
                $data['office_enter'] = Str::of($data['office_enter'])->trim()->rtrim(',');
                $data['virus'] = $virus;
                $pdf = PDF::loadView('main_admin.exports.format-five-four-pdf',
                $data, [], [
                    'margin_top' => 111,
                    'margin_bottom' => 87
                ]);
                $pdf->save(storage_path('app/public/' . $filename));
            }
             else {
                session()->flash('warning', 'No se puede generar el archivo debido a que unicamente esta disponible para los diagnósticos de Arbovirus, Rabia o Leishmaniasis.');
            }
        }

        if (!$errors) {
            $path = storage_path('app/public/' . $filename);
            return response()->download($path, $filename)->deleteFileAfterSend(true);
        }
    }


    public function isDiagnostic($datoFederal, $diagnostic)
    {
        if ( (stripos($datoFederal->dx1, $diagnostic) !== false) || (stripos($datoFederal->dx2, $diagnostic) !== false) || (stripos($datoFederal->dx3, $diagnostic) !== false) || (stripos($datoFederal->dx4, $diagnostic) !== false) || (stripos($datoFederal->dx5, $diagnostic) !== false)) {
            return true;
        } else {
            return false;
        }
    }
}

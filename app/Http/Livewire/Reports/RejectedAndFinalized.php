<?php

namespace App\Http\Livewire\Reports;

use App\DatosFederales;
use App\Exports\RejectedAndFinalizedExport;
use App\Juri;
use Livewire\Component;

class RejectedAndFinalized extends Component
{
    public $date_start_samples, $date_end_samples;

    protected $rules = [
        'date_start_samples' => ['required', 'date'],
        'date_end_samples' => ['required', 'date'],
    ];

    protected $validationAttributes = [
        'date_start_samples' => 'fecha de inicio',
        'date_end_samples' => 'fecha de fin de búsqueda',
    ];

    public function render()
    {
        return view('livewire.reports.rejected-and-finalized');
    }

    public function submit()
    {
        $this->validate();
        $jurisCollect = collect();
        $jurisdictions = Juri::where('is_active', 1)->orderBy('name', 'ASC')->get();
        foreach($jurisdictions AS $jurisdiction) {
            $rejected = DatosFederales::where('is_active', 1)->whereBetween('fecha_recepcion', [$this->date_start_samples, $this->date_end_samples])->where('hospital', $jurisdiction->name)->where('status', 'RECHAZO')->count();
            $finalized = DatosFederales::where('is_active', 1)->whereBetween('fecha_recepcion', [$this->date_start_samples, $this->date_end_samples])->where('hospital', $jurisdiction->name)->where('status', 'FINALIZADO')->count();
            $jurisCollect->push(['name'=> $jurisdiction->name, 'rejected' => $rejected, 'finalized' => $finalized]);
        }

        $filename = 'Muestras rechazadas y finalizadas por Jurisdicción ' . date('dmYHis').'.xlsx';
        return (new RejectedAndFinalizedExport($jurisCollect, $this->date_start_samples, $this->date_end_samples))->download($filename, \Maatwebsite\Excel\Excel::XLSX);
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Excel;
use App\Imports\DatosFederalesImport;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;


class Importacion extends Component
{
    use WithFileUploads;
    public $file;
    protected $listeners = ['resetFiles' => 'resetImportErrors', 'invalidFile' => 'invalidDocument'];

    public function render()
    {

        return view('livewire.importacion');
    }
    
    public function submit()
    {
        $this->validate([
            'file' => 'required|file'
        ]);

        Excel::import(new DatosFederalesImport(Auth::user()->id), $this->file);
        session()->flash('success', 'El archivo se subio con éxito.');
        $this->emit('refreshTable');
    }

    public function resetImportErrors(){
        $this->resetErrorBag();
        $this->resetValidation();
    }
    
    public function invalidDocument(){
        session()->flash('danger', 'Asegurese de haber seleccionado un archivo con extensión csv');
    }
}

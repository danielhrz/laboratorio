<?php

namespace App\Http\Livewire\Paludismo;

use App\Exports\PaludismoResultExport;
use App\InformationExtension;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class IndexComponent extends Component
{
    //Escuchadores de eventos de livewire
    protected $listeners = ['updateInformation', 'hidden'];
    //clases de tailwind para ocultar el modal
    public $hidden = 'hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full';
    public $dialog = '';
    public $alert = 0;

    public $start_date, $end_date;

    public $information = [
        'diagnostic_type' => null,
        'key_lamella' => null,
        'reception' => null,
        'epidemiological_week' => null,
        'sample_collection_time' => null,
        'reading_result' => null,
        'species' => null,
        'eas' => null,
        'ess' => null,
        'date_delivery_results_aeer' => null,
        'total_time' => null,
        'quality_control' => null,
        'quality_control_result' => null,
        'sample_bank' => null,
        'diagnostic_tests' => null,
        'observations' => null,
    ];
    public $dato_id, $reception, $sample_collection_time, $date_delivery_results_aeer;

    public function mount()
    {
        $this->start_date = Carbon::now()->isoFormat('Y-MM-DD');
        $this->end_date = Carbon::now()->isoFormat('Y-MM-DD');
    }

    public function hidden()
    {
       
        $this->hidden = 'hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal h-full';
        $this->dialog = '';
    }

    public function render()
    {
        return view('livewire.paludismo.index-component');
    }

    public function updateInformation($id)
    {
        $this->resetErrorBag();
        $this->resetValidation();

        $this->dato_id = $id;
        $info = DB::table('view_paludismo')->find($id);
        $this->information = [
            'diagnostic_type' => $info->diagnostic_type,
            'key_lamella' => $info->key_lamella,
            //'reception' => $info->reception,
            'epidemiological_week' => $info->epidemiological_week,
            //'sample_collection_time' => $info->sample_collection_time,
            'reading_result' => $info->reading_result,
            'species' => $info->species,
            'eas' => $info->eas,
            'ess' => $info->ess,
            //'date_delivery_results_aeer' => $info->date_delivery_results_aeer,
            'total_time' => $info->total_time,
            'quality_control' => $info->quality_control,
            'quality_control_result' => $info->quality_control_result,
            'sample_bank' => $info->sample_bank,
            'diagnostic_tests' => $info->diagnostic_tests,
            'observations' => $info->observations,
        ];
        $this->reception = ($info->reception) ? Carbon::parse($info->reception)->isoFormat('YYYY-MM-DD HH:mm') : '';
        $this->sample_collection_time = ($info->sample_collection_time) ? Carbon::parse($info->sample_collection_time)->isoFormat('HH:mm') : '';
        $this->date_delivery_results_aeer = ($info->date_delivery_results_aeer) ? Carbon::parse($info->date_delivery_results_aeer)->isoFormat('YYYY-MM-DD HH:mm') : '';
        //dd($this->information);
        $this->dialog = 'role=dialog';
        $this->hidden = 'fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full justify-center items-start flex';
        $this->emit('showModalCrudInformation');
    }

    public function submit()
    {
        $this->validate([
            'information.diagnostic_type' => ['required', Rule::in(config('const.diagnostic_type'))],
            'information.key_lamella' => ['required'],
            'reception' => ['required', 'regex:/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$/'],
            'information.epidemiological_week' => ['required'],
            'sample_collection_time' => ['required', 'regex:/^[0-9]{2}:[0-9]{2}$/'],
            'information.reading_result' => ['required', Rule::in(config('const.reading_result'))],
            'information.species' => ['required', Rule::in(config('const.species'))],
            'information.eas' => ['required'],
            'information.ess' => ['required'],
            'date_delivery_results_aeer' => ['required', 'regex:/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$/'],
            'information.total_time' => ['required'],
            'information.quality_control' => ['required', Rule::in(config('const.quality_control'))],
            'information.quality_control_result' => ['required', Rule::in(config('const.quality_control_result'))],
            'information.sample_bank' => ['required', Rule::in(config('const.sample_bank'))],
            'information.diagnostic_tests' => ['required', Rule::in(config('const.diagnostic_tests'))],
            'information.observations' => ['required'],
        ]);

        //dd($this->dato_id);
        if(InformationExtension::where('dato_federal_id', $this->dato_id)->exists() ) {
            
            InformationExtension::where('dato_federal_id', $this->dato_id)->update([
                'dato_federal_id' => $this->dato_id,
                'diagnostic_type' => $this->information['diagnostic_type'],
                'key_lamella' => $this->information['key_lamella'],
                'reception' => Carbon::parse($this->reception)->toDateTimeString(),
                'epidemiological_week' => $this->information['epidemiological_week'],
                'sample_collection_time' => Carbon::parse($this->sample_collection_time)->toTimeString(),
                'reading_result' => $this->information['reading_result'],
                'species' => $this->information['species'],
                'eas' => $this->information['eas'],
                'ess' => $this->information['ess'],
                'date_delivery_results_aeer' => Carbon::parse($this->date_delivery_results_aeer)->toDateTimeString(),
                'total_time' => $this->information['total_time'],
                'quality_control' => $this->information['quality_control'],
                'quality_control_result' => $this->information['quality_control_result'],
                'sample_bank' => $this->information['sample_bank'],
                'diagnostic_tests' => $this->information['diagnostic_tests'],
                'observations' => $this->information['observations'],
            ]);
        } else {
            InformationExtension::create([
                'dato_federal_id' => $this->dato_id,
                'diagnostic_type' => $this->information['diagnostic_type'],
                'key_lamella' => $this->information['key_lamella'],
                'reception' =>  Carbon::parse($this->reception)->toDateTimeString(),
                'epidemiological_week' => $this->information['epidemiological_week'],
                'sample_collection_time' => Carbon::parse($this->sample_collection_time)->toTimeString(),
                'reading_result' => $this->information['reading_result'],
                'species' => $this->information['species'],
                'eas' => $this->information['eas'],
                'ess' => $this->information['ess'],
                'date_delivery_results_aeer' => Carbon::parse($this->date_delivery_results_aeer)->toDateTimeString(),
                'total_time' => $this->information['total_time'],
                'quality_control' => $this->information['quality_control'],
                'quality_control_result' => $this->information['quality_control_result'],
                'sample_bank' => $this->information['sample_bank'],
                'diagnostic_tests' => $this->information['diagnostic_tests'],
                'observations' => $this->information['observations'],
            ]);
        }

        $this->emit('hidden');
        $this->emit('created-data');
    }

    public function export()
    {
        $information = DB::table('view_paludismo')->whereBetween('fecha_recepcion', [$this->start_date, $this->end_date])->get();
        if($information->count() >= 5000 ) {
            $this->emit('error-export');
        } else {
            $filename = 'Muestras para diagnostico de paludismo ' . date('YmdHis') . '.xlsx';
            return (new PaludismoResultExport($information))->download($filename, \Maatwebsite\Excel\Excel::XLSX);
        }
    }
}

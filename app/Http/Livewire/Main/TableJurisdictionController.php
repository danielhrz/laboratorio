<?php

namespace App\Http\Livewire\Main;

use App\DatosFederales;
use Livewire\Component;
use App\Diagnosis;
use App\Juri;

class TableJurisdictionController extends Component
{

    public $thead;
    public $information;
    protected $listeners = ['changeTableReceivedDay' => 'generateTableReceivedDay'];

    public function mount() {
        //
    }

    public function render()
    {
        return view('livewire.main.table-jurisdiction-controller');
    }

    public function generateTableReceivedDay($date) {
        $labels = array('Jurisdicción', 'Recepcionadas', 'Finalizadas', 'Rechazadas');
        $tempQuery = DatosFederales::where('is_active', 1)->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->get();
        
        $diagnostics = Diagnosis::select('diagnosis_name')->where('diagnosis_name', '!=', 'N/A')->where(function ($query) use ($tempQuery) {
            foreach ($tempQuery as $row) {
                if ($row->dx1 != 'N/A') {
                    $query->orWhere('full_name_analysis', $row->dx1);
                }
                if ($row->dx2 != 'N/A') {
                    $query->orWhere('full_name_analysis', $row->dx2);
                }
                if ($row->dx3 != 'N/A') {
                    $query->orWhere('full_name_analysis', $row->dx3);
                }
                if ($row->dx4 != 'N/A') {
                    $query->orWhere('full_name_analysis', $row->dx4);
                }
                if ($row->dx5 != 'N/A') {
                    $query->orWhere('full_name_analysis', $row->dx5);
                }
            }
        })->distinct()->get();

        //llenado de theads
        foreach ($diagnostics as $row) {
            array_push($labels, $row->diagnosis_name . ' Finalizadas');
            array_push($labels, $row->diagnosis_name . ' Rechazadas');
        }
        $this->information = null;

        if($tempQuery->count() > 0 ) {
            $jurisdictions = DatosFederales::select('hospital')->where('is_active', 1)->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->distinct()->get();
            foreach ($jurisdictions as $jurisdiction) {
                $numData = collect();
                $numData->push($jurisdiction->hospital);
                $numData->push(DatosFederales::where('is_active', 1)->where('hospital', $jurisdiction->hospital)->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->count());
                $numData->push(DatosFederales::where('is_active', 1)->where('hospital', $jurisdiction->hospital)->where('status', 'FINALIZADO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->count());
                $numData->push(DatosFederales::where('is_active', 1)->where('hospital', $jurisdiction->hospital)->where('status', 'RECHAZO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->count());
                foreach ($diagnostics as $row) {
                    $analysis = Diagnosis::where('diagnosis_name', $row->diagnosis_name)->get();

                    if ($analysis->count() > 0) {
                        $count = 0;
                        $tempQuery = DatosFederales::where('is_active', 1)->where('hospital', $jurisdiction->hospital)->where(function ($query) use ($analysis) {
                            foreach ($analysis as $diagnostic) {
                                $query->orWhere('dx1', $diagnostic->full_name_analysis)
                                    ->orWhere('dx2', $diagnostic->full_name_analysis)
                                    ->orWhere('dx3', $diagnostic->full_name_analysis)
                                    ->orWhere('dx4', $diagnostic->full_name_analysis)
                                    ->orWhere('dx5', $diagnostic->full_name_analysis);
                            }
                        })->where('status', 'FINALIZADO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->count();

                        $count = $count + $tempQuery;
                        $numData->push($count);

                        $count = 0;

                        $tempQuery = DatosFederales::where('is_active', 1)->where('hospital', $jurisdiction->hospital)->where(function ($query) use ($analysis) {
                            foreach ($analysis as $diagnostic) {
                                $query->orWhere('dx1', $diagnostic->full_name_analysis)
                                    ->orWhere('dx2', $diagnostic->full_name_analysis)
                                    ->orWhere('dx3', $diagnostic->full_name_analysis)
                                    ->orWhere('dx4', $diagnostic->full_name_analysis)
                                    ->orWhere('dx5', $diagnostic->full_name_analysis);
                            }
                        })->where('status', 'RECHAZO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->count();

                        $count = $count + $tempQuery;

                        $numData->push($count);
                    }
                }
                $this->information[] = $numData;
            }
        } else {
            $jurisdictions = Juri::where('is_active', 1)->get();
            foreach ($jurisdictions as $jurisdiction) {
                $numData = collect();
                $numData->push($jurisdiction->name);
                $numData->push(0);
                $numData->push(0);
                $numData->push(0);
                foreach ($diagnostics as $row) {
                    $analysis = Diagnosis::where('diagnosis_name', $row->diagnosis_name)->get();

                    if ($analysis->count() > 0) {
                        $numData->push(0);
                        $numData->push(0);
                    }
                }
                $this->information[] = $numData;
            }
        }
        

        

        
        //dd($information);
        $this->thead = $labels;
        $this->emit('TableReceivedDayCheck');
    }
}

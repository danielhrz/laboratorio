<?php

namespace App\Http\Livewire\Results;

use Livewire\Component;
use App\ViewResults;
use App\DatosFederales;
use App\Destinatario;

use App\Manager;
use App\Reviewer;
use App\ViewAnalysisResult;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use PDF;

class FormatThirtySeven extends Component
{
    public $oficio;
    public $asunto;

    public $type = 0;
    public $destinatario;
    public $if_result;
    public $destinatarios;
    public $initials;
    public $check;
    public $managers;
    public $encargado;
    public $lesp;

    public $influenza = 0;

    protected $rules = [
        'oficio' => ['required', 'string'],
        'if_result' => ['required', 'string'],
        'destinatario' => ['required', 'numeric', 'exists:destinatarios,id'],
        'check' =>['required', 'numeric', 'exists:reviewers,id'],
        'encargado' => ['required', 'numeric', 'exists:managers,id'],
    ];

    public function render()
    {
        $this->destinatarios = Destinatario::where('is_active', 1)->orWhere('is_active', 2)->orderBy('encargado', 'ASC')->get();
        $this->managers = Manager::all()->where('is_active', 1);
        $this->initials = Reviewer::where('is_active', 1)->orderBy('name', 'ASC')->get();
        return view('livewire.results.format-thirty-seven');
    }

    public function assignMethodPrint($type)
    {
        $this->type = $type;
    }

    public function submit()
    {
        $filename = null;
        $error = true;
        $this->validate();
        $isOficioEntrada = DatosFederales::where('oficio_entrada', $this->oficio)->first();
        if (is_null($isOficioEntrada)) {
            $error = true;
            session()->flash('danger', 'No se puede generar el archivo debido a que el oficio de entrada no existe.');
        } else {
            if (!is_null( $this->lesp ) &&  $this->lesp != '') {
                $auxiliar1 = Str::of(str_replace(' ', '', $this->lesp))->explode(',');
                //Comienza concatenado de DB
                $i = 0;
                $sQuery = '(';
                foreach ($auxiliar1 as $folio) {
                    $auxiliar2 = Str::of($folio)->explode('/');
                    if ($auxiliar2->count() > 1) {
                        $sQuery .= "(SELECT * FROM view_results WHERE oficio_entrada = '" . $this->oficio . "' AND if_exit = '" . $this->if_result . "' AND (folio_lesp >= '" . $auxiliar2[0] . "' AND folio_lesp <= '" . $auxiliar2[1] . "'))";
                        if ($i != ($auxiliar1->count() - 1)) {
                            $sQuery .= " UNION ALL ";
                        }
                    } else {
                        $sQuery .= "(SELECT * FROM view_results WHERE oficio_entrada = '" . $this->oficio . "' AND if_exit = '" . $this->if_result . "' AND folio_lesp = '" . $auxiliar2[0] . "' )";
                        if ($i != ($auxiliar1->count() - 1)) {
                            $sQuery .= " UNION ALL ";
                        }
                    }
                    $i++;
                }
                $sQuery .= ') ORDER BY folio_lesp ASC';
                $consulta = DB::select(DB::raw($sQuery));
                $resultsRegister = ViewResults::hydrate($consulta);
            } else {
                $resultsRegister = ViewResults::where('oficio_entrada', $this->oficio)->where('if_exit', $this->if_result)->orderBy('folio_lesp', 'ASC')->get();
            }
            


            if ($resultsRegister->count() > 2000) {
                $error = true;
                session()->flash('warning', 'No se puede generar el archivo PDF por que excede los 2000 registros.');
            } else if ($resultsRegister->count() == 0) {
                $error = true;
                session()->flash('warning', 'No se encontraron resultados para el archivo con los datos ingresados.');
            } else if ($this->influenza == 0 && ($resultsRegister[0]->dx1 == 'VIRUS RESPIRATORIOS 5.1 INFLUENZA' || $resultsRegister[0]->dx2 == 'VIRUS RESPIRATORIOS 5.1 INFLUENZA' || $resultsRegister[0]->dx3 == 'VIRUS RESPIRATORIOS 5.1 INFLUENZA' || $resultsRegister[0]->dx4 == 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')) {
                $this->influenza = 1;
            } else {
                    $initials = Reviewer::where('id', $this->check)->first();
                    $auxDiagnostics = collect();
                    foreach ($resultsRegister as $result) {
                        $aTemp = collect($result->toArray());
                        $temp = ViewAnalysisResult::where('result_id', $result->result_id)->get();
                        $aTemp->put('diagnostics', $temp);
                        $auxDiagnostics->push($aTemp->toArray());
                    }
                    $destino = Destinatario::where('id', $this->destinatario)->first();
                    $filename = '8 ' . date('d-m-Y His') . '.pdf';
                    $data['oficio'] = $this->oficio;
                    $data['results'] = $auxDiagnostics;
                    $data['receiver'] = $destino;
                    $data['print'] = $this->type;
                    $data['exit'] = strtoupper($resultsRegister[0]->if_exit);
                    $data['initials'] = $initials->initials;
                    $data['manager'] = Manager::find($this->encargado);
                    $pdf = PDF::loadView('main_admin.export-result.pdf-lesp-da-fo-037-004', $data,[], [
                        'title' => 'LESP-VE-FO-037/008',
                        'margin_top' => 85,
                        'margin_bottom' => 120
                    ]);
                    $pdf->save(storage_path('app/public/' . $filename));
                    $error = false;
            }

            if (!$error) {
                $this->influenza = 0;
                $this->type = 0;
                $path = storage_path('app/public/' . $filename);
                return response()->download($path, $filename)->deleteFileAfterSend(true);
            }
        }
    }
}

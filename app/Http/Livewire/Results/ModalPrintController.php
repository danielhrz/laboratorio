<?php

namespace App\Http\Livewire\Results;

use Livewire\Component;

use App\Destinatario;
use App\Manager;
use App\DatosFederales;
use App\Juri;
use App\Users;
use App\ViewResults;
use App\ViewAnalysisResult;

use PDF;

class ModalPrintController extends Component
{
    //Escuchadores de eventos de livewire
    protected $listeners = ['modalSelectGenerate' => 'modalSelectGenerate', 'hidden' => 'hidden', 'closeAlert' => 'closeAlertFlowbite'];
    //clases de tailwind para ocultar el modal
    public $hidden = 'hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full';
    public $dialog = '';
    public $alert = 0;

    public $managers;
    public $encargado;

    public $dato;

    public function render()
    {
        //$this->destinatarios = Destinatario::where('is_active', 1)->orWhere('is_active', 2)->orderBy('encargado', 'ASC')->get();
        $this->managers = Manager::all()->where('is_active', 1);
        return view('livewire.results.modal-print-controller');
    }

    public function hidden()
    {
        $this->alert = 0;
        $this->view = 'modal-clean';
        $this->hidden = 'hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal h-full';
        $this->dialog = '';
    }

    public function closeAlertFlowbite()
    {
        $this->alert = 0;
    }

    public function modalSelectGenerate($folio)
    {
        $this->dato = $folio;
        //Asigna la nueva vista
        $this->view = 'modal-created';
        //Asigna las clases para mostrar el modal
        $this->dialog = 'role=dialog';
        $this->hidden = 'overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal h-full justify-center items-center flex';

        //Emite un evento de livewire para escucha desde js
        $this->emit('openModal');
    }

    public function submit() {
        $this->validate([
            'encargado' => ['required', 'numeric', 'exists:managers,id'],
        ]);

        $datoFederal = DatosFederales::find($this->dato);
        $resultsRegister = ViewResults::where('id', $this->dato)->get();
        //dd($resultsRegister);

        $judi = Juri::find($resultsRegister[0]->jurisdiction_id );

        $initials = Users::find($datoFederal->user_id_operational_status_change);
        $auxDiagnostics = collect();
        foreach ($resultsRegister as $result) {
            $aTemp = collect($result->toArray());
            $temp = ViewAnalysisResult::where('result_id', $result->result_id)->get();
            $aTemp->put('diagnostics', $temp);
            $auxDiagnostics->push($aTemp->toArray());
        }
        $destino = Destinatario::where('jurisdiccion', $judi->name)->where('range', 1)->first();
        $filename = 'LESP-VE-FO-0037-008 ' . date('d-m-Y His') . '.pdf';
        $data['oficio'] = '';
        $data['results'] = $auxDiagnostics;
        $data['receiver'] = $destino;
        $data['print'] = 1;
        $data['exit'] = strtoupper($resultsRegister[0]->if_exit);
        $data['initials'] = $initials->initials;
        $data['manager'] = Manager::find($this->encargado);
        $data['judi'] = true;
        $pdf = PDF::loadView('main_admin.export-result.pdf-lesp-da-fo-037-004', $data, [], [
            'title' => 'LESP-VE-FO-037/008',
            'margin_top' => 85,
            'margin_bottom' => 80
        ]);
        $pdf->save(storage_path('app/public/' . $filename));
        $path = storage_path('app/public/' . $filename);
        return response()->download($path, $filename)->deleteFileAfterSend(true);
    }

}

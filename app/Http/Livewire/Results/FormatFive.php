<?php

namespace App\Http\Livewire\Results;

use App\DatosFederales;
use Livewire\Component;
use App\ViewResults;
use App\Destinatario;
use App\Email;
use App\Exports\RechazadosExport;
use App\Mail\MessageReceived;
use App\PersonalRechazos;

use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;

use PDF;

class FormatFive extends Component
{
    public $oficio;
    public $asunto;

    public $type = 0;
    public $destinatario;
    public $if_result;
    public $destinatarios;
    public $lesp;


    protected $rules = [
        'oficio' => ['required', 'string'],
        'asunto' => ['required', 'string'],
        'if_result' => ['required', 'string'],
        'destinatario' => ['required', 'numeric', 'exists:destinatarios,id'],
    ];

    public function render()
    {
        $this->destinatarios = Destinatario::where('is_active', 1)->orWhere('is_active', 2)->orderBy('encargado', 'ASC')->get();
        return view('livewire.results.format-five');
    }

    public function assignMethod($type)
    {
        $this->emit('startCreateFile');
        $this->type = $type;
    }

    public function submit()
    {
        $type = 0;
        $filename = null;
        $error = false;
        if ((is_null($this->oficio) &&  is_null($this->lesp)) ||  ($this->lesp == '' && $this->oficio == '')) {
            $this->validate([
                'lesp' => ['string', 'required'],
                'oficio' => ['required', 'string'],
                'asunto' => ['required', 'string'],
                'if_result' => ['required', 'string'],
                'destinatario' => ['required', 'numeric', 'exists:destinatarios,id'],
            ]);
        } else if (!is_null($this->oficio) && $this->oficio != '') {
            $this->validate();
            $type = 1;
            $search = DatosFederales::where('oficio_entrada', $this->oficio)->first();
            $mensaje = 'No se puede generar el archivo debido a que el oficio de entrada no existe.';
        } else if (!is_null($this->lesp) && $this->lesp != '') {
            $this->validate([
                'asunto' => ['required', 'string'],
                'destinatario' => ['required', 'numeric', 'exists:destinatarios,id'],
            ]);
            $type = 2;
            $search = DatosFederales::where('folio_lesp', $this->lesp)->first();
            $mensaje = 'No se puede generar el archivo debido a que el Folio LESP no existe.';
        }

        if (is_null($search)) {
            $error = true;
            session()->flash('danger', $mensaje);
        } else {
            if ($type == 1) {
                $resultsRegister = ViewResults::where('oficio_entrada', $this->oficio)->where('if_exit', $this->if_result)->orderBy('folio_lesp', 'ASC')->get();
                $mensaje = 'No se encontraron resultados registrados para este oficio de entrada o oficio de salida';
            } else if ($type == 2) {
                $resultsRegister = ViewResults::where('folio_lesp', $this->lesp)->orderBy('folio_lesp', 'ASC')->get();
                $mensaje = 'No se encontraron resultados registrados para este Folio LESP';
            }

            $destino = Destinatario::where('id', $this->destinatario)->first();
            if ($resultsRegister->count() > 2000) {
                $error = true;
                session()->flash('warning', 'No se puede generar el archivo PDF por que excede los 2000 registros.');
            } else if ($resultsRegister->count() == 0) {
                $error = true;
                session()->flash('warning', $mensaje);
            } else {
                $filename = 'LESP-DA-FO-005-008 ' . date('d-m-Y His') . '.pdf';
                $data['oficio'] = $search->oficio_entrada;
                $data['results'] = $resultsRegister;
                $data['receiver'] = $destino;
                $data['exit'] = $resultsRegister[0]->if_exit;
                $data['affair'] = $this->asunto;
                $pdf = PDF::loadView('main_admin.export-result.pdf-lesp-da-fo-005-003', $data, [], [
                    'title'      => 'Formato LESP-DA-FO-005/008',
                    'margin_top' => 105,
                    'margin_bottom' => 95
                ]);
                $pdf->save(storage_path('app/public/' . $filename));
            }

            if (!$error) {
                $path = storage_path('app/public/' . $filename);
                return response()->download($path, $filename)->deleteFileAfterSend(true);
            }
        }
    }
}

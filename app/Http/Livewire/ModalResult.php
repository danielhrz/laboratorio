<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\DatosFederales;
use App\Diagnosis;
use App\View_Diagnostics;

class ModalResult extends Component
{
    public $remu, $oficio_entrada, $nombre_paciente, $folio_lesp, $folio_sisver, $tipo_muestra;
    public $diagnostics;
    public $diagnosis = [];
    public $kit;

    protected $listeners = ['searchMuestra' => 'searchMuestra'];

    public function searchMuestra($value){
        $this->remu = DatosFederales::findOrFail($value);

        $this->nombre_paciente = $this->remu->nombre_paciente;
        $this->tipo_muestra = (!is_bool(DatosFederales::tipoMuestrasOptions()->search($this->remu->tipo_muestra)) ) ? $this->remu->tipo_muestra: '0';
        $this->oficio_entrada = $this->remu->oficio_entrada;
        $this->folio_sisver = $this->remu->folio_sisver;
        $this->folio_lesp = $this->remu->folio_lesp;
        $this->diagnostics = collect();
        if($this->remu->dx1 != 'N/A') {
            $result = Diagnosis::where('analysis', $this->remu->dx1)->get();
            foreach($result AS $diagnostic) {
                $this->diagnostics->push($diagnostic);
            }
        }
        if($this->remu->dx2 != 'N/A') {
            $result = Diagnosis::where('analysis', $this->remu->dx2)->get();
            foreach($result AS $diagnostic) {
                $this->diagnostics->push($diagnostic);
            }
        }
        if($this->remu->dx3 != 'N/A') {
            $result = Diagnosis::where('analysis', $this->remu->dx3)->get();
            foreach($result AS $diagnostic) {
                $this->diagnostics->push($diagnostic);
            }
        }
        if($this->remu->dx4 != 'N/A') {
            $result = Diagnosis::where('analysis', $this->remu->dx4)->get();
            foreach($result AS $diagnostic) {
                $this->diagnostics->push($diagnostic);
            }
        }
        if($this->remu->dx5 != 'N/A') {
            $result = Diagnosis::where('analysis', $this->remu->dx5)->get();
            foreach($result AS $diagnostic) {
                $this->diagnostics->push($diagnostic);
            }
        }

        if(!is_null($this->diagnostics)) {
            foreach($this->diagnostics AS $diagnosis ) { 
                $temp = View_Diagnostics::where('id', $diagnosis->id )->get();
                if($temp->isNotEmpty() ) {
                    $this->kit[$diagnosis->id] = $temp->toArray();
                }
                //dd($this->kit);
            }
        }
        
    }

    public function render()
    {
        return view('livewire.modal-result');
    }
}

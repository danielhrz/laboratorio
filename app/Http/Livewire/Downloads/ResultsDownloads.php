<?php

namespace App\Http\Livewire\Downloads;

use App\Exports\ResultsExport;
use Livewire\Component;

use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ResultsDownloads extends Component
{
    public $date_start, $date_end;

    public function render()
    {
        return view('livewire.downloads.results-downloads');
    }

    public function submit()
    {
        $this->resetErrorBag();
        $this->resetValidation();
        $this->validate([
            'date_start' => ['required', 'regex:/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/'],
            'date_end' => ['required', 'regex:/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/'],
        ]);
        $start = Carbon::createFromFormat('d/m/Y', $this->date_start);
        $end = Carbon::createFromFormat('d/m/Y', $this->date_end);

        $filename = "Reporte-general-de-resultados-" . date('dmYHis') . ".xlsx";
        $title = "Reporte general de resultados";

        $searchData = DB::table('view_full_results')
        ->join('users', 'view_full_results.user_register_result', '=', 'users.id')
        ->whereBetween('view_full_results.fecha_recepcion', [$start->toDateString(), $end->toDateString()])
        ->get();
        return Excel::download(new ResultsExport($title, $searchData), $filename);
    }
}

<?php

namespace App\Http\Livewire\Downloads;

use App\DatosFederales;
use App\Exports\SamplesExport;
use Livewire\Component;

use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class SamplesDownloads extends Component
{

    public $date_start, $date_end;

    public function render()
    {
        return view('livewire.downloads.samples-downloads');
    }

    public function submit()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        $this->validate([
            'date_start' => ['required', 'regex:/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/'],
            'date_end' => ['required', 'regex:/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/'],
        ]);
        $start = Carbon::createFromFormat('d/m/Y', $this->date_start);
        $end = Carbon::createFromFormat('d/m/Y', $this->date_end);

        $filename = "Reporte-de-muestras-recibidas-" . date('dmYHis') . ".xlsx";
        $title = "Reporte general de muestras recibidas";

        $searchData = DatosFederales::where('is_active', 1)->whereBetween('fecha_recepcion', [$start->toDateString(), $end->toDateString()])->orderBy('folio_lesp', 'ASC')->get();
        return Excel::download(new SamplesExport($title, $searchData), $filename );
    }
}

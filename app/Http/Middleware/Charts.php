<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Closure;

class Charts
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->user()->id_role == 1 or $this->auth->user()->id_role == 3 or $this->auth->user()->id_role == 6 or $this->auth->user()->id_role == 7 or $this->auth->user()->id == 18 or $this->auth->user()->id_role == 9 or $this->auth->user()->id_role == 11) {
            return $next($request);
        } else {
            return redirect('privilegios');
        }
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use App\ChangesDatosFederales;
use DB;
use App\DatosFederales;
use App\Users;
use Illuminate\Http\Request;
use Excel;
use App\Imports\DatosFederalesImport;
use App\User;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Result;

class DatosFederalesController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['destroy']]);
    }

    public function index($tipo, Request $request)
    {
        $rutaSection = "#";
        $section = 'Datos Plataforma';
        if($tipo == 'all')
        {
            $rutaSubSection = "datos/all";
            $subSection = 'Diagnostico';
        }
        else if($tipo == 'p')
        {
            $rutaSubSection = "datos/p";
            $subSection = 'Paneles';
        }
        else if($tipo == 'cci')
        {
            $rutaSubSection = "datos/cci";
            $subSection = 'Control de Calidad';
        }
        else if($tipo == 'lve')
        {
            $rutaSubSection = "datos/lve";
            $subSection = 'Referenciadas';
        }

        $datos = DB::table('datos_federales')
        ->where('status', '=', 'PENDIENTE')
        ->selectRaw('count(datos_federales.status) as total')
        ->get();
        $estatus = DB::table('datos_federales')
        ->where('status', '=', 'FINALIZADO')
        ->selectRaw('count(datos_federales.status) as total')
        ->get();
        $cancelados = DB::table('datos_federales')
        ->where('status', '=', 'CANCELADO')
        ->selectRaw('count(datos_federales.status) as total')
        ->get();
        $rechazos = DB::table('datos_federales')
        ->where('status', '=', 'RECHAZO')
        ->selectRaw('count(datos_federales.status) as total')
        ->get();
        $diagnosticos = DatosFederales::diagnosticosOptions();
        return view('main_admin.datos.index', compact('datos', 'estatus', 'cancelados', 'rechazos', 'diagnosticos', 'rutaSection', 'section', 'subSection', 'rutaSubSection', 'tipo'));
    }

    public function dataTable(Request $request)
    {
        if($request->get('tipo') == 'all'){
            $tipo = '';
            $query = DB::table('view_datos')->where('is_active', 1)
            ->where('folio_lesp', 'NOT like', 'P%')
            ->where('folio_lesp', 'NOT like', 'CCI%')
            ->where('folio_lesp', 'NOT like', 'LVE%');
        }else{
            $tipo = $request->get('tipo');
            $query = DB::table('view_datos')->where('is_active', 1)->where('folio_lesp', 'like', $tipo.'%');
        }
        
        return DataTables::of($query)
        ->filter(function($query) use ($request){

            if($request->get('dx1') != 'dx1'){
                $query->where( function($query) use ($request) {
                    $query->where('dx1', $request->get('dx1'))
                        ->orWhere('dx2', $request->get('dx1'))
                        ->orWhere('dx3', $request->get('dx1'))
                        ->orWhere('dx4', $request->get('dx1'))
                        ->orWhere('dx5', $request->get('dx1'));
                });
            }
            if(!empty($request->get('start_date')) && !empty($request->get('end_date'))){
                $query->where('fecha_recepcion', '>=', $request->get('start_date'))
                ->where('fecha_recepcion', '<=', $request->get('end_date'));
            }
        }, true) 
        ->editColumn('created_at', function ($user) {
            return $user->created_at ? with(new Carbon($user->created_at))->format('Y-m-d H:i:s') : '';
        })
        ->editColumn('aclaraciones_remu', function ($sample){
            if(Auth::user()->id_role == 9){
                return '';
            
            }else{
                return $sample->aclaraciones_remu;
            }
        })
        ->filterColumn('created_at', function ($query, $keyword) {
        $query->whereRaw("created_at like ?", ["%$keyword%"]);
        })
        ->filterColumn('status', function ($query, $keyword) {
        $query->whereRaw("status like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx1', function ($query, $keyword) {
        $query->whereRaw("dx1 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx2', function ($query, $keyword) {
        $query->whereRaw("dx2 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx3', function ($query, $keyword) {
        $query->whereRaw("dx3 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx4', function ($query, $keyword) {
        $query->whereRaw("dx4 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx5', function ($query, $keyword) {
        $query->whereRaw("dx5 like ?", ["%$keyword%"]);
        })
        ->filterColumn('anio', function ($query, $keyword) {
        $query->whereRaw("anio like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_lesp', function ($query, $keyword) {
            if( Str::lower($keyword) == 'prioritario') {
                $query->where("folio_lesp", ">=", intval(200000));
            } else if( Str::lower($keyword) == 'ordinario') {
                $query->where("folio_lesp", "<", intval(200000));
            } else {
                $query->whereRaw("folio_lesp like ?", ["%$keyword%"]);
            }
        })
        ->filterColumn('oficio_entrada', function ($query, $keyword) {
        $query->whereRaw("oficio_entrada like ?", ["%$keyword%"]);
        })
        ->filterColumn('fecha_recepcion', function ($query, $keyword) {
        $query->whereRaw("fecha_recepcion like ?", ["%$keyword%"]);
        })
        ->filterColumn('hora_recepcion', function ($query, $keyword) {
        $query->whereRaw("hora_recepcion like ?", ["%$keyword%"]);
        })
        ->filterColumn('hospital', function ($query, $keyword) {
        $query->whereRaw("hospital like ?", ["%$keyword%"]);
        })
        ->filterColumn('nombre_paciente', function ($query, $keyword) {
        $query->whereRaw("nombre_paciente like ?", ["%$keyword%"]);
        })
        ->filterColumn('tipo_muestra', function ($query, $keyword) {
        $query->whereRaw("tipo_muestra like ?", ["%$keyword%"]);
        })
        ->filterColumn('fecha_toma_muestra', function ($query, $keyword) {
        $query->whereRaw("fecha_toma_muestra like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_sisver', function ($query, $keyword) {
        $query->whereRaw("folio_sisver like ?", ["%$keyword%"]);
        })
        ->filterColumn('persona_recibe', function ($query, $keyword) {
        $query->whereRaw("persona_recibe like ?", ["%$keyword%"]);
        })
        ->filterColumn('rechazos', function ($query, $keyword) {
        $query->whereRaw("rechazos like ?", ["%$keyword%"]);
        })
        ->filterColumn('observaciones', function ($query, $keyword) {
        $query->whereRaw("observaciones like ?", ["%$keyword%"]);
        })
        ->filterColumn('aclaraciones_remu', function ($query, $keyword) {
        $query->whereRaw("aclaraciones_remu like ?", ["%$keyword%"]);
        })
        ->filterColumn('full_name', function ($query, $keyword) {
        $query->whereRaw("full_name like ?", ["%$keyword%"]);
        })
        ->addColumn('status', function ($datos) {
        if ($datos->status == 'PENDIENTE') {
            return '<span data-value="PENDIENTE" class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">PENDIENTE</span >';
        }
        if ($datos->status == 'FINALIZADO'){
            return '<span data-value="FINALIZADO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">FINALIZADO</span >';
        }
        if ($datos->status == 'CANCELADO'){
            return '<span data-value="CANCELADO" class="bg-red-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">CANCELADO</span >';
        }
        if ($datos->status == 'RECHAZO'){
            return '<span data-value="RECHAZO" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">RECHAZO</span >';
        }
        
        })
        ->addColumn('checkbox', 'main_admin.datos.check')
        ->addColumn('btn', 'main_admin.datos.btn')
        ->rawColumns(['status', 'btn', 'checkbox'])
        ->smart(false)
        ->toJson();
    }

    public function create()
    {
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {
        $dato = DatosFederales::find($id);
        $dato->is_active = 0;
        $dato->save();
    }

    public function rechazoStatus($id)
    {
        DatosFederales::where('id',$id)->update(['status'=>'RECHAZO']);
        return redirect('datos');
    }

    public function cancelado($id)
    {
        DatosFederales::where('id',$id)->update(['status'=>'CANCELADO']);
        return redirect('datos');
    }

    public function pendiente($id)
    {
        DatosFederales::where('id',$id)->update(['status'=>'PENDIENTE']);
        return redirect('datos');
    }

    public function finalizado($id)
    {
        DatosFederales::where('id',$id)->update(['status'=>'FINALIZADO']);
        return redirect('datos');
    }

    public function changeEstados(Request $request) {
        $estado = $request->input('status');
        $folios = explode(',', $request->input('folios') );
        foreach( $folios as $folio ) {
            $changes = ChangesDatosFederales::where('dato_federal_id', $folio)->get();
            $datos = DatosFederales::findOrFail($folio);
            if ($changes->isEmpty()) {
                $changes = $datos->toArray();
                $changes['dato_federal_id'] = $folio;
                $changes['user_id'] = $changes['id_users'];
                ChangesDatosFederales::create($changes);
            }
            if($estado == 'RECHAZO') {
                DatosFederales::where('id', $folio)->update(
                    [
                        'status' => $estado,
                        'status_result' => 'RECHAZO',
                        'operational_status' => 'RECHAZO',
                        'result_identifier' => 'RECHAZO'
                ]);
            } else {
                DatosFederales::where('id', $folio)->update(['status' => $estado]);
            }

            $datos = DatosFederales::findOrFail($folio);
            $changes = $datos->toArray();
            $changes['dato_federal_id'] = $folio;
            $changes['user_id'] = Auth::user()->id;
            ChangesDatosFederales::create($changes);
        }
        return response()->json(['success' => true, 'message' => 'Cambio de estatus realizado con éxito!']);
    }

    public function changeOperationalEstados(Request $request) {
        $estado = $request->input('status');
        $folios = explode(',', $request->input('folios'));
        foreach ($folios as $folio) {
            $datos = DatosFederales::findOrFail($folio);
            if($datos->operational_status != 'CONCLUIDO') {
                DatosFederales::where('id', $folio)->update(['operational_status' => $estado, 'user_id_operational_status_change' => Auth::user()->id]);
            }

            $result = Result::where('dato_federal_id',$folio)->first();
            if ($estado == 'CONCLUIDO' && ($result->date_complete_operational_status == NULL || $result->date_complete_operational_status != '')) {
                Result::where('id', $result->id)->update(
                    [
                        'date_complete_operational_status' => Carbon::now()->toDateTimeString()
                    ]
                );
            }
        }
        return response()->json(['success' => true, 'message' => 'Cambio de estatus realizado con éxito!']);
    }
}

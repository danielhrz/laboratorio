<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'id_role' => ['required', 'exists:role,id'],
            'user_name' => ['required', 'string', 'max:200', 'unique:users'],
            'name' => ['required', 'string', 'max:200'],
            'paternal_surname' => ['required', 'string', 'max:200'],
            'maternal_surname' => ['required', 'string', 'max:200'],
            'id_juris' => ['required', 'exists:juris,id'],
            'id_areas' => ['required', 'exists:areas,id'],
            'phone' => ['required', 'string', 'max:20'],
            'ext' => ['required', 'string', 'max:4'],
            'email' => ['required', 'string', 'email', 'max:200', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/', 'confirmed']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'id_role' => $data['id_role'],
            'user_name' => $data['user_name'],
            'name' => $data['name'],
            'paternal_surname' => $data['paternal_surname'],
            'maternal_surname' => $data['maternal_surname'],
            'id_juris' => $data['id_juris'],
            'id_areas' => $data['id_areas'],
            'phone' => $data['phone'],
            'ext' => $data['ext'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_active' => $data['is_active']
        ]);
    }
}

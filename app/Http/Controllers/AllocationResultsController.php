<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\DatosFederales;
use App\User;
use App\Users;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Diagnosis;
use App\ViewDiagnostics;
use App\DiagnosticsResultsKitsReferencesTechniques;
use App\Juri;
use App\Result;

class AllocationResultsController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $rutaSection = "allocation";
        $section = 'Asignación de Resultado';

        $datos = DB::table('datos_federales')->get();

        return view('main_admin.resultados.index', compact('datos', 'rutaSection', 'section'));
    }

    public function dataTableR(Request $request)
    {
        $query = DB::table('datos_federales')->where('is_active', 1);
        
        return DataTables::of($query)

        ->filterColumn('oficio_entrada', function ($query, $keyword) {
        $query->whereRaw("oficio_entrada like ?", ["%$keyword%"]);
        })
        ->filterColumn('nombre_paciente', function ($query, $keyword) {
        $query->whereRaw("nombre_paciente like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_lesp', function ($query, $keyword) {
        $query->whereRaw("folio_lesp like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_sisver', function ($query, $keyword) {
        $query->whereRaw("folio_sisver like ?", ["%$keyword%"]);
        })
        ->filterColumn('tipo_muestra', function ($query, $keyword) {
        $query->whereRaw("tipo_muestra like ?", ["%$keyword%"]);
        })
        ->filterColumn('fecha_toma_muestra', function ($query, $keyword) {
        $query->whereRaw("fecha_toma_muestra like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx1', function ($query, $keyword) {
        $query->whereRaw("dx1 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx2', function ($query, $keyword) {
        $query->whereRaw("dx2 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx3', function ($query, $keyword) {
        $query->whereRaw("dx3 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx4', function ($query, $keyword) {
        $query->whereRaw("dx4 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx5', function ($query, $keyword) {
        $query->whereRaw("dx5 like ?", ["%$keyword%"]);
        })
        ->filterColumn('result_identifier', function ($query, $keyword) {
        $query->whereRaw("result_identifier like ?", ["%$keyword%"]);
        })
        ->filterColumn('status', function ($query, $keyword) {
        $query->whereRaw("status like ?", ["%$keyword%"]);
        })
        ->filterColumn('status_result', function ($query, $keyword) {
            $query->whereRaw("status_result like ?", ["%$keyword%"]);
            })
        ->filterColumn('operational_status', function ($query, $keyword) {
            $query->whereRaw("operational_status like ?", ["%$keyword%"]);
        })
        ->addColumn('status', function ($datos) {
        if ($datos->status == 'PENDIENTE') {
            return '<span data-value="PENDIENTE" class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">PENDIENTE</span >';
        }
        if ($datos->status == 'FINALIZADO'){
            return '<span data-value="FINALIZADO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">FINALIZADO</span >';
        }
        if ($datos->status == 'CANCELADO'){
            return '<span data-value="CANCELADO" class="bg-red-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">CANCELADO</span >';
        }
        if ($datos->status == 'RECHAZO'){
            return '<span data-value="RECHAZO" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">RECHAZO</span >';
        }
        
        })
        ->addColumn('result_identifier', function ($datos) {
            if ($datos->result_identifier == 'SIN DEFINIR') {
                return '<span data-value="SIN DEFINIR" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">SIN DEFINIR</span >';
            }
            if ($datos->result_identifier == 'NEGATIVO'){
                return '<span data-value="NEGATIVO" class="bg-green-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-green-200 dark:text-green-800">NEGATIVO</span >';
            }
            if ($datos->result_identifier == 'POSITIVO'){
                return '<span data-value="POSITIVO" class="bg-red-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">POSITIVO</span >';
            }
            if ($datos->result_identifier == 'SEGUIMIENTO'){
                return '<span data-value="SEGUIMIENTO" class="bg-yellow-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-700 dark:text-yellow-300">SEGUIMIENTO</span >';
            }
            if ($datos->result_identifier == 'INDETERMINADO'){
                return '<span data-value="INDETERMINADO" class="bg-blue-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-700 dark:text-blue-300">INDETERMINADO</span >';
            }

            if ($datos->result_identifier == 'RECHAZO') {
                return '<span data-value="RECHAZO" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">RECHAZO</span >';
            }
        })
        ->addColumn('status_result', function ($datos) {
            if ($datos->status_result == 'PENDIENTE') {
                return '<span data-value="PENDIENTE" class="bg-yellow-300 text-yellow-700 text-sm font-bold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">PENDIENTE</span >';
            }
            if ($datos->status_result == 'REPROCESO'){
                return '<span data-value="REPROCESO" class="bg-blue-300 text-blue-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">REPROCESO</span >';
            }
            if ($datos->status_result == 'SIN RESULTADO'){
                return '<span data-value="SIN RESULTADO" class="bg-gray-300 text-grey text-sm font-bold mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">SIN RESULTADO</span >';
            }
            if ($datos->status_result == 'FINALIZADO'){
                return '<span data-value="FINALIZADO" class="bg-green-300 text-green-700 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">FINALIZADO</span >';
            }
            if ($datos->status_result == 'FINALIZADO/ V. EPIDEMIOLOGICO'){
                return '<span data-value="FINALIZADO/ V. EPIDEMIOLOGICO" class="bg-green-300 text-green-700 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">FINALIZADO/ V. EPIDEMIOLOGICO</span >';
            }

            if ($datos->status_result == 'RECHAZO') {
                return '<span data-value="RECHAZO" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">RECHAZO</span >';
            }
            
            
            })
        ->editColumn('operational_status', function ($datos) {
            if ($datos->operational_status == 'SIN REVISION') {
                return '<span data-value="SIN REVISION" class="bg-gray-300 text-grey text-sm font-bold mr-2 px-2.5 py-0.5 rounded ">SIN REVISION</span >';
            }
            if ($datos->operational_status == 'CONCLUIDO') {
                return '<span data-value="CONCLUIDO" class="bg-green-300 text-green-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">CONCLUIDO</span >';
            }
            if ($datos->operational_status == 'CORRECCIÓN') {
                return '<span data-value="CORRECCIÓN" class="bg-yellow-300 text-yellow-700 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">CORRECCIÓN</span >';
            }
            if($datos->operational_status == 'PENDIENTE A CONFIRMACIÓN DE INFLUENZA') {
                return '<span data-value="PENDIENTE A CONFIRMACIÓN DE INFLUENZA" class="bg-fuchsia-300 text-fuchsia-700 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">PENDIENTE A CONFIRMACIÓN DE INFLUENZA</span >';
            }
            if ($datos->operational_status == 'REPROCESO') {
                return '<span data-value="REPROCESO" class="bg-blue-300 text-blue-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">REPROCESO</span >';
            }

            if($datos->operational_status == "MUESTRA RETRASADA POR DIFICULTADES TÉCNICAS") {
                return '<span data-value="REPROCESO" class="bg-red-300 text-red-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">MUESTRA RETRASADA POR DIFICULTADES TÉCNICAS</span >';
            }

            if ($datos->operational_status == 'RECHAZO') {
                return '<span data-value="RECHAZO" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">RECHAZO</span >';
            }
        })
        ->smart(false)
        ->setRowClass(function ($datos) {
            $diagnostic = Diagnosis::where('full_name_analysis', $datos->dx1)->first();
            
            if($diagnostic->time != 0 && ($datos->status != "SIN RESULTADO" ) ) {
                $receptionTime = Carbon::parse($datos->fecha_recepcion . ' ' . $datos->hora_recepcion);
                $currentTime = Carbon::now();
                $time = $diagnostic->time / 3;
                $section = $currentTime->diffInHours($receptionTime);
                if(($datos->operational_status != "CONCLUIDO" && $datos->operational_status != "PENDIENTE A CONFIRMACIÓN DE INFLUENZA" ) && ($section < $time ) ) {
                    return '!bg-green-100 '.$section;
                } else if(($datos->operational_status != "CONCLUIDO" && $datos->operational_status != "PENDIENTE A CONFIRMACIÓN DE INFLUENZA" ) && ($time < $section && ($time * 2) > $section ) ) {
                    return '!bg-yellow-100 '.$section;
                } else if (($datos->operational_status != "CONCLUIDO" && $datos->operational_status != "PENDIENTE A CONFIRMACIÓN DE INFLUENZA" ) &&  ( ($time * 2) < $section )) {
                    return '!bg-red-100 '.$section;
                } else {
                    return ' ' . $section. ' - '.$time;
                }
            }
            //$contains = Str::of($datos->observaciones)->contains('my');
        })
        ->addColumn('checkbox', 'main_admin.resultados.check')
        ->addColumn('btn', 'main_admin.resultados.btn')
        ->rawColumns(['status_result', 'status', 'btn', 'checkbox', 'operational_status', 'result_identifier'])
        ->toJson();
    }

    public function searchMuestra($id) {
        $remu = DatosFederales::where('id', $id)->first();
        if( !is_null($remu) ) {
            $tipoMuestra = (!is_bool(DatosFederales::tipoMuestrasOptions()->search($remu->tipo_muestra)) ) ? $remu->tipo_muestra : '0';

            $diagnostics = collect();
            if($remu->dx1 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx1)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            if($remu->dx2 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx2)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            if($remu->dx3 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx3)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            if($remu->dx4 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx4)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            if($remu->dx5 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx5)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            $auxDiagnostics = collect();
            $isRespiratorio = false;
            if($diagnostics->isNotEmpty()) {
                $collect = collect();
                for($i = 0; $i < $diagnostics->count(); $i++) {
                    if( !$collect->contains($diagnostics[$i]->diagnosis_name) ) {
                        $kit = ViewDiagnostics::select('kit_id', 'kit_name')->where('diagnosis_name', $diagnostics[$i]->diagnosis_name)->distinct()->get();
                        $auxiliar['diagnosis_name'] = $diagnostics[$i]->diagnosis_name;
                        $auxiliar['diagnosis_kits'] = $kit;
                        if ($diagnostics[$i]->diagnosis_name == 'TOS FERINA') {
                            $auxiliar['tosferina'] = array('IS481', 'pIS1001', 'hIS1001', 'pxtS1', 'RP');
                        }
                        $auxDiagnostics->push($auxiliar);

                        $collect->push($diagnostics[$i]->diagnosis_name);
                    }
                }
            }
            $temp = $remu->toArray();
            $temp['tipo_muestra'] = $tipoMuestra;
            $temp['juris'] = Juri::where('is_active', 1)->orderBy('name', 'ASC')->get();
            $temp['diagnostics'] = $auxDiagnostics->toArray();
            
            return response()->json(['success' => true,'data' => $temp],200);
        } else {
            return response()->json(['success' => false,'error' => 'Muestra no encontrada en base de datos.'],404);
        }
    }

    public function store(Request $request) {
        $validateData = $request->validate([
            'cre' => ['required'],
            'coments' => ['required'],
            'interpretation_result' => ['required'],
            'if_exit' => ['required'],
            'date_delivery' => ['required', 'date'],
            'status_result' => ['required'],
            'result.*' => ['required'],
            'kit.*' => ['required', 'exists:results_kits,id']
        ]);
        $data['cre'] = $request->cre;
        $data['coments'] = $request->coments;
        $data['interpretation_result'] = $request->interpretation_result;

        $data['if_exit'] = $request->if_exit;
        $data['user_id'] = Auth::user()->id;
        $data['dato_federal_id'] = $request->dato_federal_id;
        $data['date_delivery'] = $request->date_delivery.' 00:00:00';
        $data['observations'] = $request->observations;
        if($request->status_result == 'FINALIZADO' ) {
            $data['finalized_result_date'] = Carbon::now()->toDateTimeString();
        }
        
        if(is_array($request->diagnosis) && is_array($request->kit) && is_array($request->technique) && is_array($request->reference)) {
            $datosRemu = DatosFederales::find($request->dato_federal_id);
            $juris = Juri::where('name', $datosRemu->hospital)->first();
            $data['jurisdiction_id'] = is_null($juris) ? null: $juris->id;
            $history[] = ['movement_date' => Carbon::now()->isoFormat('D MMM YYYY, HH:mm:ss'), 'user_id' => auth()->user()->id, 'initials' => auth()->user()->initials];
            //$result->history = json_encode($history);
            $data['history'] = json_encode($history);
            $result = Result::create($data);
            $datosRemu->status_result = $request->status_result;
            $datosRemu->result_identifier = $request->result_identifier;
            $datosRemu->save();
            $diagnosis = $request->diagnosis;
            $kits = $request->kit;
            $techniques = $request->technique;
            $reference = $request->reference;
            $results = $request->result;
            for($i = 0; $i < count($kits); $i++ ) {
                for( $j = 0; $j < count($diagnosis); $j++ ) {
                    $relation['result_id'] = $result->id;
                    $relation['diagnosis_id'] = $diagnosis[$j];
                    $relation['result_kit_id'] = $kits[$i];
                    $relation['result_reference_value_id'] = $reference[$j];
                    $relation['technique_id'] = $techniques[$j];
                    if ($request->input('tosferina') != null) {
                        $genDetectados = $request->input('tosferina');
                        $string = '';
                        if (is_array($genDetectados)) {
                            foreach ($genDetectados as $text) {
                                $string .= $text . '|';
                            }
                        }
                        $relation['result'] = $string . $results[$j];
                    } else {
                        $relation['result'] = $results[$j];
                    }
                    DiagnosticsResultsKitsReferencesTechniques::create($relation);
                }
            }
            return response()->json(['success' => true,'data' => 'Resultado registrado con éxito!'],201);
        } else {
            return response()->json(['success' => false,'error' => 'Error al registrar el resultado.'],404);
        }
        
    }

    public function edit($id) {
        $remu = DatosFederales::where('id', $id)->first();
        if( !is_null($remu) ) {
            $tipoMuestra = (!is_bool(DatosFederales::tipoMuestrasOptions()->search($remu->tipo_muestra)) ) ? $remu->tipo_muestra : '0';
            $registerResult = Result::where('dato_federal_id', $id)->first();
            $user = Users::where('id', $registerResult->user_id)->first();
            $diagnostics = collect();
            if($remu->dx1 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx1)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            if($remu->dx2 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx2)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            if($remu->dx3 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx3)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            if($remu->dx4 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx4)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            if($remu->dx5 != 'N/A') {
                $result = Diagnosis::where('full_name_analysis', $remu->dx5)->get();
                foreach($result AS $diagnostic) {
                    $diagnostics->push($diagnostic);
                }
            }
            $auxDiagnostics = collect();
            $isRespiratorio = false;
            if ($diagnostics->isNotEmpty()) {
                $collect = collect();
                for ($i = 0; $i < $diagnostics->count(); $i++) {
                    if (!$collect->contains($diagnostics[$i]->diagnosis_name)) {
                        //$aTemp = collect($diagnostics[$i]->toArray());
                        $temporal = DiagnosticsResultsKitsReferencesTechniques::where('result_id', $registerResult->id)->first();
                        $kit = ViewDiagnostics::select('kit_id', 'kit_name')->where('diagnosis_name', $diagnostics[$i]->diagnosis_name)->distinct()->get();
                        $auxiliar['kits_value'] = $temporal->result_kit_id;
                        $auxiliar['diagnosis_name'] = $diagnostics[$i]->diagnosis_name;
                        $auxiliar['diagnosis_kits'] = $kit;
                        if ($diagnostics[$i]->diagnosis_name == 'TOS FERINA') {
                            $auxiliar['tosferina'] = array('IS481', 'pIS1001', 'hIS1001', 'pxtS1', 'RP');
                            $results = Str::of($temporal->result)->explode('|');
                            $auxiliar['tosferina_value'] = $results;
                        }
                        $auxDiagnostics->push($auxiliar);

                        $collect->push($diagnostics[$i]->diagnosis_name);
                    }
                }
            }
            $temp = $remu->toArray();
            $temp['juris'] = Juri::where('is_active', 1)->orderBy('name', 'ASC')->get();
            $temp['tipo_muestra'] = $tipoMuestra;
            $temp['diagnostics'] = $auxDiagnostics->toArray();
            $temp['initials'] = $user->initials;
            $registerResult = $registerResult->toArray();
            
            if($registerResult['date_complete_operational_status'] != NULL) {
                $registerResult['date_complete_operational_status'] = Carbon::parse($registerResult['date_complete_operational_status'])->isoFormat('D MMM YYYY, HH:mm:ss');
            }
            if ($registerResult['finalized_result_date'] != NULL) {
                $registerResult['finalized_result_date'] = Carbon::parse($registerResult['finalized_result_date'])->isoFormat('D MMM YYYY, HH:mm:ss');
            }
            //dd($registerResult);
            $temp['result'] = $registerResult;
            $temp['revisor'] = ($remu->user_id_operational_status_change != null) ? Users::find($remu->user_id_operational_status_change): null;
            $temp['result']['date_delivery'] = Carbon::parse($temp['result']['date_delivery'] )->toDateString();
            return response()->json(['success' => true,'data' => $temp],200);
        } else {
            return response()->json(['success' => false,'error' => 'Muestra no encontrada en base de datos.'],404);
        }
    }

    public function update($id, Request $request) {
        $validateData = $request->validate([
            'cre' => ['required'],
            'coments' => ['required'],
            'interpretation_result' => ['required'],
            'if_exit' => ['required'],
            'date_delivery' => ['required', 'date'],
            'status_result' => ['required'],
            'result.*' => ['required'],
            'kit.*' => ['required', 'exists:results_kits,id']
        ]);
        
        if(is_array($request->diagnosis) && is_array($request->kit) && is_array($request->technique) && is_array($request->reference)) {
            DiagnosticsResultsKitsReferencesTechniques::where('result_id', $id)->delete();
            $result = Result::find($id);
            $datosRemu = DatosFederales::find($request->dato_federal_id);
            $juris = Juri::where('name', $datosRemu->hospital)->first();
            $history = json_decode($result->history, true);

            $datosRemu->status_result = $request->status_result;
            $datosRemu->result_identifier = $request->result_identifier;
            $datosRemu->save();
            $diagnosis = $request->diagnosis;
            $kits = $request->kit;
            $techniques = $request->technique;
            $reference = $request->reference;
            $results = $request->result;
            for ($i = 0; $i < count($kits); $i++) {
                for ($j = 0; $j < count($diagnosis); $j++) {
                    $relation['result_id'] = $result->id;
                    $relation['diagnosis_id'] = $diagnosis[$j];
                    $relation['result_kit_id'] = $kits[$i];
                    $relation['result_reference_value_id'] = $reference[$j];
                    $relation['technique_id'] = $techniques[$j];
                    if ($request->input('tosferina') != null) {
                        $genDetectados = $request->input('tosferina');
                        $string = '';
                        if (is_array($genDetectados)) {
                            foreach ($genDetectados as $text) {
                                $string .= $text . '|';
                            }
                        }
                        $relation['result'] = $string . $results[$j];
                    } else {
                        $relation['result'] = $results[$j];
                    }
                    DiagnosticsResultsKitsReferencesTechniques::create($relation);
                }
            }

            $result->cre = $request->cre;
            $result->coments = $request->coments;
            $result->interpretation_result = $request->interpretation_result;
            $result->observations = $request->observations;
            // $result->result = $request->result;
            $result->jurisdiction_id = is_null($juris) ? null : $juris->id;
            $result->if_exit = $request->if_exit;/* 
            $data['user_id'] = Auth::user()->id;
            $data['dato_federal_id'] = $request->dato_federal_id; */
            $result->date_delivery = $request->date_delivery.' 00:00:00';
            if ($request->status_result == 'FINALIZADO' && ($result->finalized_result_date == NULL || $result->finalized_result_date != '') ) {
                $result->finalized_result_date = Carbon::now()->toDateTimeString();
            }

            //if($result->isDirty();)
            $history[] = ['movement_date' => Carbon::now()->isoFormat('D MMM YYYY, HH:mm:ss'), 'user_id' => auth()->user()->id, 'initials' => auth()->user()->initials];
            $result->history = json_encode($history);
            $result->save();
            return response()->json(['success' => true,'data' => 'Resultado actualizado con éxito!'],201);
        } else {
            return response()->json(['success' => false,'error' => 'Error al registrar el resultado.'],404);
        }
    }

    public function searchDiagnosisForKit(Request $request) {
        if($request->diagnosis == 'VIRUS RESPIRATORIOS' ) {
            $data = ViewDiagnostics::where('kit_id', $request->kit)->where('diagnosis_name', $request->diagnosis)->orderBy('analysis', 'ASC')->get();
        } else {
            $remuFile = DatosFederales::where('folio_lesp', $request->lesp)->first();
            $data = ViewDiagnostics::where('kit_id', $request->kit)->where( function($query) use ($remuFile) {
                $query->orWhere('full_name_analysis', $remuFile->dx1 )
                ->orWhere('full_name_analysis', $remuFile->dx2)
                ->orWhere('full_name_analysis', $remuFile->dx3)
                ->orWhere('full_name_analysis', $remuFile->dx4)
                ->orWhere('full_name_analysis', $remuFile->dx5);
            })->orderBy('analysis', 'ASC')->get();
        }

        $data = $data->toArray();
        if( !is_null($request->input('edit')) && $request->edit == 1 ) {
            $remuFile = DatosFederales::where('folio_lesp', $request->lesp)->first();
            $registerResult = Result::where('dato_federal_id', $remuFile->id)->first();
            for($i =0; $i < count($data); $i++ ) {
                $dataDiagnosis = DiagnosticsResultsKitsReferencesTechniques::where('result_id', $registerResult->id)->where('diagnosis_id', $data[$i]['id'])->first();
                if(is_null($dataDiagnosis)) {
                    $data[$i]['techniques_value'] = 0;
                    $data[$i]['references_value'] = 0;
                    $data[$i]['result'] = "";
                } else {
                    $data[$i]['techniques_value'] = $dataDiagnosis->technique_id;
                    $data[$i]['references_value'] = $dataDiagnosis->result_reference_value_id;
                    if ($dataDiagnosis->diagnosis_id == 5) {
                        $results = Str::of($dataDiagnosis->result)->explode('|');
                        $data[$i]['result'] = $results[5];
                    } else {
                        $data[$i]['result'] = $dataDiagnosis->result;
                    }
                }
            }
        }
        return response()->json(['success' => true, 'data' => $data], 200);
    }
}

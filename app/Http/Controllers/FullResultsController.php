<?php

namespace App\Http\Controllers;
use App\ChangesDatosFederales;
use DB;
use App\DatosFederales;
use App\Users;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class FullResultsController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $rutaSection = "full";
        $section = 'Lista de Resultados';

        $datos = DB::table('view_full_results')->get();
        
        return view('main_admin.resultados.fullResults', compact('datos', 'rutaSection', 'section'));
    }

        public function dataTableFullResults(Request $request)
    {
        $query = DB::table('view_full_results')->where('is_active', 1);
        
        return DataTables::of($query)

        ->filterColumn('status', function ($query, $keyword) {
        $query->whereRaw("status like ?", ["%$keyword%"]);
        })
        ->filterColumn('status_result', function ($query, $keyword) {
        $query->whereRaw("status_result like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx1', function ($query, $keyword) {
        $query->whereRaw("dx1 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx2', function ($query, $keyword) {
        $query->whereRaw("dx2 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx3', function ($query, $keyword) {
        $query->whereRaw("dx3 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx4', function ($query, $keyword) {
        $query->whereRaw("dx4 like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx5', function ($query, $keyword) {
        $query->whereRaw("dx5 like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_lesp', function ($query, $keyword) {
        $query->whereRaw("folio_lesp like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_sisver', function ($query, $keyword) {
        $query->whereRaw("folio_sisver like ?", ["%$keyword%"]);
        })
        ->filterColumn('oficio_entrada', function ($query, $keyword) {
        $query->whereRaw("oficio_entrada like ?", ["%$keyword%"]);
        })
        ->filterColumn('nombre_paciente', function ($query, $keyword) {
        $query->whereRaw("nombre_paciente like ?", ["%$keyword%"]);
        })
        ->filterColumn('tipo_muestra', function ($query, $keyword) {
        $query->whereRaw("tipo_muestra like ?", ["%$keyword%"]);
        })
        ->filterColumn('fecha_toma_muestra', function ($query, $keyword) {
        $query->whereRaw("fecha_toma_muestra like ?", ["%$keyword%"]);
        })
        ->filterColumn('kit_name', function ($query, $keyword) {
        $query->whereRaw("kit_name like ?", ["%$keyword%"]);
        })
        ->filterColumn('technique_name', function ($query, $keyword) {
        $query->whereRaw("technique_name like ?", ["%$keyword%"]);
        })
        ->filterColumn('reference_value', function ($query, $keyword) {
        $query->whereRaw("reference_value like ?", ["%$keyword%"]);
        })
        ->filterColumn('result', function ($query, $keyword) {
        $query->whereRaw("result like ?", ["%$keyword%"]);
        })
        ->filterColumn('initials', function ($query, $keyword) {
        $query->whereRaw("initials like ?", ["%$keyword%"]);
        })
        ->filterColumn('cre', function ($query, $keyword) {
        $query->whereRaw("cre like ?", ["%$keyword%"]);
        })
        ->filterColumn('date_delivery', function ($query, $keyword) {
        $query->whereRaw("date_delivery like ?", ["%$keyword%"]);
        })
        ->filterColumn('if_exit', function ($query, $keyword) {
        $query->whereRaw("if_exit like ?", ["%$keyword%"]);
        })
        ->filterColumn('result_comments', function ($query, $keyword) {
        $query->whereRaw("result_comments like ?", ["%$keyword%"]);
        })
        ->filterColumn('interpretation_result', function ($query, $keyword) {
        $query->whereRaw("interpretation_result like ?", ["%$keyword%"]);
        })
        ->filterColumn('created_at_result', function ($query, $keyword) {
        $query->whereRaw("created_at_result like ?", ["%$keyword%"]);
        })
        ->addColumn('status', function ($datos) {
        if ($datos->status == 'PENDIENTE') {
            return '<span data-value="PENDIENTE" class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">PENDIENTE</span >';
        }
        if ($datos->status == 'FINALIZADO'){
            return '<span data-value="FINALIZADO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">FINALIZADO</span >';
        }
        if ($datos->status == 'CANCELADO'){
            return '<span data-value="CANCELADO" class="bg-red-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">CANCELADO</span >';
        }
        if ($datos->status == 'RECHAZO'){
            return '<span data-value="RECHAZO" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">RECHAZO</span >';
        }
        
        })
        ->addColumn('status_result', function ($results) {
        if ($results->status_result == 'PENDIENTE') {
            return '<span data-value="PENDIENTE" class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">PENDIENTE</span >';
        }
        if ($results->status_result == 'FINALIZADO'){
            return '<span data-value="FINALIZADO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">FINALIZADO</span >';
        }
        if ($results->status_result == 'REPROCESO'){
            return '<span data-value="REPROCESO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">REPROCESO</span >';
        }
        
        })
        ->smart(false)
        ->rawColumns(['status', 'status_result'])
        ->toJson();
    }
}

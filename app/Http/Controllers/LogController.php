<?php

namespace App\Http\Controllers;

use App\ChangesDatosFederales;
use DB;
use App\DatosFederales;
use App\Users;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LogController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {
        $rutaSection = "log";
        $section = 'Historial de Modificaciones';

        $datos = DB::table('log_change_datos_federales')->get();
        
        return view('main_admin.logChanges.index', compact('datos', 'rutaSection', 'section'));
    }

    public function dataTableLogChanges(Request $request)
    {
        $query = DB::table('view_users_log')->where('is_active', 1);
        
        return DataTables::of($query)

        ->editColumn('created_at', function ($user) {
            return $user->created_at ? with(new Carbon($user->created_at))->format('Y-m-d H:i:s') : '';
        })
        ->filterColumn('created_at', function ($query, $keyword) {
        $query->whereRaw("created_at like ?", ["%$keyword%"]);
        })
        ->filterColumn('status', function ($query, $keyword) {
        $query->whereRaw("status like ?", ["%$keyword%"]);
        })
        ->filterColumn('dx1', function ($query, $keyword) {
        $query->whereRaw("dx1 like ?", ["%$keyword%"]);
        })
        ->filterColumn('anio', function ($query, $keyword) {
        $query->whereRaw("anio like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_lesp', function ($query, $keyword) {
        $query->whereRaw("folio_lesp like ?", ["%$keyword%"]);
        })
        ->filterColumn('oficio_entrada', function ($query, $keyword) {
        $query->whereRaw("oficio_entrada like ?", ["%$keyword%"]);
        })
        ->filterColumn('fecha_recepcion', function ($query, $keyword) {
        $query->whereRaw("fecha_recepcion like ?", ["%$keyword%"]);
        })
        ->filterColumn('hora_recepcion', function ($query, $keyword) {
        $query->whereRaw("hora_recepcion like ?", ["%$keyword%"]);
        })
        ->filterColumn('hospital', function ($query, $keyword) {
        $query->whereRaw("hospital like ?", ["%$keyword%"]);
        })
        ->filterColumn('nombre_paciente', function ($query, $keyword) {
        $query->whereRaw("nombre_paciente like ?", ["%$keyword%"]);
        })
        ->filterColumn('tipo_muestra', function ($query, $keyword) {
        $query->whereRaw("tipo_muestra like ?", ["%$keyword%"]);
        })
        ->filterColumn('fecha_toma_muestra', function ($query, $keyword) {
        $query->whereRaw("fecha_toma_muestra like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_sisver', function ($query, $keyword) {
        $query->whereRaw("folio_sisver like ?", ["%$keyword%"]);
        })
        ->filterColumn('persona_recibe', function ($query, $keyword) {
        $query->whereRaw("persona_recibe like ?", ["%$keyword%"]);
        })
        ->filterColumn('rechazos', function ($query, $keyword) {
        $query->whereRaw("rechazos like ?", ["%$keyword%"]);
        })
        ->filterColumn('observaciones', function ($query, $keyword) {
        $query->whereRaw("observaciones like ?", ["%$keyword%"]);
        })
        ->filterColumn('aclaraciones_remu', function ($query, $keyword) {
        $query->whereRaw("aclaraciones_remu like ?", ["%$keyword%"]);
        })
        ->filterColumn('full_name', function ($query, $keyword) {
        $query->whereRaw("full_name like ?", ["%$keyword%"]);
        })
        ->addColumn('status', function ($datos) {
        if ($datos->status == 'PENDIENTE') {
            return '<span data-value="PENDIENTE" class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">PENDIENTE</span >';
        }
        if ($datos->status == 'FINALIZADO'){
            return '<span data-value="FINALIZADO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">FINALIZADO</span >';
        }
        if ($datos->status == 'CANCELADO'){
            return '<span data-value="CANCELADO" class="bg-red-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">CANCELADO</span >';
        }
        if ($datos->status == 'RECHAZO'){
            return '<span data-value="RECHAZO" class="bg-gray-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">RECHAZO</span >';
        }
        
        })
        ->smart(false)
        ->rawColumns(['status'])
        ->toJson();
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewChartController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
        $this->middleware('charts');
    }

    public function ingresadas()
    {
        return view('main_admin.charts.ingresadas');
    }

    public function rechazadas()
    {
        return view('main_admin.charts.rechazadas');
    }

    public function motivos()
    {
        return view('main_admin.charts.motivos');
    }

    public function jurisdicciones()
    {
        return view('main_admin.charts.jurisdicciones');
    }

    public function hospitales()
    {
        return view('main_admin.charts.hospitales');
    }

    public function muestras()
    {
        return view('main_admin.charts.muestras');
    }

    public function recepcionadas()
    {
        return view('main_admin.charts.recepcionadas');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class PaludismoDataTable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $start = Carbon::parse($request->date_start)->startOfDay()->toDateString();
        $end = Carbon::parse($request->date_end)->endOfDay()->toDateString();
        
        $query = DB::table('view_paludismo')->whereBetween('fecha_recepcion', [$start, $end]);
        
        return DataTables::of($query)
        ->addColumn('btn', 'livewire.paludismo.btn')
        ->rawColumns(['btn'])
        ->toJson();
    }
}

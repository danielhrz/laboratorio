<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interpretation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SearchInterpretationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if($request->input("result") ) {
            $index = 0;
            $scene = "";
            foreach($request->result AS $result) {
                $analysis = $request->input("virus_$index");
                $scene .= "$analysis: $result| ";
                $index++;
            }
            $scene = Str::of($scene)->lower()->trim()->rtrim('|');
            $interpretation = Interpretation::where('scene', $scene)->first();
            if( is_null($interpretation) ) {
                $data = [
                    'success' => false,
                    'error' => 'No se localizo una interpretación pre cargada para los resultados ingresados.'
                ];

                return response()->json($data, 404);
            } else {
                $data = [
                    'success' => true,
                    'data' => [
                        'interpretation' => $interpretation->interpretation
                    ]
                ];

                return response()->json($data, 200);
            }
        } else {
            $data = [
                'success' => false,
                'error' => 'Los campos de resultados no existen.'
            ];

            return response()->json($data, 404);
        }
    }
}

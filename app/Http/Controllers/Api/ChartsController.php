<?php

namespace App\Http\Controllers\Api;

use App\DatosFederales;
use App\Diagnosis;
use App\Http\Controllers\Controller;
use App\Juri;
use App\ViewResults;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ChartsController extends Controller
{
    public function entered($year) {
        $labels = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $data['labels'] = $labels;

        $muestras = collect();
        for ($i = 1; $i <= 12; $i++) {
            //Genera la primer fecha de inicio del año
            $firstDayMonth = Carbon::parse($year . '-' . $i . '-01')->startOfMonth()->toDateString();
            //Genera la ultima fecha del año
            $endDayMonth = Carbon::parse($year . '-' . $i . '-01')->endOfMonth()->toDateString();
            //Realiza el conteo de cuantos Hombre fueron registrados en un mes determinado
            $temp = DatosFederales::where('is_active', 1)->where('status', 'FINALIZADO')->whereBetween('fecha_recepcion', [$firstDayMonth, $endDayMonth])->count();
            $muestras->push($temp);
        }
        $data['datasets'] = [array(
            'backgroundColor' => 'rgba(58, 176, 255, 0.4)',
            'borderColor' => 'rgb(58, 176, 255)',
            'borderWidth' => 1,
            'type' => 'bar',
            'label' => 'Muestras',
            'data' => $muestras->toArray()
        )];
        return response()->json($data);
    }

    public function rejected($year) {
        $labels = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $data['labels'] = $labels;

        $muestras = collect();
        for ($i = 1; $i <= 12; $i++) {
            //Genera la primer fecha de inicio del año
            $firstDayMonth = Carbon::parse($year . '-' . $i . '-01')->startOfMonth()->toDateString();
            //Genera la ultima fecha del año
            $endDayMonth = Carbon::parse($year . '-' . $i . '-01')->endOfMonth()->toDateString();
            //Realiza el conteo de cuantos Hombre fueron registrados en un mes determinado
            $temp = DatosFederales::where('is_active', 1)->where('status', 'RECHAZO')->whereBetween('fecha_recepcion', [$firstDayMonth, $endDayMonth])->count();
            $muestras->push($temp);
        }
        $data['datasets'] = [array(
            'backgroundColor' => 'rgba(33, 97, 140, 0.5)',
            'borderColor' => 'rgb(33, 97, 140)',
            'borderWidth' => 1,
            'type' => 'bar',
            'label' => 'Muestras',
            'data' => $muestras->toArray()
        )];
        return response()->json($data);
    }

    public function rejectionReasons($year) {
        $data = DatosFederales::selectRaw("LTRIM( RTRIM( rechazos) ) AS rechazos")->where('status', 'RECHAZO')->orderBy('rechazos', 'ASC')->distinct()->get();
        $labels = collect();
        $muestras = collect();
        foreach( $data AS $label ) {
            $firstDayMonth = Carbon::parse($year . '-01-01')->startOfMonth()->toDateString();
            //Genera la ultima fecha del año
            $endDayMonth = Carbon::parse($year . '-12-01')->endOfMonth()->toDateString();
            $temp = DatosFederales::where('status', 'RECHAZO')->where('rechazos', $label->rechazos)->whereBetween('fecha_recepcion', [$firstDayMonth, $endDayMonth])->count();
            $muestras->push($temp);
            $labels->push($label->rechazos);
        }
        $data['labels'] = $labels->toArray();
        
        $data['datasets'] = [array(
            'backgroundColor' => 'rgba(41, 128, 185)',
            'borderColor' => 'rgb(41, 128, 185)',
            'borderWidth' => 1,
            'type' => 'bar',
            'label' => 'Muestras',
            'data' => $muestras->toArray()
        )];
        return response()->json($data);
    }

    public function rejectionJurisdiction($year)
    {
        //$labels = DatosFederales::motivosRechazosOptions();
        $labels = DatosFederales::selectRaw("LTRIM( RTRIM( rechazos) ) AS rechazos")->where('status', 'RECHAZO')->orderBy('rechazos', 'ASC')->distinct()->get();
        $jurisdictions = DatosFederales::join('juris', 'datos_federales.hospital', '=', 'juris.name')
        ->select("hospital")->where('juris.is_juri', 1)->where('datos_federales.status', 'RECHAZO')->distinct()->get();
        
        $backgrounds = collect([
            'rgba(123, 36, 28)', 'rgba(99, 57, 116 )', 'rgba(31, 97, 141)', 'rgba(20, 143, 119)', 'rgba(241, 196, 15 )', 'rgba(220, 118, 51)', 'rgba(236, 112, 99)', 'rgba(82, 190, 128)',
            'rgba(241, 148, 138)', 'rgba(215, 189, 226)', 'rgba(174, 214, 241)', 'rgba(82, 190, 128)', 'rgba(243, 156, 18)', 'rgba(69, 179, 157)', 'rgba(165, 105, 189)', 'rgba(88, 214, 141)'
        ]);
        $motivos = collect();
        $legends = collect();
        $i = 0;
        foreach ($jurisdictions as $jurisdiction ) {
            $juris = collect();
            foreach($labels as $label) {
                $firstDayMonth = Carbon::parse($year . '-01-01')->startOfMonth()->toDateString();
                //Genera la ultima fecha del año
                $endDayMonth = Carbon::parse($year . '-12-01')->endOfMonth()->toDateString();
                $temp = DatosFederales::where('status', 'RECHAZO')->where('hospital', $jurisdiction->hospital)->where('rechazos', $label->rechazos)->whereBetween('fecha_recepcion', [$firstDayMonth, $endDayMonth])->count();

                $juris->push( $temp );
            }
            
            $motivos->push(
                array(
                    'backgroundColor' => $backgrounds[$i],
                    //'borderColor' => 'rgb(41, 128, 185)',
                    //'borderWidth' => 1,
                    //'type' => 'bar',
                    'label' => $jurisdiction->hospital,
                    'data' => $juris->toArray()
                ));
            $i++;
        }

        foreach ($labels as $label) {
            $legends->push($label->rechazos);
        }
        $data['labels'] = $legends->toArray();
        $data['datasets'] = $motivos->toArray();
        //dd($data);
        return response()->json($data);
    }

    public function rejectionHospitals($year)
    {
        //$labels = DatosFederales::motivosRechazosOptions();
        $labels = DatosFederales::selectRaw("LTRIM( RTRIM( rechazos) ) AS rechazos")->where('status', 'RECHAZO')->orderBy('rechazos', 'ASC')->distinct()->get();
        $jurisdictions = DatosFederales::join('juris', 'datos_federales.hospital', '=', 'juris.name')
        ->select("hospital")->where('juris.is_juri', 0)->where('datos_federales.status', 'RECHAZO')->distinct()->get();

        $motivos = collect();
        $legends = collect();
        $i = 0;
        foreach ($jurisdictions as $jurisdiction) {
            $juris = collect();
            foreach ($labels as $label) {
                $firstDayMonth = Carbon::parse($year . '-01-01')->startOfMonth()->toDateString();
                //Genera la ultima fecha del año
                $endDayMonth = Carbon::parse($year . '-12-01')->endOfMonth()->toDateString();
                //\DB::enableQueryLog();
                $temp = DatosFederales::where('status', 'RECHAZO')->where('hospital', $jurisdiction->hospital)->where('rechazos', $label->rechazos)->whereBetween('fecha_recepcion', [$firstDayMonth, $endDayMonth])->count();
                //$temp = DatosFederales::where('status', 'RECHAZO')->where('hospital', $jurisdiction->hospital)->where('rechazos', $label->rechazos)->count();
                //dd(\DB::getQueryLog()); // Show results of log
                $juris->push($temp);
            }
            $juri = Juri::where('name', $jurisdiction->hospital)->first();
            $motivos->push(
                array(
                    'backgroundColor' => $juri->color,
                    //'borderColor' => 'rgb(41, 128, 185)',
                    //'borderWidth' => 1,
                    //'type' => 'bar',
                    'label' => $jurisdiction->hospital,
                    'data' => $juris->toArray()
                )
            );
            $i++;
        }
        foreach ($labels as $label) {
            $legends->push($label->rechazos);
        }
        $data['labels'] = $legends->toArray();
        $data['datasets'] = $motivos->toArray();
        //dd($data);
        return response()->json($data);
    }

    public function rejectionSample($year)
    {
        //$labels = DatosFederales::motivosRechazosOptions();
        $labels = DatosFederales::selectRaw("LTRIM( RTRIM( rechazos) ) AS rechazos")->where('status', 'RECHAZO')->orderBy('rechazos', 'ASC')->distinct()->get();
        $typeSamples = DatosFederales::tipoMuestrasOptions();

        $backgrounds = collect([
            'rgba(215, 189, 226)', 'rgba(174, 214, 241)', 'rgba(82, 190, 128)', 'rgba(243, 156, 18)', 'rgba(69, 179, 157)', 'rgba(165, 105, 189)', 'rgba(88, 214, 141)', 'rgb(185, 243, 228)',
            'rgba(123, 36, 28)', 'rgba(99, 57, 116 )', 'rgba(31, 97, 141)', 'rgba(20, 143, 119)', 'rgba(241, 196, 15 )', 'rgba(220, 118, 51)', 'rgba(236, 112, 99)', 'rgba(82, 190, 128)',
            'rgb(255, 151, 193)', 'rgb(255, 88, 88)', 'rgb(40, 84, 48)', 'rgb(164, 190, 123)', 'rgb(133, 88, 111)',
            'rgba(241, 148, 138)', 'rgba(215, 189, 226)', 'rgba(174, 214, 241)', 'rgba(82, 190, 128)', 'rgba(243, 156, 18)', 
            'rgba(69, 179, 157)', 'rgba(165, 105, 189)', 'rgba(88, 214, 141)', 'rgb(255, 127, 62)', 'rgb(255, 0, 0)', 'rgb(180, 123, 132)',
            'rgb(202, 166, 166)', 'rgb(255, 231, 231)'
        ]);
        $motivos = collect();
        $legends = collect();
        $i = 0;
        foreach ($typeSamples as $sample) {
            $juris = collect();
            foreach ($labels as $label) {
                $firstDayMonth = Carbon::parse($year . '-01-01')->startOfMonth()->toDateString();
                //Genera la ultima fecha del año
                $endDayMonth = Carbon::parse($year . '-12-01')->endOfMonth()->toDateString();
                $temp = DatosFederales::where('status', 'RECHAZO')->where('tipo_muestra', $sample)->where('rechazos', $label->rechazos)->whereBetween('fecha_recepcion', [$firstDayMonth, $endDayMonth])->count();

                $juris->push($temp);
            }

            $motivos->push(
                array(
                    'backgroundColor' => $backgrounds[$i],
                    //'borderColor' => 'rgb(41, 128, 185)',
                    //'borderWidth' => 1,
                    //'type' => 'bar',
                    'label' => $sample,
                    'data' => $juris->toArray()
                )
            );
            $i++;
        }

        foreach ($labels as $label) {
            $legends->push($label->rechazos);
        }
        $data['labels'] = $legends->toArray();
        $data['datasets'] = $motivos->toArray();
        //dd($data);
        return response()->json($data);
    }

    public function receivedSampleDay($date){
        $temp = explode('-', $date);
        $labels = array('Recepcionadas', 'Finalizadas', 'Rechazadas');
        
        $data['date'] = $temp[2].'/' . $temp[1] . '/' . $temp[0];

        $numData = collect();
        //array_push($labels, );
        $numData->push( DatosFederales::where('is_active', 1)->where('fecha_recepcion', 'LIKE', '%'.$date.'%')->count() );
        $numData->push( DatosFederales::where('is_active', 1)->where('status', 'FINALIZADO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->count());
        $numData->push( DatosFederales::where('is_active', 1)->where('status', 'RECHAZO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->count());

        //\DB::enableQueryLog();
        $tempQuery = DatosFederales::selectRaw('dx1 AS dx')->where('is_active', 1)->where('dx1', '!=', 'N/A')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->distinct();
        $tempQuery = $tempQuery->union(DatosFederales::selectRaw('dx2 AS dx')->where('is_active', 1)->where('dx2', '!=', 'N/A')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->distinct() );
        $tempQuery = $tempQuery->union(DatosFederales::selectRaw('dx3 AS dx')->where('is_active', 1)->where('dx3', '!=', 'N/A')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->distinct());
        $tempQuery = $tempQuery->union(DatosFederales::selectRaw('dx4 AS dx')->where('is_active', 1)->where('dx4', '!=', 'N/A')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->distinct());
        $tempQuery = $tempQuery->union(DatosFederales::selectRaw('dx5 AS dx')->where('is_active', 1)->where('dx5', '!=', 'N/A')->where('fecha_recepcion', 'LIKE', '%' . $date . '%')->distinct());

        $search = $tempQuery->get();

        $diagnostics = Diagnosis::select('diagnosis_name')->where('diagnosis_name', '!=', 'N/A')->where(function($query) use ($search) {
            foreach ($search as $row) {
                $query->orWhere('full_name_analysis', $row->dx);
            }
        })->distinct()->get();
        
        foreach($diagnostics as $row) {
            $analysis = Diagnosis::where('diagnosis_name', $row->diagnosis_name)->get();
            if($analysis->count() > 0 ) {
                $tempQuery = DatosFederales::where('is_active', 1)->where(function ($query) use ($analysis) {
                    $query->where('dx1', $analysis[0]->full_name_analysis)->orWhere('dx2', $analysis[0]->full_name_analysis)->orWhere('dx3', $analysis[0]->full_name_analysis)->orWhere('dx4', $analysis[0]->full_name_analysis)->orWhere('dx5', $analysis[0]->full_name_analysis);
                })->where('status', 'FINALIZADO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%');

                if($analysis->count() > 1) {
                    for($j = 1; $j < $analysis->count(); $j++ ) {
                        
                        $tempQuery = $tempQuery->union(DatosFederales::where('is_active', 1)->where(function ($query) use ($analysis, $j) {
                            $query->where('dx1', $analysis[$j]->full_name_analysis)->orWhere('dx2', $analysis[$j]->full_name_analysis)->orWhere('dx3', $analysis[$j]->full_name_analysis)->orWhere('dx4', $analysis[$j]->full_name_analysis)->orWhere('dx5', $analysis[$j]->full_name_analysis);
                        })->where('status', 'FINALIZADO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%'));   
                    }
                }
                $numData->push( $tempQuery->count() );
            }
            //}
            
            //[$diagnostics[0]->diagnosis_name
            array_push($labels, [$row->diagnosis_name, 'Finalizadas']);

            if ($analysis->count() > 0) {
                $tempQuery = DatosFederales::where('is_active', 1)->where(function ($query) use ($analysis) {
                    $query->where('dx1', $analysis[0]->full_name_analysis)->orWhere('dx2', $analysis[0]->full_name_analysis)->orWhere('dx3', $analysis[0]->full_name_analysis)->orWhere('dx4', $analysis[0]->full_name_analysis)->orWhere('dx5', $analysis[0]->full_name_analysis);
                })->where('status', 'RECHAZO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%');

                if ($analysis->count() > 1) {
                    for ($j = 1; $j < $analysis->count(); $j++) {

                        $tempQuery = $tempQuery->union(DatosFederales::where('is_active', 1)->where(function ($query) use ($analysis, $j) {
                            $query->where('dx1', $analysis[$j]->full_name_analysis)->orWhere('dx2', $analysis[$j]->full_name_analysis)->orWhere('dx3', $analysis[$j]->full_name_analysis)->orWhere('dx4', $analysis[$j]->full_name_analysis)->orWhere('dx5', $analysis[$j]->full_name_analysis);
                        })->where('status', 'RECHAZO')->where('fecha_recepcion', 'LIKE', '%' . $date . '%'));
                    }
                }
                $numData->push($tempQuery->count());
            }
            array_push($labels, [$row->diagnosis_name, 'Rechazadas']);
        }
        $data['labels'] = $labels;
        $data['datos'] = $numData->toArray();
        $data['background'] = [
            'rgba(52, 152, 219, 0.4)',
            'rgba(39, 174, 96, 0.4)',
            'rgba(231, 76, 60, 0.4)',
            'rgba(247, 220, 111, 0.4)',
            'rgba(174, 182, 191, 0.4)',
            'rgba(28, 130, 173, 0.4)',
            'rgba(130, 170, 227, 0.4)',
            'rgba(254, 190, 140, 0.4)',
            'rgba(247, 164, 164, 0.4)',
            'rgba(158, 213, 197, 0.4)',
            'rgba(108, 74, 182, 0.4)',
            'rgba(185, 224, 25, 0.4)'
        ];
        $data['border'] = [
            'rgb(52, 152, 219)',
            'rgb(39, 174, 96)',
            'rgb(231, 76, 60)',
            'rgb(247, 220, 111)',
            'rgb(174, 182, 191)',
            'rgb(28, 130, 173)',
            'rgb(130, 170, 227)',
            'rgb(254, 190, 140)',
            'rgb(247, 164, 164)',
            'rgb(158, 213, 197)',
            'rgb(108, 74, 182)',
            'rgb(185, 224, 255)'
        ];
        /* $data['datasets'] = [array(
            'backgroundColor' => 'rgba(58, 176, 255, 0.4)',
            'borderColor' => ,
            'borderWidth' => 1,
            'type' => 'bar',
            'label' => 'Muestras',
            'data' => 
        )]; */
        return response()->json($data);
    }

    public function respiratorios($start, $end)
    {
        
        $labels = array('RECEPCIONADAS', 'FINALIZADAS', 'SARS', 'INFLUENZA', 'VSR', 'NEGATIVAS');
        
        $temp = explode('-', $start);
        $data['start'] = $temp[2] . '/' . $temp[1] . '/' . $temp[0];
        $temp = explode('-', $end);
        $data['end'] = $temp[2] . '/' . $temp[1] . '/' . $temp[0];

        $start = $start;//.' 00:00:00' ;
        $end = $end;//.' 23:59:59' ;

        $numData = collect();
        //\DB::enableQueryLog();//whereNot
        $samplesReceived = DatosFederales::where('is_active', 1)->where(function($query){
            $query->where('dx1', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx1', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx1', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
            ->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx2', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
            ->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx3', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
            ->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx4', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2')
            ->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.3 VSR')->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.1 INFLUENZA')->orWhere('dx5', 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2');
        })->whereBetween('fecha_recepcion', [$start, $end])->count();

        $numData->push( $samplesReceived );
        //dd(\DB::getQueryLog());
        $results = DB::table('view_full_results')->select('id')->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->whereBetween('fecha_recepcion', [$start, $end])->where('status_result', 'FINALIZADO')->groupBy('id')->get();
        $numData->push( $results->count() );
        
        $negativas =DB::table('view_full_results')->whereBetween('fecha_recepcion', [$start, $end])->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->where('result', 'not like', '%posi%')->count();
        $sars = DB::table('view_full_results')->whereBetween('fecha_recepcion', [$start, $end])->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->where('analysis', 'SARS-COV-2')->where('result', 'like', '%posi%')->count();
        $influenza = DB::table('view_full_results')->whereBetween('fecha_recepcion', [$start, $end])->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->where('analysis', 'INFLUENZA')->where('result', 'like', '%posi%')->count();
        $vsr = DB::table('view_full_results')->whereBetween('fecha_recepcion', [$start, $end])->where('diagnosis_name', 'VIRUS RESPIRATORIOS')->where('analysis', 'VSR')->where('result', 'like', '%posi%')->count();

        $numData->push($sars, $influenza, $vsr, $negativas );
        
        $data['labels'] = $labels;
        $data['datos'] = $numData->toArray();
        $data['background'] = [
            'rgba(52, 152, 219, 0.4)',
            'rgba(39, 174, 96, 0.4)',
            'rgba(231, 76, 60, 0.4)',
            'rgba(247, 220, 111, 0.4)',
            'rgba(174, 182, 191, 0.4)',
            'rgba(28, 130, 173, 0.4)',
            'rgba(130, 170, 227, 0.4)',
            'rgba(254, 190, 140, 0.4)',
            'rgba(247, 164, 164, 0.4)',
            'rgba(158, 213, 197, 0.4)',
            'rgba(108, 74, 182, 0.4)',
            'rgba(185, 224, 25, 0.4)'
        ];
        $data['border'] = [
            'rgb(52, 152, 219)',
            'rgb(39, 174, 96)',
            'rgb(231, 76, 60)',
            'rgb(247, 220, 111)',
            'rgb(174, 182, 191)',
            'rgb(28, 130, 173)',
            'rgb(130, 170, 227)',
            'rgb(254, 190, 140)',
            'rgb(247, 164, 164)',
            'rgb(158, 213, 197)',
            'rgb(108, 74, 182)',
            'rgb(185, 224, 255)'
        ];
        /* $data['datasets'] = [array(
            'backgroundColor' => 'rgba(58, 176, 255, 0.4)',
            'borderColor' => ,
            'borderWidth' => 1,
            'type' => 'bar',
            'label' => 'Muestras',
            'data' => 
        )]; */
        return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\DatosFederales;
use App\Diagnosis;
use App\Exports\Charts\RejectedExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class TableRejectedController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $year = $request->year;
        $labels = collect(['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Total']);
        //$months = collect();
        $diagnostics = Diagnosis::select('diagnosis_name')->where('is_active', 1)->where('diagnosis_name', '!=', 'N/A')->groupBy('diagnosis_name')->orderBy('diagnosis_name', 'ASC')->get();
        $diagnosticsCount = collect();

        foreach ($diagnostics as $diagnostic) {
            $diagnosticsCountMount = collect();
            for ($i = 1; $i <= 12; $i++) {
                //Genera la primer fecha de inicio del año
                $firstDayMonth = Carbon::parse($year . '-' . $i . '-01')->startOfMonth()->toDateString();
                //Genera la ultima fecha del año
                $endDayMonth = Carbon::parse($year . '-' . $i . '-01')->endOfMonth()->toDateString();
                $temp = DatosFederales::where('is_active', 1)->where(function ($sql) use ($diagnostic) {
                    $sql->where('dx1', 'like', "$diagnostic->diagnosis_name%")
                    ->orWhere('dx2', 'like', "$diagnostic->diagnosis_name%")
                    ->orWhere('dx3', 'like', "$diagnostic->diagnosis_name%")
                    ->orWhere('dx4', 'like', "$diagnostic->diagnosis_name%")
                    ->orWhere('dx5', 'like', "$diagnostic->diagnosis_name%");
                })->where('status', 'RECHAZO')->whereBetween('fecha_recepcion', [$firstDayMonth, $endDayMonth])->count();

                $diagnosticsCountMount->push($temp);
            }
            $diagnosticsCount->push($diagnosticsCountMount->prepend($diagnostic->diagnosis_name));
        }

        $filename = 'Rechazadas_' . date('dmYHis') . '.xlsx';
        return Excel::download((new RejectedExport($labels, $diagnosticsCount, $year)), $filename, null, [\Maatwebsite\Excel\Excel::XLSX]);
    }
}

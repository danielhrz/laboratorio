<?php

namespace App\Http\Controllers;

use App\Role;
use App\Users;
use App\Juri;
use App\Area;
use DB;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public  function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['create', 'edit', 'destroy']]);
    }

    public function index(Request $request)
    {
        
        $rutaSection = "usuarios";
        $section = 'Lista de Usuarios';

        $usuarios = DB::table('view_users')->get();

        return view('main_admin.users.index', compact('usuarios', 'rutaSection', 'section'));
    }

    public function dataTableU(Request $request)
    {
        $query = DB::table('view_users')->where('is_active', 1);
        
        return DataTables::of($query)
        
        ->editColumn('created_at', function ($user) {
            $day = Carbon::parse($user->created_at)->diffForHumans();
            return $day;
        })
        ->filterColumn('created_at', function ($query, $keyword) {
        $query->whereRaw("created_at like ?", ["%$keyword%"]);
        })
        ->filterColumn('username', function ($query, $keyword) {
        $query->whereRaw("username like ?", ["%$keyword%"]);
        })
        ->filterColumn('full_name', function ($query, $keyword) {
        $query->whereRaw("full_name like ?", ["%$keyword%"]);
        })
        ->filterColumn('phone', function ($query, $keyword) {
        $query->whereRaw("phone like ?", ["%$keyword%"]);
        })
        ->filterColumn('ext', function ($query, $keyword) {
        $query->whereRaw("ext like ?", ["%$keyword%"]);
        })
        ->filterColumn('email', function ($query, $keyword) {
        $query->whereRaw("email like ?", ["%$keyword%"]);
        })
        ->filterColumn('name_role', function ($query, $keyword) {
        $query->whereRaw("name_role like ?", ["%$keyword%"]);
        })
        ->filterColumn('name_juris', function ($query, $keyword) {
        $query->whereRaw("name_juris like ?", ["%$keyword%"]);
        })
        ->filterColumn('name_ubicacion', function ($query, $keyword) {
        $query->whereRaw("name_ubicacion like ?", ["%$keyword%"]);
        })
        ->addColumn('btn', 'main_admin.users.btn')
        ->smart(false)
        ->rawColumns(['btn'])
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $juris = Juri::all(); 
        $roles = Role::all();
        $usuarios = Users::all();
        $areas = Area::all();

        return view('main_admin.users.create', compact('juris', 'roles', 'usuarios', 'areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* dd($request->all()); */
        $atributos = $request->validate([
            'id_role' => ['required', 'exists:role,id'],
            'username' => ['required', 'string', 'max:200', 'unique:users'],
            'name' => ['required', 'string', 'max:200'],
            'paternal_surname' => ['required', 'string', 'max:200'],
            'maternal_surname' => ['required', 'string', 'max:200'],
            'id_juris' => ['required', 'exists:juris,id'],
            'id_areas' => ['required', 'exists:areas,id'],
            'phone' => ['required', 'string', 'max:20'],
            'ext' => ['required', 'string', 'max:4'],
            'email' => ['required', 'string', 'email', 'max:200', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/', 'confirmed']
            ]);
        
        $atributos['password'] = Hash::make(request('password'));

        Users::create($atributos);

        return redirect('usuarios')->with('success', 'Usuario registrado con éxito!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show(Resources $users)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $usuario = Users::findOrFail($id);
        /* dd($usuario); */
        $juris = Juri::all(); 
        $areas = Area::all();
        $roles = Role::all();

        return view('main_admin.users.edit', compact('usuario', 'juris', 'roles', 'areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_role' => ['required', 'exists:role,id'],
            'username' => ['required', 'string', 'max:200'],
            'name' => ['required', 'string', 'max:200'],
            'paternal_surname' => ['required', 'string', 'max:200'],
            'maternal_surname' => ['required', 'string', 'max:200'],
            'id_juris' => ['required', 'exists:juris,id'],
            'id_areas' => ['required', 'exists:areas,id'],
            'phone' => ['required', 'string', 'max:20'],
            'ext' => ['required', 'string', 'max:4'],
            'email' => ['required', 'string', 'email', 'max:200'],
            'password' => ['required', 'string', 'min:8', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/', 'confirmed']
            ]);
        //
        $users=request()->except(['_token','_method','password_confirmation']); 

        $users['password'] = Hash::make(request('password'));

        Users::where('id','=',$id)->update($users);

        return redirect('usuarios')->with('success', 'Usuario actualizado con éxito!');
    }

    public function destroy($id)
    {
        $usuario = Users::find($id);
        $usuario->is_active = 0;
        $usuario->save();
    }

}


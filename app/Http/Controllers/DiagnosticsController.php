<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DiagnosticsController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
    }

    public function respiratorio()
    {
        $section = 'Estadísticos';
        return view('main_admin.diagnostic.respiratorio', compact("section"));
    }
}

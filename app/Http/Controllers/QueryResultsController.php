<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChangesDatosFederales;
use DB;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Destinatario;
use App\Manager;
use App\DatosFederales;
use App\Juri;
use App\Users;
use App\ViewResults;
use App\ViewAnalysisResult;
use PDF;

class QueryResultsController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['destroy']]);
    }

        public function index(Request $request)
    {
        $rutaSection = "full";
        $section = 'Consulta de Resultados';
        
        return view('main_admin.resultados.queryResults', compact('rutaSection', 'section'));
    }

        public function dataTableQueryResults(Request $request)
    {
        $sQuery = DatosFederales::where('is_active', 1)->where(function($query) {
            if(Auth::user()->id_role == 8) {
                $jurisdiction = Juri::find(Auth::user()->id_juris);

                $query->where('hospital', $jurisdiction->name);
            } else if(Auth::user()->id_role == 10) {
                $query->where('dx1', 'like', Auth::user()->diagnostics . '%')
                ->orWhere('dx2', 'like', Auth::user()->diagnostics.'%')
                ->orWhere('dx3', 'like', Auth::user()->diagnostics . '%')
                ->orWhere('dx4', 'like', Auth::user()->diagnostics . '%')
                ->orWhere('dx5', 'like', Auth::user()->diagnostics . '%');
            }
        })
        ->where(function($query) {
            $query->where('operational_status', '!=', 'SIN REVISION');
        })
        ->where('operational_status', '!=', 'CORRECCIÓN');
        
        return DataTables::of($sQuery)

        ->filterColumn('operational_status', function ($sQuery, $keyword) {
        $sQuery->whereRaw("operational_status like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_lesp', function ($sQuery, $keyword) {
        $sQuery->whereRaw("folio_lesp like ?", ["%$keyword%"]);
        })
        ->filterColumn('folio_sisver', function ($sQuery, $keyword) {
        $sQuery->whereRaw("folio_sisver like ?", ["%$keyword%"]);
        })
        ->filterColumn('nombre_paciente', function ($sQuery, $keyword) {
        $sQuery->whereRaw("nombre_paciente like ?", ["%$keyword%"]);
        })
        ->addColumn('operational_status', function ($results) {
            if ($results->operational_status == 'SIN REVISION') {
                return '<span data-value="SIN REVISION" class="tooltip-review bg-gray-300 text-grey text-sm font-bold mr-2 px-2.5 py-0.5 rounded ">SIN REVISION</span >';
            }
            if ($results->operational_status == 'CONCLUIDO') {
                return '<span data-value="CONCLUIDO" class="tooltip-completed bg-green-300 text-green-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">CONCLUIDO</span >';
            }
            if ($results->operational_status == 'CORRECCIÓN') {
                return '<span data-value="CORRECCIÓN" class="tooltip-correct bg-yellow-300 text-yellow-700 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">CORRECCIÓN</span >';
            }
            if ($results->operational_status == 'PENDIENTE A CONFIRMACIÓN DE INFLUENZA') {
                return '<span data-value="PENDIENTE A CONFIRMACIÓN DE INFLUENZA" class="tooltip-influenza bg-fuchsia-300 text-fuchsia-700 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">PENDIENTE A CONFIRMACIÓN DE INFLUENZA</span >';
            }
            if ($results->operational_status == 'REPROCESO') {
                return '<span data-value="REPROCESO" class="tooltip-return bg-blue-300 text-blue-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">REPROCESO</span >';
            }
            if ($results->operational_status == 'MUESTRA RETRASADA POR DIFICULTADES TÉCNICAS') {
                return '<span data-value="MUESTRA RETRASADA POR DIFICULTADES TÉCNICAS" class="tooltip-return bg-red-300 text-red-800 text-sm font-bold mr-2 px-2.5 py-0.5 rounded">MUESTRA RETRASADA POR DIFICULTADES TÉCNICAS</span >';
            }
        
        })
        ->smart(false)
        ->addColumn('btn', 'main_admin.resultados.btn')
        ->rawColumns(['btn', 'operational_status'])
        ->toJson();
    }

    public function exportJurisdiction($id) {
        $datoFederal = DatosFederales::find($id);
        $resultsRegister = ViewResults::where('id', $id)->get();
        $judi = Juri::find($resultsRegister[0]->jurisdiction_id);

        $diagnosis = ViewAnalysisResult::where('result_id', $resultsRegister[0]->result_id)->first();
        $manager = Manager::where('diagnosis', $diagnosis->diagnosis_name)->first();

        $initials = Users::find($datoFederal->user_id_operational_status_change);
        $auxDiagnostics = collect();
        foreach ($resultsRegister as $result) {
            $aTemp = collect($result->toArray());
            $temp = ViewAnalysisResult::where('result_id', $result->result_id)->get();
            $aTemp->put('diagnostics', $temp);
            $auxDiagnostics->push($aTemp->toArray());
        }
        $destino = Destinatario::where('jurisdiccion', $judi->name)->where('range', 1)->first();
        $filename = 'LESP-VE-FO-0037-008 ' . date('d-m-Y His') . '.pdf';
        $data['oficio'] = '';
        $data['results'] = $auxDiagnostics;
        $data['receiver'] = $destino;
        $data['print'] = 1;
        $data['exit'] = strtoupper($resultsRegister[0]->if_exit);
        $data['initials'] = $initials->initials;
        $data['manager'] = $manager;

        $temp = Users::where('id', $resultsRegister[0]->user_register_result)->first();
        $data['rubrica'][] = $temp->rubric;
        $data['rubrica'][] = $initials->rubric;

        $data['genero'] = $resultsRegister[0]->initials;
        $data['judi'] = true;
        $pdf = PDF::loadView('main_admin.export-result.pdf-lesp-da-fo-037-004', $data, [], [
            'title'      => 'LESP-VE-FO-037/008',
            'margin_top' => 85,
            'margin_bottom' => 80
        ]);
        return $pdf->stream('LESP-VE-FO-0037-008 ' . date('d-m-Y His') . '.pdf');
    }
}

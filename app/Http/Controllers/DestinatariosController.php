<?php

namespace App\Http\Controllers;

use App\Destinatario;
use Illuminate\Http\Request;
use DataTables;

class DestinatariosController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rutaSection = "destinatarios";
        $section = 'Destinatarios';
        return view('main_admin.destinatarios.index', compact('rutaSection', 'section'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('main_admin.destinatarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $atributos = $request->validate([
            'jurisdiccion' => ['required', 'string'],
            'encargado' => ['required', 'string', 'max:200'],
            'address' => ['required', 'string'],
            'position' => ['required', 'string'],
        ]);

        $atributos['is_active'] = 2;
        Destinatario::create($atributos);

        return redirect('destinatarios')->with('success', 'Destinatario registrado con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $destinatario = Destinatario::findOrFail($id);
        return view('main_admin.destinatarios.edit', compact('destinatario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $atributos = $request->validate([
            'jurisdiccion' => ['required', 'string'],
            'encargado' => ['required', 'string', 'max:200'],
            'address' => ['required', 'string'],
            'position' => ['required', 'string'],
        ]);

        $destinatario = request()->except(['_token', '_method']);

        Destinatario::where('id', '=', $id)->update($destinatario);
        return redirect('destinatarios')->with('success', 'Destinatario actualizado con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destinatario = Destinatario::find($id);
        $destinatario->is_active = 0;
        $destinatario->save();
    }

    public function BuildDataTable(Request $request) {
        $data = Destinatario::all()->where('is_active', '!=', 0);
        return DataTables::of($data)
            // ->editColumn('full_name', function ($patients) {
            //     return mb_strtoupper($patients->full_name);
            // })
            // ->editColumn('curp', function ($patients) {
            //     return mb_strtoupper($patients->curp);
            // })
            // ->editColumn('gender', function ($patients) {
            //     return mb_strtoupper($patients->gender);
            // })
            ->addColumn('btn', 'main_admin.destinatarios.btn')
            ->rawColumns(['btn'])
            ->toJson();
    }
}

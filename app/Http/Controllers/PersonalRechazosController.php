<?php

namespace App\Http\Controllers;

use App\PersonalRechazos;
use Illuminate\Http\Request;

class PersonalRechazosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonalRechazos  $personalRechazos
     * @return \Illuminate\Http\Response
     */
    public function show(PersonalRechazos $personalRechazos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonalRechazos  $personalRechazos
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonalRechazos $personalRechazos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonalRechazos  $personalRechazos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonalRechazos $personalRechazos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonalRechazos  $personalRechazos
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonalRechazos $personalRechazos)
    {
        //
    }
}

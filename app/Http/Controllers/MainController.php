<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\support\Facades\Auth;

class MainController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $fecha_actual = Carbon::now()->formatLocalized('%d/%B/%Y');
        return view('main_admin.main.index', compact('fecha_actual'));
    }

    public function downloadGeneral() {
        if(Auth::user()->id_role == 1 || Auth::user()->id_role == 2 || Auth::user()->id_role == 3 || Auth::user()->id_role == 6 || Auth::user()->id_role == 5 || Auth::user()->id_role == 7 || Auth::user()->id == 18 || Auth::user()->id_role == 11) {
            return view('main_admin.downloads.index');
        } else {
            return redirect('main_admin');
        }
    }
}

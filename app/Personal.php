<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    protected $table = 'personal';

    public function RecepcionMuestras()
    {
        return $this->hasMany(RecepcionMuestras::class);
    }

    public function DatosFederales()
    {
        return $this->belongsTo(DatosFederales::class);
    }
}

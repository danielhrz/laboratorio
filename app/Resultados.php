<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resultados extends Model
{
    protected $table = 'resultados';

    public function RecepcionMuestras()
    {
        return $this->belongsTo(RecepcionMuestras::class);
    }
}

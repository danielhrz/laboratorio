<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewAnalysisResult extends Model
{
    protected $table = "view_analysis_result";
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformationExtension extends Model
{
    /** 801589824461A60A00WX1J
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Relaciones uno a uno
     */
    public function datoFederalPaludismo()
    {
        return $this->hasOne(DatosFederales::class);
    }
}

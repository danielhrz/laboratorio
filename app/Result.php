<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';

    protected $fillable = ['jurisdiction_id', 'coments', 'interpretation_result', 'sample_collection_time', 'status', 'result', 'cre', 'user_initials', 'user_initials_send', 'kit_id',
                            'if_exit', 'date_delivery', 'date_emission', 'date_send', 'date_delivery_remu', 'date_delivery_user', 'user_id', 'dato_federal_id', 'is_active',
                            'observations', 'finalized_result_date', 'date_complete_operational_status', 'history'
                        ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destinatario extends Model
{
    protected $table = 'destinatarios';

    protected $fillable = ['jurisdiccion', 'id', 'created_at', 'updated_at', 'is_active', 'encargado', 'address', 'position'];
}

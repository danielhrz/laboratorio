<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadesMedicas extends Model
{
    protected $table = 'unidades_medicas';

    public function DatosPacientes(){

        return $this->hasMany(DatosPacientes::class);
    }
}

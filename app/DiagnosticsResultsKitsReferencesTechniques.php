<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiagnosticsResultsKitsReferencesTechniques extends Model
{
    protected $table = 'diagnostics_results_kits_references_techniques';

    protected $fillable = ['is_active', 'result_id', 'diagnosis_id', 'result_kit_id', 'result_reference_value_id', 'technique_id', 'result'];
}

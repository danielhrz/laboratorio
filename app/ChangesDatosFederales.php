<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangesDatosFederales extends Model
{
    protected $table = 'log_change_datos_federales';

    protected $fillable = [ 'anio', 'folio_lesp', 'dx1', 'dx2', 'dx3', 'dx4',
        'dx5', 'oficio_entrada', 'fecha_recepcion', 'hora_recepcion', 'hospital',
        'nombre_paciente', 'tipo_muestra', 'fecha_toma_muestra', 'folio_sisver',
        'persona_recibe', 'status', 'rechazos', 'observaciones', 'aclaraciones_remu',
        'user_id', 'is_active', 'dato_federal_id', 'hora_recepcion'
    ];
}

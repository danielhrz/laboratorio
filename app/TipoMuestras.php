<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoMuestras extends Model
{
    protected $table = 'tipo_muestras';

    public function DatosFederales()
    {
        return $this->belongsTo(DatosFederales::class);
    }

    public function RecepcionMuestras()
    {
        return $this->belongsTo(RecepcionMuestras::class);
    }
}

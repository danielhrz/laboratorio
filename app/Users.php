<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = [

        'id_role', 'username', 'name', 'paternal_surname', 'maternal_surname', 'id_juris', 'id_areas', 'phone', 'ext', 'email', 'password'  

    ];
    
    protected $hidden = [

        'password', 'remember_token',

    ];

    public function areas(){

        return $this->belongsTo(Area::class, 'id_areas');

    }

    public function juris(){

        return $this->belongsTo(Juri::class, 'id_juris');

    }

    public function role(){

        return $this->belongsTo(Role::class, 'id_role');

    }

    public function DatosFederales(){

        return $this->hasMany(DatosFederales::class);

    }

    public function scopeBuscador($query, $usuario){

        if ($usuario != ""){

            $query->where(\DB::raw("CONCAT(username, ' ', name, ' ', paternal_surname, ' ', maternal_surname, ' ', phone, ' ', ext, ' ', email)"), "LIKE", "%$usuario%");
            /* $query->where('admin_unit', "LIKE", "%$volante%"); */

        }

    }

    public function getFullNameAttribute() {
        return $this->name.' '.$this->paternal_surname.' '.$this->maternal_surname;
    }
}

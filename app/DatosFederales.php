<?php

namespace App;

use App\Traits\HasTipoMuestras;
use App\Traits\HasPersonalRecibe;
use App\Traits\HasHospitalesJuris;
use App\Traits\HasDiagnosticos;
use App\Traits\HasStatus;
use App\Traits\HasMotivosRechazos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class DatosFederales extends Model
{
    const MUESTRAS_EXUDADO_FARÍNGEO = 'EXUDADO FARINGEO';
    const MUESTRAS_EXUDADO_NASOFARINGEO = 'EXUDADO NASOFARINGEO';
    const MUESTRAS_LAMINILLA = 'LAMINILLA';
    const MUESTRAS_EXPECTORACIÓN = 'EXPECTORACION';
    const MUESTRAS_BIOPSIA = 'BIOPSIA';
    const MUESTRAS_SANGRE_TOTAL = 'SANGRE TOTAL';
    const MUESTRAS_MATERIA_FECAL = 'MATERIA FECAL';
    const MUESTRAS_LAMINILLA_SANGRE_TOTAL = 'LAMINILLA/SANGRE TOTAL';
    const MUESTRAS_LAVADO_BRONQUIAL = 'LAVADO BRONQUIAL';
    const MUESTRAS_LAVADO_GASTRICO = 'LAVADO GASTRICO';
    const MUESTRAS_ENCÉFALO_CANINO = 'ENCEFALO CANINO';
    const MUESTRAS_ENCÉFALO_FELINO = 'ENCEFALO FELINO';
    const MUESTRAS_SALIVA = 'SALIVA';
    const MUESTRAS_LÍQUIDO_CEFALORRAQUÍDEO = 'LIQUIDO CEFALORRAQUIDEO';
    const MUESTRAS_LIQUÍDO_PLEURAL = 'LIQUIDO PLEURAL';
    const MUESTRAS_OTROS_LÍQUIDOS = 'OTROS LIQUIDOS';
    const MUESTRAS_EXUDADO_FARINGEO_EXUDADO_NASOFARINGEO = 'EXUDADO FARINGEO / EXUDADO NASOFARINGEO';
    const MUESTRAS_ESPÉCIMEN_COMPLETO = 'ESPECIMEN COMPLETO';
    const MUESTRAS_NA = 'N/A';
    const MUESTRAS_SD = 'S/D';
    const MUESTRAS_RNA = 'RNA';
    const MUESTRAS_CLINICA = 'MUESTRA CLINICA';
    const MUESTRAS_SUERO = 'SUERO';
    const MUESTRAS_QUIROPTERO = 'QUIROPTERO';
    const MUESTRAS_CEPA = 'CEPA';
    const MUESTRAS_ORINA = 'ORINA';
    const MUESTRAS_LÍQUIDO_AMNIÓTICO= 'LÍQUIDO AMNIÓTICO';
    const MUESTRAS_NECROPSIA= 'NECROPSIA';
    const MUESTRAS_ADN= 'ADN';
    const MUESTRAS_ENCÉFALO= 'ENCEFALO';
    const MUESTRAS_EXTRACTO= 'EXTRACTO';
    const MUESTRAS_FROTIS= 'FROTIS';
    const MUESTRAS_EXTENDIDO_MEDULA_OSEA= 'EXTENDIDO DE MÉDULA ÓSEA';
    const MUESTRAS_IMPRONTA_PAPEL_WHATMAN= 'IMPRONTA EN PAPEL WHATMAN';

    const PERSONAL_UNO = 'NATALIA NADIR SOTO ESCOBAR';
    const PERSONAL_DOS = 'JAIME ANTONIO BECERRA GARCÍA';
    /* const PERSONAL_TRES = 'FRANCISCO RAYMUNDO FUENTES GONZÁLEZ'; */
    const PERSONAL_CUATRO = 'BENITO GACHUZ NÁJERA';
    const PERSONAL_CINCO = 'LAURA ISABEL DÍAZ TÉLLEZ';
    /* const PERSONAL_SEIS = 'EDUARDO JAVIER MEDINA IRIARTE'; */
    /* const PERSONAL_SIETE = 'DULCE GUADALUPE MEZA PÉREZ'; */
    /* const PERSONAL_OCHO = 'CARLOS RAUL NAVARRETE JIMÉNEZ'; */
    const PERSONAL_NUEVE = 'NADIA ARTEAGA VILLEDA';
    /* const PERSONAL_DIES = 'JONATHAN MARTIN BENITEZ RAMIREZ'; */
    /* const PERSONAL_ONCE = 'LUIS DANIEL VILLAVERDE RAMIREZ'; */
    /* const PERSONAL_DOCE = 'MARIA LUISA DE LOS ANGELES AGUIRRE MANZO'; */
    const PERSONAL_TRECE = 'LUCIA HERNÁNDEZ ACEVEDO';
    const PERSONAL_CATORCE = 'REY DAVID BALTAZAR FERNÁNDEZ';

    const HOSPITALES_1 = 'N/A';
    const HOSPITALES_2 = 'ALVARO OBREGON';
    const HOSPITALES_3 = 'AZCAPOTZALCO';
    const HOSPITALES_4 = 'BENITO JUAREZ';
    const HOSPITALES_5 = 'CLINICA HOSPITAL EMILIANO ZAPATA';
    const HOSPITALES_6 = 'CLINICA HOSPITAL ESP TOXICOLOGICAS V. CARRANZA';
    const HOSPITALES_7 = 'COYOACAN';
    const HOSPITALES_8 = 'CUAJIMALPA';
    const HOSPITALES_9 = 'CUAUHTEMOC';
    const HOSPITALES_10 = 'FISCALIA GENERAL DE JUSTICIA';
    const HOSPITALES_11 = 'GUSTAVO A. MADERO';
    const HOSPITALES_12 = 'HOSPITAL GENERAL AJUSCO MEDIO';
    const HOSPITALES_13 = 'HOSPITAL GENERAL BALBUENA';
    const HOSPITALES_14 = 'HOSPITAL GENERAL DR ENRIQUE CABRERA';
    const HOSPITALES_15 = 'HOSPITAL GENERAL DR GREGORIO SALAS';
    const HOSPITALES_16 = 'HOSPITAL GENERAL DR RUBEN LEÑERO';
    const HOSPITALES_17 = 'HOSPITAL GENERAL LA VILLA';
    const HOSPITALES_18 = 'HOSPITAL GENERAL TICOMAN';
    const HOSPITALES_19 = 'HOSPITAL GENERAL TLAHUAC';
    const HOSPITALES_20 = 'HOSPITAL GENERAL XOCO';
    const HOSPITALES_21 = 'HOSPITAL MATERNO INFANTIL INGUARAN';
    const HOSPITALES_22 = 'HOSPITAL PEDIATRICO AZCAPOTZALCO';
    const HOSPITALES_23 = 'HOSPITAL PEDIATRICO COYOACAN';
    const HOSPITALES_24 = 'HOSPITAL PEDIATRICO LA VILLA';
    const HOSPITALES_25 = 'HOSPITAL PEDIATRICO LEGARIA';
    const HOSPITALES_26 = 'HOSPITAL PEDIATRICO PERALVILLO';
    const HOSPITALES_27 = 'HOSPITAL PEDIATRICO TACUBAYA';
    const HOSPITALES_28 = 'IZTACALCO';
    const HOSPITALES_29 = 'IZTAPALAPA';
    const HOSPITALES_30 = 'LVE-CDMX';
    const HOSPITALES_31 = 'MAGDALENA CONTRERAS';
    const HOSPITALES_32 = 'MIGUEL HIDALGO';
    const HOSPITALES_33 = 'MILPA ALTA';
    const HOSPITALES_34 = 'TLAHUAC';
    const HOSPITALES_35 = 'TLALPAN';
    const HOSPITALES_36 = 'VENUSTIANO CARRANZA';
    const HOSPITALES_37 = 'XOCHIMILCO';
    const HOSPITALES_38 = 'HOSPITAL ESPECIALIDADES DR. BELISARIO DOMINGUEZ';
    const HOSPITALES_39 = 'CENTRO ESPECIALIZADO DE INTERNAMIENTO PREVENTIVO PARA ADOLESCENTES';
    const HOSPITALES_40 = 'CENTRO ESPECIALIZADO PARA MUJERES ADOLESCENTES';
    const HOSPITALES_41 = 'LESP-CDMX';
    const HOSPITALES_42 = 'HOSPITAL PEDIATRICO SAN JUAN DE ARAGON';
    const HOSPITALES_43 = 'CENTRO DERMATOLOGICO DR LADISLAO DE LA PASCUA';
    const HOSPITALES_44 = 'INSTITUTO NACIONAL DE MIGRACION';
    const HOSPITALES_45 = 'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL ORIENTE';
    const HOSPITALES_46 = 'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL SUR';
    const HOSPITALES_47 = 'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL NORTE';
    const HOSPITALES_48 = 'UNIDAD MEDICA DE LA PENITENCIARIA DE LA CDMX';
    const HOSPITALES_49 = 'INDRE';
    const HOSPITALES_50 = 'C.C.C. LUIS PASTEUR';
    const HOSPITALES_51 = 'C.C.C. DR. ALFONSO ANGELLINI DE LA GARZA';
    const HOSPITALES_52 = 'UNIDAD MEDICA DEL CENTRO VARONIL DE REINSERCION SOCIAL SANTA MARTHA ACATITLA';
    const HOSPITALES_53 = 'OMS';
    const HOSPITALES_54 = 'HOSPITAL PEDIATRICO MOCTEZUMA';
    const HOSPITALES_55 = 'UNIDAD MÉDICA DEL CENTRO DE READAPTACIÓN SOCIAL VARONIL SANTA MARTHA ACATITLA';
    const HOSPITALES_56 = 'ÓRGANO DE OPERACIÓN ADMINISTRATIVA DESCONCENTRADA NORTE DEL DISTRITO FEDERAL';

    const DX_1 = 'ARBOVIRUS 1.1 DENGUE';
    const DX_2 = 'ARBOVIRUS 1.2 ZIKA';
    const DX_3 = 'ARBOVIRUS 1.3 CHIKUNGUNYA';
    const DX_4 = 'TOS FERINA 2.1 B. PERTUSSIS, B. PARAPERTUSSIS, B. HOLMESII';
    const DX_5 = 'BRUCELOSIS 3.1 BRUCELLA';
    const DX_6 = 'ENFERMEDAD FEBRIL EXANTEMÁTICA 4.1 SARAMPIÓN';
    const DX_7 = 'ENFERMEDAD FEBRIL EXANTEMÁTICA 4.2 RUBÉOLA';
    const DX_8 = 'VIRUS RESPIRATORIOS 5.1 INFLUENZA';
    const DX_9 = 'VIRUS RESPIRATORIOS 5.2 SARS-COV-2';
    const DX_10 = 'TUBERCULOSIS 6.1 GENEXPERT';
    const DX_11 = 'TUBERCULOSIS 6.2 BACILOSCOPIAS CCI';
    const DX_12 = 'TUBERCULOSIS 6.3 BACILOSCOPIAS DX';
    const DX_13 = 'LEPRA 7.1 BACILOSCOPIA CCI';
    const DX_14 = 'LEPRA 7.2 DX';
    const DX_15 = 'ROTAVIRUS 8.1 ROTAVIRUS';
    const DX_16 = 'PALUDISMO 9.1 PLASMODIUM';
    const DX_17 = 'PALUDISMO 9.2 CONTROL DE CALIDAD';
    const DX_18 = 'MUESTRAS ENVIADAS AL INDRE PARA DIAGNÓSTICO (REFERENCIADAS) 10.1 Rabia';
    const DX_19 = 'MUESTRAS ENVIADAS AL INDRE PARA DIAGNÓSTICO (REFERENCIADAS) 10.2 IIB por Streptococcus pneumoniae';
    const DX_20 = 'MUESTRAS ENVIADAS AL INDRE PARA DIAGNÓSTICO (REFERENCIADAS) 10.3 IIB por Neisseria meningitidis';
    const DX_21 = 'MUESTRAS ENVIADAS AL INDRE PARA DIAGNÓSTICO (REFERENCIADAS) 10.4 IIB Haemophilus influenzae';
    const DX_22 = 'MUESTRAS ENVIADAS AL INDRE PARA DIAGNÓSTICO (REFERENCIADAS) 10.5 Leishmaniasis';
    const DX_23 = 'MUESTRAS ENVIADAS AL INDRE PARA DIAGNÓSTICO (REFERENCIADAS) 10.6 Arbovirus';
    const DX_24 = 'CHAGAS 11.1 TRIPANOSOMA';
    const DX_25 = 'LEISHMANIASIS 12.1 LEISHMANIA';
    const DX_26 = 'N/A';


    const STATUS_1 = 'PENDIENTE';
    const STATUS_2 = 'RECHAZO';
    const STATUS_3 = 'CANCELADO';
    const STATUS_4 = 'FINALIZADO';

    const R_1 = 'N/A';
    const R_2 = 'TUBO SIN HISOPOS';
    const R_3 = 'DATOS NO COINCIDEN';
    const R_4 = 'VIRADA';
    const R_5 = 'VOLUMEN INSUFICIENTE';
    const R_6 = 'TUBO ROTO y/o TAPA ROTA';
    const R_7 = 'ETIQUETA NO LEGIBLE Y ROTA';
    const R_8 = 'SIN ETIQUETA';
    const R_9 = 'SIN MEDIO DE TRANSPORTE VIRAL';
    const R_10 = 'NO LLEGO LA MUESTRA';
    const R_11 = 'HISOPOS POR ENCIMA DEL MEDIO VIRAL DE TRANSPORTE';
    const R_12 = 'DIAS DE TRANSITO';
    const R_13 = 'MAL REFERENCIADA';
    const R_14 = 'HISOPO SIN RECORTAR';
    const R_15 = 'MUESTRA CON RESULTADO EN PLATAFORMA';
    const R_16 = 'HISOPO DE MADERA';
    const R_17 = 'MUESTRA DUPLICADA';
    const R_18 = 'MUESTRA MAL TOMADA';
    const R_19 = 'NO REGISTRADA EN PLATAFORMA';
    const R_20 = 'MUESTRA NO REFERENCIADA EN OFICIO';
    const R_21 = 'MUESTRA RECHAZADA EN PLATAFORMA';
    const R_22 = 'LA MUESTRA NO CUMPLE CON LAS ESPECIFICACIONES DE LAVADO BRONQUIAL';
    const R_23 = 'NO CUMPLE CON LA RED DE FRIO';
    /* const R_24 = 'LA MUESTRA NO CUMPLE CON LOS CRITERIOS DE ACEPTACION Y RECHAZO'; */
    const R_25 = 'MUESTRA DERRAMADA';
    const R_26 = 'ENVASE INADECUADO';
    const R_27 = 'MUESTRA INADECUADA';
    const R_28 = 'MUESTRA EN ESTADO PUTREFACTO';
    const R_29 = 'EMBALAJE INADECUADO';
    const R_30 = 'SIN OFICIO DE SOLICITUD DE ANALISIS';
    const R_31 = 'SIN FORMATO DE SOLICITUD DE ANALISIS';
    const R_32 = 'FORMATO DE SOLICITUD DE ANALISIS INCOMPLETO';
    const R_33 = 'DOCUMENTACION INCOMPLETA';
    const R_34 = 'NO HAY CONCORDANCIA ENTRE MUESTRAS ENVIADAS Y REFERIDAS EN LA DOCUMENTACION';
    const R_35 = 'NO CUMPLE CON DEFINICION OPERACIONAL DE CASO SOSPECHOSO';
    const R_36 = 'LA OPORTUNIDAD DE LA TOMA DE MUESTRA NO FUE ADECUADO';
    const R_37 = 'NO CUMPLE CON LOS FACTORES DE RIESGO';
    const R_38 = 'LA OPORTUNIDAD EN EL ENVIO DE MUESTRAS NO FUE ADECUADA';
    const R_39 = 'NO SE CUENTA CON EL DIAGNOSTICO APERTURADO PARA EL PROCESAMIENTO';
    const R_40 = 'SIN REACTIVO PARA PODER REALIZAR SU PROCESAMIENTO';
    const R_41 = 'REGISTRO INCOMPLETO EN PLATAFORMA';
    const R_42 = 'LAMINILLA ROTA';
    const R_43 = 'LAMINILLAS CONTAMINADAS';
    const R_44 = 'MATRIZ INADECUADA';

    use HasTipoMuestras;
    use HasPersonalRecibe;
    use HasHospitalesJuris;
    use HasDiagnosticos;
    use HasStatus;
    use HasMotivosRechazos;

    protected $fillable = [
        'anio', 
        'folio_lesp',
        'dx1', 
        'dx2', 
        'dx3', 
        'dx4', 
        'dx5', 
        'oficio_entrada',
        'fecha_recepcion', 
        'hora_recepcion', 
        'hospital',
        'nombre_paciente', 
        'tipo_muestra',
        'fecha_toma_muestra',
        'folio_sisver', 
        'persona_recibe',
        'status',
        'rechazos',
        'observaciones', 
        'aclaraciones_remu',
        'id_users',
        'operational_status',
        'user_id_operational_status_change'
    ];

    public function users()
    {
        return $this->belongsTo(Users::class, 'id_users');
    }

    public function scopeFolioStart($query, $folio ) {
        if( !is_null($folio) ) {
            return $query->where('folio_lesp', '>=', $folio);
        }
    }

    public function scopeFolioEnd($query, $folio)
    {
        if (!is_null($folio)) {
            return $query->where('folio_lesp', '<=', $folio);
        }
    }

    public function scopeRangeDate($query, $start, $end)
    {
        if (!is_null($start) && !is_null($end) &&$start != '' && $end != '') {
            return $query->whereBetween('fecha_recepcion', [$start, $end]);
        }
    }

    public function scopeTypeDiagnosis($query, $diagnostics ) {
        return $query->where(function($query) use($diagnostics) {
            foreach($diagnostics AS $diagnostic ) {
                $query->orWhere('dx1', $diagnostic->full_name_analysis)
                ->orWhere('dx2', $diagnostic->full_name_analysis)
                ->orWhere('dx3', $diagnostic->full_name_analysis)
                ->orWhere('dx4', $diagnostic->full_name_analysis)
                ->orWhere('dx5', $diagnostic->full_name_analysis);
            }/* 
            $query->where('dx1', $diagnosis)
            ->orWhere('dx2', $diagnosis)
            ->orWhere('dx3', $diagnosis)
            ->orWhere('dx4', $diagnosis)
            ->orWhere('dx5', $diagnosis); */
        });
    }

    public function scopeExcept($query, $except) {
        if( !is_null($except) ) {
            $notExport = collect();
            $auxiliar1 = Str::of( str_replace(' ', '', $except) )->explode(',');
            foreach( $auxiliar1 as $folio ) {
                $auxiliar2 = Str::of( $folio )->explode('-');

                if( $auxiliar2->count() > 1 ) {
                    for( $i = $auxiliar2[0]; $i <= $auxiliar2[1]; $i++ ) {
                        $notExport->push( intval($i) );
                    }
                } else {
                    $notExport->push( intval($auxiliar2[0]) );
                }
            }
            return $query->whereNotIn('folio_lesp', $notExport );
        }
    }

}

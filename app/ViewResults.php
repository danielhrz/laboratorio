<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewResults extends Model
{
    protected $table = 'view_results';
}

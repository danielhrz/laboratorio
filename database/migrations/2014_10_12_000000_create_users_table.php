<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_role')->unsigned();
            $table->foreign('id_role')->references('id')->on('role');
            $table->string('username', 200);
            $table->string('name', 200);
            $table->string('paternal_surname', 200);
            $table->string('maternal_surname', 200);
            $table->bigInteger('id_juris')->unsigned();
            $table->foreign('id_juris')->references('id')->on('juris');
            $table->bigInteger('id_areas')->unsigned();
            $table->foreign('id_areas')->references('id')->on('areas');
            $table->string('phone', 20);
            $table->string('ext', 4);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->boolean('is_active')->default($value = 1);
            $table->string('initials', 25)->nullable();
            $table->text('firm')->nullable();
            $table->text('rubric')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

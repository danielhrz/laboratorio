<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateInterpretationKitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interpretation_kit', function (Blueprint $table) {
            $table->id();
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
            $table->boolean("is_active")->default(1)->comment("Dato para determinar si el registro es tomado en cuenta o no.");
            $table->unsignedBigInteger("interpretation_id")->nullable()->comment("Columna para determinar el kit al que pertenece.");
            $table->unsignedBigInteger("kit_id")->nullable()->comment("Columna para determinar el kit al que pertenece.");

            $table->foreign("interpretation_id")->references("id")->on("interpretations");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interpretation_kit');
    }
}

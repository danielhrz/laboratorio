<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateInformationExtensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_extensions', function (Blueprint $table) {
            $table->id();
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
            $table->boolean("is_active")->default(1)->comment("Dato para determinar si el registro es tomado en cuenta o no.");

            $table->unsignedBigInteger('dato_federal_id')->comment("Id/Tabla de datos federales");
            $table->enum("diagnostic_type", ["Seguimiento", "Búsqueda"])->comment("Tipo de diagnostico");
            $table->string("key_lamella", 100)->comment("Clave de la laminilla");
            $table->timestamp("reception")->nullable()->comment("Fecha de recepción en el area.");
            $table->string("epidemiological_week", 50)->comment("Semana epidemiológica");
            $table->time("sample_collection_time")->nullable()->comment("Hora de toma de muestra");
            $table->enum("reading_result", ["Positivo", "Negativo"])->comment("Resultado de la lectura");
            $table->enum("species", ["P.falciparum", "P. malarie", "P. ovale", "P.vivax", "N/A"])->comment("Especie");
            $table->string("eas", 100)->comment("Densidad parasitaria EAS");
            $table->string("ess", 100)->comment("Densidad parasitaria Ess");
            $table->timestamp("date_delivery_results_aeer")->nullable()->comment("Entrega de resultados al AEER.");
            $table->time("total_time")->nullable()->comment("Tiempo total del proceso");
            $table->enum("quality_control", ["Sí", "No"])->comment("Control de Calidad InDRE");
            $table->enum("quality_control_result", ["Concordante", "Discordante", "N/A"])->comment("Resultado Control de Calidad InDRE");
            $table->enum("sample_bank", ["Sí", "No"])->comment("Banco de muestras");
            $table->enum("diagnostic_tests", ["Sí(sin registro de resultado)", "No", "Negativa", "P.vivax", "P.falciparum", "P.vivax/P.falciparum"])->comment("Pruebas de diagnostico rápido PDR");
            $table->text("observations")->comment("Observaciones");
        });

        /**
         * view: paludismo
         * SELECT
	t1.id,
	t1.folio_lesp,
	t1.folio_sisver,
	t3.diagnostic_type,
	t3.key_lamella,
	t1.hora_recepcion,
	t1.fecha_recepcion,
	t3.reception,
	t1.tipo_muestra,
	t1.nombre_paciente,
	t1.fecha_toma_muestra,
	t3.sample_collection_time,
	t1.hospital,
	t3.species,
	t3.eas,
	t3.ess,
	t3.date_delivery_results_aeer,
	t3.total_time,
	t3.quality_control,
	t3.quality_control_result,
	t3.sample_bank,
	t3.diagnostic_tests,
	t3.observations
FROM datos_federales t1 JOIN results t2 ON t2.dato_federal_id = t1.id LEFT JOIN information_extensions t3 ON t3.dato_federal_id = t1.id AND (t1.dx1 LIKE 'PALUDISMO%' OR t1.dx2 LIKE 'PALUDISMO%' OR t1.dx3 LIKE 'PALUDISMO%' OR t1.dx4 LIKE 'PALUDISMO%' OR t1.dx5 LIKE 'PALUDISMO%')
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_extensions');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogChangeDatosFederalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_change_datos_federales', function (Blueprint $table) {
            $table->id()->comment('Campo auto incrementable (PK)');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->char('is_active', 1)->default(1)->comment('Comprobación de si es activo el registro');
            $table->unsignedBigInteger('user_id')->comment('Id del usuario que modifico el registro');
            $table->unsignedBigInteger('dato_federal_id')->comment('Id del usuario que modifico el registro');

            $table->string('anio', 20);
            $table->string('folio_lesp', 250);
            $table->string('dx1', 500);
            $table->string('dx2', 500);
            $table->string('dx3', 500);
            $table->string('dx4', 500);
            $table->string('dx5', 500);
            $table->string('oficio_entrada', 250);
            $table->date('fecha_recepcion');
            $table->string('hospital', 250);
            $table->string('nombre_paciente', 250);
            $table->string('tipo_muestra', 250);
            $table->date('fecha_toma_muestra');
            $table->string('folio_sisver', 200);
            $table->string('persona_recibe', 250);
            $table->string('status', 250)->default($value = 'PENDIENTE');
            $table->string('rechazos', 500);
            $table->text('observaciones', 500);
            $table->text('aclaraciones_remu', 500);
            $table->time('hora_recepcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_change_datos_federales');
    }
}

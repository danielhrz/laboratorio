<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->id();
            $table->string('cre');
            $table->text('coments');
            $table->text('interpretation_result')->nullable();
            $table->time('sample_collection_time')->nullable();
            /* $table->string('status'); */
            //$table->string('result')->nullable();
            $table->string('user_initials')->nullable();
            $table->string('user_initials_send')->nullable();
            $table->string('if_exit')->nullable();
            $table->timestamp('date_delivery')->nullable();
            $table->timestamp('date_emission')->nullable();
            $table->timestamp('date_send')->nullable();
            $table->timestamp('date_delivery_remu')->nullable();
            $table->timestamp('date_delivery_user')->nullable();
            $table->unsignedBigInteger('user_id')->comment('Id del usuario que modifico el registro');
            $table->unsignedBigInteger('dato_federal_id')->comment('Id de los datos federales (remu)');
            //$table->unsignedBigInteger('diagnosis_id')->comment('Id del diagnostico');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->char('is_active', 1)->default(1)->comment('Comprobación de si es activo el registro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}

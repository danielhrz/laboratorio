<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosFederalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_federales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('anio', 20);
            $table->string('folio_lesp', 250);
            $table->string('dx1', 500);
            $table->string('dx2', 500);
            $table->string('dx3', 500);
            $table->string('dx4', 500);
            $table->string('dx5', 500);
            $table->string('oficio_entrada', 250);
            $table->date('fecha_recepcion');
            $table->string('hospital', 250);
            $table->string('nombre_paciente', 250);
            $table->string('tipo_muestra', 250);
            $table->date('fecha_toma_muestra');
            $table->string('folio_sisver', 200);
            $table->string('persona_recibe', 250);
            $table->string('status', 250)->default($value = 'PENDIENTE');
            $table->string('rechazos', 500);
            $table->text('observaciones', 500);
            $table->text('aclaraciones_remu', 500);
            $table->bigInteger('id_users')->unsigned();
            $table->foreign('id_users')->references('id')->on('users');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->boolean('is_active')->default($value = 1);
            $table->time('hora_recepcion');
            $table->string('status_result', 100)->default($value = 'SIN RESULTADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_federales');
    }
}

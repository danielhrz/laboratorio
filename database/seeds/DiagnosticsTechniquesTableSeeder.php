<?php

/* namespace Database\Seeders; */

use Illuminate\Database\Seeder;

class DiagnosticsTechniquesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('diagnostics_techniques')->delete();
        
        \DB::table('diagnostics_techniques')->insert(array (
            0 => 
            array (
                'created_at' => '2022-10-14 16:37:57',
                'diagnosis_id' => 2,
                'id' => 1,
                'is_active' => '1',
                'technique_id' => 1,
                'updated_at' => '2022-10-14 16:37:57',
            ),
            1 => 
            array (
                'created_at' => '2022-10-14 16:38:08',
                'diagnosis_id' => 3,
                'id' => 2,
                'is_active' => '1',
                'technique_id' => 1,
                'updated_at' => '2022-10-14 16:38:08',
            ),
            2 => 
            array (
                'created_at' => '2022-10-14 16:38:16',
                'diagnosis_id' => 4,
                'id' => 3,
                'is_active' => '1',
                'technique_id' => 1,
                'updated_at' => '2022-10-14 16:38:16',
            ),
            3 => 
            array (
                'created_at' => '2022-10-14 16:40:03',
                'diagnosis_id' => 5,
                'id' => 4,
                'is_active' => '1',
                'technique_id' => 3,
                'updated_at' => '2022-10-14 16:40:03',
            ),
            4 => 
            array (
                'created_at' => '2022-10-14 16:40:23',
                'diagnosis_id' => 6,
                'id' => 5,
                'is_active' => '1',
                'technique_id' => 2,
                'updated_at' => '2022-10-14 16:40:23',
            ),
            5 => 
            array (
                'created_at' => '2022-10-14 16:41:30',
                'diagnosis_id' => 7,
                'id' => 6,
                'is_active' => '1',
                'technique_id' => 2,
                'updated_at' => '2022-10-14 16:41:30',
            ),
            6 => 
            array (
                'created_at' => '2022-10-14 16:41:39',
                'diagnosis_id' => 8,
                'id' => 7,
                'is_active' => '1',
                'technique_id' => 2,
                'updated_at' => '2022-10-14 16:41:39',
            ),
            7 => 
            array (
                'created_at' => '2022-10-14 16:42:03',
                'diagnosis_id' => 9,
                'id' => 8,
                'is_active' => '1',
                'technique_id' => 4,
                'updated_at' => '2022-10-14 16:44:12',
            ),
            8 => 
            array (
                'created_at' => '2022-10-14 16:42:08',
                'diagnosis_id' => 10,
                'id' => 9,
                'is_active' => '1',
                'technique_id' => 4,
                'updated_at' => '2022-10-14 16:44:15',
            ),
            9 => 
            array (
                'created_at' => '2022-10-14 16:42:12',
                'diagnosis_id' => 11,
                'id' => 10,
                'is_active' => '1',
                'technique_id' => 5,
                'updated_at' => '2022-10-14 16:45:12',
            ),
            10 => 
            array (
                'created_at' => '2022-10-14 16:42:18',
                'diagnosis_id' => 12,
                'id' => 11,
                'is_active' => '1',
                'technique_id' => 6,
                'updated_at' => '2022-10-14 16:46:00',
            ),
            11 => 
            array (
                'created_at' => '2022-10-14 16:42:22',
                'diagnosis_id' => 13,
                'id' => 12,
                'is_active' => '1',
                'technique_id' => 6,
                'updated_at' => '2022-10-14 16:46:03',
            ),
            12 => 
            array (
                'created_at' => '2022-10-14 16:42:26',
                'diagnosis_id' => 14,
                'id' => 13,
                'is_active' => '1',
                'technique_id' => 7,
                'updated_at' => '2022-10-14 16:46:21',
            ),
            13 => 
            array (
                'created_at' => '2022-10-14 16:42:31',
                'diagnosis_id' => 15,
                'id' => 14,
                'is_active' => '1',
                'technique_id' => 8,
                'updated_at' => '2022-10-14 16:46:34',
            ),
            14 => 
            array (
                'created_at' => '2022-10-14 16:42:37',
                'diagnosis_id' => 16,
                'id' => 15,
                'is_active' => '1',
                'technique_id' => 9,
                'updated_at' => '2022-10-14 16:46:49',
            ),
            15 => 
            array (
                'created_at' => '2022-10-14 16:42:41',
                'diagnosis_id' => 17,
                'id' => 16,
                'is_active' => '1',
                'technique_id' => 8,
                'updated_at' => '2022-10-14 16:47:04',
            ),
            16 => 
            array (
                'created_at' => '2022-10-14 16:42:46',
                'diagnosis_id' => 18,
                'id' => 17,
                'is_active' => '1',
                'technique_id' => 8,
                'updated_at' => '2022-10-14 16:47:09',
            ),
            17 => 
            array (
                'created_at' => '2022-10-14 16:42:49',
                'diagnosis_id' => 19,
                'id' => 18,
                'is_active' => '1',
                'technique_id' => 10,
                'updated_at' => '2022-10-14 16:47:48',
            ),
            18 => 
            array (
                'created_at' => '2022-10-14 16:42:53',
                'diagnosis_id' => 20,
                'id' => 19,
                'is_active' => '1',
                'technique_id' => 10,
                'updated_at' => '2022-10-14 16:47:53',
            ),
            19 => 
            array (
                'created_at' => '2022-10-14 16:47:57',
                'diagnosis_id' => 21,
                'id' => 20,
                'is_active' => '1',
                'technique_id' => 10,
                'updated_at' => '2022-10-14 16:47:57',
            ),
        ));
        
        
    }
}
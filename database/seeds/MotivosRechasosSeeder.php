<?php

use Illuminate\Database\Seeder;

class MotivosRechasosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('motivos_rechazos')->insert([
        [
            'nombre'=>'N/A',
            'is_active'=>'1'
        ],[
            'nombre'=>'TUBO SIN HISOPOS',
            'is_active'=>'1'
        ],[
            'nombre'=>'DATOS NO COINCIDEN',
            'is_active'=>'1'
        ],[
            'nombre'=>'VIRADA',
            'is_active'=>'1'
        ],[
            'nombre'=>'VOLUMEN INSUFICIENTE',
            'is_active'=>'1'
        ],[
            'nombre'=>'TUBO ROTO y/o TAPA ROTA',
            'is_active'=>'1'
        ],[
            'nombre'=>'ETIQUETA NO LEGIBLE Y ROTA',
            'is_active'=>'1'
        ],[
            'nombre'=>'SIN ETIQUETA',
            'is_active'=>'1'
        ],[
            'nombre'=>'SIN MEDIO DE TRANSPORTE VIRAL',
            'is_active'=>'1'
        ],[
            'nombre'=>'NO LLEGÓ LA MUESTRA',
            'is_active'=>'1'
        ],[
            'nombre'=>'HISOPOS POR ENCIMA DEL MEDIO VIRAL DE TRANSPORTE',
            'is_active'=>'1'
        ],[
            'nombre'=>'DÍAS DE TRANSITO',
            'is_active'=>'1'
        ],[
            'nombre'=>'MAL REFERENCIADA',
            'is_active'=>'1'
        ],[
            'nombre'=>'HISOPO SIN RECORTAR',
            'is_active'=>'1'
        ],[
            'nombre'=>'MUESTRA CON RESULTADO EN PLATAFORMA',
            'is_active'=>'1'
        ],[
            'nombre'=>'HISOPO DE MADERA',
            'is_active'=>'1'
        ],[
            'nombre'=>'FOLIO CANCELADO',
            'is_active'=>'1'
        ],[
            'nombre'=>'MUESTRA MAL TOMADA',
            'is_active'=>'1'
        ],[
            'nombre'=>'NO REGISTRADA EN PLATAFORMA',
            'is_active'=>'1'
        ],[
            'nombre'=>'MUESTRA NO REFERENCIADA EN OFICIO',
            'is_active'=>'1'
        ],[
            'nombre'=>'MUESTRA RECHAZADA EN PLATAFORMA',
            'is_active'=>'1'
        ]
        ]);
    }
}

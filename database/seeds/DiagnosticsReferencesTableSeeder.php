<?php

/* namespace Database\Seeders;
 */

use Illuminate\Database\Seeder;

class DiagnosticsReferencesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('diagnostics_references')->delete();
        
        \DB::table('diagnostics_references')->insert(array (
            0 => 
            array (
                'created_at' => '2022-10-14 16:49:07',
                'diagnosis_id' => 2,
                'id' => 1,
                'is_active' => '1',
                'reference_id' => 3,
                'updated_at' => '2022-10-14 16:50:46',
            ),
            1 => 
            array (
                'created_at' => '2022-10-14 16:49:11',
                'diagnosis_id' => 3,
                'id' => 2,
                'is_active' => '1',
                'reference_id' => 5,
                'updated_at' => '2022-10-14 16:50:55',
            ),
            2 => 
            array (
                'created_at' => '2022-10-14 16:49:18',
                'diagnosis_id' => 4,
                'id' => 3,
                'is_active' => '1',
                'reference_id' => 4,
                'updated_at' => '2022-10-14 16:51:05',
            ),
            3 => 
            array (
                'created_at' => '2022-10-14 16:49:22',
                'diagnosis_id' => 5,
                'id' => 4,
                'is_active' => '1',
                'reference_id' => 6,
                'updated_at' => '2022-10-14 16:51:14',
            ),
            4 => 
            array (
                'created_at' => '2022-10-14 16:49:25',
                'diagnosis_id' => 6,
                'id' => 5,
                'is_active' => '1',
                'reference_id' => 2,
                'updated_at' => '2022-10-14 16:51:31',
            ),
            5 => 
            array (
                'created_at' => '2022-10-14 16:49:30',
                'diagnosis_id' => 7,
                'id' => 6,
                'is_active' => '1',
                'reference_id' => 2,
                'updated_at' => '2022-10-14 16:51:31',
            ),
            6 => 
            array (
                'created_at' => '2022-10-14 16:49:33',
                'diagnosis_id' => 8,
                'id' => 7,
                'is_active' => '1',
                'reference_id' => 2,
                'updated_at' => '2022-10-14 16:51:32',
            ),
            7 => 
            array (
                'created_at' => '2022-10-14 16:49:36',
                'diagnosis_id' => 9,
                'id' => 8,
                'is_active' => '1',
                'reference_id' => 7,
                'updated_at' => '2022-10-14 16:51:42',
            ),
            8 => 
            array (
                'created_at' => '2022-10-14 16:49:39',
                'diagnosis_id' => 10,
                'id' => 9,
                'is_active' => '1',
                'reference_id' => 7,
                'updated_at' => '2022-10-14 16:51:43',
            ),
            9 => 
            array (
                'created_at' => '2022-10-14 16:49:42',
                'diagnosis_id' => 11,
                'id' => 10,
                'is_active' => '1',
                'reference_id' => 7,
                'updated_at' => '2022-10-14 16:51:43',
            ),
            10 => 
            array (
                'created_at' => '2022-10-14 16:49:46',
                'diagnosis_id' => 12,
                'id' => 11,
                'is_active' => '1',
                'reference_id' => 7,
                'updated_at' => '2022-10-14 16:51:43',
            ),
            11 => 
            array (
                'created_at' => '2022-10-14 16:49:51',
                'diagnosis_id' => 13,
                'id' => 12,
                'is_active' => '1',
                'reference_id' => 7,
                'updated_at' => '2022-10-14 16:51:43',
            ),
            12 => 
            array (
                'created_at' => '2022-10-14 16:49:55',
                'diagnosis_id' => 14,
                'id' => 13,
                'is_active' => '1',
                'reference_id' => 7,
                'updated_at' => '2022-10-14 16:51:44',
            ),
            13 => 
            array (
                'created_at' => '2022-10-14 16:49:59',
                'diagnosis_id' => 15,
                'id' => 14,
                'is_active' => '1',
                'reference_id' => 7,
                'updated_at' => '2022-10-14 16:51:44',
            ),
            14 => 
            array (
                'created_at' => '2022-10-14 16:50:03',
                'diagnosis_id' => 16,
                'id' => 15,
                'is_active' => '1',
                'reference_id' => 8,
                'updated_at' => '2022-10-14 16:54:19',
            ),
            15 => 
            array (
                'created_at' => '2022-10-14 16:50:06',
                'diagnosis_id' => 17,
                'id' => 16,
                'is_active' => '1',
                'reference_id' => 9,
                'updated_at' => '2022-10-14 16:53:52',
            ),
            16 => 
            array (
                'created_at' => '2022-10-14 16:50:10',
                'diagnosis_id' => 18,
                'id' => 17,
                'is_active' => '1',
                'reference_id' => 9,
                'updated_at' => '2022-10-14 16:52:02',
            ),
            17 => 
            array (
                'created_at' => '2022-10-14 16:50:13',
                'diagnosis_id' => 19,
                'id' => 18,
                'is_active' => '1',
                'reference_id' => 10,
                'updated_at' => '2022-10-14 16:51:52',
            ),
            18 => 
            array (
                'created_at' => '2022-10-14 16:50:16',
                'diagnosis_id' => 20,
                'id' => 19,
                'is_active' => '1',
                'reference_id' => 10,
                'updated_at' => '2022-10-14 16:51:52',
            ),
            19 => 
            array (
                'created_at' => '2022-10-14 16:50:20',
                'diagnosis_id' => 21,
                'id' => 20,
                'is_active' => '1',
                'reference_id' => 10,
                'updated_at' => '2022-10-14 16:51:52',
            ),
        ));
        
        
    }
}
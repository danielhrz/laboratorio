<?php

use Illuminate\Database\Seeder;

class InterpretationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Interpretation::create([
            'id' => 1,
            'scene' => 'chikungunya: negativo, dengue: negativo, zika: negativo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA NO SE DETECTA PRESENCIA DE ARN VIRAL DE DENGUE, ZIKA O CHIKUNGUNYA'
        ]);

        \App\Interpretation::create([
            'id' => 2,
            'scene' => 'chikungunya: negativo, dengue: positivo a denv-1, zika: negativo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA SE DETECTA LA PRESENCIA DE ARN VIRAL DE DENGUE SEROTIPO 1'
        ]);

        \App\Interpretation::create([
            'id' => 3,
            'scene' => 'chikungunya: negativo, dengue: positivo a denv-2, zika: negativo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA SE DETECTA LA PRESENCIA DE ARN VIRAL DE DENGUE SEROTIPO  2'
        ]);

        \App\Interpretation::create([
            'id' => 4,
            'scene' => 'chikungunya: negativo, dengue: positivo a denv-3, zika: negativo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA SE DETECTA LA PRESENCIA DE ARN VIRAL DE DENGUE SEROTIPO 3'
        ]);

        \App\Interpretation::create([
            'id' => 5,
            'scene' => 'chikungunya: negativo, dengue: positivo a denv-4, zika: negativo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA SE DETECTA LA PRESENCIA DE ARN VIRAL DE DENGUE SEROTIPO 4'
        ]);

        \App\Interpretation::create([
            'id' => 6,
            'scene' => 'chikungunya: negativo, dengue: positivo a denv no tipificable, zika: negativo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA SE DETECTA LA PRESENCIA DE ARN VIRAL DE DENGUE, DEL CUAL NO FUE POSIBLE DETERMINAR EL SEROTIPO'
        ]);

        \App\Interpretation::create([
            'id' => 7,
            'scene' => 'chikungunya: negativo, dengue: negativo, zika: positivo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA SE DETECTA LA PRESENCIA DE ARN VIRAL DE ZIKA'
        ]);

        \App\Interpretation::create([
            'id' => 8,
            'scene' => 'chikungunya: positivo, dengue: negativo, zika: negativo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA SE DETECTA LA PRESENCIA DE ARN VIRAL DE CHIKUNGUNYA'
        ]);

        \App\Interpretation::create([
            'id' => 9,
            'scene' => 'chikungunya: positivo, dengue: positivo a denv-2, zika: negativo',
            'interpretation' => 'EN LA MUESTRA ANALIZADA SE DETECTA LA PRESENCIA DE ARN VIRAL DE CHIKUNGUNYA Y DENGUE SEROTIPO 2'
        ]);

        \App\Interpretation::create([
            'id' => 10,
            'scene' => 'chikungunya: no adecuado, dengue: no adecuado, zika: no adecuado',
            'interpretation' => 'EN LA MUESTRA ANALIZADA NO SE DETECTA PRESENCIA DE ARN VIRAL DE DENGUE, ZIKA, CHIKUNGUNYA O ARN HUMANO'
        ]);
    }
}

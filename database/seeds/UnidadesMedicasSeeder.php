<?php

use Illuminate\Database\Seeder;

class UnidadesMedicasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('areas')->insert([
        [
            'id'=>'1',
            'id_juris'=>'1',
            'name'=>'C.S.T-I TETELPAN',
            'clue'=>'DFSSA002281',
            'is_active'=>'1'
        ],[
            'id'=>'2',
            'id_juris'=>'1',
            'name'=>'C.S.T-I CORPUS CHRISTY',
            'clue'=>'DFSSA002305',
            'is_active'=>'1'
        ],[
            'id'=>'3',
            'id_juris'=>'1',
            'name'=>'C.S.T-II LA CASCADA',
            'clue'=>'DFSSA002310',
            'is_active'=>'1'
        ],[
            'id'=>'4',
            'id_juris'=>'1',
            'name'=>'',
            'clue'=>'DFSSA002322',
            'is_active'=>'1'
        ],
        ]);
    }
}

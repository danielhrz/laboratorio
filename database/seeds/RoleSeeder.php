<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->insert([
            [
            'id'=>'1',
            'name'=> 'ADMINISTRADOR',
            'is_active'=>'1'
            ],[
            'id'=>'2',
            'name'=> 'CAPTURISTA',
            'is_active'=>'1'
            ],[
            'id'=>'3',
            'name'=> 'SUPERVISOR',
            'is_active'=>'1'
            ],[
            'id'=>'4',
            'name'=> 'REPORTE DIARIO',
            'is_active'=>'1'
            ],[
            'id'=>'5',
            'name'=> 'ASIGNACION DE RESULTADOS',
            'is_active'=>'1'
            ]
        ]);
    }
}

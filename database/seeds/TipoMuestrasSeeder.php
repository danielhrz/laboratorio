<?php

use Illuminate\Database\Seeder;

class TipoMuestrasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('tipo_muestras')->insert([
        [
            'nombre'=>'EXUDADO FARÍNGEO',
            'abreviatura'=>'Exf',
            'is_active'=>'1'
        ],[
            'nombre'=>'EXUDADO NASOFARINGEO',
            'abreviatura'=>'Exn',
            'is_active'=>'1'
        ],[
            'nombre'=>'LAMINILLA',
            'abreviatura'=>'Lam',
            'is_active'=>'1'
        ],[
            'nombre'=>'EXPECTORACIÓN',
            'abreviatura'=>'Exp',
            'is_active'=>'1'
        ],[
            'nombre'=>'BIOPSIA',
            'abreviatura'=>'Bio',
            'is_active'=>'1'
        ],[
            'nombre'=>'SANGRE TOTAL',
            'abreviatura'=>'Sat',
            'is_active'=>'1'
        ],[
            'nombre'=>'MATERIA FECAL',
            'abreviatura'=>'Maf',
            'is_active'=>'1'
        ],[
            'nombre'=>'LAMINILLA/SANGRE TOTAL',
            'abreviatura'=>'Lam/Sat',
            'is_active'=>'1'
        ],[
            'nombre'=>'LAVADO BRONQUIAL',
            'abreviatura'=>'Lab',
            'is_active'=>'1'
        ],[
            'nombre'=>'LAVADO GASTRICO',
            'abreviatura'=>'Lag',
            'is_active'=>'1'
        ],[
            'nombre'=>'ENCÉFALO CANINO',
            'abreviatura'=>'EncCan',
            'is_active'=>'1'
        ],[
            'nombre'=>'ENCÉFALO FELINO',
            'abreviatura'=>'EncFel',
            'is_active'=>'1'
        ],[
            'nombre'=>'SALIVA',
            'abreviatura'=>'Sal',
            'is_active'=>'1'
        ],[
            'nombre'=>'LÍQUIDO CEFALORRAQUÍDEO',
            'abreviatura'=>'Lcr',
            'is_active'=>'1'
        ],[
            'nombre'=>'LIQUÍDO PLEURAL',
            'abreviatura'=>'Líp',
            'is_active'=>'1'
        ],[
            'nombre'=>'OTROS LÍQUIDOS',
            'abreviatura'=>'Olí',
            'is_active'=>'1'
        ],[
            'nombre'=>'EXUDADO FARINGEO / EXUDADO NASOFARINGEO',
            'abreviatura'=>'Exf / Exn',
            'is_active'=>'1'
        ],[
            'nombre'=>'ESPÉCIMEN COMPLETO',
            'abreviatura'=>'EsC',
            'is_active'=>'1'
        ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class ResultKitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results_kits')->insert([
            [
                'id' => 1,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'N/A'
            ],[
                'id' => 2,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'EN ESPERA DE VERIFICARSE'
            ],[
                'id' => 3,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'EL DIAGNOSTICO NO CUENTA CON UN KIT LA TÉCNICA ES DE MANERA MANUAL'
            ],[
                'id' => 4,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'ALL PLEX'
            ],[
                'id' => 5,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'ELOGIX'
            ],[
                'id' => 6,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'VITRO'
            ],[
                'id' => 7,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'PROTOCOLO CDC'
            ],[
                'id' => 8,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'GENE XPERT'
            ],[
                'id' => 9,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'NO HACE USO DE KIT LA TÉCNICA ES MANUAL'
            ],[
                'id' => 10,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'kit_name' => 'AUN NO SE ENCUENTRAN HABILITADOS EN EL LABORATORIO'
            ]
        ]);
    }
}

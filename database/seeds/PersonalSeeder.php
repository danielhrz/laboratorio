<?php

use Illuminate\Database\Seeder;

class PersonalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('personal')->insert([
        [
            'nombre'=>'ROSA ALEJANDRA JIMÉNEZ SALAS',
            'iniciales'=>'RAJS',
            'is_active'=>'1'
        ],[
            'nombre'=>'JAIME ANTONIO BECERRA GARCÍA',
            'iniciales'=>'JABG',
            'is_active'=>'1'
        ],[
            'nombre'=>'FRANCISCO RAYMUNDO FUENTES GONZÁLEZ',
            'iniciales'=>'FRFG',
            'is_active'=>'1'
        ],[
            'nombre'=>'BENITO GACHUZ NÁJERA',
            'iniciales'=>'BGN',
            'is_active'=>'1'
        ],[
            'nombre'=>'LAURA ISABEL DÍAZ TÉLLEZ',
            'iniciales'=>'LIDT',
            'is_active'=>'1'
        ],[
            'nombre'=>'EDUARDO JAVIER MEDINA IRIARTE',
            'iniciales'=>'EJMI',
            'is_active'=>'1'
        ],[
            'nombre'=>'DULCE GUADALUPE MEZA PÉREZ',
            'iniciales'=>'DGMP',
            'is_active'=>'1'
        ],[
            'nombre'=>'CARLOS RAUL NAVARRETE JIMÉNEZ',
            'iniciales'=>'CRNJ',
            'is_active'=>'1'
        ],[
            'nombre'=>'NADIA ARTEAGA VILLEDA',
            'iniciales'=>'NAV',
            'is_active'=>'1'
        ],[
            'nombre'=>'JONATHAN MARTIN BENITEZ RAMIREZ',
            'iniciales'=>'BRJM',
            'is_active'=>'1'
        ],[
            'nombre'=>'LUIS DANIEL VILLAVERDE RAMIREZ',
            'iniciales'=>'LDVR',
            'is_active'=>'1'
        ],[
            'nombre'=>'MARIA LUISA DE LOS ANGELES AGUIRRE MANZO',
            'iniciales'=>'MLAAM',
            'is_active'=>'1'
        ]
        ]);
    }
}

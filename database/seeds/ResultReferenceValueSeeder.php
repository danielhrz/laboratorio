<?php

use Illuminate\Database\Seeder;

class ResultReferenceValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results_references_values')->insert([
            [
                'id' => 1,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'N/A'
            ],[
                'id' => 2,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'EN ESPERA DE VERIFICARSE'
            ],[
                'id' => 3,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'DENV: Negativo'
            ],[
                'id' => 4,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'CHIKV: Negativo'
            ],[
                'id' => 5,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'ZIKV: Negativo'
            ],[
                'id' => 6,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'Ausencia de Bordetella spp'
            ],[
                'id' => 7,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'NEGATIVO'
            ],[
                'id' => 8,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'Ausencia de Rotavirus'
            ],[
                'id' => 9,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'Ausencia de Plasmodium spp'
            ],[
                'id' => 10,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'reference_value' => 'NO HABILITADOS EN EL LVE'
            ]
        ]);
    }
}

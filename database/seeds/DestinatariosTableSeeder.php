<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DestinatariosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('destinatarios')->delete();
        
        \DB::table('destinatarios')->insert(array (
            0 => 
            array (
                'address' => 'Rosa Blanca 95, Molino de Rosas, Álvaro Obregón, 01470 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. VICTOR JAVIER KAWAS BUSTAMANTE',
                'id' => 1,
                'is_active' => 1,
                'jurisdiccion' => 'ALVARO OBREGON',
                'position' => 'DIRECTOR DE LA JURISDICCION SANITARIA ALVARO OBREGON',
                'updated_at' => '2022-10-21 16:43:33',
            ),
            1 => 
            array (
                'address' => 'C. Cedro 4, Sta María la Ribera, Cuauhtémoc, 06400 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. LETICIA HERNANDEZ SANCHEZ',
                'id' => 2,
                'is_active' => 1,
                'jurisdiccion' => 'AZCAPOTZALCO',
                'position' => 'DIRECTORA DE LA JURISDICCION SANITARIA AZCAPOTZALCO',
                'updated_at' => '2022-10-24 21:56:04',
            ),
            2 => 
            array (
                'address' => 'Calz. de Tlalpan 1133, San Simón, Benito Juárez, 03660 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MONICA RAMIREZ VARGAS',
                'id' => 3,
                'is_active' => 1,
                'jurisdiccion' => 'BENITO JUAREZ',
                'position' => 'DIRECTORA DE LA JURISDICCION SANITARIA BENITO JUAREZ',
                'updated_at' => '2022-10-24 21:55:43',
            ),
            3 => 
            array (
                'address' => 'S/N Col. M, Av. H. Escuela Naval Militar, San Francisco Culhuacan',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'MVZ. JESUS GUSTAVO MARTINEZ GARCIA',
                'id' => 4,
                'is_active' => 1,
                'jurisdiccion' => 'C.C.C "DR. ALFONSO ANGELLINI DE LA GARZA"',
                'position' => 'DIRECTORA DEL CENTRO DE CONTROL CANINO "DR. ALFONSO ANGELLINI DE LA GARZA"',
                'updated_at' => '2022-10-24 21:57:33',
            ),
            4 => 
            array (
                'address' => 'Av. 510, Pueblo de San Juan de Aragón, Gustavo A. Madero, 07920 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'MVZ. CHRISTIAN AMED ORTEGA HERRERA',
                'id' => 5,
                'is_active' => 1,
                'jurisdiccion' => 'C.C.C. "DR. LUIS PASTEUR"',
                'position' => 'DIRECTOR DEL CENTRO DE CONTROL CANINO "DR. LUIS PASTEUR"',
                'updated_at' => '2022-10-21 17:50:33',
            ),
            5 => 
            array (
                'address' => 'C. Dr. José María Vértiz 464, Buenos Aires, Cuauhtémoc, 06780 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. FERMIN JURADO SANTA CRUZ',
                'id' => 6,
                'is_active' => 1,
                'jurisdiccion' => 'CENTRO DERMATOLOGICO "DR LADISLAO DE LA PASCUA"',
                'position' => 'DIRECTOR DEL CENTRO DERMATOLOGICO "DR. LADISLAO DE LA PASCUA"',
                'updated_at' => '2022-10-21 17:56:08',
            ),
            6 => 
            array (
                'address' => 'Peten sin Numero, Narvarte Poniente, Benito Juárez, Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ABIGAIL RODRIGUEZ ISLAS',
                'id' => 7,
                'is_active' => 1,
                'jurisdiccion' => 'CENTRO ESPECIALIZADO DE INTERNAMIENTO PREVENTIVO PARA ADOLESCENTES',
                'position' => 'RESPONSABLE DE LA UNIDAD MÉDICA DEL CENTRO ESPECIALIZADO DE INTERNAMIENTO PREVENTIVO PARA ADOLESCENTES',
                'updated_at' => '2022-10-21 17:56:36',
            ),
            7 => 
            array (
                'address' => 'Anillo Periférico Sur 4866, Tlalpan, Guadalupita, 14388 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MARIA ISABEL HERNANDEZ FLORES',
                'id' => 8,
                'is_active' => 1,
                'jurisdiccion' => 'CENTRO ESPECIALIZADO PARA MUJERES ADOLESCENTES',
                'position' => 'RESPONSABLE DE LA UNIDAD MÉDICA DEL CENTRO ESPECIALIZADO DE INTERNAMIENTO PARA MUJERES ADOLESCENTES',
                'updated_at' => '2022-10-21 17:58:08',
            ),
            8 => 
            array (
                'address' => 'Cuco Sánchez, Amp Emiliano Zapata, Iztapalapa, 09638 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. JUDITH MELENDEZ VIANA',
                'id' => 9,
                'is_active' => 1,
                'jurisdiccion' => 'CLINICA HOSPITAL EMILIANO ZAPATA',
                'position' => 'DIRECTORA DE LA CLINICA HOSPITAL EMILIANO ZAPATA',
                'updated_at' => '2022-10-21 17:58:31',
            ),
            9 => 
            array (
                'address' => 'Av. Río Churubusco S / N, Cuchilla Pantitlán, Venustiano Carranza, 15610 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. SANDRA RAQUEL RAMIREZ CHAIRES',
                'id' => 10,
                'is_active' => 1,
                'jurisdiccion' => 'CLINICA HOSPITAL ESPECIALIDADES TOXICOLOGICAS V. CARRANZA',
                'position' => 'DIRECTORA DE LA CLINICA HOSPITAL ESPECIALIDADES TOXICOLOGICAS V.CARRANZA',
                'updated_at' => '2022-10-21 17:59:04',
            ),
            10 => 
            array (
                'address' => 'Calz. de Tlalpan, San Simón, Benito Juárez, 03660 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. JOSE OCTAVIO ORLANDO MARTINEZ MORENO',
                'id' => 11,
                'is_active' => 1,
                'jurisdiccion' => 'COYOACAN',
                'position' => 'DIRECTOR DE LA JURISDICCIÓN SANITARIA COYOACAN',
                'updated_at' => '2022-10-21 18:00:24',
            ),
            11 => 
            array (
                'address' => 'Cda. Juárez SN, Cuajimalpa, Cuajimalpa de Morelos, 05050 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. GUADALUPE SARAH URBINA GRANADOS',
                'id' => 12,
                'is_active' => 1,
                'jurisdiccion' => 'CUAJIMALPA',
                'position' => 'DIRECTORA DE LA JURISDICCIÓN SANITARIA CUAJIIMALPA',
                'updated_at' => '2022-10-21 18:01:45',
            ),
            12 => 
            array (
                'address' => 'Juventino Rosas 78, Ex Hipódromo de Peralvillo, Cuauhtémoc, 06250 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. SANDRIN RIVERA HERNANDEZ',
                'id' => 13,
                'is_active' => 1,
                'jurisdiccion' => 'CUAUHTEMOC',
                'position' => 'DIRECTORA DE LA JURISDICCIÓN SANITARIA CUAUHTÉMOC',
                'updated_at' => '2022-10-21 18:03:17',
            ),
            13 => 
            array (
                'address' => 'Av. Tlahuac 4866, San Lorenzo Tezonco, Iztapalapa, 09930 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ROCIO FLORES ANTONIO',
                'id' => 14,
                'is_active' => 1,
                'jurisdiccion' => 'H DE ESPECIALIDADES. BELISARIO DOMINGUEZ',
                'position' => 'DIRECTORA DEL HOSPITAL DE ESPECIALIDADES BELISARIO DOMINGUEZ',
                'updated_at' => '2022-10-21 18:05:03',
            ),
            14 => 
            array (
                'address' => 'Dr. Lavista 78, Doctores, Cuauhtémoc, 06720 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'LCDA. MARISOL ODRIOZOLA CORDOVA',
                'id' => 15,
                'is_active' => 1,
                'jurisdiccion' => 'FISCALIA GENERAL DE JUSTICIA',
                'position' => 'DIRECTORA OPERATIVA DE DIRECCION GENERAL DEL CENTRO DE ESTANCIA TRANSITORIA PARA NIÑOS Y NIÑAS',
                'updated_at' => '2022-10-21 18:05:24',
            ),
            15 => 
            array (
                'address' => NULL,
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'FOLIO CANCELADO',
                'id' => 16,
                'is_active' => 1,
                'jurisdiccion' => 'FOLIO CANCELADO',
                'position' => NULL,
                'updated_at' => '2022-09-27 16:06:58',
            ),
            16 => 
            array (
                'address' => 'Juventino Rosas 78, Ex Hipódromo de Peralvillo, Cuauhtémoc, 06250 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ELSA AGUILAR PEREZ',
                'id' => 17,
                'is_active' => 1,
                'jurisdiccion' => 'GUSTAVO A. MADERO',
                'position' => 'DIRECTORA DE LA JURISDICCIÓN SANITARIA GUSTAVO A. MADERO',
                'updated_at' => '2022-10-21 18:16:42',
            ),
            17 => 
            array (
                'address' => 'Av. Azcapotzalco 731, Col. Azcapotzalco, Azcapotzalco, 02000 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. LUCILA HUITZIL ROSAS',
                'id' => 18,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO AZCAPOTZALCO',
                'position' => 'DIRECTORA DEL HOSPITAL PEDIATRICO AZCAPOTZALCO',
                'updated_at' => '2022-10-21 18:18:04',
            ),
            18 => 
            array (
                'address' => 'Prol. Moctezuma 18, Del Carmen, Coyoacán, 04000 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MARIA DEL CARMEN JAIMES TORRES',
                'id' => 19,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO COYOACAN',
                'position' => 'DIRECTORA DEL HOSPITAL PEDIATRICO COYOACAN',
                'updated_at' => '2022-10-21 18:18:29',
            ),
            19 => 
            array (
                'address' => 'Coyuya S/N, La Cruz Coyuya, Iztacalco, 08310 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. OSCAR ZAPATA CRUZ',
                'id' => 20,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO IZTACALCO',
                'position' => 'DIRECTOR DEL HOSPITAL PEDIATRICO IZTACALCO',
                'updated_at' => '2022-10-21 18:18:54',
            ),
            20 => 
            array (
                'address' => 'Ermita Iztapalapa 780, Granjas San Antonio, Iztapalapa, 09070 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. JESÚS CAMARGO OVIEDO',
                'id' => 21,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO IZTAPALAPA',
                'position' => 'DIRECTOR DEL HOSPITAL PEDIATRICO IZTAPALAPA',
                'updated_at' => '2022-10-21 18:19:36',
            ),
            21 => 
            array (
                'address' => 'F.C. Hidalgo 200, Villa Gustavo A. Madero, Gustavo A. Madero, 07050 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. TANIA MARGARITA SILVA CRUZ',
                'id' => 22,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO LA VILLA',
                'position' => 'DIRECTORA DEL HOSPITAL PEDIATRICO LA VILLA',
                'updated_at' => '2022-10-24 22:35:51',
            ),
            22 => 
            array (
                'address' => 'Calz Legaria 371, Panteón Frances, Miguel Hidalgo, 11260 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. JUAN FRANCISCO DIAZ SOTELO',
                'id' => 23,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO LEGARIA',
                'position' => 'DIRECTOR DEL HOSPITAL PEDIATRICO LEGARIA',
                'updated_at' => '2022-10-21 18:20:36',
            ),
            23 => 
            array (
                'address' => 'C. Ote. 158 189, Moctezuma 2da Secc, Venustiano Carranza, 15530 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. RODOLFO ZAPATA CASTORENA',
                'id' => 24,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO MOCTEZUMA',
                'position' => 'DIRECTOR DEL HOSPITAL PEDIATRICO MOCTEZUMA',
                'updated_at' => '2022-10-21 18:21:22',
            ),
            24 => 
            array (
                'address' => 'Calz San Simon 14, San Simón Tolnahuac, Cuauhtémoc, 06920 Tolnahuac, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. EDUARDO CORREA GANNAM',
                'id' => 25,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO PERALVILLO',
                'position' => 'DIRECTOR DEL HOSPITAL PEDIATRICO PERALVILLO',
                'updated_at' => '2022-10-21 18:21:49',
            ),
            25 => 
            array (
                'address' => 'Av. 506, San Juan de Aragón I Secc, Gustavo A. Madero, 07969 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ANGELES YOZABETH IBARIAS CARRANZA',
                'id' => 26,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO SAN JUAN DE ARAGON',
                'position' => 'DIRECTORA DEL HOSPITAL PEDIATRICO SAN JUAN DE ARAGON',
                'updated_at' => '2022-10-24 22:48:15',
            ),
            26 => 
            array (
                'address' => 'Arquitecto Carlos Lazo 25, Tacubaya, Miguel Hidalgo, 11870 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. LAURA MUNGUIA MARTINEZ',
                'id' => 27,
                'is_active' => 1,
                'jurisdiccion' => 'H PEDIATRICO TACUBAYA',
                'position' => 'DIRECTORA DEL HOSPITAL PEDIATRICO TACUBAYA',
                'updated_at' => '2022-10-24 22:48:53',
            ),
            27 => 
            array (
                'address' => 'Encinos 41, Miguel Hidalgo 4ta Secc, Tlalpan, 14250 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. SOFIA DE FATIMA HERNANDEZ GASCA',
                'id' => 28,
                'is_active' => 1,
                'jurisdiccion' => 'HG AJUSCO MEDIO',
                'position' => 'DIRECTORA DEL HOSPITAL GENERAL AJUSCO MEDIO',
                'updated_at' => '2022-10-24 22:49:34',
            ),
            28 => 
            array (
                'address' => 'Sur 111 S/N, Aeronáutica Militar, Venustiano Carranza, 15970 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. FELIPE DE JESUS CASILLAS FRANCO',
                'id' => 29,
                'is_active' => 1,
                'jurisdiccion' => 'HG BALBUENA',
                'position' => 'DIRECTOR DEL HOSPITAL GENERAL BALBUENA',
                'updated_at' => '2022-10-21 18:24:18',
            ),
            29 => 
            array (
                'address' => NULL,
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. JUAN CARLOS DE LA CERDA ANGELES',
                'id' => 30,
                'is_active' => 1,
                'jurisdiccion' => 'HG DR BELISARIO RODRIGUEZ',
                'position' => NULL,
                'updated_at' => '2022-10-21 18:24:55',
            ),
            30 => 
            array (
                'address' => 'Avenida Prolongación 5 de mayo #3170 esq. Centenario, col. Ex-Hacienda de Tarango. 11800',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. J. VICENTE O. ZAPATA JIMENEZ',
                'id' => 31,
                'is_active' => 1,
                'jurisdiccion' => 'HG DR ENRIQUE CABRERA',
                'position' => 'DIRECTOR DEL HOSPITAL GENERAL DR ENRIQUE CABRERA',
                'updated_at' => '2022-10-21 18:26:01',
            ),
            31 => 
            array (
                'address' => 'Plan de San Luis y Prolongación Salvador Díaz Mirón s/n, col. Santo Tomás. 11340',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MARIA DE JESUS HERVER CABRERA',
                'id' => 32,
                'is_active' => 1,
                'jurisdiccion' => 'HG DR RUBEN LEÑERO',
                'position' => 'DIRECTORA DEL HOSPITAL  GENERAL DR RUBEN LEÑERO',
                'updated_at' => '2022-10-24 22:51:26',
            ),
            32 => 
            array (
                'address' => 'Cuco Sánchez, Amp Emiliano Zapata, Iztapalapa, 09638 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. OSCAR CHÁVEZ OROPEZA',
                'id' => 33,
                'is_active' => 1,
                'jurisdiccion' => 'HG EMILIANO ZAPATA',
                'position' => 'DIRECTOR DEL HOSPITAL  GENERAL EMILIANO ZAPATA',
                'updated_at' => '2022-10-21 18:26:01',
            ),
            33 => 
            array (
                'address' => 'C. del Carmen 42, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06020 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MARIA DEL ROCIO LIMA CARCAÑO',
                'id' => 34,
                'is_active' => 1,
                'jurisdiccion' => 'HG GREGORIO SALAS FLORES',
                'position' => 'DIRECTORA DEL HOSPITAL GENERAL GREGORIO SALAS FLORES',
                'updated_at' => '2022-10-24 22:53:09',
            ),
            34 => 
            array (
                'address' => 'Av. Ermita Iztapalapa #3810 col. Citlali. 09660',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MA. FELICITAS MARTINEZ RUGERIO',
                'id' => 35,
                'is_active' => 1,
                'jurisdiccion' => 'HG IZTAPALAPA',
                'position' => 'DIRECTORA DEL HOSPITAL GENERAL IZTAPALAPA',
                'updated_at' => '2022-10-24 22:53:34',
            ),
            35 => 
            array (
                'address' => 'Calz. San Juan de Aragón 285, Granjas Modernas, Gustavo A. Madero, 07460 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. VICTOR JOSE CUEVAS OSORIO',
                'id' => 36,
                'is_active' => 1,
                'jurisdiccion' => 'HG LA VILLA',
                'position' => 'DIRECTOR DEL HOSPITAL GENERAL LA VILLA',
                'updated_at' => '2022-10-21 18:26:01',
            ),
            36 => 
            array (
                'address' => 'Blvd. José López Portillo 386, Santa Cruz, Milpa Alta, 12000 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ANGELICA PATRICIA CABALLERO HERNANDEZ',
                'id' => 37,
                'is_active' => 1,
                'jurisdiccion' => 'HG MILPA ALTA',
                'position' => 'DIRECTORA DEL HOSPITAL GENERAL MILPA ALTA',
                'updated_at' => '2022-10-24 22:54:29',
            ),
            37 => 
            array (
                'address' => 'Plan de San Luis 7, La Purísima Ticoman, Gustavo A. Madero, 07330 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. ALFREDO ALTAMIRANO MARTINEZ',
                'id' => 38,
                'is_active' => 1,
                'jurisdiccion' => 'HG TICOMAN',
                'position' => 'DIRECTOR DEL HOSPITAL GENERAL TICOMAN',
                'updated_at' => '2022-10-21 18:27:30',
            ),
            38 => 
            array (
                'address' => 'Av. la Turba 655, Villa Centroamericana I, Tláhuac, 13278 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. FERNANDO GONZALEZ ROMERO',
                'id' => 39,
                'is_active' => 1,
                'jurisdiccion' => 'HG TLAHUAC',
                'position' => 'DIRECTOR DEL HOSPITAL GENERAL TLAHUAC',
                'updated_at' => '2022-10-21 18:27:30',
            ),
            39 => 
            array (
                'address' => 'Tetenco, San Miguel Topilejo, Tlalpan, 14500 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. JENIFFER CUAPANTECATL MENDIETA',
                'id' => 40,
                'is_active' => 1,
                'jurisdiccion' => 'HG TOPILEJO',
                'position' => 'DIRECTORA DEL HOSPITAL GENERAL TOPILEJO',
                'updated_at' => '2022-10-24 22:55:45',
            ),
            40 => 
            array (
                'address' => 'amino Real a Xochimilco 31, La Noria, Xochimilco, 16030 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ROSALBA GALINDO ANGELES',
                'id' => 41,
                'is_active' => 1,
                'jurisdiccion' => 'HG TORRE MEDICA TEPEPAN',
                'position' => 'DIRECTOR DEL HOSPITAL GENERAL TORRE MEDICA TEPEPAN',
                'updated_at' => '2022-10-21 18:27:30',
            ),
            41 => 
            array (
                'address' => 'Av. México Coyoacán S/N, Gral Anaya, Benito Juárez, 03340 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. BERENICE SUAREZ MORELOS',
                'id' => 42,
                'is_active' => 1,
                'jurisdiccion' => 'HG XOCO',
                'position' => 'DIRECTORA DEL HOSPITAL  GENERAL  XOCO',
                'updated_at' => '2022-10-24 22:57:01',
            ),
            42 => 
            array (
                'address' => 'Av. 16 de Septiembre S/N, Xaltocan, Xochimilco, 16090 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MINERVA RUTH ZAVALA DURAN',
                'id' => 43,
                'is_active' => 1,
                'jurisdiccion' => 'HM PEDIATRICO XOCHIMILCO',
                'position' => 'DIRECTORA DEL HOSPITAL MATERNO PEDIATRICO XOCHIMILCO',
                'updated_at' => '2022-10-24 23:01:29',
            ),
            43 => 
            array (
                'address' => 'Av. Emiliano Zapata 700, Cuautepec de Madero, Gustavo A. Madero, 85800 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. EUNICE ARELI CISNETOS TAPIA',
                'id' => 44,
                'is_active' => 1,
                'jurisdiccion' => 'HMI CUAUTEPEC',
                'position' => 'DIRECTORA DEL HOSPITAL MATERNO INFANTIL CUAUTEPEC',
                'updated_at' => '2022-10-24 23:02:22',
            ),
            44 => 
            array (
                'address' => 'Calle Gustavo Jiménez, Víctor Hernández Esq, Francisco Villa, Azcapotzalco, 02400 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ELIDA SUSANA TORBELLIN ALDANA',
                'id' => 45,
                'is_active' => 1,
                'jurisdiccion' => 'HMI DR. NICOLAS M CEDILLO',
                'position' => 'DIRECTORA DEL HOSPITAL MATERNO INFANTIL DR. NICOLAS M CEDILLO',
                'updated_at' => '2022-10-24 23:03:36',
            ),
            45 => 
            array (
                'address' => 'Estaño 307, Felipe Ángeles, Venustiano Carranza, 15310 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MARIA DEL CARMEN MORALES CARDENAS',
                'id' => 46,
                'is_active' => 1,
                'jurisdiccion' => 'HMI INGUARAN',
                'position' => 'DIRECTORA DEL HOSPITAL  MATERNO INFANTIL INGUARAN',
                'updated_at' => '2022-10-24 23:04:04',
            ),
            46 => 
            array (
                'address' => 'Av. Luis Cabrera 619, Lomas Quebradas, La Magdalena Contreras, 10200 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MARCELA MERINO BAUTISTA',
                'id' => 47,
                'is_active' => 1,
                'jurisdiccion' => 'HMI MAGDALENA CONTRERAS',
                'position' => 'DIRECTORA DEL HOSPITAL  MATERNO INFANTIL MAGDALENA CONTRERAS',
                'updated_at' => '2022-10-24 23:04:29',
            ),
            47 => 
            array (
                'address' => 'Calz.Tláhuac Chalco 231, La Loma, Tláhuac, 13099 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. RAUL MATEO RÍOS FABIÁN',
                'id' => 48,
                'is_active' => 1,
                'jurisdiccion' => 'HMI TLAHUAC',
                'position' => 'DIRECTOR DEL HOSPITAL MATERNO INFANTIL TLAHUAC',
                'updated_at' => '2022-10-21 18:28:37',
            ),
            48 => 
            array (
                'address' => NULL,
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. EN C. MAURICIO VÁZQUEZ PICHARDO',
                'id' => 49,
                'is_active' => 1,
                'jurisdiccion' => 'INDRE',
                'position' => NULL,
                'updated_at' => '2022-10-21 18:29:03',
            ),
            49 => 
            array (
                'address' => 'Corregidora 135, Santa Anita, Iztacalco, 08300 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ANA BEATRIZ VILCHIS MORA',
                'id' => 50,
                'is_active' => 1,
                'jurisdiccion' => 'IZTACALCO',
                'position' => 'DIRECTORA DE LA JURISDICCIÓN SANITARIA IZTACALCO',
                'updated_at' => '2022-10-21 18:30:30',
            ),
            50 => 
            array (
                'address' => 'Teniente Fausto Vega Santander SN, Escuadrón 201, Iztapalapa, 09060 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. FCO JAVIER SERNA ALVARADO',
                'id' => 51,
                'is_active' => 1,
                'jurisdiccion' => 'IZTAPALAPA',
                'position' => 'DIRECTOR DE LA JURISDICCIÓN SANITARIA IZTAPALAPA',
                'updated_at' => '2022-10-21 18:30:58',
            ),
            51 => 
            array (
                'address' => 'Av. Insurgentes Nte. 423, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR.JOSE JESUS TRUJILLO GUTIERREZ',
                'id' => 52,
                'is_active' => 1,
                'jurisdiccion' => 'LVE-CDMX',
                'position' => 'DIRECTOR DE EPIDEMIOLOGIA DE LOS SERVICIOS DE SALUD PUBLICA DE LA CDMX',
                'updated_at' => '2022-10-21 18:32:56',
            ),
            52 => 
            array (
                'address' => 'Soledad, San Bernabé Ocotepec, La Magdalena Contreras, 10300 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'M.C. Y M.S.P. JOSE ANTONIO JIMENEZ JACINTO',
                'id' => 53,
                'is_active' => 1,
                'jurisdiccion' => 'MAGDALENA CONTRERAS',
                'position' => 'DIRECTOR DE LA JURISDICCIÓN SANITARIA MAGDALENA CONTRERAS',
                'updated_at' => '2022-10-21 18:35:03',
            ),
            53 => 
            array (
                'address' => 'Lago Iseo 106, Anáhuac I Secc, Miguel Hidalgo, 11310 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'M.S.P. LUIS MIGUEL ROBLES GONZALEZ',
                'id' => 54,
                'is_active' => 1,
                'jurisdiccion' => 'MIGUEL HIDALGO',
                'position' => 'DIRECTOR DE LA JURISDICCIÓN SANITARIA MIGUEL HIDALGO',
                'updated_at' => '2022-10-21 18:36:12',
            ),
            54 => 
            array (
                'address' => 'Ignacio Aldama 43, Tenantitla, Milpa Alta, 12100 San Antonio Tecómitl, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. FRANCISCO ALVA ROLDAN',
                'id' => 55,
                'is_active' => 1,
                'jurisdiccion' => 'MILPA ALTA',
                'position' => 'DIRECTOR DE LA JURISDICCIÓN SANITARIA MILPA ALTA',
                'updated_at' => '2022-10-21 18:37:38',
            ),
            55 => 
            array (
                'address' => '13450, Carlos A. Vidal 12, Guadalupe, Tláhuac, Ciudad de México, CDMX3450, Carlos A. Vidal 12, Guadalupe, Tláhuac, Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. ALIED BENCOMO ALERM',
                'id' => 56,
                'is_active' => 1,
                'jurisdiccion' => 'TLAHUAC',
                'position' => 'DIRECTORA DE LA JURISDICCIÓN SANITARIA TLAHUAC',
                'updated_at' => '2022-10-21 18:37:59',
            ),
            56 => 
            array (
                'address' => 'Coapa y Carrazco Sn, Toriello Guerra, Tlalpan, 14050 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'M.C. M.G.I.R.D. YERANIA EMIRREE ENRIQUEZ LOPEZ',
                'id' => 57,
                'is_active' => 1,
                'jurisdiccion' => 'TLALPAN',
                'position' => 'DIRECTORA DE LA JURISDICCIÓN SANITARIA TLALPAN',
                'updated_at' => '2022-10-21 18:39:37',
            ),
            57 => 
            array (
                'address' => 'Plaza las Atenas, Santa Martha Acatitla, Iztapalapa, 09910 CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. KARINA LIZBETH MENDEZ PEREZ',
                'id' => 58,
                'is_active' => 1,
                'jurisdiccion' => 'UNIDAD MEDICA DE LA PENITENCIARIA DE LA CDMX',
                'position' => 'RESPONSABLE DE LA UNIDAD MÉDICA DE LA PENITENCIARIA DE LA CDMX',
                'updated_at' => '2022-10-21 18:40:36',
            ),
            58 => 
            array (
                'address' => 'Reclusorio Nte, 07210 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. DANIEL GALINDO HERNANDEZ',
                'id' => 59,
                'is_active' => 1,
                'jurisdiccion' => 'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL NORTE',
                'position' => 'RESPONSABLE DE LA UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL NORTE',
                'updated_at' => '2022-10-21 18:41:11',
            ),
            59 => 
            array (
                'address' => 'Av. Reforma 100, Lomas de San Lorenzo, Iztapalapa, 09780 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. PRICILA OROZCO BAZAN',
                'id' => 60,
                'is_active' => 1,
                'jurisdiccion' => 'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL ORIENTE',
                'position' => 'RESPONSABLE DE LA UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL ORIENTE',
                'updated_at' => '2022-10-21 18:41:11',
            ),
            60 => 
            array (
                'address' => 'Circuito Javier Piña y Palacios esq, Lic. Martínez de Castro S/N, San Mateo Xalpa, Xochimilco, 16800 Xochimilco, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. RAUL BARAJAS CEJA',
                'id' => 61,
                'is_active' => 1,
                'jurisdiccion' => 'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL SUR',
                'position' => 'RESPONSABLE DE LA UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL NORTE',
                'updated_at' => '2022-10-21 18:41:16',
            ),
            61 => 
            array (
                'address' => 'Avenida Emiliano Zapata #700 Colonia Cuautepec Barrio Alto, Código postal 07100, Alcaldía Gustavo A. Madero, Ciudad de México',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DRA. MARÍA TONANSI JUAREZ CUEVAS',
                'id' => 62,
                'is_active' => 1,
                'jurisdiccion' => 'UNIDAD TEMPORAL LA PASTORA',
                'position' => 'RESPONSABLE DE LA UNIDAD MEDICA TEMPORAL LA PASTORA',
                'updated_at' => '2022-10-21 18:41:53',
            ),
            62 => 
            array (
                'address' => 'Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. ROMEO ADALID MARTINEZ CISNEROS',
                'id' => 63,
                'is_active' => 1,
                'jurisdiccion' => 'VENUSTIANO CARRANZA',
                'position' => 'DIRECTORA DE LA JURISDICCIÓN SANITARIA VENUSTIANO CARRANZA',
                'updated_at' => '2022-10-21 18:41:53',
            ),
            63 => 
            array (
                'address' => 'Benito Juárez, El Rosario, Xochimilco, 16070 Ciudad de México, CDMX',
                'created_at' => '2022-09-27 16:06:58',
                'encargado' => 'DR. JUAN CARLOS OCAMPO JIMENEZ',
                'id' => 64,
                'is_active' => 1,
                'jurisdiccion' => 'XOCHIMILCO',
                'position' => 'DIRECTOR DE LA JURISDICCIÓN SANITARIA VENUSTIANO CARRANZA',
                'updated_at' => '2022-10-24 23:14:03',
            ),
            64 => 
            array (
                'address' => 'Rosa Blanca 95, Molino de Rosas, Álvaro Obregón, 01470 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 16:43:14',
                'encargado' => 'DRA. JESSICA VARGAS CERMEÑO',
                'id' => 65,
                'is_active' => 1,
                'jurisdiccion' => 'ALVARO OBREGON',
                'position' => 'SUBDIRECTOR DE EPIDEMIOLOGIA DE LA JURISDICCIÓN ALVARO OBREGÓN',
                'updated_at' => '2022-10-21 16:43:14',
            ),
            65 => 
            array (
                'address' => 'C. Cedro 4, Sta María la Ribera, Cuauhtémoc, 06400 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 16:45:33',
                'encargado' => 'DR.FRANCISCO SILVANO CAMACHO IYAÑEZ',
                'id' => 66,
                'is_active' => 1,
                'jurisdiccion' => 'AZCAPOTZALCO',
                'position' => 'SUBDIRECTOR DE EPIDEMIOLOGIA DE LA JURISDICCIÓN AZCAPOTZALCO',
                'updated_at' => '2022-10-21 16:45:33',
            ),
            66 => 
            array (
                'address' => 'Calz. de Tlalpan 1133, San Simón, Benito Juárez, 03660 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 17:48:56',
                'encargado' => 'MTRA. REINA MARINA JAIMES SALDIVAR',
                'id' => 67,
                'is_active' => 1,
                'jurisdiccion' => 'BENITO JUAREZ',
                'position' => 'SUBDIRECTORA DE EPIDEMIOLOGIA DE LA JURISDICCIÓN BENITO JUAREZ',
                'updated_at' => '2022-10-24 21:56:42',
            ),
            67 => 
            array (
                'address' => 'Calz. de Tlalpan, San Simón, Benito Juárez, 03660 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:01:06',
                'encargado' => 'DR. RAUL BARRERA SAN MIGUEL',
                'id' => 68,
                'is_active' => 1,
                'jurisdiccion' => 'COYOACAN',
                'position' => 'SUBDIRECTOR DE EPIDEMIOLOGIA DE LA JURISDICCIÓN COYOACAN',
                'updated_at' => '2022-10-21 18:01:06',
            ),
            68 => 
            array (
                'address' => 'Cda. Juárez SN, Cuajimalpa, Cuajimalpa de Morelos, 05050 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:02:23',
                'encargado' => 'DR. ISIDRO MANUEL PEREZ MARTINEZ',
                'id' => 69,
                'is_active' => 1,
                'jurisdiccion' => 'CUAJIMALPA',
                'position' => 'SUBDIRECTOR DE EPIDEMIOLOGIA DE LA JURISDICCIÓN CUAJIMALPA',
                'updated_at' => '2022-10-21 18:02:23',
            ),
            69 => 
            array (
                'address' => 'Juventino Rosas 78, Ex Hipódromo de Peralvillo, Cuauhtémoc, 06250 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:03:53',
                'encargado' => 'DR. MAURICIO CASTAÑEDA MARTINEZ ',
                'id' => 70,
                'is_active' => 1,
                'jurisdiccion' => 'CUAUHTEMOC',
                'position' => 'SUBDIRECTOR DE EPIDEMIOLOGIA DE LA JURISDICCIÓN CUAUHTEMOC',
                'updated_at' => '2022-10-21 18:03:53',
            ),
            70 => 
            array (
                'address' => 'Juventino Rosas 78, Ex Hipódromo de Peralvillo, Cuauhtémoc, 06250 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:17:31',
                'encargado' => 'DRA. LOURDES ISABEL ALMEYDA GONZALEZ',
                'id' => 71,
                'is_active' => 1,
                'jurisdiccion' => 'GUSTAVO A. MADERO',
                'position' => 'SUBDIRECTORA DE EPIDEMIOLOGIA DE LA JURISDICCIÓN GUSTAVO A. MADERO',
                'updated_at' => '2022-10-24 22:26:09',
            ),
            71 => 
            array (
                'address' => 'Teniente Fausto Vega Santander SN, Escuadrón 201, Iztapalapa, 09060 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:31:38',
                'encargado' => 'DR. VICTOR MANUEL ROSAS ROSAS',
                'id' => 72,
                'is_active' => 1,
                'jurisdiccion' => 'IZTAPALAPA',
                'position' => 'SUBDIRECTOR DE EPIDEMIOLOGIA DE LA JURISDICCIÓN SANITARIA IZTAPALAPA',
                'updated_at' => '2022-10-21 18:31:38',
            ),
            72 => 
            array (
                'address' => 'Encinos 41, Miguel Hidalgo 4ta Secc, Tlalpan, 14250 Ciudad de México, CDMX. Semisotano del Hospital Ajusco Medio.',
                'created_at' => '2022-10-21 18:33:31',
                'encargado' => 'M. EN C. JUAN CARLOS PILIADO VELASCO',
                'id' => 73,
                'is_active' => 1,
                'jurisdiccion' => 'LVE-CDMX',
                'position' => 'JEFE DEL LABORATORIO DE VIGILANCIA EPIDEMIOLOGICA',
                'updated_at' => '2022-10-21 18:33:31',
            ),
            73 => 
            array (
                'address' => 'Soledad, San Bernabé Ocotepec, La Magdalena Contreras, 10300 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:35:45',
                'encargado' => 'DRA. LILIANA JIMENEZ JUSTO',
                'id' => 74,
                'is_active' => 1,
                'jurisdiccion' => 'MAGDALENA CONTRERAS',
                'position' => 'SUBDIRECTORA DE EPIDEMIOLOGIA DE LA JURISDICCIÓN SANITARIA MAGDALENA CONTRERAS',
                'updated_at' => '2022-10-24 23:08:22',
            ),
            74 => 
            array (
                'address' => 'Lago Iseo 106, Anáhuac I Secc, Miguel Hidalgo, 11310 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:37:07',
                'encargado' => 'DRA. MARIA DE LA LUZ PEREZ MENDOZA ',
                'id' => 75,
                'is_active' => 1,
                'jurisdiccion' => 'MIGUEL HIDALGO',
                'position' => 'SUBDIRECTORA DE EPIDEMIOLOGIA DE LA JURISDICCIÓN SANITARIA MIGUEL HIDALGO',
                'updated_at' => '2022-10-24 23:09:00',
            ),
            75 => 
            array (
                'address' => '13450, Carlos A. Vidal 12, Guadalupe, Tláhuac, Ciudad de México, CDMX3450, Carlos A. Vidal 12, Guadalupe, Tláhuac, Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:39:14',
                'encargado' => 'DR. ROGELIO PEÑA MARTINEZ',
                'id' => 76,
                'is_active' => 1,
                'jurisdiccion' => 'TLAHUAC',
                'position' => 'SUBDIRECTOR DE EPIDEMIOLOGIA DE LA JURISDICCIÓN SANITARIA TLAHUAC',
                'updated_at' => '2022-10-21 18:39:14',
            ),
            76 => 
            array (
                'address' => 'Coapa y Carrazco Sn, Toriello Guerra, Tlalpan, 14050 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:40:12',
                'encargado' => 'DR. JOSE DAMEAN CARDENAS FISCAL',
                'id' => 77,
                'is_active' => 1,
                'jurisdiccion' => 'TLALPAN',
                'position' => 'SUBDIRECTOR DE EPIDEMIOLOGIA DE LA JURISDICCIÓN SANITARIA TLALPAN',
                'updated_at' => '2022-10-21 18:40:12',
            ),
            77 => 
            array (
                'address' => 'Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX',
                'created_at' => '2022-10-21 18:43:11',
                'encargado' => 'DRA. SARA IBET GONZALEZ JACOBET',
                'id' => 78,
                'is_active' => 1,
                'jurisdiccion' => 'VENUSTIANO CARRANZA',
                'position' => 'SUBDIRECTORA DE EPIDEMIOLOGIA DE LA JURISDICCIÓN SANITARIA VENUSTIANO CARRANZA',
                'updated_at' => '2022-10-24 23:13:14',
            ),
        ));
        
        
    }
}
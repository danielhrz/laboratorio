<?php

use Illuminate\Database\Seeder;

class HospitalesJurisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('hospitales_jurisdicciones')->insert([
        [
            'nombre'=>'ALVARO OBREGON',
            'is_active'=>'1'
        ],[
            'nombre'=>'AZCAPOTZALCO',
            'is_active'=>'1'
        ],[
            'nombre'=>'BENITO JUAREZ',
            'is_active'=>'1'
        ],[
            'nombre'=>'CLINICA HOSPITAL EMILIANO ZAPATA',
            'is_active'=>'1'
        ],[
            'nombre'=>'CLINICA HOSPITAL ESP TOXICOLOGICAS',
            'is_active'=>'1'
        ],[
            'nombre'=>'COYOACAN',
            'is_active'=>'1'
        ],[
            'nombre'=>'CUAJIMALPA',
            'is_active'=>'1'
        ],[
            'nombre'=>'CUAUHTEMOC',
            'is_active'=>'1'
        ],[
            'nombre'=>'FISCALIA GENERAL DE JUSTICIA',
            'is_active'=>'1'
        ],[
            'nombre'=>'GUSTAVO A. MADERO',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL AJUSCO MEDIO',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL BALBUENA',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL DR. ENRIQUE CABRERA',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL DR GREGORIO SALAS',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL DR RUBEN LEÑERO',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL LA VILLA',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL TICOMAN',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL TLAHUAC',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL GENERAL XOCO',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL MATERNO INFANTIL INGUARAN',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL PEDIATRICO AZCAPOTZALCO',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL PEDIATRICO COYOACAN',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL PEDIATRICO LA VILLA',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL PEDIATRICO LEGARIA',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL PEDIATRICO PERALVILLO',
            'is_active'=>'1'
        ],[
            'nombre'=>'HOSPITAL PEDIATRICO TACUBAYA',
            'is_active'=>'1'
        ],[
            'nombre'=>'IZTACALCO',
            'is_active'=>'1'
        ],[
            'nombre'=>'IZTAPALAPA',
            'is_active'=>'1'
        ],[
            'nombre'=>'LVE CDMX',
            'is_active'=>'1'
        ],[
            'nombre'=>'MAGDALENA CONTRERAS',
            'is_active'=>'1'
        ],[
            'nombre'=>'MIGUEL HIDALGO',
            'is_active'=>'1'
        ],[
            'nombre'=>'MILPA ALTA',
            'is_active'=>'1'
        ],[
            'nombre'=>'TLAHUAC',
            'is_active'=>'1'
        ],[
            'nombre'=>'TLALPAN',
            'is_active'=>'1'
        ],[
            'nombre'=>'VENUSTIANO CARRANZA',
            'is_active'=>'1'
        ],[
            'nombre'=>'XOCHIMILCO',
            'is_active'=>'1'
        ],[
            'nombre'=>'N/A',
            'is_active'=>'1'
        ]
        ]);
    }
}

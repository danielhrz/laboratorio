<?php

use Illuminate\Database\Seeder;

class JurisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('juris')->insert([
            [
            'id'=>'1',
            'name'=>'JURISDICCIÓN ÁLVARO OBREGÓN',
            'is_active'=>'1'
            ],[
            'id'=>'2',
            'name'=>'JURISDICCIÓN AZCAPOTZALCO',
            'is_active'=>'1'
            ],[
            'id'=>'3',
            'name'=>'JURISDICCIÓN BENITO JUARÉZ',
            'is_active'=>'1'
            ],[
            'id'=>'4',
            'name'=>'JURISDICCIÓN COYOACÁN',
            'is_active'=>'1'
            ],[
            'id'=>'5',
            'name'=>'JURISDICCIÓN CUAUHTÉMOC',
            'is_active'=>'1'
            ],[
            'id'=>'6',
            'name'=>'JURISDICCIÓN CUAJIMALPA',
            'is_active'=>'1'
            ],[
            'id'=>'7',
            'name'=>'JURISDICCIÓN IZTACALCO',
            'is_active'=>'1'
            ],[
            'id'=>'8',
            'name'=>'JURISDICCIÓN IZTAPALAPA',
            'is_active'=>'1'
            ],[
            'id'=>'9',
            'name'=>'JURISDICCIÓN GUSTAVO A. MADERO',
            'is_active'=>'1'
            ],[
            'id'=>'10',
            'name'=>'JURISDICCIÓN MAGDALENA CONTRERAS',
            'is_active'=>'1'
            ],[
            'id'=>'11',
            'name'=>'JURISDICCIÓN MIGUEL HIDALGO',
            'is_active'=>'1'
            ],[
            'id'=>'12',
            'name'=>'JURISDICCIÓN MILPA ALTA',
            'is_active'=>'1'
            ],[
            'id'=>'13',
            'name'=>'JURISDICCIÓN TLÁHUAC',
            'is_active'=>'1'
            ],[
            'id'=>'14',
            'name'=>'JURISDICCIÓN TLALPAN',
            'is_active'=>'1'
            ],[
            'id'=>'15',
            'name'=>'JURISDICCIÓN VENUSTIANO CARRANZA',
            'is_active'=>'1'
            ],[
            'id'=>'16',
            'name'=>'JURISDICCIÓN XOCHIMILCO',
            'is_active'=>'1'
            ],[
            'id'=>'17',
            'name'=>'OFICINAS CENTRALES',
            'is_active'=>'1'    
            ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class PersonalRechazosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('personal_rechazos')->insert([
            [
            'id'=>'1',
            'nombre'=> 'T.L.C. CARLOS RAÚL NAVARRETE JIMÉNEZ'
            ],[
            'id'=>'2',
            'name'=> 'T.L.C. NADIA ARTEAGA VILLEDA'
            ],[
            'id'=>'3',
            'name'=> 'Q.F.B. EDUARDO JAVIER MEDINA IRIARTE'
            ],[
            'id'=>'4',
            'name'=> 'M. en C. JONATHAN MARTÍN BENÍTES RAMÍREZ'
            ],[
            'id'=>'5',
            'name'=> 'Q.F.B. DULCE GUADALUPE MEZA PÉREZ'
            ],[
            'id'=>'6',
            'name'=> 'M. en S.P.I. LUIS DANIEL VILLAVERDE RAMÍREZ'
            ],[
            'id'=>'7',
            'name'=> 'BIOL. FRANCISCO RAYMUNDO FUENTES GONZÁLEZ'
            ],[
            'id'=>'8',
            'name'=> 'LIC. ROSA ALEJANDRA JIMÉNEZ SALAS'
            ],[
            'id'=>'9',
            'name'=> 'M. en C. MARÍA LUISA DE LOS ÁNGELES AGUIRRE MANZO'
            ]
        ]);
    }
}

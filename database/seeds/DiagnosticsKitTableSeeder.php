<?php
/* 
namespace Database\Seeders; */

use Illuminate\Database\Seeder;

class DiagnosticsKitTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('diagnostics_kit')->delete();
        
        \DB::table('diagnostics_kit')->insert(array (
            0 => 
            array (
                'created_at' => '2022-10-14 16:23:43',
                'diagnosis_id' => 2,
                'id' => 1,
                'is_active' => '1',
                'kit_id' => 2,
                'updated_at' => '2022-10-14 16:23:43',
            ),
            1 => 
            array (
                'created_at' => '2022-10-14 16:24:22',
                'diagnosis_id' => 3,
                'id' => 2,
                'is_active' => '1',
                'kit_id' => 2,
                'updated_at' => '2022-10-14 16:25:09',
            ),
            2 => 
            array (
                'created_at' => '2022-10-14 16:24:29',
                'diagnosis_id' => 4,
                'id' => 3,
                'is_active' => '1',
                'kit_id' => 2,
                'updated_at' => '2022-10-14 16:25:11',
            ),
            3 => 
            array (
                'created_at' => '2022-10-14 16:25:03',
                'diagnosis_id' => 5,
                'id' => 4,
                'is_active' => '1',
                'kit_id' => 3,
                'updated_at' => '2022-10-14 16:25:03',
            ),
            4 => 
            array (
                'created_at' => '2022-10-14 16:25:41',
                'diagnosis_id' => 6,
                'id' => 5,
                'is_active' => '1',
                'kit_id' => 3,
                'updated_at' => '2022-10-14 16:25:41',
            ),
            5 => 
            array (
                'created_at' => '2022-10-14 16:26:32',
                'diagnosis_id' => 7,
                'id' => 6,
                'is_active' => '1',
                'kit_id' => 3,
                'updated_at' => '2022-10-14 16:26:32',
            ),
            6 => 
            array (
                'created_at' => '2022-10-14 16:26:40',
                'diagnosis_id' => 8,
                'id' => 7,
                'is_active' => '1',
                'kit_id' => 3,
                'updated_at' => '2022-10-14 16:26:40',
            ),
            7 => 
            array (
                'created_at' => '2022-10-14 16:28:25',
                'diagnosis_id' => 9,
                'id' => 8,
                'is_active' => '1',
                'kit_id' => 7,
                'updated_at' => '2022-10-14 16:28:25',
            ),
            8 => 
            array (
                'created_at' => '2022-10-14 16:28:34',
                'diagnosis_id' => 10,
                'id' => 9,
                'is_active' => '1',
                'kit_id' => 4,
                'updated_at' => '2022-10-14 16:28:34',
            ),
            9 => 
            array (
                'created_at' => '2022-10-14 16:28:50',
                'diagnosis_id' => 10,
                'id' => 10,
                'is_active' => '1',
                'kit_id' => 5,
                'updated_at' => '2022-10-14 16:28:50',
            ),
            10 => 
            array (
                'created_at' => '2022-10-14 16:28:55',
                'diagnosis_id' => 10,
                'id' => 11,
                'is_active' => '1',
                'kit_id' => 6,
                'updated_at' => '2022-10-14 16:28:55',
            ),
            11 => 
            array (
                'created_at' => '2022-10-14 16:33:41',
                'diagnosis_id' => 11,
                'id' => 12,
                'is_active' => '1',
                'kit_id' => 8,
                'updated_at' => '2022-10-14 16:33:41',
            ),
            12 => 
            array (
                'created_at' => '2022-10-14 16:34:07',
                'diagnosis_id' => 12,
                'id' => 13,
                'is_active' => '1',
                'kit_id' => 9,
                'updated_at' => '2022-10-14 16:34:07',
            ),
            13 => 
            array (
                'created_at' => '2022-10-14 16:34:12',
                'diagnosis_id' => 13,
                'id' => 14,
                'is_active' => '1',
                'kit_id' => 9,
                'updated_at' => '2022-10-14 16:34:12',
            ),
            14 => 
            array (
                'created_at' => '2022-10-14 16:34:17',
                'diagnosis_id' => 14,
                'id' => 15,
                'is_active' => '1',
                'kit_id' => 9,
                'updated_at' => '2022-10-14 16:34:17',
            ),
            15 => 
            array (
                'created_at' => '2022-10-14 16:34:37',
                'diagnosis_id' => 15,
                'id' => 16,
                'is_active' => '1',
                'kit_id' => 9,
                'updated_at' => '2022-10-14 16:34:37',
            ),
            16 => 
            array (
                'created_at' => '2022-10-14 16:34:56',
                'diagnosis_id' => 16,
                'id' => 17,
                'is_active' => '1',
                'kit_id' => 9,
                'updated_at' => '2022-10-14 16:34:56',
            ),
            17 => 
            array (
                'created_at' => '2022-10-14 16:35:11',
                'diagnosis_id' => 17,
                'id' => 18,
                'is_active' => '1',
                'kit_id' => 9,
                'updated_at' => '2022-10-14 16:35:11',
            ),
            18 => 
            array (
                'created_at' => '2022-10-14 16:35:16',
                'diagnosis_id' => 18,
                'id' => 19,
                'is_active' => '1',
                'kit_id' => 9,
                'updated_at' => '2022-10-14 16:35:16',
            ),
            19 => 
            array (
                'created_at' => '2022-10-14 16:35:59',
                'diagnosis_id' => 19,
                'id' => 20,
                'is_active' => '1',
                'kit_id' => 10,
                'updated_at' => '2022-10-14 16:35:59',
            ),
            20 => 
            array (
                'created_at' => '2022-10-14 16:36:05',
                'diagnosis_id' => 20,
                'id' => 21,
                'is_active' => '1',
                'kit_id' => 10,
                'updated_at' => '2022-10-14 16:36:05',
            ),
            21 => 
            array (
                'created_at' => '2022-10-14 16:36:12',
                'diagnosis_id' => 21,
                'id' => 22,
                'is_active' => '1',
                'kit_id' => 10,
                'updated_at' => '2022-10-14 16:36:12',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class InterpretationKitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\InterpretationKit::create([
            'interpretation_id' => 1,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 2,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 3,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 4,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 5,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 6,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 7,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 8,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 9,
            'kit_id' => 2
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 10,
            'kit_id' => 2
        ]);

        //
        \App\InterpretationKit::create([
            'interpretation_id' => 1,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 2,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 3,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 4,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 5,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 6,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 7,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 8,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 9,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 10,
            'kit_id' => 11
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 1,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 2,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 3,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 4,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 5,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 6,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 7,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 8,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 9,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 10,
            'kit_id' => 12
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 1,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 2,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 3,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 4,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 5,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 6,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 7,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 8,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 9,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 10,
            'kit_id' => 13
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 1,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 2,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 3,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 4,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 5,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 6,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 7,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 8,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 9,
            'kit_id' => 14
        ]);

        \App\InterpretationKit::create([
            'interpretation_id' => 10,
            'kit_id' => 14
        ]);
    }
}

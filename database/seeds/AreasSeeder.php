<?php

use Illuminate\Database\Seeder;

class AreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('areas')->insert([
        [
            'id'=>'1',
            'id_juris'=>'17',
            'name'=>'DIRECCIÓN GENERAL',
            'is_active'=>'1'
        ],[
            'id'=>'2',
            'id_juris'=>'17',
            'name'=>'DIRECCIÓN DE ATENCIÓN MÉDICA',
            'is_active'=>'1'
        ],[
            'id'=>'3',
            'id_juris'=>'17',
            'name'=>'DIRECCIÓN DE EPIDEMIOLOGÍA Y MEDICINA PREVENTIVA',
            'is_active'=>'1'
        ],[
            'id'=>'4',
            'id_juris'=>'17',
            'name'=>'DIRECCIÓN DE PROMOCIÓN DE LA SALUD',
            'is_active'=>'1'
        ],[
            'id'=>'5',
            'id_juris'=>'17',
            'name'=>'DIRECCIÓN DE ADMINISTRACIÓN Y FINANZAS',
            'is_active'=>'1'
        ],[
            'id'=>'6',
            'id_juris'=>'17',
            'name'=>'DIRECCIÓN DE ASUNTOS JURÍDICOS',
            'is_active'=>'1'
        ],[
            'id'=>'7',
            'id_juris'=>'17',
            'name'=>'L.C.P. DE CONTROL Y SEGUMIENTO',
            'is_active'=>'1'
        ],[
            'id'=>'8',
            'id_juris'=>'17',
            'name'=>'L.C.P. DE NORMATIVIDAD OPERATIVA',
            'is_active'=>'1'
        ],[
            'id'=>'9',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE ABASTECIMIENTO DE INSUMOS',
            'is_active'=>'1'
        ],[
            'id'=>'10',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE ATENCIÓN DOMICILIARIA',
            'is_active'=>'1'
        ],[
            'id'=>'11',
            'id_juris'=>'17',
            'name'=>'L.C.P. DE SEGUIMIENTO DE LA ATENCIÓN DOMICILIARIA "A"',
            'is_active'=>'1'
        ],[
            'id'=>'12',
            'id_juris'=>'17',
            'name'=>'L.C.P. DE  SEGUIMIENTO DE LA ATENCIÓN DOMICILIARIA "B"',
            'is_active'=>'1'
        ],[
            'id'=>'13',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE OPERACIÓN Y SUPERVISIÓN',
            'is_active'=>'1'
        ],[
            'id'=>'14',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE PROGRAMACIÓN Y EVALUACIÓN',
            'is_active'=>'1'
        ],[
            'id'=>'15',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE SOPORTE PARA EL DIAGNÓSTICO Y TRATAMIENTO',
            'is_active'=>'1'
        ],[
            'id'=>'16',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE ANÁLISIS E INVESTIGACIÓN EPIDEMIOLOGÍA',
            'is_active'=>'1'
        ],[
            'id'=>'17',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE VIGILANCIA Y EVALUACIÓN EPIDEMIOLOGÍA',
            'is_active'=>'1'
        ],[
            'id'=>'18',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE ENFERMEDADES TRANSMISIBLES',
            'is_active'=>'1'
        ],[
            'id'=>'19',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE ENFERMEDADES NO TRANSMISIBLES',
            'is_active'=>'1'
        ],[
            'id'=>'20',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE PROYECTOS E INTERVENCIONES ESTRATÉGICAS',
            'is_active'=>'1'
        ],[
            'id'=>'21',
            'id_juris'=>'17',
            'name'=>'L.C.P. DE EVALUACIÓN DE MORBILIDAD Y MORTALIDAD',
            'is_active'=>'1'
        ],[
            'id'=>'22',
            'id_juris'=>'17',
            'name'=>'L.C.P. DE PREVENCIÓN DEL VIH SIDA',
            'is_active'=>'1'
        ],[
            'id'=>'23',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE EDUCACIÓN PARA LA SALUD',
            'is_active'=>'1'
        ],[
            'id'=>'24',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE PARTICIPACIÓN SOCIAL',
            'is_active'=>'1'
        ],[
            'id'=>'25',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE EDUCACIÓN Y CULTURA DEL ENVEJECIMIENTO',
            'is_active'=>'1'
        ],[
            'id'=>'26',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE PROMOCIÓN DE LA SALUD PARA LOS GRUPOS DE MAYOR RIESGO',
            'is_active'=>'1'
        ],[
            'id'=>'27',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE ADMINISTRACIÓN DE CAPITAL HUMANO',
            'is_active'=>'1'
        ],[
            'id'=>'28',
            'id_juris'=>'17',
            'name'=>'L.C.P. DE ADMINISTRACIÓN DE CAPITAL HUMANO',
            'is_active'=>'1'
        ],[
            'id'=>'29',
            'id_juris'=>'17',
            'name'=>'J.U.D DE CONTROL DE PERSONAL',
            'is_active'=>'1'
        ],[
            'id'=>'30',
            'id_juris'=>'17',
            'name'=>'J.U.D. DE PRESTACIONES Y POLÍTICA LABORAL',
            'is_active'=>'1'
        ],[
            'id'=>'31',
            'id_juris'=>'17',
            'name'=>'J.U.D. DE NOMINAS',
            'is_active'=>'1'
        ],[
            'id'=>'32',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE RECURSOS MATERIALES, ABASTECIMIENTO Y SERVICIOS',
            'is_active'=>'1'
        ],[
            'id'=>'33',
            'id_juris'=>'17',
            'name'=>'J.U.D. DE COMPRAS Y CONTROL DE MATERIALES',
            'is_active'=>'1'
        ],[
            'id'=>'34',
            'id_juris'=>'17',
            'name'=>'J.U.D. DE ALMACENES E INVENTARIOS',
            'is_active'=>'1'
        ],[
            'id'=>'35',
            'id_juris'=>'17',
            'name'=>'J.U.D. DE ABASTECIMIENTO Y SERVICIOS',
            'is_active'=>'1'
        ],[
            'id'=>'36',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE FINANZAS',
            'is_active'=>'1'
        ],[
            'id'=>'37',
            'id_juris'=>'17',
            'name'=>'L.C.P DE SEGUIMIENTO FINANCIERO',
            'is_active'=>'1'
        ],[
            'id'=>'38',
            'id_juris'=>'17',
            'name'=>'J.U.D. DE CONTROL PRESUPUESTAL',
            'is_active'=>'1'
        ],[
            'id'=>'39',
            'id_juris'=>'17',
            'name'=>'J.U.D. DE CONTABILIDAD Y REGISTRO',
            'is_active'=>'1'
        ],[
            'id'=>'40',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE ASUNTOS ADMINISTRATIVOS',
            'is_active'=>'1'
        ],[
            'id'=>'41',
            'id_juris'=>'17',
            'name'=>'SUBDIRECCIÓN DE ASUNTOS CONTENCIOSOS',
            'is_active'=>'1'
        ],[
            'id'=>'42',
            'id_juris'=>'1',
            'name'=>'DIRECCIÓN ÁLVARO OBREGÓN',
            'is_active'=>'1'
        ],[
            'id'=>'43',
            'id_juris'=>'2',
            'name'=>'DIRECCIÓN AZCAPOTZALCO',
            'is_active'=>'1'
        ],[
            'id'=>'44',
            'id_juris'=>'3',
            'name'=>'DIRECCIÓN BENITO JUARÉZ',
            'is_active'=>'1'
        ],[
            'id'=>'45',
            'id_juris'=>'4',
            'name'=>'DIRECCIÓN COYOACÁN',
            'is_active'=>'1'
        ],[
            'id'=>'46',
            'id_juris'=>'5',
            'name'=>'DIRECCIÓN CUAUHTÉMOC',
            'is_active'=>'1'
        ],[
            'id'=>'47',
            'id_juris'=>'6',
            'name'=>'DIRECCIÓN CUAJIMALPA',
            'is_active'=>'1'
        ],[
            'id'=>'48',
            'id_juris'=>'7',
            'name'=>'DIRECCIÓN IZTACALCO',
            'is_active'=>'1'
        ],[
            'id'=>'49',
            'id_juris'=>'8',
            'name'=>'DIRECCIÓN IZTAPALAPA',
            'is_active'=>'1'
        ],[
            'id'=>'50',
            'id_juris'=>'9',
            'name'=>'DIRECCIÓN GUSTAVO A. MADERO',
            'is_active'=>'1'
        ],[
            'id'=>'51',
            'id_juris'=>'10',
            'name'=>'DIRECCIÓN MAGDALENA CONTRERAS',
            'is_active'=>'1'
        ],[
            'id'=>'52',
            'id_juris'=>'11',
            'name'=>'DIRECCIÓN MIGUEL HIDALGO',
            'is_active'=>'1'
        ],[
            'id'=>'53',
            'id_juris'=>'12',
            'name'=>'DIRECCIÓN MILPA ALTA',
            'is_active'=>'1'
        ],[
            'id'=>'54',
            'id_juris'=>'13',
            'name'=>'DIRECCIÓN TLÁHUAC',
            'is_active'=>'1'
        ],[
            'id'=>'55',
            'id_juris'=>'14',
            'name'=>'DIRECCIÓN TLALPAN',
            'is_active'=>'1'
        ],[
            'id'=>'56',
            'id_juris'=>'15',
            'name'=>'DIRECCIÓN VENUSTIANO CARRANZA',
            'is_active'=>'1'
        ],[
            'id'=>'57',
            'id_juris'=>'16',
            'name'=>'DIRECCIÓN XOCHIMILCO',
            'is_active'=>'1'
        ],[
            'id'=>'58',
            'id_juris'=>'14',
            'name'=>'LABORATORIO ESTATAL',
            'is_active'=>'1'
        ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class ResultTechniqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results_techniques')->insert([
            [
                'id' => 1,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'rRT-PCR (Reacción en Cadena de la Polimerasa con Transcriptasa Reversa en tiempo real'
            ],[
                'id' => 2,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'EN ESPERA DE APERTURA'
            ],[
                'id' => 3,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'PCR Tiempo Real'
            ],[
                'id' => 4,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'Multiplex real-time one-step RT-PCR'
            ],[
                'id' => 5,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'PCR en tiempo real'
            ],[
                'id' => 6,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'MICROSCOPIA Baciloscopía por Ziehl Neelsen'
            ],[
                'id' => 7,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'CULTIVO'
            ],[
                'id' => 8,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'MICROSCOPIA'
            ],[
                'id' => 9,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'Electroforesis en Geles de Poliacrilamida (PAGE)ELECTROFORESIS'
            ],[
                'id' => 10,
                'is_active' => 1,
                'created_at' => '2022-10-13 18:00:00',
                'updated_at' => '2022-10-13 18:00:00',
                'is_active' => '1',
                'technique_name' => 'NO HABILITADOS EN EL LVE'
            ]
        ]);
    }
}

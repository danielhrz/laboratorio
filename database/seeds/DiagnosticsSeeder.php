<?php

use Illuminate\Database\Seeder;

class DiagnosisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('diagnostics')->insert([
        [
            'diagnosis_name'=>'N/A',
            'analysis'=>'N/A',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'ARBOVIRUS',
            'analysis'=>'ARBOVIRUS 1.1 DENGUE',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'ARBOVIRUS',
            'analysis'=>'ARBOVIRUS 1.2 ZIKA',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'ARBOVIRUS',
            'analysis'=>'ARBOVIRUS 1.3 CHIKUNGUNYA',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'TOS FERINA ',
            'analysis'=>'TOS FERINA 2.1 B. PERTUSSIS, B. PARAPERTUSSIS, B. HOLMESSI',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'BRUCELOSIS',
            'analysis'=>'BRUCELOSIS 3.1 BRUCELLA',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'ENFERMEDAD FEBRIL EXANTEMÁTICA',
            'analysis'=>'ENFERMEDAD FEBRIL EXANTEMÁTICA 4.1 SARAMPIÓN',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'ENFERMEDAD FEBRIL EXANTEMÁTICA',
            'analysis'=>'ENFERMEDAD FEBRIL EXANTEMÁTICA 4.2 RUBÉOLA',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'VIRUS RESPIRATORIOS',
            'analysis'=>'VIRUS RESPIRATORIOS 5.1 INFLUENZA',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'VIRUS RESPIRATORIOS',
            'analysis'=>'VIRUS RESPIRATORIOS 5.2 SARS-COV-2',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'TUBERCULOSIS',
            'analysis'=>'TUBERCULOSIS 6.1 GENEXPERT',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'TUBERCULOSIS',
            'analysis'=>'TUBERCULOSIS 6.2 BACILOSCOPIAS CC',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'TUBERCULOSIS',
            'analysis'=>'TUBERCULOSIS 6.3 BACILOSCOPIAS DX',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'TUBERCULOSIS',
            'analysis'=>'TUBERCULOSIS 6.4 CULTIVO',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'LEPRA',
            'analysis'=>'LEPRA 7.1 M. LEPRAE',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'ROTAVIRUS',
            'analysis'=>'ROTAVIRUS 8.1 ROTAVIRUS',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'PALUDISMO',
            'analysis'=>'PALUDISMO 9.1 PLASMODIUM',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'PALUDISMO',
            'analysis'=>'PALUDISMO 9.2 CONTROL DE CALIDAD',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'RABIA',
            'analysis'=>'RABIA 10.1 RABIA',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'CHAGAS',
            'analysis'=>'CHAGAS 11.1 TRIPANOSOMA',
            'is_active'=>'1'
        ],[
            'diagnosis_name'=>'LEISHMANIASIS',
            'analysis'=>'LEISHMANIASIS 12.1 LEISHMANIA',
            'is_active'=>'1'
        ]
        ]);
    }
}

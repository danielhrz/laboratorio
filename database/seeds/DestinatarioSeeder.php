<?php

use Illuminate\Database\Seeder;

class DestinatarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('destinatarios')->insert([
            [
            'id'=>'1',
            'jurisdiccion'=>'ALVARO OBREGON',
            'encargado'=>'DR. VICTOR JAVIER KAWAS BUSTAMANTE'
            ],[
            'id'=>'2',
            'jurisdiccion'=>'AZCAPOTZALCO',
            'encargado'=>'DRA. LETICIA HERNANDEZ SANCHEZ'
            ],[
            'id'=>'3',
            'jurisdiccion'=>'BENITO JUAREZ',
            'encargado'=>'DRA. MONICA RAMIREZ VARGAS'
            ],[
            'id'=>'4',
            'jurisdiccion'=>'C.C.C "DR. ALFONSO ANGELLINI DE LA GARZA"',
            'encargado'=>'MVZ. JESUS GUSTAVO MARTINEZ GARCIA'
            ],[
            'id'=>'5',
            'jurisdiccion'=>'C.C.C. "DR. LUIS PASTEUR"',
            'encargado'=>'MVZ. CHRISTIAN AMED ORTEGA HERRERA'
            ],[
            'id'=>'6',
            'jurisdiccion'=>'CENTRO DERMATOLOGICO "DR LADISLAO DE LA PASCUA"',
            'encargado'=>'DR. FERMIN JURADO SANTA CRUZ'
            ],[
            'id'=>'7',
            'jurisdiccion'=>'CENTRO ESPECIALIZADO DE INTERNAMIENTO PREVENTIVO PARA ADOLESCENTES',
            'encargado'=>'DRA. ABIGAIL RODRIGUEZ ISLAS'
            ],[
            'id'=>'8',
            'jurisdiccion'=>'CENTRO ESPECIALIZADO PARA MUJERES ADOLESCENTES',
            'encargado'=>'DRA. MARIA ISABEL HERNANDEZ FLORES'
            ],[
            'id'=>'9',
            'jurisdiccion'=>'CLINICA HOSPITAL EMILIANO ZAPATA',
            'encargado'=>'DRA. JUDITH MELENDEZ VIANA'
            ],[
            'id'=>'10',
            'jurisdiccion'=>'CLINICA HOSPITAL ESPECIALIDADES TOXICOLOGICAS V. CARRANZA',
            'encargado'=>'DRA. SANDRA RAQUEL RAMIREZ CHAIRES'
            ],[
            'id'=>'11',
            'jurisdiccion'=>'COYOACAN',
            'encargado'=>'DR. JOSE OCTAVIO ORLANDO MARTINEZ MORENO'
            ],[
            'id'=>'12',
            'jurisdiccion'=>'CUAJIMALPA',
            'encargado'=>'DRA. GUADALUPE SARAH URBINA GRANADOS'
            ],[
            'id'=>'13',
            'jurisdiccion'=>'CUAUHTEMOC',
            'encargado'=>'DRA. SANDRIN RIVERA HERNANDEZ'
            ],[
            'id'=>'14',
            'jurisdiccion'=>'ESPECIALIDADES. BELISARIO DOMINGUEZ',
            'encargado'=>'DRA. ROCIO FLORES ANTONIO'
            ],[
            'id'=>'15',
            'jurisdiccion'=>'FISCALIA GENERAL DE JUSTICIA',
            'encargado'=>'LCDA. MARISOL ODRIOZOLA CORDOVA'
            ],[
            'id'=>'16',
            'jurisdiccion'=>'FOLIO CANCELADO',
            'encargado'=>'FOLIO CANCELADO'
            ],[
            'id'=>'17',
            'jurisdiccion'=>'GUSTAVO A. MADERO',
            'encargado'=>'DRA. ELSA AGUILAR PEREZ'
            ],[
            'id'=>'18',
            'jurisdiccion'=>'H PEDIATRICO AZCAPOTZALCO',
            'encargado'=>'DRA. LUCILA HUITZIL ROSAS'
            ],[
            'id'=>'19',
            'jurisdiccion'=>'H PEDIATRICO COYOACAN',
            'encargado'=>'DRA. MARIA DEL CARMEN JAIMES TORRES'
            ],[
            'id'=>'20',
            'jurisdiccion'=>'H PEDIATRICO IZTACALCO',
            'encargado'=>'DR. OSCAR ZAPATA CRUZ'
            ],[
            'id'=>'21',
            'jurisdiccion'=>'H PEDIATRICO IZTAPALAPA',
            'encargado'=>'DR. JESÚS CAMARGO OVIEDO'
            ],[
            'id'=>'22',
            'jurisdiccion'=>'H PEDIATRICO LA VILLA',
            'encargado'=>'DRA. TANIA MARGARITA SILVA CRUZ'
            ],[
            'id'=>'23',
            'jurisdiccion'=>'H PEDIATRICO LEGARIA',
            'encargado'=>'DR. JUAN FRANCISCO DIAZ SOTELO'
            ],[
            'id'=>'24',
            'jurisdiccion'=>'H PEDIATRICO MOCTEZUMA',
            'encargado'=>'DR. RODOLFO ZAPATA CASTORENA'
            ],[
            'id'=>'25',
            'jurisdiccion'=>'H PEDIATRICO PERALVILLO',
            'encargado'=>'DR. EDUARDO CORREA GANNAM'
            ],[
            'id'=>'26',
            'jurisdiccion'=>'H PEDIATRICO SAN JUAN DE ARAGON',
            'encargado'=>'DRA. ANGELES YOZABETH IBARIAS CARRANZA'
            ],[
            'id'=>'27',
            'jurisdiccion'=>'H PEDIATRICO TACUBAYA',
            'encargado'=>'DRA. LAURA MUNGUIA MARTINEZ'
            ],[
            'id'=>'28',
            'jurisdiccion'=>'HG AJUSCO MEDIO',
            'encargado'=>'DRA. SOFIA DE FATIMA HERNANDEZ GASCA'
            ],[
            'id'=>'29',
            'jurisdiccion'=>'HG BALBUENA',
            'encargado'=>'DR. FELIPE DE JESUS CASILLAS FRANCO'
            ],[
            'id'=>'30',
            'jurisdiccion'=>'HG DR BELISARIO RODRIGUEZ',
            'encargado'=>'DR. JUAN CARLOS DE LA CERDA ANGELES'
            ],[
            'id'=>'31',
            'jurisdiccion'=>'HG DR ENRIQUE CABRERA',
            'encargado'=>'DR. J. VICENTE O. ZAPATA JIMENEZ'
            ],[
            'id'=>'32',
            'jurisdiccion'=>'HG DR RUBEN LEÑERO',
            'encargado'=>'DRA. MARIA DE JESUS HERVER CABRERA'
            ],[
            'id'=>'33',
            'jurisdiccion'=>'HG EMILIANO ZAPATA',
            'encargado'=>'DR. OSCAR CHÁVEZ OROPEZA'
            ],[
            'id'=>'34',
            'jurisdiccion'=>'HG GREGORIO SALAS FLORES',
            'encargado'=>'DRA. MARIA DEL ROCIO LIMA CARCAÑO'
            ],[
            'id'=>'35',
            'jurisdiccion'=>'HG IZTAPALAPA',
            'encargado'=>'DRA. MA. FELICITAS MARTINEZ RUGERIO'
            ],[
            'id'=>'36',
            'jurisdiccion'=>'HG LA VILLA',
            'encargado'=>'DR. VICTOR JOSE CUEVAS OSORIO'
            ],[
            'id'=>'37',
            'jurisdiccion'=>'HG MILPA ALTA',
            'encargado'=>'DRA. ANGELICA PATRICIA CABALLERO HERNANDEZ'
            ],[
            'id'=>'38',
            'jurisdiccion'=>'HG TICOMAN',
            'encargado'=>'DR. ALFREDO ALTAMIRANO MARTINEZ'
            ],[
            'id'=>'39',
            'jurisdiccion'=>'HG TLAHUAC',
            'encargado'=>'DR. FERNANDO GONZALEZ ROMERO'
            ],[
            'id'=>'40',
            'jurisdiccion'=>'HG TOPILEJO',
            'encargado'=>'DRA. JENIFFER CUAPANTECATL MENDIETA'
            ],[
            'id'=>'41',
            'jurisdiccion'=>'HG TORRE MEDICA TEPEPAN',
            'encargado'=>'DRA. ROSALBA GALINDO ANGELES'
            ],[
            'id'=>'42',
            'jurisdiccion'=>'HG XOCO',
            'encargado'=>'DRA. BERENICE SUAREZ MORELOS'
            ],[
            'id'=>'43',
            'jurisdiccion'=>'HM PEDIATRICO XOCHIMILCO',
            'encargado'=>'DRA. MINERVA RUTH ZAVALA DURAN'
            ],[
            'id'=>'44',
            'jurisdiccion'=>'HMI CUAUTEPEC',
            'encargado'=>'DRA. EUNICE ARELI CISNETOS TAPIA'
            ],[
            'id'=>'45',
            'jurisdiccion'=>'HMI DR. NICOLAS M CEDILLO',
            'encargado'=>'DRA. ELIDA SUSANA TORBELLIN ALDANA'
            ],[
            'id'=>'46',
            'jurisdiccion'=>'HMI INGUARAN',
            'encargado'=>'DRA. MARIA DEL CARMEN MORALES CARDENAS'
            ],[
            'id'=>'47',
            'jurisdiccion'=>'HMI MAGDALENA CONTRERAS',
            'encargado'=>'DRA. MARCELA MERINO BAUTISTA'
            ],[
            'id'=>'48',
            'jurisdiccion'=>'HMI TLAHUAC',
            'encargado'=>'DR. RAUL MATEO RÍOS FABIÁN'
            ],[
            'id'=>'49',
            'jurisdiccion'=>'INDRE',
            'encargado'=>'DR. EN C. MAURICIO VÁZQUEZ PICHARDO'
            ],[
            'id'=>'50',
            'jurisdiccion'=>'IZTACALCO',
            'encargado'=>'DRA. ANA BEATRIZ VILCHIS MORA'
            ],[
            'id'=>'51',
            'jurisdiccion'=>'IZTAPALAPA',
            'encargado'=>'DR. FCO JAVIER SERNA ALVARADO'
            ],[
            'id'=>'52',
            'jurisdiccion'=>'LVE-CDMX',
            'encargado'=>'S/D'
            ],[
            'id'=>'53',
            'jurisdiccion'=>'MAGDALENA CONTRERAS',
            'encargado'=>'M.C. Y M.S.P. JOSE ANTONIO JIMENEZ JACINTO'
            ],[
            'id'=>'54',
            'jurisdiccion'=>'MIGUEL HIDALGO',
            'encargado'=>'M.S.P. LUIS MIGUEL ROBLES GONZALEZ'
            ],[
            'id'=>'55',
            'jurisdiccion'=>'MILPA ALTA',
            'encargado'=>'DR. FRANCISCO ALVA ROLDAN'
            ],[
            'id'=>'56',
            'jurisdiccion'=>'TLAHUAC',
            'encargado'=>'DRA. ALIED BENCOMO ALERM'
            ],[
            'id'=>'57',
            'jurisdiccion'=>'TLALPAN',
            'encargado'=>'M.C. M.G.I.R.D. YERANIA EMIRREE ENRIQUEZ LOPEZ'
            ],[
            'id'=>'58',
            'jurisdiccion'=>'UNIDAD MEDICA DE LA PENITENCIARIA DE LA CDMX',
            'encargado'=>'DRA. KARINA LIZBETH MENDEZ PEREZ'
            ],[
            'id'=>'59',
            'jurisdiccion'=>'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL NORTE',
            'encargado'=>'DR. DANIEL GALINDO HERNANDEZ'
            ],[
            'id'=>'60',
            'jurisdiccion'=>'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL ORIENTE',
            'encargado'=>'DRA. PRICILA OROZCO BAZAN'
            ],[
            'id'=>'61',
            'jurisdiccion'=>'UNIDAD MEDICA DEL RECLUSORIO PREVENTIVO VARONIL SUR',
            'encargado'=>'DR. RAUL BARAJAS CEJA'
            ],[
            'id'=>'62',
            'jurisdiccion'=>'UNIDAD TEMPORAL LA PASTORA',
            'encargado'=>'DRA. MARÍA TONANSI JUAREZ CUEVAS'
            ],[
            'id'=>'63',
            'jurisdiccion'=>'VENUSTIANO CARRANZA',
            'encargado'=>'DR. ROMEO ADALID MARTINEZ CISNEROS'
            ],[
            'id'=>'64',
            'jurisdiccion'=>'XOCHIMILCO',
            'encargado'=>'DR. JUAN CARLOS OCAMPO JIMENEZ'
            ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('users')->insert([
            [
                'id_role' => '1',
                'username' => 'Admin',
                'name' => 'Admin',
                'paternal_surname' => 'Admin', 
                'maternal_surname' => 'Admin',
                'id_juris' => '17',
                'id_areas' => '1',
                'phone' => '50381700',
                'ext' => '5428',
                'email' => 'jdhernandez@sersalud.df.gob.mx',
                'password' => bcrypt('Admin2021')
            ],[
                'id_role' => '2',
                'username' => 'sistemas',
                'name' => 'sistemas',
                'paternal_surname' => 'sistemas', 
                'maternal_surname' => 'sistemas',
                'id_juris' => '16',
                'id_areas' => '57',
                'phone' => '50381700',
                'ext' => '5875',
                'email' => 'sistemas@localhost.com',
                'password' => bcrypt('sistemas2021')
            ],[
                'id_role' => '2',
                'username' => 'jBecerra',
                'name' => 'Jaime Antonio',
                'paternal_surname' => 'Becerra', 
                'maternal_surname' => 'García',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'jaimeb.lesp.lve@gmail.com',
                'password' => bcrypt('rSWfEinKx2vf')
            ],[
                'id_role' => '2',
                'username' => 'jBenitez',
                'name' => 'Jonathan Martin',
                'paternal_surname' => 'Benítez', 
                'maternal_surname' => 'Ramírez',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'jonathanb.lesp.lve@gmail.com',
                'password' => bcrypt('DJd5s34rhyiS')
            ],[
                'id_role' => '2',
                'username' => 'eMedina',
                'name' => 'Eduardo Javier',
                'paternal_surname' => 'Medina', 
                'maternal_surname' => 'Iriarte',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'javiermi.lesp.lve@gmail.com',
                'password' => bcrypt('M2IGxZvYVSAV')
            ],[
                'id_role' => '3',
                'username' => 'mAguirre',
                'name' => 'María Luisa de los Ángeles',
                'paternal_surname' => 'Aguirre', 
                'maternal_surname' => 'Manzo',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'mariaa.lesp.lve@gmail.com',
                'password' => bcrypt('V1GpETHVpDdg')
            ],[
                'id_role' => '2',
                'username' => 'nArteaga',
                'name' => 'Nadia',
                'paternal_surname' => 'Arteaga', 
                'maternal_surname' => 'Villeda',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'nadiaa.lesp.lve@gmail.com',
                'password' => bcrypt('fn5SjDFKwXzw')
            ],[
                'id_role' => '2',
                'username' => 'bGachuz',
                'name' => 'Benito',
                'paternal_surname' => 'Gachuz', 
                'maternal_surname' => 'Nájera',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'benitog.lesp.lve@gmail.com',
                'password' => bcrypt('gdU8i7wNuBPc')
            ],[
                'id_role' => '3',
                'username' => 'rJimenez',
                'name' => 'Rosa Alejandra',
                'paternal_surname' => 'Jiménez', 
                'maternal_surname' => 'Salas',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'alejandraj.lesp.lve@gmail.com',
                'password' => bcrypt('3Z5Het9smeDK')
            ],[
                'id_role' => '3',
                'username' => 'dMeza',
                'name' => 'Dulce Guadalupe',
                'paternal_surname' => 'Meza', 
                'maternal_surname' => 'Pérez',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'dulcem.lesp.lve@gmail.com',
                'password' => bcrypt('wAQQaKUYLRa6')
            ],[
                'id_role' => '3',
                'username' => 'lVillaverde',
                'name' => 'Luis Daniel',
                'paternal_surname' => 'Villaverde', 
                'maternal_surname' => 'Ramírez',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'danielv.lesp.lve@gmail.com',
                'password' => bcrypt('8It3dCUUL36D')
            ],[
                'id_role' => '2',
                'username' => 'lDiaz',
                'name' => 'Laura',
                'paternal_surname' => 'Díaz', 
                'maternal_surname' => 'Téllez',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'laurad.lesp.lve@gmail.com',
                'password' => bcrypt('LTtA2xvcxNma')
            ],[
                'id_role' => '3',
                'username' => 'jpiliado',
                'name' => 'Juan Carlos',
                'paternal_surname' => 'Piliado', 
                'maternal_surname' => 'Velasco',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'jpiliado@sersalud.cdmx.gob.mx',
                'password' => bcrypt('vmrq7Iiegf9LBJv')
            ],[
                'id_role' => '2',
                'username' => 'cnavarrete',
                'name' => 'Carlos Raúl',
                'paternal_surname' => 'Navarrete', 
                'maternal_surname' => 'Jiménez',
                'id_juris' => '14',
                'id_areas' => '58',
                'phone' => '50381700',
                'ext' => '6879',
                'email' => 'carlosn.lesp.lve@gmail.com',
                'password' => bcrypt('Bq68BBZ8AjFsd2b')
            ]
        ]);
    }
}

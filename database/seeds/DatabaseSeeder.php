<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
/*             JurisSeeder::class,
            AreasSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            DestinatariosTableSeeder::class,
            PersonalRechazosSeeder::class,
            EmailsDestinatariosSeeder::class, */

            DiagnosisSeeder::class,
            ResultKitSeeder::class,
            ResultReferenceValueSeeder::class,
            ResultTechniqueSeeder::class,

            DiagnosticsKitTableSeeder::class,
            DiagnosticsReferencesTableSeeder::class,
            DiagnosticsTechniquesTableSeeder::class
        ]);
    }
}

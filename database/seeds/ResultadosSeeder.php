<?php

use Illuminate\Database\Seeder;

class ResultadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('resultados')->insert([
        [
            'nombre'=>'BORDETELLA HOLMESII',
            'is_active'=>'1'
        ],[
            'nombre'=>'BORDETELLA PARAPERTUSSIS',
            'is_active'=>'1'
        ],[
            'nombre'=>'BORDETELLA PERTUSSIS',
            'is_active'=>'1'
        ],[
            'nombre'=>'H1N2V',
            'is_active'=>'1'
        ],[
            'nombre'=>'H3N2V',
            'is_active'=>'1'
        ],[
            'nombre'=>'INDETERMINADO**',
            'is_active'=>'1'
        ],[
            'nombre'=>'INFLUENZA A NO SUPTIPIFICABLE',
            'is_active'=>'1'
        ],[
            'nombre'=>'INFLUENZA A(H1N1)PDM09',
            'is_active'=>'1'
        ],[
            'nombre'=>'INFLUENZA B LINAJE NO DETERMINADO',
            'is_active'=>'1'
        ],[
            'nombre'=>'INFLUENZA B LINAJE VICTORIA',
            'is_active'=>'1'
        ],[
            'nombre'=>'INFLUENZA B LINAJE YAMAGATA',
            'is_active'=>'1'
        ],[
            'nombre'=>'INFLUENZA ESTACIONAL A H3',
            'is_active'=>'1'
        ],[
            'nombre'=>'NEGATIVO',
            'is_active'=>'1'
        ],[
            'nombre'=>'NO ADECUADA',
            'is_active'=>'1'
        ],[
            'nombre'=>'NO SE PUEDE DIAGNOSTICAR',
            'is_active'=>'1'
        ],[
            'nombre'=>'PLASMODIUM FALCIPARUM',
            'is_active'=>'1'
        ],[
            'nombre'=>'PLASMODIUM VIVAX',
            'is_active'=>'1'
        ],[
            'nombre'=>'POSITIVO',
            'is_active'=>'1'
        ],[
            'nombre'=>'POSITIVO PATRÓN CORTO',
            'is_active'=>'1'
        ],[
            'nombre'=>'POSITIVO PATRÓN LARGO',
            'is_active'=>'1'
        ],[
            'nombre'=>'SARS-COV-2',
            'is_active'=>'1'
        ],[
            'nombre'=>'VSR',
            'is_active'=>'1'
        ],[
            'nombre'=>'DE 1 A) BAAR, INFORMAR EL NÚMERO DE BACILOS EN 100 CAMPOS OBSERVADOS',
            'is_active'=>'1'
        ],[
            'nombre'=>'(+), MENOS DE UN BACILO POR CAMPO EN PROMEDIO (DE 10 A 99 BACILOS) EN 100 CAMPOS OBSERVADOS',
            'is_active'=>'1'
        ],[
            'nombre'=>'(++), DE UNO A DIEZ BACILOS POR CAMPO EN PROMEDIO EN 50 CAMPOS OBSERVADOS',
            'is_active'=>'1'
        ],[
            'nombre'=>'(+++), MÁS DE DIEZ BACILOS POR CAMPO EN 20 CAMPOS OBSERVADOS',
            'is_active'=>'1'
        ],[
            'nombre'=>'(-) NO SE OBSERVAN BACILOS ÁCIDO-ALCOHOL RESISTENTE EN 100 CAMPOS MICROSCÓPICOS',
            'is_active'=>'1'
        ],[
            'nombre'=>'T= MTB DETECTADO, SIN RESISTENCIA A RIFAMPICINA',
            'is_active'=>'1'
        ],[
            'nombre'=>'RR= MTB DETECTADA, CON RESISTENCIA A LA RIFAMPICINA',
            'is_active'=>'1'
        ],[
            'nombre'=>'TI= MTB DETECTADA, CON RESISTENCIA INDETERMINADA A LA RIFAMPICINA',
            'is_active'=>'1'
        ],[
            'nombre'=>'I= INVALIDO / SIN RESULTADO / ERROR',
            'is_active'=>'1'
        ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class EmailsDestinatariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emails')->insert([
            [
                'id_juri' => 1,
                'email' => 'epi.jsalvaroo@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 1,
                'email' => 'vramos@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 2,
                'email' => 'epi.jsazcapotzalco@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 3,
                'email' => 'epi.jsbenitojuarez@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 3,
                'email' => 'lalmeyda@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 4,
                'email' => 'jgmartinez@sersalud.df.gob.mx'
            ],[
                'id_juri' => 5,
                'email' => 'aortega@sersalud.df.gob.mx'
            ],[
                'id_juri' => 6,
                'email' => 'dermapascua_direccion@hotmail.com'
            ],[
                'id_juri' => 7,
                'email' => 'roia82@hotmail.com'
            ],[
                'id_juri' => 8,
                'email' => 'smcema@hotmail.com'
            ],[
                'id_juri' => 9,
                'email' => 'chezdireccion@gmail.com'
            ],[
                'id_juri' => 10,
                'email' => 'toxicologicovc@gmail.com'
            ],[
                'id_juri' => 11,
                'email' => 'epi.jscoyoacan@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 12,
                'email' => 'epi.jscuajimalpa@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 12,
                'email' => 'epxchong@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 13,
                'email' => 'epi.jscuauhtemoc@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 14,
                'email' => 'hebd.epidemiologia@gmail.com'
            ],[
                'id_juri' => 17,
                'email' => 'epi.jsgustavoam@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 18,
                'email' => 'aps_hpazcapotzalco@hotmail.com'
            ],[
                'id_juri' => 19,
                'email' => 'autan_aranda@outlook.com'
            ],[
                'id_juri' => 20,
                'email' => 'epidemiologiahpiztacalco@hotmail.com'
            ],[
                'id_juri' => 21,
                'email' => 'hjcamargoviedo@hotmail.com'
            ],[
                'id_juri' => 22,
                'email' => 'epidemiologiapv@hotmail.com'
            ],[
                'id_juri' => 23,
                'email' => 'uvehpleg@hotmail.com'
            ],[
                'id_juri' => 24,
                'email' => 'rzcmlj@hotmail.com'
            ],[
                'id_juri' => 25,
                'email' => 'peralvillopreventiva@gmail.com'
            ],[
                'id_juri' => 26,
                'email' => 'duranperezsergio86@gmail.com'
            ],[
                'id_juri' => 27,
                'email' => 'hospedtacubaya@gmail.com'
            ],[
                'id_juri' => 28,
                'email' => 'epidemiologia_hgam@hotmail.com'
            ],[
                'id_juri' => 29,
                'email' => 'apsbalbuena@yahoo.com.mx'
            ],[
                'id_juri' => 30,
                'email' => 'hebd.epidemiologia@gmail.com'
            ],[
                'id_juri' => 31,
                'email' => 'epi_enriquecabrera@yahoo.com.mx'
            ],[
                'id_juri' => 32,
                'email' => 'carsolcor58@gmail.com'
            ],[
                'id_juri' => 33,
                'email' => 'chezepidemiologia@hotmail.com'
            ],[
                'id_juri' => 34,
                'email' => 'elicano30@yahoo.com.mx'
            ],[
                'id_juri' => 35,
                'email' => 'hospgralizta@yahoo.com.mx'
            ],[
                'id_juri' => 36,
                'email' => 'gvillaaps@hotmail.com'
            ],[
                'id_juri' => 37,
                'email' => 'epi.hgma15@gmail.com'
            ],[
                'id_juri' => 38,
                'email' => 'epidemiologiaymp.hgt@gmail.com'
            ],[
                'id_juri' => 39,
                'email' => 'epidemiohgt_01@outlook.com'
            ],[
                'id_juri' => 40,
                'email' => 'jeny_mc24@hotmail.com'
            ],[
                'id_juri' => 41,
                'email' => 'epidemiologia.19hgtmt@gmail.com'
            ],[
                'id_juri' => 42,
                'email' => 'xocoepidemiologia20@gmail.com'
            ],[
                'id_juri' => 43,
                'email' => 'hxochimilcoepidem@gmail.com'
            ],[
                'id_juri' => 44,
                'email' => 'dra.cisneros29@gmail.com'
            ],[
                'id_juri' => 45,
                'email' => 'elidasta@yahoo.com.mx'
            ],[
                'id_juri' => 46,
                'email' => 'melymor@hotmail.com'
            ],[
                'id_juri' => 47,
                'email' => 'vigepidemmcontcdmx@gmail.com'
            ],[
                'id_juri' => 48,
                'email' => 'dr_riosfabian@hotmail.com'
            ],[
                'id_juri' => 50,
                'email' => 'epi.jsiztacalco@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 51,
                'email' => 'depi.jsiztapalapa@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 53,
                'email' => 'epi.jsmagdalena@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 54,
                'email' => 'epi.jsmiguelh@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 55,
                'email' => 'epi.jsmilpaalta@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 56,
                'email' => 'epi.jstlahuac@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 56,
                'email' => 'dr.ramsesgomez@gmail.com'
            ],[
                'id_juri' => 57,
                'email' => 'epi.jstlalpan@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 58,
                'email' => 'hcrspenitenciaria@hotmail.com'
            ],[
                'id_juri' => 59,
                'email' => 'aldaexma_3205@hotmail.com'
            ],[
                'id_juri' => 60,
                'email' => 'aldaexma_3205@hotmail.com'
            ],[
                'id_juri' => 61,
                'email' => 'umrpvsur@hotmail.com'
            ],[
                'id_juri' => 62,
                'email' => 'dra.juareztonansi@gmail.com'
            ],[
                'id_juri' => 63,
                'email' => 'epi.jsvcarranza@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 64,
                'email' => 'depi.jsxochimilco@sersalud.cdmx.gob.mx'
            ],[
                'id_juri' => 64,
                'email' => 'jsx.epidem@sersalud.cdmx.gob.mx'
            ]
        ]);
    }
}

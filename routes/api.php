<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/* return response()->json([ 'valid' => auth()->check() ]); */
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'charts'], function() {
    //Ruta de api para la gráfica de muestras ingresadas
    Route::get('entered/{year}', 'Api\ChartsController@entered');

    //Ruta de api para la gráfica de muestras rechazadas
    Route::get('rejected/{year}', 'Api\ChartsController@rejected');

    //Ruta de api para la gráfica de motivos rechazados
    Route::get('rejected/reasons/{year}', 'Api\ChartsController@rejectionReasons');

    //Ruta de api para la gráfica de motivos rechazados por jurisdicción
    Route::get('rejected/jurisdiction/{year}', 'Api\ChartsController@rejectionJurisdiction');

    //Ruta de api para la gráfica de motivos rechazados por centro medico
    Route::get('rejected/hospitals/{year}', 'Api\ChartsController@rejectionHospitals');

    Route::get('rejected/sample/{year}', 'Api\ChartsController@rejectionSample');

    Route::get('received/date/{date}', 'Api\ChartsController@receivedSampleDay');

    Route::get('respiratorios/{start}/{end}', 'Api\ChartsController@respiratorios');
});

Route::get('search/interpretation', 'Api\SearchInterpretationController');

Route::group(['prefix' => 'tables'], function () {
    //Ruta de api generación de excel de tabla de muestras finalizadas
    Route::post('entered', 'Api\TableEnteredController');

    //Ruta de api generación de excel de tabla de muestras rechazadas
    Route::post('rejected', 'Api\TableRejectedController');
});

Route::get('datatable/paludismo', 'Api\PaludismoDataTable');
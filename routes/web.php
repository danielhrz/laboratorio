<?php

use Illuminate\Support\Facades\Route;
use App\Mail\MessageReceived;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

ini_set('memory_limit', '-1');
set_time_limit(0);

Auth::routes();

Route::get('/', function () {
    $existSession =Auth::user();
    if(is_null($existSession) ) {
        return view('main_admin.login');
    } else {
        return redirect('main_admin');
    }
});

/* Ruta del login */
Route::get('/laboratorio_sspcdmx', function () {
    $existSession = Auth::user();
    if(is_null($existSession) ) {
        return view('main_admin.login');
    } else {
        return redirect('main_admin');
    }

})->name('laboratorio_sspcdmx');

/* Ruta para acceder al panel principal */
Route::resource('main_admin', 'MainController');

Route::group(['middleware' => 'users'], function () {
    /* Ruta para CRUD de usuarios */
    Route::get('dataTableUsers', 'UsersController@dataTableU')->name('dataTableUsers');

    Route::resource('usuarios', 'UsersController');

});

/* Ruta para CRUD de datos pacientes*/
/* Route::resource('datos', 'DatosFederalesController'); */
Route::group(['middleware' => 'admin'], function () {

        /* Ruta index Datos Federales*/
    Route::get('datos/{tipo}', 'DatosFederalesController@index');

        /* Ruta para destroy de datos */
    Route::delete('datos/{datos}', 'DatosFederalesController@destroy')->name('datos.destroy');

});


Route::get('dataTableDatos', 'DatosFederalesController@dataTable')->name('dataTableDatos');

Route::patch('datos/{dato}/cancelado', 'DatosFederalesController@cancelado')->name('datos.cancelado');

Route::patch('datos/{dato}/pendiente', 'DatosFederalesController@pendiente')->name('datos.pendiente');

Route::patch('datos/{dato}/finalizado', 'DatosFederalesController@finalizado')->name('datos.finalizado');

Route::get('privilegios', function () {
    return view('errors.404');
});

Route::put('estados', 'DatosFederalesController@changeEstados');

//Rutas para reportes
Route::resource('/reportes', 'ReportesController' );

//Ruta index para proceso de asignación de resultados
Route::get('allocation', 'AllocationResultsController@index')->name('allocation');

Route::get('dataTableAsignaciones', 'AllocationResultsController@dataTableR')->name('dataTableAsignaciones');

//Ruta index para el log de modificaciones
Route::get('log', 'LogController@index')->name('log');

Route::get('dataTableLog', 'LogController@dataTableLogChanges')->name('dataTableLog');

Route::get('search/muestra/{id}', 'AllocationResultsController@searchMuestra');

Route::get('search/result/{id}', 'AllocationResultsController@edit');

Route::get('search/kit', 'AllocationResultsController@searchDiagnosisForKit');

Route::post('allocation', 'AllocationResultsController@store')->name('allocation');

Route::put('allocation/{id}', 'AllocationResultsController@update');

Route::resource('/results', 'ResultsController');

Route::resource('/destinatarios', 'DestinatariosController');

Route::get('dataTableDestinatarios', 'DestinatariosController@BuildDataTable')->name('dataTableADestinatarios');
/* Route::get('/import-form', 'DatosFederalesController@importForm');
Route::post('import', 'DatosFederalesController@import')->name('datos.import'); */

Route::get('full', 'FullResultsController@index')->name('full');
Route::get('dataTableFull', 'FullResultsController@dataTableFullResults')->name('dataTableFull');

Route::get('query', 'QueryResultsController@index')->name('query');
Route::get('dataTableQuery', 'QueryResultsController@dataTableQueryResults')->name('dataTableQuery');

Route::put('operational/status', 'DatosFederalesController@changeOperationalEstados');

Route::get('export/jurisdiction/{id}', 'QueryResultsController@exportJurisdiction');

Route::get('descarga', 'MainController@downloadGeneral');

Route::prefix('grafica')->group(function() {
    Route::get('/ingresadas', 'ViewChartController@ingresadas');

    Route::get('/rechazadas', 'ViewChartController@rechazadas');

    Route::get('/motivos', 'ViewChartController@motivos');

    Route::get('/jurisdicciones', 'ViewChartController@jurisdicciones');

    Route::get('/hospitales', 'ViewChartController@hospitales');

    Route::get('/muestras', 'ViewChartController@muestras');

    Route::get('/recepcionadas', 'ViewChartController@recepcionadas');
});

Route::prefix('diagnostico')->group(function () {
    Route::get('/respiratorios', 'DiagnosticsController@respiratorio');

});

Route::get('paludismo', 'PaludismoController')->name('paludismo.index');
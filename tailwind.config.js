/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/views/layouts/*.blade.php",
    "./resources/**/*.js",
    "./public/panel/js/jquery-allocation.js",
    "./public/js/sidebar.js",
    "./resources/**/*.vue",
    './app/Http/Controllers/*.php',
    "./node_modules/flowbite/**/*.js",
    './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('flowbite/plugin'),
    
    require('tailwindcss-plugins/pagination')
]
}

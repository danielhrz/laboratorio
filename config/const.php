<?php
return [
    "diagnostic_type" => ["Seguimiento", "Búsqueda"],
    "reading_result" => ["Positivo", "Negativo"],
    "species" => ["P. falciparum", "P. malarie", "P. ovale", "P. vivax", "N/A"],
    "quality_control" => ["Sí", "No"],
    "quality_control_result" => ["Concordante", "Discordante", "N/A"],
    "sample_bank" => ["Sí", "No"],
    "diagnostic_tests" => ["Sí(sin registro de resultado)", "No", "Negativa", "P. vivax", "P. falciparum", "P. vivax/P. falciparum"],
];
?>
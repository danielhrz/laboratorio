<?php

return [
    'mode'                     => '',
    'format'                   => 'A4',
    'default_font_size'        => '12',
    'default_font'             => 'sans-serif',
    'margin_left'              => 10,
    'margin_right'             => 10,
    'margin_top'               => 10,
    'margin_bottom'            => 20,
    'margin_header'            => 0,
    'margin_footer'            => 1,
    'orientation'              => 'P',
    'title'                    => 'Laravel mPDF',
    'subject'                  => '',
    'author'                   => '',
    'watermark'                => '',
    'show_watermark'           => false,
    'show_watermark_image'     => false,
    'watermark_font'           => 'sans-serif',
    'display_mode'             => 'fullpage',
    'watermark_text_alpha'     => 0.1,
    'watermark_image_path'     => '',
    'watermark_image_alpha'    => 0.2,
    'watermark_image_size'     => 'D',
    'watermark_image_position' => 'P',
    'custom_font_dir'          => '',
    'custom_font_data'         => [],
    'auto_language_detection'  => false,
    'temp_dir'                 => rtrim(sys_get_temp_dir(), DIRECTORY_SEPARATOR),
    'pdfa'                     => false,
    'pdfaauto'                 => false,
    'use_active_forms'         => false,
    'custom_font_dir'  => base_path('public/fonts/'), // don't forget the trailing slash!
    'custom_font_data' => [
        'roboto' => [ // must be lowercase and snake_case
            'R'  => 'Roboto-Regular.ttf',    // regular font
            'B'  => 'Roboto-Bold.ttf',       // optional: bold font
            'I'  => 'Roboto-Italic.ttf',     // optional: italic font
            'BI' => 'Roboto-BoldItalic.ttf' // optional: bold-italic font
        ],
        'cabin' => [
            'R'  => 'Cabin-Regular.ttf',    // regular font
            'B'  => 'Cabin-Bold.ttf',       // optional: bold font
            'I'  => 'Cabin-Italic.ttf',     // optional: italic font
            'BI' => 'Cabin-BoldItalic.ttf' // optional: bold-italic font
        ]
        // ...add as many as you want.
    ]
];

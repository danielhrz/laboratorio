<?php

return [
    'arbovirus' => [
        'mandated_origin' => 'BIOL. IRMA LÓPEZ MARTÍNEZ',
        'position_origin' => 'DIRECTORA DE DIAGNÓSTICO Y REFERENCIA, InDRE',
        'mandated_received' => 'AT’N. M. EN C. MAURICIO VÁZQUEZ PICHARDO',
        'position_received' => 'JEFE DEL LABORATORIO DE ARBOVIRUS Y VIRUS HEMORRÁGICOS DEL InDRE'
    ],
    'rabia' => [
        'mandated_origin' => 'BIOL. IRMA LÓPEZ MARTÍNEZ',
        'position_origin' => 'DIRECTORA DE DIAGNÓSTICO Y REFERENCIA, InDRE',
        'mandated_received' => 'AT’N. IBT. SUSANA CHAVEZ LÓPEZ',
        'position_received' => 'ENCARGADA DEL LABORATORIO DE RABIA DEL InDRE'
    ],
    'leishmaniasis' => [
        'mandated_origin' => 'BIOL. IRMA LÓPEZ MARTÍNEZ',
        'position_origin' => 'DIRECTORA DE DIAGNÓSTICO Y REFERENCIA, InDRE',
        'mandated_received' => 'AT’N. MTRO. OCTAVIO CÉSAR RIVERA HERNÁNDEZ',
        'position_received' => 'JEFE DEL LABORATORIO DE LEISHMANIA DEL InDRE'
    ]
];

var table;
$(document).ready(function () {

    $("#start_date").datetimepicker({
        onShow: function (ct) {
            this.setOptions({
                maxDate: $("#end_date").val() ? $("#end_date").val() : false
            })
        },
        format: 'Y-m-d',
        timepicker: false,
    });

    $("#end_date").datetimepicker({
        onShow: function (ct) {
            this.setOptions({
                minDate: jQuery("#start_date").val() ? jQuery("#start_date").val() : false
            })
        },
        format: 'Y-m-d',
        timepicker: false,
    });

    if (typeof DataTable === 'function') {
        table = $("#table_paludismo").DataTable({
            dom: 'lrtip',
            paging: true,
            lengthChange: true,
            serverSide: true,
            processing: true,
            searching: true,
            bFilter: true,
            ordering: true,
            info: true,
            autoWidth: false,
            pagingType: 'full_numbers', //Para poner la paginacíon completa
            //order: [1, 'DESC'],
            ajax: { //genera la petición de información a la ruta api
                headers: {
                    //'Authorization': 'Bearer ' + $('.wrapper-container').data('token'),
                    "Accept": "Application/Json"
                },
                url: $("#rutaFullResults").val() + '/api/datatable/paludismo',
                data: function (d) {
                    d.date_start = $("#start_date").val(),
                        d.dateend = $("#end_date").val()
                },
                type: "GET"
            },
            columns: [{
                    data: 'id',
                    orderable: false
                },
                {
                    data: 'folio_lesp',
                    //title: 'Folio'
                },
                {
                    data: 'nombre_paciente',
                    //title: 'Fecha de alta'
                },
                {
                    data: 'fecha_recepcion',
                    //title: 'Fecha de alta'
                },
                {
                    data: 'folio_sisver',
                    //title: 'Jurisdicción'
                },
                {
                    data: 'diagnostic_type'
                },
                {
                    data: 'key_lamella'
                },
                {
                    data: 'epidemiological_week'
                },
                {
                    data: 'total_time'
                },
                {
                    data: 'sample_bank'
                },
                {
                    data: 'diagnostic_tests'
                },
                {
                    data: 'btn',
                    orderable: false
                }
            ],
            language: {
                "lengthMenu": "Mostrar " +
                    `<select class="custom-select custom-select-sm form-control form-control-sm">
                            <option value = '10'>10</option>
                            <option value = '25'>25</option>
                            <option value = '50'>50</option>
                            <option value = '100'>100</option>
                        </select>` +
                    " registros por página",
                "zeroRecords": "No se encontraron registros",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "sProcessing": "<i class='fas fa-spinner'></i> Cargando...",
                "oPaginate": {
                    "sNext": "<i class='fa-solid fa-angle-right'></i>",
                    "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                    "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                    "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            },
            initComplete: function () {
                var classTh = "px-1 py-1 text-white text-sm md:text-base border border-box first:border-l-0 last:border-r-0";
                $('#table_paludismo thead').append("<tr id='tickets__input__search'></tr>");
                this.api()
                    .columns()
                    .every(function () {
                        var column = this;
                        var title = column.header().textContent;
                        if (title != '#' && title != 'Opciones') {
                            // Create input element and add event listener
                            $('<th class="' + classTh + '"><input type="text" placeholder="Filtrar..." class="block w-full border-gray-300 rounded shadow-sm transition duration-150 ease-in-out focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 text-slate-500 text-xs md:text-sm px-1.5 py-1.5 md:py-2 font-medium"/></th>')
                                .appendTo($("#tickets__input__search"))
                                .on('keyup change clear', $.debounce(250, function (e) {
                                    if (column.search() !== $(this).children().val()) {
                                        column.search($(this).children().val()).draw();
                                    }
                                }));
                        } else {
                            $('<th class="' + classTh + '"></th>')
                                .appendTo($("#tickets__input__search"));
                        }
                    });
            }
        });

        $("#search_data").click(function () {
            table.draw();
        });

        //Una vez que la pagina de la tabla este cargada con la información
        table.on('draw.td', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) { //Genera un numero consecutivo en la tabla como index
                cell.innerHTML = i + 1 + info.start;
            });
        });
        var targetModalCrud = document.getElementById('modal_information_extension');
        const options = {
            placement: 'bottom-right',
            backdrop: 'dynamic',
            backdropClasses: 'bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40',
            /* onHide: () => {
                console.log('modal is hidden');
            },
            onShow: () => {
                console.log('modal is shown');
            },
            onToggle: () => {
                console.log('modal has been toggled');
            } */
        };

        var modalInformationExtension = new Modal(targetModalCrud, options);
        Livewire.on('showModalCrudInformation', function () {
            modalInformationExtension.show();
        });

        $("#table_paludismo").on('click', '.button__edit', function (e) {
            var dato_id = $(this).data('identifier');

            Livewire.emit('updateInformation', dato_id);
        })

        Livewire.on('hidden', function () {
            modalInformationExtension.hide();
            table.ajax.reload(null, false);
        });
    }

    $("#reception").datetimepicker({
        format: 'Y-m-d H:i',
        //timepicker: false,
    });

    $("#sample_collection_time").datetimepicker({
        format: 'H:i',
        datepicker: false,
    });

    $("#date_delivery_results_aeer").datetimepicker({
        format: 'Y-m-d H:i',
        //timepicker: false,
    });

    Livewire.on('created-data', () => {
        Swal.fire(
            'Registro actualizado',
            'El registro ha sido modificado con éxito!',
            'success'
        )
    });

    Livewire.on('error-export', () => {
        Swal.fire(
            'Error',
            'El número de muestras a exportar excede los 5000 registros',
            'error'
        )
    });
})

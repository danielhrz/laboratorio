$(document).ready(function () {
    var hasCloseSideBar = 0;
        if ($(window).width() < 1000) {
            //$('.sidebar-navigation').addClass('aside-close');
            $('.sidebar-navigation').addClass('aside-responsive')
            $(".side-bar").addClass("aside-visible");
            $(".close-side-responsive").addClass("close-visible");
            $('.wrapper-main-container').addClass('container-full');
    
        }
    
        $('.action-menu').click(function(e) {
            e.preventDefault();
            if( $('.sidebar-navigation').hasClass('aside-responsive') ) {
                $('.sidebar-navigation').addClass('aside-open');
                hasCloseSideBar = 1;
            } else if( $('.sidebar-navigation').hasClass('aside-close') ) {
                $('.sidebar-navigation').removeClass('aside-close');
                $('.wrapper-main-container').removeClass('container-full');
            } else {
                $('.sidebar-navigation').addClass('aside-close');
                $('.wrapper-main-container').addClass('container-full');
            }
            
        });
    
        $('.container-full').click( function() {
            if( hasCloseSideBar == 0 ) {
                $('.sidebar-navigation').removeClass('aside-open');
            } else {
                hasCloseSideBar = 0;
            }
        });
    
        $(window).resize(function () {
    
            var ancho = $(window).width();
    
            if (ancho < 1000) {
    
                $('.sidebar-navigation').addClass('aside-responsive')
                $(".side-bar").addClass("aside-visible");
                $(".close-side-responsive").addClass("close-visible");
                $('.wrapper-main-container').addClass('container-full');
                $('.sidebar-navigation').removeClass('aside-close');
    
            } else {
    
                $('.sidebar-navigation').removeClass('aside-responsive');
                $('.wrapper-main-container').removeClass('container-full');
                $('.sidebar-navigation').removeClass('aside-open');
                hasCloseSideBar = 0;
    
            }
        });
        $(".down-aside-menu ul").removeClass('down-aside-item-active');

    $(".down-aside-menu").click(function (e) {
        if ($('a', this).hasClass('down-aside-menu-visible')) {
            $('ul', this).hide('slow');
            $('a', this).removeClass('down-aside-menu-visible');
            $('a i:last-child', this).removeClass('-rotate-90');
            //$('a i:last-child', this).removeClass('fa-chevron-up');
            //$('a i:last-child', this).addClass('fa-chevron-left');
        } else {
            $('ul', this).show('slow');
            $('a', this).addClass('down-aside-menu-visible');
            $('a i:last-child', this).addClass('-rotate-90');
            //$('a i:last-child', this).removeClass('fa-chevron-left');
            //$('a i:last-child', this).addClass('fa-chevron-up');
        }
    });
    
        
    $('.down-aside-menu').each(function (key, value) {
        if ($('ul', this).hasClass('down-aside-item-active')) {
            $('a', this).addClass('down-aside-menu-visible');
            $('a i:last-child', this).removeClass('-rotate-90');
            //$('a i:last-child', this).removeClass('fa-chevron-left');
            //$('a i:last-child', this).addClass('fa-chevron-up');
        }
    });
    });
    var t;
    var influenza = 0;
    var columIndex;
    var columnOrder;
    $(document).ready(function () {
        var datosForm = ['coments', 'cre', 'date_delivery', 'interpretation_result', 'result.0', , 'result.1', , 'result.2', 'result.4', 'result.5', 'if_exit', 'status_result', 'kit.0', , 'kit.1', , 'kit.2', 'kit.4', 'kit.5'];
        var classError = 'shadow shadow-red-500/50 border-red-600 focus:ring-red-700 focus:border-red-700';
        var classSuccess = 'border-gray-300 focus:ring-green-700 focus:border-green-700';

        var role = $("#role-user").val();
        var columnsDataTable = null;
        if (role == 1 || role == 6 || role == 9) { 
            columIndex = 1;
            columnOrder = 1;
            columnsDataTable = [{
                    data: 'checkbox'
                },
                {
                    data: 'id',
                    searchable: false,
                },
                {
                    data: 'oficio_entrada'
                },
                {
                    data: 'nombre_paciente'
                },
                {
                    data: 'folio_lesp'
                },
                {
                    data: 'folio_sisver'
                },
                {
                    data: 'tipo_muestra'
                },
                {
                    data: 'fecha_toma_muestra'
                },
                {
                    data: 'dx1'
                },
                {
                    data: 'observaciones'
                },
                {
                    data: 'aclaraciones_remu'
                },
                {
                    data: 'fecha_recepcion'
                },
                {
                    data: 'hora_recepcion'
                },
                {
                    data: 'result_identifier'
                },
                {
                    data: 'status'
                },
                {
                    data: 'status_result'
                },
                {
                    data: 'operational_status'
                },
                {
                    data: 'btn',
                    orderable: false,
                    searchable: false,
                },
            ];
        } else {
            columIndex = 0;
            $('.filter-input').each(
                function () {
                    let val = $(this).data('column');
                    //console.log('Valor viejo: ' + val );
                    
                    $(this).data('column', --val);
                    //console.log('Valor nuevo: ' + val );
                }
            );
            columnOrder = 0;
            columnsDataTable = [
                {
                    data: 'created_at',
                    searchable: false,
                },
                {
                    data: 'oficio_entrada'
                },
                {
                    data: 'nombre_paciente'
                },
                {
                    data: 'folio_lesp'
                },
                {
                    data: 'folio_sisver'
                },
                {
                    data: 'tipo_muestra'
                },
                {
                    data: 'fecha_toma_muestra'
                },
                {
                    data: 'dx1'
                },
                {
                    data: 'observaciones'
                },
                {
                    data: 'aclaraciones_remu'
                },
                {
                    data: 'fecha_recepcion'
                },
                {
                    data: 'hora_recepcion'
                },
                {
                    data: 'result_identifier'
                },
                {
                    data: 'status'
                },
                {
                    data: 'status_result'
                },
                {
                    data: 'operational_status'
                },
                {
                    data: 'btn',
                    orderable: false,
                    searchable: false,
                },
            ];
        }
        //console.log(columnsDataTable);
        t = $('#asignaciones').DataTable({
            dom: 'lrtip',
            paging: true,
            lengthChange: true,
            serverSide: true,
            processing: true,
            searching: true,
            bFilter: true,
            ordering: true,
            info: true,
            autoWidth: false,
            pagingType: 'full_numbers',
            ajax: {
                url: $('#rutaAsignaciones').val(),
            },
            columnDefs: [
                {
                    targets: [ 10 ],
                    visible: false,
                    searchable: true,
                    orderable: true
                }
            ],
            order: [
                [columnOrder, "desc"]
            ],
            columns: columnsDataTable,
            language: {
                "lengthMenu": "Mostrar " +
                    `<select class="custom-select custom-select-sm form-control form-control-sm">
                            <option value = '10'>10</option>
                            <option value = '25'>25</option>
                            <option value = '50'>50</option>
                            <option value = '100'>100</option>
                        </select>` +
                    " registros por página",
                "zeroRecords": "No se encontraron registros",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "sProcessing": "<i class='fas fa-spinner'></i> Cargando...",
                "oPaginate": {
                    "sNext": "<i class='fa-solid fa-angle-right'></i>",
                    "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                    "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                    "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            },
            orderCellsTop: true,
        });

        $('#colums-dataTables-asignaciones a').on( 'click', function (e) {
            e.preventDefault();
            // Get the column API object
            var column = t.column( $(this).attr('data-column') );
            // Toggle the visibility
            column.visible( ! column.visible() );
        });

        t.on('draw.dt', function () {
            $('#check-all').prop('checked', false);
            $("#container-buttons-status").attr('data-folios', '');
            $("#container-buttons-status").css('display', 'none');
            var PageInfo = $('#asignaciones').DataTable().page.info();
            t.column(columIndex, {
                page: 'current'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
            var targetEdit = document.getElementById('tooltipEdit');
            var buttonsEdit = document.getElementsByClassName('edit-result');

            var targetCreate = document.getElementById('tooltipCreate');
            var buttonsCreate = document.getElementsByClassName('show-result');

            var targetView = document.getElementById('tooltipShow');
            var buttonsView = document.getElementsByClassName('view-result');

            Array.from(buttonsEdit).forEach(function (element) {
                let tooltip = new Tooltip(targetEdit, element);
            }, false);

            Array.from(buttonsCreate).forEach(function (element) {
                let tooltip = new Tooltip(targetCreate, element);
            }, false);

            Array.from(buttonsView).forEach(function (element) {
                let tooltip = new Tooltip(targetView, element);
            }, false);


            $('.container-table').on('keyup', '.filter-input', function () {
                t.column($(this).data('column'))
                    .search($(this).val())
                    .draw();
            });
            $('#filtrar').click(function () {
                t.draw();
            });
        });
        var targetModalResult = document.getElementById('modal-result');
        var targetModalResultUpdate = document.getElementById('update-modal-result');
        var targetModalResultView = document.getElementById('viewModalResult');
        var modalResult, modalResultEdit, viewModalResult;
        var options = {
            placement: 'top-center',
            backdropClasses: 'bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40',
            onHide: () => {
                /* console.log('modal is hidden'); */
            },
            onShow: () => {
                /* console.log('modal is shown'); */
            },
            onToggle: () => {
                /* console.log('modal has been toggled'); */
            }
        };

        $("#update_status_result").change(function(e) {
            if($(this).val() == 'FINALIZADO/ V. EPIDEMIOLOGICO') {
                $("#update_coments").val("MUESTRA DE ALTO VALOR EPIDEMIOLOGICO.");
            }
        });

        $("#status_result").change(function(e) {
            if($(this).val() == 'FINALIZADO/ V. EPIDEMIOLOGICO') {
                $("#coments").val("MUESTRA DE ALTO VALOR EPIDEMIOLOGICO.");
            }
        });

        $(".container-table").on('click', '.show-result', function () {
            if (targetModalResult != null) {
                $.ajax({
                    url: $("#url").val() + '/search/muestra/' + $(this).data('muestra'),
                    method: "GET",
                    beforeSend: function () {
                        $.each(datosForm, function (index, value) {
                            $("#" + value).removeClass(classError);
                            $("#" + value).addClass(classSuccess);
                            $("#error_" + value).addClass('hidden');
                            $("#" + value).val('');
                        });
                    },
                    success: function (response) {
                        if (response.success) {
                            //console.log(response.data);
                            $("#body-result-select").empty()
                            $("#oficio_entrada").val(response.data.oficio_entrada);
                            $("#nombre_paciente").val(response.data.nombre_paciente);
                            $("#folio_lesp").val(response.data.folio_lesp);
                            $("#folio_sisver").val(response.data.folio_sisver);
                            $("#tipo_muestra").val(response.data.tipo_muestra);
                            $("#fecha_toma_muestra").val(response.data.fecha_toma_muestra);
                            $("#dato_federal_id").val(response.data.id);
                            var html = '';
                            for (let i = 0; i < response.data.diagnostics.length; i++) {
                                if(response.data.diagnostics[i].diagnosis_name == 'PALUDISMO') {
                                    $("#coments").val("FECHA DE TOMA DE MUESTRA:\n\nHORA DE TOMA DE MUESTRA:");
                                }else if (response.data.diagnostics[i].diagnosis_name == 'LEISHMANIASIS'){
                                    $("#coments").val("FECHA DE TOMA DE MUESTRA:\n\nHORA DE TOMA DE MUESTRA:");
                                }
                                html +=
                                    '<hr class="my-2">\
                                <div class="grid xl:grid-cols-2 xl:gap-x-6 xl:gap-y-0.5">\
                                    <div class="mb-1 group">\
                                        <label for="diagnosis" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Diagnostico solicitado:</label>\
                                        <input name="diagnosis" value="' + response.data.diagnostics[i].diagnosis_name + '" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700 bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="diagnosis">\
                                    </div>\
                                    <div class="mb-1 group">\
                                        <label for="kit_' + i + '" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Kit:</label>\
                                        <div class="col-12 col-md-6">\
                                            <select id="kit_' + i + '" data-select="' + i + '" data-diagnosis="' + response.data.diagnostics[i].diagnosis_name + '" class="select-kit border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="kit[]">\
                                                <option value="0">Seleccione un kit...</option>';
                                for (let j = 0; j < response.data.diagnostics[i].diagnosis_kits.length; j++) {
                                    html += '<option value="' + response.data.diagnostics[i].diagnosis_kits[j].kit_id + '">' + response.data.diagnostics[i].diagnosis_kits[j].kit_name + '</option>';
                                }
                                html += '</select>\
                                        </div>\
                                        <div id="error_kit_' + i + '" class="hidden"></div>\
                                    </div>';
                                let isTosFerina = response.data.diagnostics[i].hasOwnProperty('tosferina');
                                if (isTosFerina) {
                                    for (let j = 0; j < response.data.diagnostics[i].tosferina.length; j++) {
                                        html += '<div class="mb-1 group">\
                                                    <label for="reference" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">' + response.data.diagnostics[i].tosferina[j] + '</label>\
                                                    <div class="col-12 col-md-6">\
                                                    <select class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="tosferina[]" id="reference">\
                                                    <option value="NEGATIVO">NEGATIVO</option>\
                                                    <option value="POSITIVO">POSITIVO</option></select>\
                                                    </div>\
                                                </div>';
                                    }
                                }
                                html += '</div>\
                                <div id="diagnosis_' + i + '"></div>';
                            }
                            $("#result_identifier").val(response.data.result_identifier);
                            $("#body-result-select").html(html);
                            $("#interpretation_result").val('NA');
                            $("#observations").val('NA');
                            
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) { //Se captura el error en la petición HTTP
                        var error = '';
                        if (jqXHR.status == 0) {
                            error = 'No se estableció conexión: verifique su red.';
                        } else if (jqXHR.status == 404) {
                            error = 'Solicitud no encontrada [404]';
                        } else if (jqXHR.status == 500) {
                            error = 'Error en el servidor [500].';
                        } else if (textStatus == 'parsererror') {
                            error = 'Requested JSON parse failed.';
                        } else if (textStatus == 'timeout') {
                            error = 'Time out error.';
                        } else if (textStatus == 'abort') {
                            error = 'Petición ajax abortada.';
                        } else {
                            error = 'Error desconocido: ' + jqXHR.responseText;
                        }

                        if (typeof jqXHR.responseJSON != 'undefined') {
                            //En caso de que se notifique un error por parte del controlador indicar aquí
                        }
                    }
                });
                modalResult = new Modal(targetModalResult, options);
                modalResult.show();
            }
        })
        $("#body-result-select").on('change', '.select-kit', function () {
            var diagnosisSection = $(this).data('select');
            influenza = 0;
            //$("diagnosis_"+diagnosisSection).empty();
            $.ajax({
                url: $("#url").val() + '/search/kit',
                data: {
                    diagnosis: $(this).data('diagnosis'),
                    kit: $(this).val(),
                    lesp: $("#folio_lesp").val()
                },
                method: "GET",
                beforeSend: function () {
                    $.each(datosForm, function (index, value) {
                        // $("#" + value).removeClass(classError);
                        // $("#" + value).addClass(classSuccess);
                        // $("#error_" + value).addClass('hidden');
                        // $("#" + value).val('');
                    });
                },
                success: function (response) {
                    var html = '';
                    for (let i = 0; i < response.data.length; i++) {
                        var display = 'block';
                        //alert(response.data[i].analysis);

                        if (response.data[i].analysis == "INFLUENZA") {
                            influenza = 1;
                        }
                        html += '<br><fieldset class="border mt-4 p-4">\
                        <div class="grid xl:grid-cols-2 xl:gap-x-6 xl:gap-y-0.5">\
                            <div class="mb-1 group">\
                                <label for="virus_' + i + '" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Análisis:</label>\
                                <input name="virus_' + i + '" value="' + response.data[i].analysis + '" type="text" readonly class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2">\
                                <input type="hidden" value="' + response.data[i].id + '" name="diagnosis[]">\
                            </div>\
                            <div class="mb-1 group">\
                                <label for="technique" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Técnica Utilizada</label>\
                                <div class="col-12 col-md-6">\
                                    <select class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="technique[]" id="technique">\
                                        <option value="' + response.data[i].technique_id + '">' + response.data[i].technique_name + '</option>\
                                    </select>\
                                </div>\
                            </div>';
                            if (response.data[i].analysis == "ROTAVIRUS") {
                                var display = 'none';
                                html += '<div class="mb-1 group">\
                                <input type="hidden" name="reference[]" id="reference" value="' + response.data[i].reference_id+'"/>\
                                <table style="width: 100%; border-collapse: collapse;">\
                                                <tr>\
                                                    <td align="center" style="border: solid 1px #000;" colspan="2">Valor de corte</td>\
                                                </tr>\
                                                <tr>\
                                                    <td align="center" style="border: solid 1px #000;">NEGATIVO</td>\
                                                    <td align="center" style="border: solid 1px #000;"> =< 0.150 UA</td>\
                                                </tr>\
                                                <tr>\
                                                    <td align="center" style="border: solid 1px #000;">POSITIVO</td>\
                                                    <td align="center" style="border: solid 1px #000;"> > 0.150 UA</td>\
                                                </tr>\
                                            </table></div>';
                            } else {
                                html += '<div class="mb-1 group">\
                                <label style="display: '+display+'" for="reference" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Valor de Referencia</label>\
                                <div style="display: '+display+'" class="col-12 col-md-6">\
                                    <select class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="reference[]" id="reference">\
                                        <option value="' + response.data[i].reference_id + '">' + response.data[i].reference_value + '</option>\
                                    </select>\
                                </div>\
                            </div>';
                            }
                            
                            html += '<div class="mb-1 group">\
                                <label for="result_' + i + '" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Resultado:</label>\
                                <input name="result[]" type="text" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="result_' + i + '" placeholder="Ingresar el resultado">\
                                <div id="error_result_' + i + '" class="hidden"></div>\
                            </div>\
                        </div></fieldset>';

                    }
                    $("#diagnosis_" + diagnosisSection).html(html);
                },
                error: function (jqXHR, textStatus, errorThrown) { //Se captura el error en la petición HTTP
                    var error = '';
                    if (jqXHR.status == 0) {
                        error = 'No se estableció conexión: verifique su red.';
                    } else if (jqXHR.status == 404) {
                        error = 'Solicitud no encontrada [404]';
                    } else if (jqXHR.status == 500) {
                        error = 'Error en el servidor [500].';
                    } else if (textStatus == 'parsererror') {
                        error = 'Requested JSON parse failed.';
                    } else if (textStatus == 'timeout') {
                        error = 'Time out error.';
                    } else if (textStatus == 'abort') {
                        error = 'Petición ajax abortada.';
                    } else {
                        error = 'Error desconocido: ' + jqXHR.responseText;
                    }

                    if (typeof jqXHR.responseJSON != 'undefined') {
                        //En caso de que se notifique un error por parte del controlador indicar aquí
                    }
                }
            });
        });

        $("#form-result").on('change', '.select-status', function () {
            if (influenza == 1 && $(this).val() == 'PENDIENTE') {
                $('#coments').val('Pendiente a confirmación influenza');
            }
        });

        $(".close-modal").click(function (e) {
            e.preventDefault();
            if ($(this).data('modal') == 'create') {
                modalResult.hide();
            } else if ($(this).data('modal') == 'update') {
                modalResultEdit.hide();
            } else if ($(this).data('modal') == 'view') {
                viewModalResult.hide();
            }
        });

        $("#form-result").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: $("#url").val() + '/allocation',
                data: $("#form-result").serialize(),
                method: 'POST',
                beforeSend: function () {
                    $.each(datosForm, function (index, value) {
                        $("#" + (new String(value).replace(".", "_"))).removeClass(classError);
                        $("#" + (new String(value).replace(".", "_"))).addClass(classSuccess);
                        $("#error_" + (new String(value).replace(".", "_"))).addClass('hidden');
                    });
                },
                success: function (response) {
                    if (response.success) {
                        //Comprueba si la respuesta es exitosa actualiza la tabla sin refrescar la pagina
                        t.ajax.reload(null, false);
                        modalResult.hide();
                        //Muestra el mensaje de que el usuario fue eliminado
                        Swal.fire(
                            'Resultado asignado',
                            response.data,
                            'success'
                        )
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    let error = '';
                    if (jqXHR.status === 0) {
                        error = 'No se estableció conexión: verifique su red.';
                    } else if (jqXHR.status == 404) {
                        error = 'Solicitud no encontrada [404]';
                    } else if (jqXHR.status == 422) {
                        //Unprocessable Content
                        error = 'Unprocessable Content';
                    } else if (jqXHR.status == 500) {
                        error = 'Error en el servidor [500].';
                    } else if (textStatus === 'parsererror') {
                        error = 'Requested JSON parse failed.';
                    } else if (textStatus === 'timeout') {
                        error = 'Time out error.';
                    } else if (textStatus === 'abort') {
                        error = 'Petición ajax abortada.';
                    } else {
                        error = 'Error desconocido: ' + jqXHR.responseText;
                    }

                    if (typeof jqXHR.responseJSON != 'undefined') {
                        // console.log(jqXHR.responseJSON);
                        error = jqXHR.responseJSON.errors;
                        $.each(datosForm, function (index, value) {
                            let hasKey = error.hasOwnProperty(value);
                            if (hasKey) {
                                //console.log((value.replace(".", "_")));
                                $("#error_" + (new String(value).replace(".", "_"))).empty();
                                $("#error_" + (new String(value).replace(".", "_"))).append('<p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">' + error[value][0] + '</p>');
                                $("#" + (new String(value).replace(".", "_"))).addClass(classError);
                                $("#" + (new String(value).replace(".", "_"))).removeClass(classSuccess);
                                $("#error_" + (new String(value).replace(".", "_"))).removeClass('hidden');
                            }
                        });
                    }
                }
            });
        });

        $(".container-table").on('click', '.edit-result', function () {
            if (targetModalResultUpdate != null) {
                influenza = 0;
                $.ajax({
                    url: $("#url").val() + '/search/result/' + $(this).data('muestra'),
                    method: "GET",
                    beforeSend: function () {
                        $.each(datosForm, function (index, value) {
                            $("#update_" + value).removeClass(classError);
                            $("#update_" + value).addClass(classSuccess);
                            $("#error_update_" + value).addClass('hidden');
                            $("#update_" + value).val('');
                        });
                    },
                    success: function (response) {
                        if (response.success) {
                            //console.log(response.data);
                            $("#update_body-result-select").empty()
                            $("#update_oficio_entrada").val(response.data.oficio_entrada);
                            $("#update_nombre_paciente").val(response.data.nombre_paciente);
                            $("#update_folio_lesp").val(response.data.folio_lesp);
                            $("#update_folio_sisver").val(response.data.folio_sisver);
                            $("#update_tipo_muestra").val(response.data.tipo_muestra);
                            $("#update_fecha_toma_muestra").val(response.data.fecha_toma_muestra);
                            $("#update_dato_federal_id").val(response.data.id);
                            $("#update_result_id").val(response.data.result.id);
                            var html = '';
                            for (let i = 0; i < response.data.diagnostics.length; i++) {
                                html +=
                                    '<hr class="my-2">\
                                <div class="grid xl:grid-cols-2 xl:gap-x-6 xl:gap-y-0.5">\
                                    <div class="mb-1 group">\
                                        <label for="diagnosis" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Diagnostico solicitado:</label>\
                                        <input name="diagnosis" value="' + response.data.diagnostics[i].diagnosis_name + '" type="text" readonly class="text-uppercase border border-green-700 focus:ring-green-700 focus:border-green-700 bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="diagnosis">\
                                    </div>\
                                    <div class="mb-1 group">\
                                        <label for="kit_' + i + '" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Kit:</label>\
                                        <div class="col-12 col-md-6">\
                                            <select id="update_kit_' + i + '" data-select="' + i + '" data-diagnosis="' + response.data.diagnostics[i].diagnosis_name + '" class="select-kit border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="kit[]">\
                                                <option value="0">Seleccione un kit...</option>';
                                for (let j = 0; j < response.data.diagnostics[i].diagnosis_kits.length; j++) {
                                    html += '<option  value="' + response.data.diagnostics[i].diagnosis_kits[j].kit_id + '" ' + (response.data.diagnostics[i].diagnosis_kits[j].kit_id == response.data.diagnostics[i].kits_value ? "selected" : "") + '>' + response.data.diagnostics[i].diagnosis_kits[j].kit_name + '</option>';
                                }
                                html += '</select>\
                                        </div>\
                                        <div id="error_update_kit_' + i + '" class="hidden"></div>\
                                    </div>';
                                let isTosFerina = response.data.diagnostics[i].hasOwnProperty('tosferina');
                                if (isTosFerina) {
                                    for (let j = 0; j < response.data.diagnostics[i].tosferina.length; j++) {
                                        html += '<div class="mb-1 group">\
                                                <label for="reference" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">' + response.data.diagnostics[i].tosferina[j] + '</label>\
                                                <div class="col-12 col-md-6">\
                                                <select class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="tosferina[]" id="reference">\
                                                <option value="NEGATIVO" ' + (response.data.diagnostics[i].tosferina_value[j] == 'NEGATIVO' ? 'selected' : '') + '>NEGATIVO</option>\
                                                <option value="POSITIVO" ' + (response.data.diagnostics[i].tosferina_value[j] == 'POSITIVO' ? 'selected' : '') + '>POSITIVO</option></select>\
                                                </div>\
                                            </div>';
                                    }
                                }
                                html += '</div>\
                                <div id="edit_diagnosis_' + i + '">';
                                html += createStructureSelectKit(response.data.diagnostics[i].diagnosis_name, response.data.diagnostics[i].kits_value, response.data.folio_lesp, 1);
                                html += '</div>';
                            }

                            $("#body-update-result-select").html(html);

                            $("#update_if_exit").val(response.data.result.if_exit);
                            $("#update_cre").val(response.data.result.cre);
                            $("#update_result_identifier").val(response.data.result_identifier);
                            $("#update_date_delivery").val(response.data.result.date_delivery);
                            $("#update_status_result").val(response.data.status_result);
                            $("#update_result").val(response.data.result.result);
                            $("#update_coments").val(response.data.result.coments);
                            $("#update_observations").val(response.data.result.observations);
                            $("#update_interpretation_result").val(response.data.result.interpretation_result);

                            modalResultEdit = new Modal(targetModalResultUpdate, options);
                            modalResultEdit.show();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) { //Se captura el error en la petición HTTP
                        var error = '';
                        if (jqXHR.status == 0) {
                            error = 'No se estableció conexión: verifique su red.';
                        } else if (jqXHR.status == 404) {
                            error = 'Solicitud no encontrada [404]';
                        } else if (jqXHR.status == 500) {
                            error = 'Error en el servidor [500].';
                        } else if (textStatus == 'parsererror') {
                            error = 'Requested JSON parse failed.';
                        } else if (textStatus == 'timeout') {
                            error = 'Time out error.';
                        } else if (textStatus == 'abort') {
                            error = 'Petición ajax abortada.';
                        } else {
                            error = 'Error desconocido: ' + jqXHR.responseText;
                        }

                        if (typeof jqXHR.responseJSON != 'undefined') {
                            //En caso de que se notifique un error por parte del controlador indicar aquí
                        }
                    }
                });

            }
        });

        $("#body-update-result-select").on('change', '.select-kit', function () {
            var diagnosisSection = $(this).data('select');
            var html = createStructureSelectKit($(this).data('diagnosis'), $(this).val(), $("#update_folio_lesp").val(), 0);
            $("#edit_diagnosis_" + diagnosisSection).html(html);
        });

        $("#update-form-result").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: $("#url").val() + '/allocation/' + $("#update_result_id").val(),
                data: $("#update-form-result").serialize(),
                method: 'PUT',
                beforeSend: function () {
                    $.each(datosForm, function (index, value) {
                        $("#update_" + (new String(value).replace(".", "_"))).removeClass(classError);
                        $("#update_" + (new String(value).replace(".", "_"))).addClass(classSuccess);
                        $("#error_update_" + (new String(value).replace(".", "_"))).addClass('hidden');
                    });
                },
                success: function (response) {
                    if (response.success) {
                        //Comprueba si la respuesta es exitosa actualiza la tabla sin refrescar la pagina
                        t.ajax.reload(null, false);
                        modalResultEdit.hide();
                        //Muestra el mensaje de que el usuario fue eliminado
                        Swal.fire(
                            'Resultado editado',
                            response.data,
                            'success'
                        )
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    let error = '';
                    if (jqXHR.status === 0) {
                        error = 'No se estableció conexión: verifique su red.';
                    } else if (jqXHR.status == 404) {
                        error = 'Solicitud no encontrada [404]';
                    } else if (jqXHR.status == 422) {
                        //Unprocessable Content
                        error = 'Unprocessable Content';
                    } else if (jqXHR.status == 500) {
                        error = 'Error en el servidor [500].';
                    } else if (textStatus === 'parsererror') {
                        error = 'Requested JSON parse failed.';
                    } else if (textStatus === 'timeout') {
                        error = 'Time out error.';
                    } else if (textStatus === 'abort') {
                        error = 'Petición ajax abortada.';
                    } else {
                        error = 'Error desconocido: ' + jqXHR.responseText;
                    }

                    if (typeof jqXHR.responseJSON != 'undefined') {
                        // console.log(jqXHR.responseJSON);
                        error = jqXHR.responseJSON.errors;
                        $.each(datosForm, function (index, value) {
                            let hasKey = error.hasOwnProperty(value);
                            if (hasKey) {
                                $("#error_update_" + (new String(value).replace(".", "_"))).empty();
                                $("#error_update_" + (new String(value).replace(".", "_"))).append('<p class="mt-2 text-xs sm:text-sm text-red-600"><span class="font-medium">' + error[value][0] + '</p>');
                                $("#update_" + (new String(value).replace(".", "_"))).addClass(classError);
                                $("#update_" + (new String(value).replace(".", "_"))).removeClass(classSuccess);
                                $("#error_update_" + (new String(value).replace(".", "_"))).removeClass('hidden');
                            }
                        });
                    }
                }
            });
        });

        $(".container-table").on('click', '.view-result', function () {
            if (targetModalResultView != null) {
                $.ajax({
                    url: $("#url").val() + '/search/result/' + $(this).data('muestra'),
                    method: "GET",
                    success: function (response) {
                        if (response.success) {
                            var history = JSON.parse(response.data.result.history);
                            console.log(history);
                            $("#view-oficio-entrada").val(response.data.oficio_entrada);
                            $("#view_nombre_paciente").val(response.data.nombre_paciente);
                            $("#view_folio_lesp").val(response.data.folio_lesp);
                            $("#view_folio_sisver").val(response.data.folio_sisver);
                            $("#view_tipo_muestra").val(response.data.tipo_muestra);
                            $("#view_fecha_toma_muestra").val(response.data.fecha_toma_muestra);
                            $("#view_hospital").val(response.data.hospital);
                            $("#view_user_initials").val(response.data.initials);

                            var html2 = '';
                            for(var i = 0; i< history.length; i++) {
                                html2 += '<div class="flex justify-center items-center mb-4">\
                                    <label class="font-normal text-slate-700 text-right w-full">Iniciales:</label>\
                                    <input type="text" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" value="'+history[i].initials+'" readonly>\
                                </div>\
                                <div class="flex justify-center items-center mb-4">\
                                    <label class="font-normal text-slate-700 text-right w-full">Fecha de resultado:</label>\
                                    <input type="text" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" value="'+history[i].movement_date+'" readonly>\
                                </div>';
                            }
                            $("#view_history").html(html2);                            
                            if(response.data.revisor!=null){
                                $("#view_verificador").val(response.data.revisor.initials);
                            } else{
                                $("#view_verificador").val('SIN REVISION');
                            }
                            $("#view_cre").val(response.data.result.cre);
                            var text = "1: " + response.data.dx1 + "<br>" + "2: " + response.data.dx2 + "<br>" + "3: " + response.data.dx3 + "<br>" + "4: " + response.data.dx4 + "<br>" + "5: " + response.data.dx5 + "<br>";
                            $('#view_diagnosis').html(text);
                            $("#view_date_delivery").val(response.data.result.date_delivery);
                            text = '';
                            if (response.data.status_result == 'PENDIENTE') {
                                text = '<span data-value="PENDIENTE" class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">PENDIENTE A CONFIRMACIÓN DE INFLUENZA</span >';
                            }
                            if (response.data.status_result == 'REPROCESO') {
                                text = '<span data-value="REPROCESO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">REPROCESO</span >';
                            }
                            if (response.data.status_result == 'FINALIZADO') {
                                text = '<span data-value="FINALIZADO" class="bg-green-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">FINALIZADO</span >';
                            }
                            $("#view_status_result").html(text);
                            $("#view_if_exit").val(response.data.result.if_exit);
                            $("#view_result").val(response.data.result.result);
                            $("#view_coments").val(response.data.result.coments);
                            $("#view_finalized_result_date").val(response.data.result.finalized_result_date);
                            $("#view_date_complete_operational_status").val(response.data.result.date_complete_operational_status);
                            $("#view_observations").val(response.data.result.observations);
                            $("#view_interpretation_result").val(response.data.result.interpretation_result);
                            $("#body-view-result").empty();
                            var html = '';
                            for (let i = 0; i < response.data.diagnostics.length; i++) {
                                html +=
                                    '<div class="flex justify-center items-center mb-4">\
                                        <label class="font-normal text-slate-700 text-right w-full">Diagnostico solicitado:</label>\
                                        <input type="text" id="view-oficio-entrada" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" value="' + response.data.diagnostics[i].diagnosis_name + '" readonly>\
                                    </div>\
                                    <div class="flex justify-center items-center mb-4">\
                                        <label class="font-normal text-slate-700 text-right w-full">Kit empleado:</label>';
                                for (let j = 0; j < response.data.diagnostics[i].diagnosis_kits.length; j++) {
                                    if (response.data.diagnostics[i].diagnosis_kits[j].kit_id == response.data.diagnostics[i].kits_value) {
                                        html += '<input type="text" id="view-oficio-entrada" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" value="' + response.data.diagnostics[i].diagnosis_kits[j].kit_name + '" readonly>';
                                    }
                                }

                                html += '</div>\
                                    <div id="view_diagnosis_' + i + '">';
                                html += viewStructureSelectKit(response.data.diagnostics[i].diagnosis_name, response.data.diagnostics[i].kits_value, response.data.folio_lesp, 1);
                                html += '</div><br>';
                            }

                            $("#body-view-result").html(html);

                            viewModalResult = new Modal(targetModalResultView, options);
                            viewModalResult.show();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) { //Se captura el error en la petición HTTP
                        var error = '';
                        if (jqXHR.status == 0) {
                            error = 'No se estableció conexión: verifique su red.';
                        } else if (jqXHR.status == 404) {
                            error = 'Solicitud no encontrada [404]';
                        } else if (jqXHR.status == 500) {
                            error = 'Error en el servidor [500].';
                        } else if (textStatus == 'parsererror') {
                            error = 'Requested JSON parse failed.';
                        } else if (textStatus == 'timeout') {
                            error = 'Time out error.';
                        } else if (textStatus == 'abort') {
                            error = 'Petición ajax abortada.';
                        } else {
                            error = 'Error desconocido: ' + jqXHR.responseText;
                        }

                        if (typeof jqXHR.responseJSON != 'undefined') {
                            //En caso de que se notifique un error por parte del controlador indicar aquí
                        }
                    }
                });

            }
        })

        $("#update-modal-result").on('change', '.select-status', function () {
            if (influenza == 1 && $(this).val() == 'PENDIENTE') {
                $('#update_coments').val('Pendiente a confirmación influenza');
            }
        });

        $(".container-table").on('click', '.change-status-massive', function (event) {
            let visibleButtons = 0;
            let buttonTechnicalDifficulties = 0;
            let folios = '';
            $('.change-status-massive:checked').each(
                function () {
                    if($(this).data('action') == 0 ) {
                        buttonTechnicalDifficulties = 2;//estado de que no se puede habilitar dificultades técnicas
                    } else if(buttonTechnicalDifficulties == 0 &&  $(this).data('action') == 5 ) {
                        buttonTechnicalDifficulties = 1;//estado de que si se puede habilitar dificultades técnicas
                    }

                    folios = folios + ',' + $(this).val();
                    visibleButtons = 1;
                }
            );
            if(buttonTechnicalDifficulties == 0 || buttonTechnicalDifficulties == 2) {
                $(".btn-difficulties").addClass('hidden');
            } else {
                $(".btn-difficulties").removeClass('hidden');
            }
            
            if (visibleButtons == 1) {
                $("#container-buttons-status").attr('data-folios', folios.slice(1));
                $("#container-buttons-status").css('display', 'block');
            } else {
                $("#container-buttons-status").attr('data-folios', '');
                $("#container-buttons-status").css('display', 'none');
            }
        });

        $(".container-table").on('click', '#check-all', function (e) {
            let visibleButtons = 0;
            let buttonTechnicalDifficulties = 0;
            let folios = '';
            if ($('#check-all').prop('checked')) {
                $('.change-status-massive').prop('checked', true);
            } else {
                $('.change-status-massive').prop('checked', false);
            }

            $('.change-status-massive:checked').each(
                function () {
                    if($(this).data('action') == 0 ) {
                        buttonTechnicalDifficulties = 2;//estado de que no se puede habilitar dificultades técnicas
                    } else if(buttonTechnicalDifficulties == 0 &&  $(this).data('action') == 5 ) {
                        buttonTechnicalDifficulties = 1;//estado de que si se puede habilitar dificultades técnicas
                    }
                    folios = folios + ',' + $(this).val();
                    visibleButtons = 1;
                }
            );
            if(buttonTechnicalDifficulties == 0 || buttonTechnicalDifficulties == 2) {
                $(".btn-difficulties").addClass('hidden');
            } else {
                $(".btn-difficulties").removeClass('hidden');
            }
            if (visibleButtons == 1) {
                $("#container-buttons-status").attr('data-folios', folios.slice(1));
                $("#container-buttons-status").css('display', 'block');
            } else {
                $("#container-buttons-status").attr('data-folios', '');
                $("#container-buttons-status").css('display', 'none');
            }
        });

        $(".btn-action-status").click(function (event) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: $("#url").val() + '/operational/status',
                data: {
                    status: $(this).data('status'),
                    folios: $("#container-buttons-status").attr('data-folios')
                },
                method: 'PUT',
                success: function (response) {
                    $("#container-buttons-status").attr('data-folios', '');
                    $("#container-buttons-status").css('display', 'none');
                    t.ajax.reload();
                }
            });
        });

        $("#btn_search_interpretation_create").click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $("#url").val() + '/api/search/interpretation',
                data: $("#form-result").serialize(),
                method: 'GET',
                beforeSend: function () {
                    //Acción durante el envió de la petición
                },
                success: function (response) {
                    if (response.success) {
                        $("#interpretation_result").val(response.data.interpretation);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    let error = '';
                    if (jqXHR.status === 0) {
                        error = 'No se estableció conexión: verifique su red.';
                    } else if (jqXHR.status == 404) {
                        error = 'Solicitud no encontrada [404]';
                    } else if (jqXHR.status == 422) {
                        //Unprocessable Content
                        error = 'Unprocessable Content';
                    } else if (jqXHR.status == 500) {
                        error = 'Error en el servidor [500].';
                    } else if (textStatus === 'parsererror') {
                        error = 'Requested JSON parse failed.';
                    } else if (textStatus === 'timeout') {
                        error = 'Time out error.';
                    } else if (textStatus === 'abort') {
                        error = 'Petición ajax abortada.';
                    } else {
                        error = 'Error desconocido: ' + jqXHR.responseText;
                    }

                    if (typeof jqXHR.responseJSON != 'undefined') {
                        // console.log(jqXHR.responseJSON);
                        error = jqXHR.responseJSON.error;
                    }

                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: error
                    })
                }
            });
        });

        $("#btn_search_interpretation_edit").click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $("#url").val() + '/api/search/interpretation',
                data: $("#update-form-result").serialize(),
                method: 'GET',
                beforeSend: function () {
                    //Acción durante el envió de la petición
                },
                success: function (response) {
                    if (response.success) {
                        $("#update_interpretation_result").val(response.data.interpretation);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    let error = '';
                    if (jqXHR.status === 0) {
                        error = 'No se estableció conexión: verifique su red.';
                    } else if (jqXHR.status == 404) {
                        error = 'Solicitud no encontrada [404]';
                    } else if (jqXHR.status == 422) {
                        //Unprocessable Content
                        error = 'Unprocessable Content';
                    } else if (jqXHR.status == 500) {
                        error = 'Error en el servidor [500].';
                    } else if (textStatus === 'parsererror') {
                        error = 'Requested JSON parse failed.';
                    } else if (textStatus === 'timeout') {
                        error = 'Time out error.';
                    } else if (textStatus === 'abort') {
                        error = 'Petición ajax abortada.';
                    } else {
                        error = 'Error desconocido: ' + jqXHR.responseText;
                    }

                    if (typeof jqXHR.responseJSON != 'undefined') {
                        // console.log(jqXHR.responseJSON);
                        error = jqXHR.responseJSON.error;
                    }

                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: error
                    })
                }
            });
        });
    });


    function createStructureSelectKit(diagnosis, kit, lesp, edit) {
        var html = '';
        $.ajax({
            url: $("#url").val() + '/search/kit',
            data: {
                diagnosis: diagnosis,
                kit: kit,
                lesp: lesp,
                edit: edit
            },
            method: "GET",
            async: false,
            beforeSend: function () {
                //
            },
            success: function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    var display = 'block';
                    //alert(response.data[i].analysis);
                    if (response.data[i].analysis == "INFLUENZA") {
                        influenza = 1;
                    }
                    html += '<br><fieldset class="border mt-4 p-4">\
                        <div class="grid xl:grid-cols-2 xl:gap-x-6 xl:gap-y-0.5">\
                            <div class="mb-1 group">\
                                <label for="virus_' + i + '" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Análisis:</label>\
                                <input name="virus_' + i + '" value="' + response.data[i].analysis + '" type="text" readonly class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-gray-50 text-slate-600 text-xs sm:text-sm rounded block w-full p-2">\
                                <input type="hidden" value="' + response.data[i].id + '" name="diagnosis[]">\
                            </div>\
                            <div class="mb-1 group">\
                                <label for="technique" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Técnica Utilizada</label>\
                                <div class="col-12 col-md-6">\
                                    <select class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="technique[]" id="technique">\
                                        <option value="' + response.data[i].technique_id + '">' + response.data[i].technique_name + '</option>\
                                    </select>\
                                </div>\
                            </div>';
                    if (response.data[i].analysis == "ROTAVIRUS") {
                                var display = 'none';
                                html += '<div class="mb-1 group">\
                                <input type="hidden" name="reference[]" id="reference" value="' + response.data[i].reference_id+'"/>\
                                <table style="width: 100%; border-collapse: collapse;">\
                                                <tr>\
                                                    <td align="center" style="border: solid 1px #000;" colspan="2">Valor de corte</td>\
                                                </tr>\
                                                <tr>\
                                                    <td align="center" style="border: solid 1px #000;">NEGATIVO</td>\
                                                    <td align="center" style="border: solid 1px #000;"> =< 0.150 UA</td>\
                                                </tr>\
                                                <tr>\
                                                    <td align="center" style="border: solid 1px #000;">POSITIVO</td>\
                                                    <td align="center" style="border: solid 1px #000;"> > 0.150 UA</td>\
                                                </tr>\
                                            </table></div>';
                            } else {
                                html += '\
                                <div class="mb-1 group">\
                                    <label for="reference" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Valor de Referencia</label>\
                                    <div class="col-12 col-md-6">\
                                        <select class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" name="reference[]" id="reference">\
                                            <option value="' + response.data[i].reference_id + '">' + response.data[i].reference_value + '</option>\
                                        </select>\
                                    </div>\
                                </div>';
                            }
                            
                            html += '<div class="mb-1 group">\
                                <label for="update_result_' + i + '" class="block mb-2 text-xs sm:text-sm font-medium text-slate-600">Resultado:</label>\
                                <input name="result[]" type="text" value="' + (edit == 1 ? response.data[i].result : "") + '" class="text-uppercase border border-gray-300 focus:ring-green-700 focus:border-green-700 bg-white text-slate-600 text-xs sm:text-sm rounded block w-full p-2" id="update_result_' + i + '" placeholder="Ingresar el resultado">\
                                <div id="error_update_result_' + i + '" class="hidden"x></div>\
                            </div>\
                        </div></fieldset>';
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { //Se captura el error en la petición HTTP
                var error = '';
                if (jqXHR.status == 0) {
                    error = 'No se estableció conexión: verifique su red.';
                } else if (jqXHR.status == 404) {
                    error = 'Solicitud no encontrada [404]';
                } else if (jqXHR.status == 500) {
                    error = 'Error en el servidor [500].';
                } else if (textStatus == 'parsererror') {
                    error = 'Requested JSON parse failed.';
                } else if (textStatus == 'timeout') {
                    error = 'Time out error.';
                } else if (textStatus == 'abort') {
                    error = 'Petición ajax abortada.';
                } else {
                    error = 'Error desconocido: ' + jqXHR.responseText;
                }

                if (typeof jqXHR.responseJSON != 'undefined') {
                    //En caso de que se notifique un error por parte del controlador indicar aquí
                }
            }
        });
        return html;
    }

    function viewStructureSelectKit(diagnosis, kit, lesp, edit) {
        var html = '';
        $.ajax({
            url: $("#url").val() + '/search/kit',
            data: {
                diagnosis: diagnosis,
                kit: kit,
                lesp: lesp,
                edit: edit
            },
            method: "GET",
            async: false,
            beforeSend: function () {
                //
            },
            success: function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    html += '<fieldset class="border mt-4">\
                    <legend align="center" class="font-normal text-slate-700 px-2">' + response.data[i].analysis + '</legend>\
                    <div class="flex justify-center items-center mb-4">\
                        <label class="font-normal text-slate-700 text-right w-full">Técnica Utilizada</label>\
                        <input value="' + response.data[i].technique_name + '" type="text" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>\
                    </div>\
                    <div class="flex justify-center items-center mb-4">\
                    <label class="font-normal text-slate-700 text-right w-full">Valor de Referencia</label>';
                    if (response.data[i].analysis == "ROTAVIRUS") {
                        html += '<div style="padding: 10px;" class="w-full">\
                        <table style="width: 100%; border-collapse: collapse;">\
                            <tr>\
                                <td align="center" style="border: solid 1px #000;" colspan="2">Valor de corte</td>\
                            </tr>\
                            <tr>\
                                <td align="center" style="border: solid 1px #000;">NEGATIVO</td>\
                                <td align="center" style="border: solid 1px #000;"> =< 0.150 UA</td>\
                            </tr>\
                            <tr>\
                                <td align="center" style="border: solid 1px #000;">POSITIVO</td>\
                                <td align="center" style="border: solid 1px #000;"> > 0.150 UA</td>\
                            </tr>\
                        </table></div>';
                    } else {
                        html += '<input value="' + response.data[i].reference_value + '" type="text" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>';
                    }
                    html += '</div>\
                    <div class="flex justify-center items-center mb-4">\
                        <label class="font-normal text-slate-700 text-right w-full">Resultado:</label>\
                        <input value="' + response.data[i].result + '" type="text" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>\
                    </div></fieldset>';
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { //Se captura el error en la petición HTTP
                var error = '';
                if (jqXHR.status == 0) {
                    error = 'No se estableció conexión: verifique su red.';
                } else if (jqXHR.status == 404) {
                    error = 'Solicitud no encontrada [404]';
                } else if (jqXHR.status == 500) {
                    error = 'Error en el servidor [500].';
                } else if (textStatus == 'parsererror') {
                    error = 'Requested JSON parse failed.';
                } else if (textStatus == 'timeout') {
                    error = 'Time out error.';
                } else if (textStatus == 'abort') {
                    error = 'Petición ajax abortada.';
                } else {
                    error = 'Error desconocido: ' + jqXHR.responseText;
                }

                if (typeof jqXHR.responseJSON != 'undefined') {
                    //En caso de que se notifique un error por parte del controlador indicar aquí
                }
            }
        });
        return html;
    }

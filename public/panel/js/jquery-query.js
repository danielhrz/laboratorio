var t;
$(document).ready(function () {

    t = $('#queryResults').DataTable({
        dom: 'lrtip',
        paging: true,
        lengthChange: true,
        serverSide: true,
        processing: true,
        searching: true,
        bFilter: true,
        ordering: true,
        info: true,
        autoWidth: false,
        pagingType: 'full_numbers',
        ajax: {
            url: $('#rutaQueryResults').val(),
        },
        order: [
            [0, "desc"]
        ],
        columns: [{
                data: 'created_at',
                searchable: false,
            },
            {
                data: 'nombre_paciente'
            },
            {
                data: 'folio_lesp'
            },
            {
                data: 'folio_sisver'
            },
            {
                data: 'oficio_entrada'
            },
            {
                data: 'operational_status'
            },
            {
                data: 'btn',
                orderable: false,
                searchable: false,
            },
        ],
        language: {
            "lengthMenu": "Mostrar " +
                `<select class="custom-select custom-select-sm form-control form-control-sm">
                            <option value = '10'>10</option>
                            <option value = '25'>25</option>
                            <option value = '50'>50</option>
                            <option value = '100'>100</option>
                        </select>` +
                " registros por página",
            "zeroRecords": "No se encontraron registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "sProcessing": "<i class='fas fa-spinner'></i> Cargando...",
            "oPaginate": {
                "sNext": "<i class='fa-solid fa-angle-right'></i>",
                "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                "sLast": "<i class='fa-solid fa-angles-right'></i>"
            }
        },
        orderCellsTop: true,
    });

    t.on('draw.dt', function () {

        var targetView = document.getElementById('tooltipShow');
        var buttonsView = document.getElementsByClassName('view-result');

        Array.from(buttonsView).forEach(function (element) {
            let tooltip = new Tooltip(targetView, element);
        }, false);

        var PageInfo = $('#queryResults').DataTable().page.info();
        t.column(0, {
            page: 'current'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });

        //tooltip-completed
        const popover = {
            placement: 'bottom',
            triggerType: 'hover',
            offset: 10,
            onHide: () => {
                //console.log('popover is shown');
            },
            onShow: () => {
                //console.log('popover is hidden');
            }
        };
        var targetTooltipCompleted = document.getElementById('tooltipCompleted');
        var buttonsTooltipCompleted = document.getElementsByClassName('tooltip-completed');
        var targetTooltipInfluenza = document.getElementById('tooltipInfluenza');
        var buttonsTooltipInfluenza = document.getElementsByClassName('tooltip-influenza');
        var targetTooltipReturn = document.getElementById('tooltipReturn');
        var buttonsTooltipReturn = document.getElementsByClassName('tooltip-return');
        Array.from(buttonsTooltipCompleted).forEach(function (element) {
            let pop = new Popover(targetTooltipCompleted, element, popover);
        }, false);
        Array.from(buttonsTooltipInfluenza).forEach(function (element) {
            let pop = new Popover(targetTooltipInfluenza, element, popover);
        }, false);
        Array.from(buttonsTooltipReturn).forEach(function (element) {
            let pop = new Popover(targetTooltipReturn, element, popover);
        }, false);

        $('.container-table').on('keyup', '.filter-input', function () {
            t.column($(this).data('column'))
                .search($(this).val())
                .draw();
        });
        $('#filtrar').click(function () {
            t.draw();
        });
    });

    var targetModalResultView = document.getElementById('viewModalResult');
    var viewModalResult;
    var options = {
        placement: 'top-center',
        backdropClasses: 'bg-gray-900 bg-opacity-50 dark:bg-opacity-80 fixed inset-0 z-40',
        onHide: () => {
            /* console.log('modal is hidden'); */
        },
        onShow: () => {
            /* console.log('modal is shown'); */
        },
        onToggle: () => {
            /* console.log('modal has been toggled'); */
        }
    };

    $(".close-modal").click(function (e) {
        e.preventDefault();
        if ($(this).data('modal') == 'create') {
            modalResult.hide();
        } else if ($(this).data('modal') == 'update') {
            modalResultEdit.hide();
        } else if ($(this).data('modal') == 'view') {
            viewModalResult.hide();
        }
    });

    $(".container-table").on('click', '.view-result', function () {
        if (targetModalResultView != null) {
            $.ajax({
                url: $("#url").val() + '/search/result/' + $(this).data('muestra'),
                method: "GET",
                success: function (response) {
                    if (response.success) {
                        console.log(response);
                        $("#view-oficio-entrada").val(response.data.oficio_entrada);
                        $("#view_nombre_paciente").val(response.data.nombre_paciente);
                        $("#view_folio_lesp").val(response.data.folio_lesp);
                        $("#view_folio_sisver").val(response.data.folio_sisver);
                        $("#view_tipo_muestra").val(response.data.tipo_muestra);
                        $("#view_fecha_toma_muestra").val(response.data.fecha_toma_muestra);
                        $("#view_hospital").val(response.data.hospital);
                        $("#view_user_initials").val(response.data.initials);
                        $("#view_cre").val(response.data.result.cre);
                        var text = "1: " + response.data.dx1 + "<br>" + "2: " + response.data.dx2 + "<br>" + "3: " + response.data.dx3 + "<br>" + "4: " + response.data.dx4 + "<br>" + "5: " + response.data.dx5 + "<br>";
                        $('#view_diagnosis').html(text);
                        $("#view_date_delivery").val(response.data.result.date_delivery);
                        text = '';
                        if (response.data.status_result == 'PENDIENTE') {
                            text = '<span data-value="PENDIENTE" class="bg-yellow-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">PENDIENTE A CONFIRMACIÓN DE INFLUENZA</span >';
                        }
                        if (response.data.status_result == 'REPROCESO') {
                            text = '<span data-value="REPROCESO" class="bg-blue-300 text-gray text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">REPROCESO</span >';
                        }
                        if (response.data.status_result == 'FINALIZADO') {
                            text = '<span data-value="FINALIZADO" class="bg-green-300 text-grey text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">FINALIZADO</span >';
                        }
                        $("#view_status_result").html(text);
                        $("#view_if_exit").val(response.data.result.if_exit);
                        $("#view_result").val(response.data.result.result);
                        $("#view_coments").val(response.data.result.coments);
                        $("#view_interpretation_result").val(response.data.result.interpretation_result);
                        $("#body-view-result").empty();
                        var html = '';
                        for (let i = 0; i < response.data.diagnostics.length; i++) {
                            html +=
                                '<div class="flex justify-center items-center mb-4">\
                                        <label class="font-normal text-slate-700 text-right w-full">Diagnostico solicitado:</label>\
                                        <input type="text" id="view-oficio-entrada" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" value="' + response.data.diagnostics[i].diagnosis_name + '" readonly>\
                                    </div>\
                                    <div class="flex justify-center items-center mb-4">\
                                        <label class="font-normal text-slate-700 text-right w-full">Kit empleado:</label>';
                            for (let j = 0; j < response.data.diagnostics[i].diagnosis_kits.length; j++) {
                                if (response.data.diagnostics[i].diagnosis_kits[j].kit_id == response.data.diagnostics[i].kits_value) {
                                    html += '<input type="text" id="view-oficio-entrada" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" value="' + response.data.diagnostics[i].diagnosis_kits[j].kit_name + '" readonly>';
                                }
                            }

                            html += '</div>\
                                    <div id="view_diagnosis_' + i + '">';
                            html += viewStructureSelectKit(response.data.diagnostics[i].diagnosis_name, response.data.diagnostics[i].kits_value, response.data.folio_lesp, 1);
                            html += '</div><br>';
                        }

                        $("#body-view-result").html(html);

                        viewModalResult = new Modal(targetModalResultView, options);
                        viewModalResult.show();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) { //Se captura el error en la petición HTTP
                    var error = '';
                    if (jqXHR.status == 0) {
                        error = 'No se estableció conexión: verifique su red.';
                    } else if (jqXHR.status == 404) {
                        error = 'Solicitud no encontrada [404]';
                    } else if (jqXHR.status == 500) {
                        error = 'Error en el servidor [500].';
                    } else if (textStatus == 'parsererror') {
                        error = 'Requested JSON parse failed.';
                    } else if (textStatus == 'timeout') {
                        error = 'Time out error.';
                    } else if (textStatus == 'abort') {
                        error = 'Petición ajax abortada.';
                    } else {
                        error = 'Error desconocido: ' + jqXHR.responseText;
                    }

                    if (typeof jqXHR.responseJSON != 'undefined') {
                        //En caso de que se notifique un error por parte del controlador indicar aquí
                    }
                }
            });

        }
    })

});

function viewStructureSelectKit(diagnosis, kit, lesp, edit) {
    var html = '';
    $.ajax({
        url: $("#url").val() + '/search/kit',
        data: {
            diagnosis: diagnosis,
            kit: kit,
            lesp: lesp,
            edit: edit
        },
        method: "GET",
        async: false,
        beforeSend: function () {
            //
        },
        success: function (response) {
            for (let i = 0; i < response.data.length; i++) {
                html += '<fieldset class="border mt-4">\
                    <legend align="center" class="font-normal text-slate-700 px-2">' + response.data[i].analysis + '</legend>\
                    <div class="flex justify-center items-center mb-4">\
                        <label class="font-normal text-slate-700 text-right w-full">Técnica Utilizada</label>\
                        <input value="' + response.data[i].technique_name + '" type="text" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>\
                    </div>\
                    <div class="flex justify-center items-center mb-4">\
                        <label class="font-normal text-slate-700 text-right w-full">Valor de Referencia</label>\
                        <input value="' + response.data[i].reference_value + '" type="text" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>\
                    </div>\
                    <div class="flex justify-center items-center mb-4">\
                        <label class="font-normal text-slate-700 text-right w-full">Resultado:</label>\
                        <input value="' + response.data[i].result + '" type="text" class="ml-2 border-0 text-slate-600 text-xs sm:text-sm rounded block w-full p-2 focus:border-0 focus:right-0" readonly>\
                    </div></fieldset>';
            }
        },
        error: function (jqXHR, textStatus, errorThrown) { //Se captura el error en la petición HTTP
            var error = '';
            if (jqXHR.status == 0) {
                error = 'No se estableció conexión: verifique su red.';
            } else if (jqXHR.status == 404) {
                error = 'Solicitud no encontrada [404]';
            } else if (jqXHR.status == 500) {
                error = 'Error en el servidor [500].';
            } else if (textStatus == 'parsererror') {
                error = 'Requested JSON parse failed.';
            } else if (textStatus == 'timeout') {
                error = 'Time out error.';
            } else if (textStatus == 'abort') {
                error = 'Petición ajax abortada.';
            } else {
                error = 'Error desconocido: ' + jqXHR.responseText;
            }

            if (typeof jqXHR.responseJSON != 'undefined') {
                //En caso de que se notifique un error por parte del controlador indicar aquí
            }
        }
    });
    return html;
}

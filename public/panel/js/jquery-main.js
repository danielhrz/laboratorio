//Obtenemos el componete donde se cargara la gráfica
var targetEntered;
var targetRejected;
var targetRejectionReasons;
var targetRejectionJurisdiction;
var targetRejectionHospitals;
var targetRejectionSample;
var targetSampleReceivedDay;
//Se crea una variable global para el archivo
var chartEntered;
var chartRejected;
var chartRejectionReasons;
var chartRejectionJurisdiction;
var chartRejectionHospitals;
var chartRejectionSample;
var chartSampleReceivedDay;

//Variables de check
var chartDay, tableDay;
const plugin = {
    id: 'custom_canvas_background_color',
    beforeDraw: (chart) => {
        const {
            ctx
        } = chart;
        ctx.save();
        ctx.globalCompositeOperation = 'destination-over';
        ctx.fillStyle = '#FFFFFF';
        ctx.fillRect(0, 0, chart.width, chart.height);
        ctx.restore();
    }
};
//Una vez que el documento HTML este completamente cargado
$(document).ready(function () {
    Datepicker.locales.es = {
        days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Hoy",
        monthsTitle: "Meses",
        clear: "Borrar",
        weekStart: 1,
        format: "dd/mm/yyyy"
    };

    var calendar = document.getElementsByClassName('datepicker');
    Array.from(calendar).forEach(function (element) {
        let datepicker = new Datepicker(element, {
            language: 'es',
            autohide: true,
            clearBtn: true,
        });
    }, false);
    //realiza la búsqueda de datos con el año pro default
    if (document.getElementById('container-chart-entered') != null) {
        targetEntered = document.getElementById('container-chart-entered').getContext('2d');
        entered(0);
    }

    if (document.getElementById('container-chart-rejected') != null) {
        targetRejected = document.getElementById('container-chart-rejected').getContext('2d');
        rejected(0);
    }
    if (document.getElementById('container-chart-rejected-reasons') != null) {
        targetRejectionReasons = document.getElementById('container-chart-rejected-reasons').getContext('2d');
        rejectionReasons(0);
    }
    if (document.getElementById('container-chart-rejected-jurisdiction') != null) {
        targetRejectionJurisdiction = document.getElementById('container-chart-rejected-jurisdiction').getContext('2d');
        rejectionJurisdiction(0);
    }
    if (document.getElementById('container-chart-rejected-hospitals') != null) {
        targetRejectionHospitals = document.getElementById('container-chart-rejected-hospitals').getContext('2d');
        rejectionHospitals(0);
    }
    if (document.getElementById('container-chart-rejected-sample') != null) {
        targetRejectionSample = document.getElementById('container-chart-rejected-sample').getContext('2d');
        rejectionSample(0);
    }
    if (document.getElementById('container-chart-samples-received-day') != null) {
        targetSampleReceivedDay = document.getElementById('container-chart-samples-received-day').getContext('2d');
        sampleReceivedDay(0);
    }

    //Cuando el select del año cambia
    $("#year-entered").change(function () {
        entered(1);
    });

    $("#year-rejected").change(function () {
        rejected(1);
    });

    $("#year-rejected-reasons").change(function () {
        rejectionReasons(1);
    });

    $("#year-rejected-jurisdiction").change(function () {
        rejectionJurisdiction(1);
    });

    $("#year-rejected-hospitals").change(function () {
        rejectionHospitals(1);
    });

    $("#year-rejected-sample").change(function () {
        rejectionSample(1);
    });

    $("#search-samples-received-day").click(function () {
        sampleReceivedDay(1);
    });

    $('.download').click(function () {
        var chart = $(this).data('chart');
        var canvas, title;
        if (chart == 'entered') {
            canvas = document.getElementById('container-chart-entered');
            title = 'muestras ingresadas por mes.jpg';
        } else if (chart == 'rejected') {
            canvas = document.getElementById('container-chart-rejected');
            title = 'muestras rechazadas por mes.jpg';
        } else if (chart == 'rejected-reasons') {
            canvas = document.getElementById('container-chart-rejected-reasons');
            title = 'Motivos de rechazo generales en el año.jpg';
        } else if (chart == 'rejected-jurisdiction') {
            canvas = document.getElementById('container-chart-rejected-jurisdiction');
            title = 'Muestras rechazadas por jurisdicción Sanitaria.jpg';
        } else if (chart == 'rejected-hospitals') {
            canvas = document.getElementById('container-chart-rejected-hospitals');
            title = 'Motivos de rechazo por hospital y centro canino.jpg';
        } else if (chart == 'rejected-sample') {
            canvas = document.getElementById('container-chart-rejected-sample');
            title = 'Motivos de rechazo por tipo de muestra.jpg';
        } else if (chart == 'samples-received-day') {
            canvas = document.getElementById('container-chart-samples-received-day');
            title = 'Muestras recepcionadas por día.jpg';
        }
        var a = document.createElement('a');
        a.href = canvas.toDataURL("image/jpeg");
        a.download = title;
        // Trigger the download
        a.click();
    });

    var targetDownload = document.getElementById('tooltipDownload');
    var buttonsDownload = document.getElementsByClassName('download');
    Array.from(buttonsDownload).forEach(function (element) {
        let tooltip = new Tooltip(targetDownload, element);
    }, false);

    Livewire.on('TableReceivedDayCheck', () => {
        tableDay = true;
        Livewire.emit('TableReceivedDay');
    });

    Livewire.on('TableReceivedDay', () => {
        if (chartDay == true && tableDay == true) {
            $('.container-loader-samples-received-day').addClass('hidden');
        }
    });

    $("#download_entered_file").click(function(e) {
        e.preventDefault();
        tableEntered();
    });

    $("#download_rejected_file").click(function(e) {
        e.preventDefault();
        tableRejected();
    });
});
function tableEntered() {
    $.ajax({
        url: $('#url').val() + '/api/tables/entered',
        xhrFields: {
            responseType: 'blob',
        },
        type: 'POST',
        data: {
            'year': $("#year-entered").val()
        },
        //async: false,
        beforeSend: function () {
            $('.container-loader-entered').removeClass('hidden');
        },
        success: function(result, status, xhr) {

        var disposition = xhr.getResponseHeader('content-disposition');
        var matches = /"([^"]*)"/.exec(disposition);
        var date = new Date();
        var filename = (matches != null && matches[1] ? matches[1] : 'Finalizas_'+date.getMonth()+date.getDate()+date.getFullYear()+date.getHours()+date.getMinutes()+date.getSeconds() +'.xlsx');

        // The actual download
        var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        document.body.appendChild(link);

        link.click();
        document.body.removeChild(link);
        $('.container-loader-entered').addClass('hidden');
        }
    });
}

function tableRejected() {
    $.ajax({
        url: $('#url').val() + '/api/tables/rejected',
        xhrFields: {
            responseType: 'blob',
        },
        type: 'POST',
        data: {
            'year': $("#year-rejected").val()
        },
        //async: false,
        beforeSend: function () {
            $('.container-loader-rejected').removeClass('hidden');
        },
        success: function(result, status, xhr) {

        var disposition = xhr.getResponseHeader('content-disposition');
        var matches = /"([^"]*)"/.exec(disposition);
        var date = new Date();
        var filename = (matches != null && matches[1] ? matches[1] : 'Rechazadas_'+date.getMonth()+date.getDate()+date.getFullYear()+date.getHours()+date.getMinutes()+date.getSeconds() +'.xlsx');

        // The actual download
        var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        document.body.appendChild(link);

        link.click();
        document.body.removeChild(link);
        $('.container-loader-rejected').addClass('hidden');
        }
    });
}

//Función para la gráfica de muestras ingresadas
function entered(status) {

    if (status == 1) {
        chartEntered.destroy();
    }

    $.ajax({
        url: $('#url').val() + '/api/charts/entered/' + $("#year-entered").val(),
        method: 'GET',
        //async: false,
        beforeSend: function () {
            $('.container-loader-entered').removeClass('hidden');
        },
        success: function (response) {
            let title = 'Muestras ingresadas en el ' + $("#year-entered").val();
            chartEntered = new Chart(targetEntered, {
                type: 'bar',
                data: {
                    labels: response.labels,
                    datasets: [{
                        //label: labels,
                        data: response.datasets[0].data,
                        backgroundColor: response.datasets[0].backgroundColor,
                        borderColor: response.datasets[0].borderColor,
                        borderWidth: 1,
                        font: function (context) {
                            var width = context.chart.width;
                            var size = Math.round(width / 32);

                            return {
                                weight: 'bold',
                                size: size
                            };
                        }
                    }]
                },
                plugins: [plugin],
                options: {
                    scales: {
                        y: {
                            grid: {
                                drawBorder: false
                            },
                            beginAtZero: true,
                            display: true,
                            stacked: true,
                            title: {
                                display: true,
                                text: 'Número de muestras',
                                color: '#212F3D',
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 36);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            }
                        },
                        x: {
                            grid: {
                                display: false,
                            },
                            beginAtZero: true,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                            autoSkip: false,
                            maxRotation: 80,
                            minRotation: 0
                        }
                    },
                    plugins: {
                        legend: false,
                        title: {
                            display: true,
                            text: title,
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 34);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                        }
                    }
                }
            });
            $('.container-loader-entered').addClass('hidden');
        }
    });

    //return canvas;

}

//Función que crea la gráfica según las especificaciones de la librería ChartJS
function rejected(status) {
    if (status == 1) {
        chartRejected.destroy();
    }

    chartRejected = $.ajax({
        url: $('#url').val() + '/api/charts/rejected/' + $("#year-rejected").val(),
        method: 'GET',
        //async: false,
        beforeSend: function () {
            $('.container-loader-rejected').removeClass('hidden');
        },
        success: function (response) {
            let title = 'Muestras rechazadas por mes ' + $("#year-rejected").val();
            chartRejected = new Chart(targetRejected, {
                type: 'bar',
                data: {
                    labels: response.labels,
                    datasets: response.datasets
                },
                plugins: [ChartDataLabels, plugin],
                options: {
                    scales: {
                        y: {
                            grid: {
                                display: false
                            },
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                // para ocultar numeros laterales
                                callback: function (value, index, ticks) {
                                    return '';
                                }
                            },
                            beginAtZero: true,
                            display: true,
                            stacked: true,
                            title: {
                                display: true,
                                text: 'Número de muestras',
                                color: '#212F3D',
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 36);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                padding: {
                                    top: 30,
                                    left: 0,
                                    right: 0,
                                    bottom: 0
                                }
                            }
                        },
                        x: {
                            beginAtZero: true,
                            fontSize: 14,
                            autoSkip: false,
                            maxRotation: 80,
                            minRotation: 0,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            }
                        }
                    },
                    plugins: {
                        datalabels: {
                            align: 'end',
                            anchor: 'end',
                            color: '#212F3D',
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 50);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                            formatter: Math.round
                        },
                        legend: false,
                        title: {
                            display: true,
                            text: title,
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 34);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                        }
                    }
                }
            });
            $('.container-loader-rejected').addClass('hidden');
            /* return chart; */
        }
    });
    //return chartRejected;
}

function rejectionReasons(status) {
    if (status == 1) {
        chartRejectionReasons.destroy();
    }

    $.ajax({
        url: $('#url').val() + '/api/charts/rejected/reasons/' + $("#year-rejected-reasons").val(),
        method: 'GET',
        //async: false,
        beforeSend: function () {
            $('.container-loader-rejected-reasons').removeClass('hidden');
        },
        success: function (response) {
            let title = 'Motivos de rechazo generales en el año ' + $("#year-rejected-reasons").val();
            chartRejectionReasons = new Chart(targetRejectionReasons, {
                type: 'bar',
                data: {
                    labels: response.labels,
                    datasets: response.datasets
                },
                plugins: [ChartDataLabels, plugin],
                options: {
                    scales: {
                        y: {
                            beginAtZero: true,
                            display: true,
                            stacked: true,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                            title: {
                                display: true,
                                text: 'Número de muestras',
                                color: '#212F3D',
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 36);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                padding: {
                                    top: 30,
                                    left: 0,
                                    right: 0,
                                    bottom: 0
                                }
                            }
                        },
                        x: {
                            grid: {
                                display: false,
                            },
                            beginAtZero: true,
                            fontSize: 8,
                            autoSkip: false,
                            maxRotation: 120,
                            minRotation: 90,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            }
                        }
                    },
                    plugins: {
                        datalabels: {
                            align: 'end',
                            anchor: 'end',
                            color: '#212F3D',
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 50);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                            formatter: Math.round
                        },
                        legend: false,
                        title: {
                            display: true,
                            text: title,
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 34);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                        }
                    }
                }
            });
            $('.container-loader-rejected-reasons').addClass('hidden');
        }
    });
}

function rejectionJurisdiction(status) {
    if (status == 1) {
        chartRejectionJurisdiction.destroy();
    }

    $.ajax({
        url: $('#url').val() + '/api/charts/rejected/jurisdiction/' + $("#year-rejected-jurisdiction").val(),
        method: 'GET',
        //async: false,
        beforeSend: function () {
            $('.container-loader-rejected-jurisdiction').removeClass('hidden');
        },
        success: function (response) {
            let title = 'Muestras rechazadas por jurisdicción Sanitaria ' + $("#year-rejected-jurisdiction").val();
            chartRejectionJurisdiction = new Chart(targetRejectionJurisdiction, {
                type: 'bar',
                data: {
                    labels: response.labels,
                    datasets: response.datasets
                },
                plugins: [plugin],
                options: {
                    indexAxis: 'y',
                    //responsive: true,
                    scales: {
                        y: {
                            beginAtZero: true,
                            display: true,
                            stacked: true,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                            title: {
                                display: true,
                                text: 'Motivos de rechazo',
                                color: '#212F3D',
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 36);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                padding: {
                                    top: 0,
                                    left: 0,
                                    right: 0,
                                    bottom: 0
                                }
                            }
                        },
                        x: {
                            grid: {
                                display: false,
                            },
                            beginAtZero: true,
                            stacked: true,
                            fontSize: 8,
                            autoSkip: false,
                            maxRotation: 120,
                            minRotation: 90,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                        }
                    },
                    plugins: {
                        legend: {
                            display: true,
                            position: 'top',
                            labels: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                boxWidth: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 20);

                                    return size;
                                }
                            },
                        },
                        title: {
                            display: true,
                            text: title,
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 34);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                        }
                    }
                }
            });
            $('.container-loader-rejected-jurisdiction').addClass('hidden');
        }
    });
}

function rejectionHospitals(status) {
    if (status == 1) {
        chartRejectionHospitals.destroy();
    }

    $.ajax({
        url: $('#url').val() + '/api/charts/rejected/hospitals/' + $("#year-rejected-hospitals").val(),
        method: 'GET',
        //async: false,
        beforeSend: function () {
            $('.container-loader-rejected-hospitals').removeClass('hidden');
        },
        success: function (response) {
            let title = 'Motivos de rechazo por hospital y centro canino del año ' + $("#year-rejected-hospitals").val();
            chartRejectionHospitals = new Chart(targetRejectionHospitals, {
                type: 'bar',
                data: {
                    labels: response.labels,
                    datasets: response.datasets
                },
                plugins: [plugin],
                options: {
                    indexAxis: 'y',
                    //responsive: true,
                    scales: {
                        y: {
                            beginAtZero: true,
                            display: true,
                            stacked: true,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                            title: {
                                display: true,
                                text: 'Motivos de rechazo',
                                color: '#212F3D',
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 36);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                padding: {
                                    top: 0,
                                    left: 0,
                                    right: 0,
                                    bottom: 0
                                }
                            }
                        },
                        x: {
                            grid: {
                                display: false,
                            },
                            beginAtZero: true,
                            stacked: true,
                            fontSize: 8,
                            autoSkip: false,
                            maxRotation: 120,
                            minRotation: 90,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                        }
                    },
                    plugins: {
                        legend: {
                            display: true,
                            position: 'top',
                            labels: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                boxWidth: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 20);

                                    return size;
                                }
                            },
                        },
                        title: {
                            display: true,
                            text: title,
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 34);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                        }
                    }
                }
            });
            $('.container-loader-rejected-hospitals').addClass('hidden');
        }
    });
}

function rejectionSample(status) {
    if (status == 1) {
        chartRejectionSample.destroy();
    }

    $.ajax({
        url: $('#url').val() + '/api/charts/rejected/sample/' + $("#year-rejected-sample").val(),
        method: 'GET',
        //async: false,
        beforeSend: function () {
            $('.container-loader-rejected-sample').removeClass('hidden');
        },
        success: function (response) {
            let title = 'Motivos de rechazo por tipo de muestra del año ' + $("#year-rejected-sample").val();
            chartRejectionSample = new Chart(targetRejectionSample, {
                type: 'bar',
                data: {
                    labels: response.labels,
                    datasets: response.datasets
                },
                plugins: [plugin],
                options: {
                    indexAxis: 'y',
                    //responsive: true,
                    scales: {
                        y: {
                            beginAtZero: true,
                            display: true,
                            stacked: true,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                            title: {
                                display: true,
                                text: 'Motivos de rechazo',
                                color: '#212F3D',
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 36);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                padding: {
                                    top: 0,
                                    left: 0,
                                    right: 0,
                                    bottom: 0
                                }
                            }
                        },
                        x: {
                            grid: {
                                display: false,
                            },
                            beginAtZero: true,
                            stacked: true,
                            fontSize: 8,
                            autoSkip: false,
                            maxRotation: 120,
                            minRotation: 90,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                        }
                    },
                    plugins: {
                        legend: {
                            display: true,
                            position: 'top',
                            labels: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                boxWidth: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 20);

                                    return size;
                                }
                            },
                        },
                        title: {
                            display: true,
                            text: title,
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 34);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                        }
                    }
                }
            });
            $('.container-loader-rejected-sample').addClass('hidden');
        }
    });
}

function sampleReceivedDay(status) {
    if (status == 1) {
        chartSampleReceivedDay.destroy();
    }

    var day = $("#date-samples-received-day").val();
    if (day != '') {
        day = day.split('/');
        day = day[2] + '-' + day[1] + '-' + day[0];
    } else {
        day = formatDate(new Date());
    }

    Livewire.emit('changeTableReceivedDay', day);

    $.ajax({
        url: $('#url').val() + '/api/charts/received/date/' + day,
        method: 'GET',
        //async: false,
        beforeSend: function () {
            $('.container-loader-samples-received-day').removeClass('hidden');
            chartDay = false;
            tableDay = false;
        },
        success: function (response) {
            let title = 'Muestras recepcionadas por día ' + response.date;
            chartSampleReceivedDay = new Chart(targetSampleReceivedDay, {
                type: 'bar',
                data: {
                    labels: response.labels,
                    datasets: [{
                        //label: labels,
                        data: response.datos,
                        backgroundColor: response.background,
                        borderColor: response.border,
                        borderWidth: 2,
                        font: function (context) {
                            var width = context.chart.width;
                            var size = Math.round(width / 32);

                            return {
                                weight: 'bold',
                                size: size
                            };
                        }
                    }]
                },
                plugins: [ChartDataLabels, plugin],
                options: {
                    scales: {
                        y: {
                            beginAtZero: true,
                            display: true,
                            stacked: true,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            },
                            title: {
                                display: true,
                                text: 'Número de muestras',
                                color: '#212F3D',
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 36);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                                padding: {
                                    top: 30,
                                    left: 0,
                                    right: 0,
                                    bottom: 0
                                }
                            }
                        },
                        x: {
                            grid: {
                                display: false,
                            },
                            beginAtZero: true,
                            fontSize: 8,
                            autoSkip: false,
                            maxRotation: 120,
                            minRotation: 90,
                            ticks: {
                                font: function (context) {
                                    var width = context.chart.height;
                                    var size = Math.round(width / 50);

                                    return {
                                        //weight: 'bold',
                                        style: 'normal',
                                        size: size
                                    };
                                },
                            }
                        }
                    },
                    plugins: {
                        datalabels: {
                            align: 'end',
                            anchor: 'end',
                            color: '#212F3D',
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 50);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                            formatter: Math.round
                        },
                        legend: false,
                        title: {
                            display: true,
                            text: title,
                            font: function (context) {
                                var width = context.chart.height;
                                var size = Math.round(width / 34);

                                return {
                                    //weight: 'bold',
                                    style: 'normal',
                                    size: size
                                };
                            },
                        }
                    }
                }
            });
            chartDay = true;
            Livewire.emit('TableReceivedDay');
            //$('.container-loader-samples-received-day').addClass('hidden');
        }
    });
}

function formatDate(date) {
    return [
        date.getFullYear(),
        padTo2Digits(date.getMonth() + 1),
        padTo2Digits(date.getDate()),
    ].join('-');
}

function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
}


    var t;
    $(document).ready(function(){

        t = $('#users').DataTable({
            paging: true,
            lengthChange: true,
            serverSide: true,
            processing: true,
            searching: true,
            bFilter: true,
            ordering: true,
            info: true,
            autoWidth: false,
            pagingType: 'full_numbers',
            ajax: {
                    url: $('#rutaUsers').val(),
                },
            columnDefs: [
                {
                    targets: [ 0, 10 ],
                    searchable: false,
                    orderable: false
                }
            ],
            order: [[ 9, "desc" ]],
            columns: [
                {data: 'id'},
                {data: 'name_role'},
                {data: 'username'},
                {data: 'full_name'},
                {data: 'name_juris'},
                {data: 'name_ubicacion'},
                {data: 'phone'},
                {data: 'ext'},
                {data: 'email'},
                {data: 'created_at'},
                {data: 'btn',
                className: 'column-two-options'
                },
            ],
            language: {
                    "lengthMenu": "Mostrar " +
                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                            <option value = '10'>10</option>
                            <option value = '25'>25</option>
                            <option value = '50'>50</option>
                            <option value = '100'>100</option>
                        </select>` + 
                    " registros por página",
                    "zeroRecords": "No se encontraron registros",
                    "info": "Página _PAGE_ de _PAGES_",
                    "search": "Buscar:",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                    "sProcessing": "<i class='fas fa-spinner'></i> Cargando...",
                    "oPaginate": {
                        "sNext": "<i class='fa-solid fa-angle-right'></i>",
                        "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                        "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                        "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            },
            /* scrollY: 200, */
/*             scrollX: 500,
            scrollCollapse: true,
            orderCellsTop: true, */
        });

        t.on( 'draw.dt', function () {
            var PageInfo = $('#users').DataTable().page.info();
                t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                    cell.innerHTML = i + 1 + PageInfo.start;
            });

            var targetEdit = document.getElementById('tooltipEdit');
            var buttonsEdit = document.getElementsByClassName('tooltipButtonEdit');

            var targetDelete = document.getElementById('tooltipDelete');
            var buttonsDelete = document.getElementsByClassName('tooltipButtonDelete');

            Array.from(buttonsEdit).forEach(function (element) {
                let tooltip = new Tooltip(targetEdit, element);
            }, false);

            Array.from(buttonsDelete).forEach(function (element) {
                let tooltip = new Tooltip(targetDelete, element);
            }, false);

        $('.table-responsive').on('keyup','.filter-input',function(){
            t.column($(this).data('column'))
            .search($(this).val())
            .draw();
            });
            $('#filtrar').click(function(){
                t.draw(); 
            });
        });

        $(".container-table").on('click', '.btn-option-delete', function (event) {
        Swal.fire({
            title: 'Eliminar',
            text: "¿Deseas eliminar este usuario?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: $(this).data('action'),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'delete',
                        success: function (response){
                            t.ajax.reload(null, false);
                            Swal.fire(
                                '¡Eliminado!',
                                'Usuario eliminado con éxito!',
                                'success'
                            )
                        }
                    });
                }
            });
        });
    });

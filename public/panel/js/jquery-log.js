
    var t;
    $(document).ready(function(){

        t = $('#logChanges').DataTable({
            dom: 'lrtip',
            paging: true,
            lengthChange: true,
            serverSide: true,
            processing: true,
            searching: true,
            bFilter: true,
            ordering: true,
            info: true,
            autoWidth: false,
            pagingType: 'full_numbers',
            ajax: {
                    url: $('#rutaLogChanges').val(),
                },
                columnDefs: [
                    {
                        targets: [ 0 ],
                        searchable: false,
                        orderable: false
                    }
                ],
            order: [[ 22, "desc" ]],
            columns: [
                {data: 'id'},
                {data: 'anio'},
                {data: 'folio_lesp'},
                {data: 'dx1'},
                {data: 'dx2'},
                {data: 'dx3'},
                {data: 'dx4'},
                {data: 'dx5'},
                {data: 'oficio_entrada'},
                {data: 'fecha_recepcion'},
                {data: 'hora_recepcion'},
                {data: 'hospital'},
                {data: 'nombre_paciente'},
                {data: 'tipo_muestra'},
                {data: 'fecha_toma_muestra'},
                {data: 'folio_sisver'},
                {data: 'persona_recibe'},
                {data: 'status'},
                {data: 'rechazos'},
                {data: 'observaciones'},
                {data: 'aclaraciones_remu'},
                {data: 'full_name'},
                {data: 'created_at'},
            ],
            language: {
                    "lengthMenu": "Mostrar " +
                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                            <option value = '10'>10</option>
                            <option value = '25'>25</option>
                            <option value = '50'>50</option>
                            <option value = '100'>100</option>
                        </select>` + 
                    " registros por página",
                    "zeroRecords": "No se encontraron registros",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                    "sProcessing": "<i class='fas fa-spinner'></i> Cargando...",
                    "oPaginate": {
                        "sNext": "<i class='fa-solid fa-angle-right'></i>",
                        "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                        "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                        "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            },
            orderCellsTop: true,
        });

        t.on( 'draw.dt', function () {
            var PageInfo = $('#logChanges').DataTable().page.info();
                t.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                    cell.innerHTML = i + 1 + PageInfo.start;
                });

        $('.container-table').on('keyup','.filter-input',function(){
            t.column($(this).data('column'))
            .search($(this).val())
            .draw();
            });
            $('#filtrar').click(function(){
                t.draw(); 
            });
        });
    });

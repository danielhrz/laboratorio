
    var t;
    $(document).ready(function(){
        $(".container-table").on('click', '.change-status-massive', function(event) {
            let visibleButtons = 0;
            let folios = '';
            $('.change-status-massive:checked').each(
                function() {
                    folios = folios + ','+$(this).val();
                    visibleButtons = 1;
                }
            );
            if( visibleButtons == 1 ) {
                $("#container-buttons-status").attr('data-folios', folios.slice(1));
                $("#container-buttons-status").css('display', 'block');
            } else {
                $("#container-buttons-status").attr('data-folios', '');
                $("#container-buttons-status").css('display', 'none');
            }
        });
        $(".btn-action-status").click(function(event) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: $("#ruta").val()+'/estados',
                data: {
                    status: $(this).data('status'),
                    folios: $("#container-buttons-status").attr('data-folios')
                },
                method: 'PUT',
                success: function( response ) {
                    $("#container-buttons-status").attr('data-folios', '');
                    $("#container-buttons-status").css('display', 'none');
                    t.ajax.reload();
                }
            });
        });

        $(".dx1").select();

        t = $('#federales').DataTable({
            dom: 'lrtip',
            paging: true,
            lengthChange: true,
            serverSide: true,
            processing: true,
            searching: true,
            bFilter: true,
            ordering: true,
            info: true,
            autoWidth: false,
            pagingType: 'full_numbers',
            ajax: {
                    url: $('#rutaDatos').val(),
                    data: function(d) {
                    d.tipo = $('#tipoFolio').val(),
                    d.start_date = $('#start_date').val(),
                    d.end_date = $('#end_date').val(),
                    d.dx1 = $('#dx1').val()
                    }
            },
            columnDefs: [
                {
                    targets: [ 0, 1, 24 ],
                    searchable: false,
                    orderable: false
                },
                {
                    targets: [ 5, 6, 7, 8, 22, 23 ],
                    visible: false,
                    searchable: true,
                    orderable: true
                }
            ],
            order: [[ 3, "desc" ]],
            columns: [
                {data: 'checkbox'},
                {data: 'id'},
                {data: 'anio'},
                {data: 'folio_lesp'},
                {data: 'dx1'},
                {data: 'dx2'},
                {data: 'dx3'},
                {data: 'dx4'},
                {data: 'dx5'},
                {data: 'oficio_entrada'},
                {data: 'fecha_recepcion'},
                {data: 'hora_recepcion'},
                {data: 'hospital'},
                {data: 'nombre_paciente'},
                {data: 'tipo_muestra'},
                {data: 'fecha_toma_muestra'},
                {data: 'folio_sisver'},
                {data: 'persona_recibe'},
                {data: 'status'},
                {data: 'rechazos'},
                {data: 'observaciones'},
                {data: 'aclaraciones_remu'},
                {data: 'created_at'},
                {data: 'full_name'},
                {data: 'btn'},
            ],
            language: {
                    "lengthMenu": "Mostrar " +
                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                            <option value = '10'>10</option>
                            <option value = '25'>25</option>
                            <option value = '50'>50</option>
                            <option value = '100'>100</option>
                        </select>`+ 
                    " registros por página",
                    "zeroRecords": "No se encontraron registros",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                    "sProcessing": "<i class='fas fa-spinner'></i> Cargando...",
                    "search": "Buscar:",
                    "oPaginate": {
                        "sNext": "<i class='fa-solid fa-angle-right'></i>",
                        "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                        "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                        "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            },
            /* scrollY: 500, */
            /* scrollX: 500, */
            /* scrollCollapse: true, */
            orderCellsTop: true,
        });

        $('#colums-dataTables a').on( 'click', function (e) {
            e.preventDefault();
            // Get the column API object
            var column = t.column( $(this).attr('data-column') );
            // Toggle the visibility
            column.visible( ! column.visible() );
        });

        t.on( 'draw.dt', function () {
        var PageInfo = $('#federales').DataTable().page.info();
            t.column(1, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });

            var targetEdit = document.getElementById('tooltipEdit');
            var buttonsEdit = document.getElementsByClassName('tooltipButtonEdit');
            var targetDelete = document.getElementById('tooltipDelete');
            var buttonsDelete = document.getElementsByClassName('tooltipButtonDelete');
            Array.from(buttonsEdit).forEach(function (element) {
                let tooltip = new Tooltip(targetEdit, element);
            }, false);
            Array.from(buttonsDelete).forEach(function (element) {
                let tooltip = new Tooltip(targetDelete, element);
            }, false);
        });

        $('.container-table').on('keyup','.filter-input',function(){
        t.column($(this).data('column'))
        .search($(this).val())
        .draw();
        });
        $('#filtrar').click(function(){
            t.draw(); 
        });

        $('.container-table').on('click', '.btn-option-delete', function (){
            Swal.fire({
            title: 'Eliminar',  
            text: "¿Deseas eliminar el registro?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: $(this).data('datos'),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'delete',
                        success: function (response){
                            t.ajax.reload(null, false);
                            Swal.fire(
                                '¡Eliminado!',
                                'Registro eliminado con éxito!',
                                'success'
                            )
                        }
                    });
                }
            });
        });
    });

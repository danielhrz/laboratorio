var table;
$(document).ready(function () {
    if (typeof DataTable === 'function') {
        table = $('#table-destinatarios').DataTable({
            paging: true,
            lengthChange: true,
            serverSide: true,
            processing: true,
            searching: true,
            bFilter: true,
            ordering: true,
            info: true,
            autoWidth: false,
            responsive: true,
            order: [0, 'DESC'], //Habilita el ordenamiento por defecto en la columna 0
            pagingType: 'full_numbers', //Para poner la paginacíon completa
            deferRender: true,
            ajax: {
                url: $('#ruta-destinatarios').val(),
                type: "GET"
            },
            columns: [{
                    data: 'id'
                },
                {
                    data: 'jurisdiccion'
                },
                {
                    data: 'encargado'
                },
                {
                    data: 'position'
                },
                {
                    data: 'address'
                },
                {
                    data: 'btn',
                    orderable: false,
                    className: 'column-two-options'
                }
            ],
            "language": {
                "lengthMenu": "Mostrar " +
                    `<select>
                            <option value = '10'>10</option>
                            <option value = '25'>25</option>
                            <option value = '50'>50</option>
                            <option value = '100'>100</option>
                        </select>` +
                    " registros por página",
                "zeroRecords": "No se encontraron registros",
                //"info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "sProcessing": "Procesando...",
                "search": "Buscar:",
                "sInfo": "_END_ de _TOTAL_ registros",
                "oPaginate": {
                    "sNext": "<i class='fa-solid fa-angle-right'></i>",
                    "sPrevious": "<i class='fa-solid fa-angle-left'></i>",
                    "sFirst": "<i class='fa-solid fa-angles-left'></i>",
                    "sLast": "<i class='fa-solid fa-angles-right'></i>"
                }
            }
        });

        table.on('draw.td', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) { //Genera un numero consecutivo en la tabla como index
                cell.innerHTML = i + 1 + info.start;
            });
            //Se generan los tooltips para los botones dentro de la tabla
            var targetEdit = document.getElementById('tooltipDestinatariosEdit');
            var buttonsEdit = document.getElementsByClassName('btn-option-edit');

            var targetDelete = document.getElementById('tooltipDestinatariosDelete');
            var buttonsDelete = document.getElementsByClassName('btn-option-delete');

            Array.from(buttonsEdit).forEach(function (element) {
                let tooltip = new Tooltip(targetEdit, element);
            }, false);

            Array.from(buttonsDelete).forEach(function (element) {
                let tooltip = new Tooltip(targetDelete, element);
            }, false);
        });

        $(".container-table").on('click', '.btn-option-delete', function (event) {
            Swal.fire({
                title: 'Eliminar',
                text: "¿Deseas eliminar este destinatario?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: $(this).data('action'),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'delete',
                        success: function (response) {
                            table.ajax.reload(null, false);
                            Swal.fire(
                                '¡Eliminado!',
                                'Usuario eliminado con éxito!',
                                'success'
                            )
                        }
                    });
                }
            });
        });
    }
});
